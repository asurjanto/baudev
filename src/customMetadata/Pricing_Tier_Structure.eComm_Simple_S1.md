<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>eComm Simple S1</label>
    <protected>false</protected>
    <values>
        <field>End_Value__c</field>
        <value xsi:type="xsd:double">4999.0</value>
    </values>
    <values>
        <field>Headline_Monthly_Access_Fees__c</field>
        <value xsi:type="xsd:string">30</value>
    </values>
    <values>
        <field>Headline_Price__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>Product__c</field>
        <value xsi:type="xsd:string">eCommerce</value>
    </values>
    <values>
        <field>Rate_Type__c</field>
        <value xsi:type="xsd:string">Simple Pricing</value>
    </values>
    <values>
        <field>Segment__c</field>
        <value xsi:type="xsd:string">S1</value>
    </values>
    <values>
        <field>Start_Value__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
</CustomMetadata>
