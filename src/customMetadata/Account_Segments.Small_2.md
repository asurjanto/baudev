<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Small 2</label>
    <protected>false</protected>
    <values>
        <field>End_Value__c</field>
        <value xsi:type="xsd:double">166666.0</value>
    </values>
    <values>
        <field>Start_Value__c</field>
        <value xsi:type="xsd:double">83334.0</value>
    </values>
</CustomMetadata>
