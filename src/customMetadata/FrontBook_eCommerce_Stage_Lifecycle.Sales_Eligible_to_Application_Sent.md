<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Sales Eligible to Application Sent</label>
    <protected>false</protected>
    <values>
        <field>Allowed_Profile__c</field>
        <value xsi:type="xsd:string">Sales</value>
    </values>
    <values>
        <field>End_Stage__c</field>
        <value xsi:type="xsd:string">Application Sent</value>
    </values>
    <values>
        <field>From_Stage__c</field>
        <value xsi:type="xsd:string">Eligible</value>
    </values>
</CustomMetadata>
