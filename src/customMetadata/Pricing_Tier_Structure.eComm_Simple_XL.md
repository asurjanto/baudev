<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>eComm Simple XL</label>
    <protected>false</protected>
    <values>
        <field>End_Value__c</field>
        <value xsi:type="xsd:double">1.0E9</value>
    </values>
    <values>
        <field>Headline_Monthly_Access_Fees__c</field>
        <value xsi:type="xsd:string">0</value>
    </values>
    <values>
        <field>Headline_Price__c</field>
        <value xsi:type="xsd:double">1.65</value>
    </values>
    <values>
        <field>Product__c</field>
        <value xsi:type="xsd:string">eCommerce</value>
    </values>
    <values>
        <field>Rate_Type__c</field>
        <value xsi:type="xsd:string">Simple Pricing</value>
    </values>
    <values>
        <field>Segment__c</field>
        <value xsi:type="xsd:string">XL</value>
    </values>
    <values>
        <field>Start_Value__c</field>
        <value xsi:type="xsd:double">50000.0</value>
    </values>
</CustomMetadata>
