<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Large</label>
    <protected>false</protected>
    <values>
        <field>End_Value__c</field>
        <value xsi:type="xsd:double">5000000.0</value>
    </values>
    <values>
        <field>Start_Value__c</field>
        <value xsi:type="xsd:double">833334.0</value>
    </values>
</CustomMetadata>
