<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Suspicion Email Receivers</label>
    <protected>false</protected>
    <values>
        <field>Email_Receivers__c</field>
        <value xsi:type="xsd:string">htaylor@tyro.com;sbarber@tyro.com;einglis@tyro.com;marnaez@tyro.com;mparkes@tyro.com;Merchant-compliance@tyro.com</value>
    </values>
    <values>
        <field>Email_Template_Unique_Name__c</field>
        <value xsi:type="xsd:string">Suspicion_Call_Reporting</value>
    </values>
</CustomMetadata>
