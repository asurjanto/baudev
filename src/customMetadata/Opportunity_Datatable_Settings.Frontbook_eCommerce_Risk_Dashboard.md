<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Fontbook eCommerce Risk Dashboard</label>
    <protected>false</protected>
    <values>
        <field>Filter_by_Stage_Name__c</field>
        <value xsi:type="xsd:string">Risk Review</value>
    </values>
    <values>
        <field>Record_Type_Name__c</field>
        <value xsi:type="xsd:string">Frontbook eCommerce</value>
    </values>
    <values>
        <field>Table_Header_JSON__c</field>
        <value xsi:type="xsd:string">{label:&apos;Opportuniy No.&apos;,fieldName:&apos;opportunityLink&apos;,sortable:true,type:&apos;url&apos;,typeAttributes:{label:{fieldName:&apos;opportunityNumber&apos;},target:&apos;_blank&apos;},editable:false},{label:&apos;ABN&apos;,fieldName:&apos;accountABN&apos;,sortable:true,type:&apos;text&apos;,editable:false},{label:&apos;MID&apos;,fieldName:&apos;midId&apos;,sortable:true,type:&apos;url&apos;,typeAttributes:{label:{fieldName:&apos;midName&apos;},target:&apos;_blank&apos;},editable:false},{label:&apos;Company&apos;,fieldName:&apos;accountId&apos;,sortable:true,type:&apos;url&apos;,typeAttributes:{label:{fieldName:&apos;accountName&apos;},target:&apos;_blank&apos;},editable:false},{label:&apos;Website&apos;,fieldName:&apos;websiteURL&apos;,sortable:true,type:&apos;url&apos;,editable:false},{label:&apos;Risk Assessor&apos;,fieldName:&apos;riskAssessorName&apos;,sortable:true,type:&apos;text&apos;,editable:false},{label:&apos;Last Modified Date&apos;,fieldName:&apos;lastModifiedDate&apos;,sortable:true,type:&apos;text&apos;,editable:false},{label:&apos;Priority&apos;,fieldName:&apos;priority&apos;,sortable:true,type:&apos;text&apos;,editable:false},{type:&apos;action&apos;,typeAttributes:{rowActions:[{label:&apos;Assign To Me&apos;,name:&apos;Assign_To_Me&apos;}]}}</value>
    </values>
    <values>
        <field>Table_Name__c</field>
        <value xsi:type="xsd:string">All FrontBook eCommerce Pending Applications</value>
    </values>
</CustomMetadata>
