<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MID pos product notification email</label>
    <protected>false</protected>
    <values>
        <field>FieldName__c</field>
        <value xsi:type="xsd:string">POS_Product_Notification_Contact_Email__c</value>
    </values>
    <values>
        <field>MaskingRule__c</field>
        <value xsi:type="xsd:string">Random Email</value>
    </values>
    <values>
        <field>MaskingText__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ObjectName__c</field>
        <value xsi:type="xsd:string">Merchant_ID__c</value>
    </values>
</CustomMetadata>
