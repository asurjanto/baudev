<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Business Contact Number</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Field_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ID_Log_Field__c</field>
        <value xsi:type="xsd:string">Business_Contact_Number_Medium__c</value>
    </values>
    <values>
        <field>Object_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Question__c</field>
        <value xsi:type="xsd:string">Please provide us your Business Contact Number</value>
    </values>
    <values>
        <field>Response_Type__c</field>
        <value xsi:type="xsd:string">Phone</value>
    </values>
    <values>
        <field>Score_Points__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Severity_Level__c</field>
        <value xsi:type="xsd:string">medium</value>
    </values>
    <values>
        <field>System_Validation_Required__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
