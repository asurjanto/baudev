<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Sales App Received To Ready for OB</label>
    <protected>false</protected>
    <values>
        <field>Allowed_Profile__c</field>
        <value xsi:type="xsd:string">Sales</value>
    </values>
    <values>
        <field>End_Stage__c</field>
        <value xsi:type="xsd:string">Onboarding Team Review</value>
    </values>
    <values>
        <field>From_Stage__c</field>
        <value xsi:type="xsd:string">Application Received</value>
    </values>
</CustomMetadata>
