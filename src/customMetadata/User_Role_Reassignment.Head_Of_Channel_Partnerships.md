<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Head Of Channel Partnerships</label>
    <protected>false</protected>
    <values>
        <field>Is_Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Role_name__c</field>
        <value xsi:type="xsd:string">Head Of Channel Partnerships</value>
    </values>
    <values>
        <field>Team_name__c</field>
        <value xsi:type="xsd:string">Channel Partnerships</value>
    </values>
</CustomMetadata>
