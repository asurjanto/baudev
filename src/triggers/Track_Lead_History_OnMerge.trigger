/*************************************************************************
Author : Isha Saxena
Description: To update the time status for lead to track the status changes while lead merge: NEO335
Created Date: 22 March 2017

*************************************************************************/

trigger Track_Lead_History_OnMerge  on Lead_Stage_History__c (after update)
{
      BusinessHours stdBusinessHours = [select id,Name from businesshours where Name = 'Marketing_Hours'];
      System.debug('######Hours'+stdBusinessHours);
      List<Lead_Stage_History__c> leadlist = new List<Lead_Stage_History__c>();
      //List<Lead> LeadsToUpdate = new List<Lead>();   // Lead list
      DateTime startDate;
      Double dayct ;
      DateTime endDate;
      
       for (Lead_Stage_History__c lsh : Trigger.new)
       {
                   Lead_Stage_History__c  Oldlsh = Trigger.oldMap.get(lsh.Id);
                   //LeadsToUpdate = [SELECT Id,Name FROM Lead WHERE Id = : lsh.Lead__c];
                   //System.debug('#####Leadlist'+LeadsToUpdate);
                   
                    If(lsh.Stage_Change_Date__c != null)
                    {        
                        System.assertnotequals(lsh.Stage_Change_Date__c,null);
                        If((Oldlsh.Status_Entry_Date_Time__c != lsh.Status_Entry_Date_Time__c)||(Oldlsh.Stage_Change_Date__c != lsh.Stage_Change_Date__c))
                        {
                                startDate = lsh.Status_Entry_Date_Time__c;
                                endDate = lsh.Stage_Change_Date__c;
                                System.debug('&&&&&&Startdate'+startDate);
                                System.debug('&&&&&&endDate'+endDate);
                                Long diff = BusinessHours.diff(stdBusinessHours.Id,startDate,endDate); // getting the difference as per business hours in milliseconds
                                System.debug('&&&&&&diff'+diff);
                                
                                Long minutes = diff / (60 * 1000); // converting milliseconds to minutes
                                double min = diff/60000;
                                System.debug('&&&&&&min'+minutes);
                                 System.debug('&&&&&Daymin'+min);
                               
                                String hrs = (minutes / 60) + '.' + Math.mod(minutes, 60); // converting minutes to hours
                                double  business_cal = double.valueof(hrs);
                                System.debug('&&&&&&dbl'+business_cal);
                                System.debug('&&&&&&hrs'+hrs);
                                       
                                dayct = min/480;
                                System.debug('&&&&&&days'+dayct);
                                Lead_Stage_History__c lt=new Lead_Stage_History__c();
                                lt.id=lsh.id;
                                lt.Business_hour__c= business_cal;
                                lt.Days__c = dayct;
                                       
                                system.debug('$$$$$Business hour'+lt.Business_hour__c);
                                system.debug('$$$$$Business day'+lt.Days__c);
                                    
                                
                                       
                                leadlist.add(lt); // add records in leaslist
                                 
                        }
                    }
       }
        system.debug('Exited CalcBusinessHours Method'+leadlist);
        update leadlist; //update leadlist
}