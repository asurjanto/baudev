//Updates Case for SFDC_Shipment__c object if Shipment_Status__c is Outbound
trigger Case_shipped on SFDC_Shipment__c (after insert) {

    Set<id> caseIds = new Set<id>();
    Case[] caseToUpdate = new case[]{};
        
    //Check if the SFDC_Shipment__c.Shipment_Status__c = Outbound
    for(SFDC_Shipment__c a: Trigger.new)
    {
        if (a.Shipment_Status__c == 'Outbound')
        {
            caseIds.add(a.Case__c);
        }
    }
    if (caseIds.size() > 0)
    {
        caseToUpdate = [select id, Status from Case where id in :caseIds];
    }
    
    //Set the Case.Status to Shipped for all the SFDC_Shipment__c.Shipment_Status__c = Outbound
    if (caseToUpdate.size() > 0)
    {
        for (Case current: caseToUpdate)
        {
            if(current.Status != 'Closed')
            {
            current.Status = 'Shipped';
            }
        }
        update caseToUpdate;
    }
}