//Sets Cases' Priority, ApexMarker__c, Account and Contact fields
//The decision of the values is taken based on Subject and SuppliedEmail fields of the Case
trigger Email_To_Case on Case (before insert) 
{
	String[] emailsAdd = new String[]{};
	//String[] leftOverEmailsAdd = new String[]{};
	
	//List<Case> caseToCheck = new List<Case>([select id, Subject, Priority, ApexMarker__c, SuppliedEmail, contactid, accountid from Case where id in :trigger.new]);
	
	for (Case a : trigger.new)
	{
		if (a.SuppliedEmail != null)
		{
			emailsAdd.add(a.SuppliedEmail);
		
			if (a.Subject.startswith('p1')|| a.Subject.startswith('P1'))
			{
				a.Priority = '1';
			}
			else if (a.Subject.startswith('Please board')|| a.Subject.startswith('please board'))
			{
				a.Priority = '4';
			}
			else if (a.Subject.contains('New message 1 in mailbox 793') || a.Subject.contains('New voicemail from'))
			{
				a.Priority = '2';
				a.ApexMarker__c = 'Contains voicemail';
			}
		}
	}
	
	if (emailsAdd.size() > 0)
	{
		List<Contact> contactList = new List<Contact>([select id, account.Id, account.Name, account.Merchant_Admin_Email__c, email from Contact where email in :emailsAdd]);
		List<Account> accountList = new List<Account>([select id, Merchant_Admin_Email__c from Account where Merchant_Admin_Email__c in :emailsAdd]);
		
		for (Case a : trigger.new)
		{
			if(a.SuppliedEmail.contains('@tyro')== FALSE)
			{
				for (Contact c : contactList)
				{
					if (c.email == a.SuppliedEmail && c.account.Name == 'MoneySwitch Limited')
					{
						a.accountid = null;
						a.Contactid = null;					
					}
					else if (c.email == a.SuppliedEmail)
					{
						a.contactid = c.Id;
						a.accountid = c.accountid;
					}
				}
			}
			if(a.SuppliedEmail.contains('@tyro'))
			{
				a.accountid = null;
				a.Contactid = null;
			}
			
			if(a.contactid == null && a.accountid == null)
			{
				for (Account acc : accountList)
				{
					if(acc.Merchant_Admin_Email__c == a.SuppliedEmail)
					{
						a.accountid = acc.id;
					}
				}
			}
		}
	}
	//update caseToCheck;
}