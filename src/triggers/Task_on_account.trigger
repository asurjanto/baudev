//Updates Account's Has_Task__c and Sales_Merchant_Status__c fields if a Task is created agains the Account
//Update by Arvind. on 21st Jan, 2019. To be optimized later on
trigger Task_on_account on Task (before insert, before update) {
    

    Map<id, String> taskCategory = new Map<id, String>();
    List<Account> accountsToUpdate = new List<Account>();
    Boolean changesMade = FALSE;
    
    for(Task tasks : Trigger.new) {
        
        if(!tasks.Subject.startsWith('Mass Email') && !String.isBlank(tasks.Task_Category__c)) {  
            taskCategory.put(tasks.whatID, tasks.Task_Category__c);
        }
        
    }
    
    for(Account accounts : [SELECT Has_Task__c, Sales_Merchant_Status__c 
                                FROM Account 
                                WHERE Id IN :taskCategory.keyset()]) {
        
        String taskCatVal = taskCategory.get(accounts.Id);
        changesMade = FALSE;
        
        if (!accounts.Has_Task__c) {
            accounts.has_task__c = TRUE;
            changesMade = TRUE;
        }
        
        if (taskCatVal == 'First Call' && accounts.Sales_Merchant_Status__c != 'Prospect') {
            accounts.Sales_Merchant_Status__c = 'Prospect';
            changesMade = TRUE;
        } else if (taskCatVal == 'Initial Contact Follow Up' && accounts.Sales_Merchant_Status__c != 'Initial Contact: Follow up') {
            accounts.Sales_Merchant_Status__c = 'Initial Contact: Follow up';
            changesMade = TRUE;
        }else if (taskCatVal == 'Objection Handling' && accounts.Sales_Merchant_Status__c != 'Objection Handling') {
            accounts.Sales_Merchant_Status__c = 'Objection Handling';
            changesMade = TRUE;
        }else if (taskCatVal == 'Application Chase' && accounts.Sales_Merchant_Status__c != 'Verbal Yes: Chasing App') {
            accounts.Sales_Merchant_Status__c = 'Verbal Yes: Chasing App';
            changesMade = TRUE;
        }else if (taskCatVal == 'Application received: Chase Info' && accounts.Sales_Merchant_Status__c != 'Application Received: Chase Info') {
            accounts.Sales_Merchant_Status__c = 'Application Received: Chase Info';
            changesMade = TRUE;
        }else if (taskCatVal == 'Account Management' && accounts.Sales_Merchant_Status__c != 'Application Received') {
            accounts.Sales_Merchant_Status__c = 'Application Received';
            changesMade = TRUE;
        }
        
        if(changesMade) {
            accountsToUpdate.add(accounts);
        }
    }
    
    if(!accountsToUpdate.isEmpty()) {
        try {
            update accountsToUpdate;
        }Catch (DmlException e) {
            system.debug('Error Updating records');
        }
    }
    
    
}