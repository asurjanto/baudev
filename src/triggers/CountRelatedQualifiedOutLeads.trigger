//Updates Qualified_Out_Leads__c field on Integration_Product__c for Leads
//If Lead has not be converted and Status is 'Qualified Out', the Qualified_Out_Leads__c field is set to 1
trigger CountRelatedQualifiedOutLeads on Lead (after insert, after update, after delete, after undelete) 
{
      
      Set<Id> updateIntegrationProductIds = new Set<Id>();

      Map<Id,Integration_Product__c> updateIntegrationProduct = new Map<Id,Integration_Product__c>();
      
      
      // If we are inserting, updating, or undeleting, use the new ID values
      if(Trigger.isInsert || Trigger.isUpdate || Trigger.isUndelete)
      for(Lead testing:Trigger.new)
      
      
      updateIntegrationProductIds.add(testing.Integration_Product__c);
        
      // If we are updating, some Integration Products might change, so include that as well as deletes
      if(Trigger.isUpdate || Trigger.isDelete)
      for(Lead testing:Trigger.old)
      
        
      updateIntegrationProductIds.add(testing.Integration_Product__c);
            
      // Do not create a record for null field
      updateIntegrationProductIds.remove(null);
  
      
      // Create in-memory copies for all Integration Products that will be affected
      for(Id IntegrationProductId:updateIntegrationProductIds)
      updateIntegrationProduct.put(IntegrationProductId,new Integration_Product__c(id=IntegrationProductId,Qualified_Out_Leads__c=0));
    
      // Run an optimized query that looks for all Integration Products that meet the if/then criteria
      for(Lead testing:[select id,Integration_Product__c,IsConverted,Status from Lead where Integration_Product__c in :updateIntegrationProductIds])
      // check if lead has been converted to an account
      if (testing.IsConverted == False && testing.Status == 'Qualified Out')
      {
          updateIntegrationProduct.get(testing.Integration_Product__c).Qualified_Out_Leads__c++;

      }  
      // Update all the Integration Products with new values.
      Database.update(updateIntegrationProduct.values());
}