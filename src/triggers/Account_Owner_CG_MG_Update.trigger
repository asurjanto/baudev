/*****************************************************************************************************************************
Author : Isha Saxena
Description: To update all the Account Owners referring the Account Allocation Rule if Account Manager is changed : NEO 720
Created Date: 14 August 2017
Last modified Date:  14 August 2017
******************************************************************************************************************************/

trigger Account_Owner_CG_MG_Update on Account (after Update)
{
    // Checks if this is after event
    if (Trigger.isAfter) 
    { 
        /* After Update */
        if(Trigger.IsUpdate)
        {
                AccountTriggerHandler.OnAfterUpdate(Trigger.new, Trigger.newMap,
                                                    Trigger.old, Trigger.oldMap,
                                                    Trigger.isInsert, Trigger.isUpdate);
        }
   }     
       
}