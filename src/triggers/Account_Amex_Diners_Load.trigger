trigger Account_Amex_Diners_Load on Account (after update) 
{
    ///Jira Items: SF-504/SF-507
    ///The code below is redundant and thereforce commented
    
    
    /*String currentUsersEmail = UserInfo.getUserName();
    if(trigger.new.size() == 1)
    {
        //List<Attachment> amexRecords = new List<Attachment>();
        //List<Attachment> dinersRecords = new List<Attachment>();
        Account a = trigger.new[0];
        Account prevVersion = Trigger.oldMap.get(a.id);
        if ((a.AMEX_Mer__c != null && a.AMEX_Mer__c != prevVersion.AMEX_Mer__c && a.Merchant_ID__c != null && a.ShippingStreet != null) || (a.AMEX_Mer__c != null && a.Merchant_ID__c != prevVersion.Merchant_ID__c && a.ShippingStreet != null && a.Merchant_ID__c != null))
        {
            Blob amexLoad = AmexFunctions.createAmexLoad(a);
            Attachment amexRecord = new Attachment();
            amexRecord.name = 'Amex Load.csv';
            amexRecord.Body = amexLoad;
            amexRecord.ParentId = a.id;
            amexRecord.ContentType = 'csv/text';
        
            insert amexRecord;      
            AmexFunctions.emailAmexLoad(amexLoad, currentUsersEmail);       
        }
            
        if ((a.Diners_Merchant_No__c != null && a.Diners_Merchant_No__c != prevVersion.Diners_Merchant_No__c && a.Merchant_ID__c != null && a.ShippingStreet != null) || (a.Diners_Merchant_No__c != null && a.Merchant_ID__c != prevVersion.Merchant_ID__c && a.Merchant_ID__c != null && a.ShippingStreet != null))
        {
            Blob dinersLoad = DinersFunctions.createDinersLoad(a);
            Attachment dinersRecord = new Attachment();
            dinersRecord.name = 'Diners Load.csv';
            dinersRecord.Body = dinersLoad;
            dinersRecord.ParentId = a.id;
            dinersRecord.ContentType = 'csv/text';
            
            insert  dinersRecord;
            DinersFunctions.emailDinersLoad(dinersLoad, currentUsersEmail);     
        }
    }*/
}