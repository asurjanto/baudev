trigger ExceptionEventTrigger on Exception_Event__e (after insert) {
    
    new ExceptionEventTriggerHandler().run();
    
}