//Formats Contact's Phone and Mobile with HUD format
trigger Contact_Formatting on Contact (before insert, before update) {
    
    for (Contact a : trigger.new)
    {
        if(a.Phone != null )
        {
            a.HUD_Phone_No__c = FormatPhoneForHUD.FormatPhoneForHUD(a.Phone);
        }
        
        if(a.MobilePhone != null )
        {
            a.HUD_Mob_No__c = FormatPhoneForHUD.FormatPhoneForHUD(a.MobilePhone);
        }
        
        if(a.MobilePhone == null )
            a.HUD_Mob_No__c = null;
        
        if(a.Phone == null )
            a.HUD_Phone_No__c = null;   
    }
}