//Updates Assets for Shipment_Content__c.TNT_Shipment__r.Shipment_Type__c = Outbound/Inbound 
trigger Asset_shipped_TNT on Shipment_Content__c (after insert) {
    
    ID rmaAccount = [select id from Account where Name = 'Tyro Stock' limit 1].id;
    
    List<Shipment_Content__c> contentList = new List<Shipment_Content__c>([select id, TNT_Shipment__r.Shipment_Type__c, TNT_Shipment__r.Case__r.Accountid, TNT_Shipment__r.Case__r.id, Asset__c, Asset__r.name from Shipment_Content__c where id in :Trigger.new]);
    
    Map<id, Shipment_Content__c> OutboundAssetIdsMap = new Map<id, Shipment_Content__c>();
    
    Asset[] outboundAssetsToUpdate = new Asset[]{};
        
        Map<id, Shipment_Content__c> inboundAssetIdsMap = new Map<id, Shipment_Content__c>();
    
    Asset[] inboundAssetsToUpdate = new Asset[]{};
        
        
        
        for(Shipment_Content__c b: contentList )
    {
        //Get list of All Assets for Shipment_Content.TNT_Shipment__r.Shipment_Type__c = Outbound
        if(b.TNT_Shipment__r.Shipment_Type__c == 'Outbound')
        {
            ID tempassetid = b.Asset__c;
            outboundAssetIdsMap .put(tempassetid, b);
        }
        
        //Get list of All Assets for Shipment_Content.TNT_Shipment__r.Shipment_Type__c = Inbound
        if(b.TNT_Shipment__r.Shipment_Type__c == 'Inbound')
        {
            ID tempassetid = b.Asset__c;
            inboundAssetIdsMap .put(tempassetid, b);
        }
    }
    
    
    Set<id> outboundAssetIds  = new Set<id>(outboundAssetIdsMap .keySet());
    Set<id> inboundAssetIds  = new Set<id>(inboundAssetIdsMap .keySet());
    
    
    
    if (outboundAssetIds .size() > 0)
    {
        
        outboundAssetsToUpdate  = [select id, Accountid, Status__c from Asset where id in :outboundAssetIds ];
    }
    
    
    if (inboundAssetIds .size() > 0)
    {
        inboundAssetsToUpdate  = [select id, Accountid, Status__c from Asset where id in :inboundAssetIds ];
    }
    
    
    //If Shipment_Content.TNT_Shipment__r.Shipment_Type__c = Outbound
    //	Set Status, Status__c and AccountId for the Asset
    if (outboundAssetsToUpdate .size() > 0)
    {
        for (Asset current: outboundAssetsToUpdate )
        {
            current.status = 'Fleet';
            current.Status__c = 'Shipped';
            Shipment_Content__c currentContent = new Shipment_Content__c();
            currentContent = outboundAssetIdsMap .get(current.id);
            id tempAccountId  = currentContent.TNT_Shipment__r.Case__r.Accountid;
            current.accountid = tempAccountId ;
        }
        update outboundAssetsToUpdate ;
    }
    
    //If Shipment_Content.TNT_Shipment__r.Shipment_Type__c = Inbound
    //	Set Status, Status__c and AccountId for the Asset
    if (inboundAssetsToUpdate .size() > 0)
    {
        for (Asset current: inboundAssetsToUpdate )
        {   
            current.status = 'Stock';   
            current.Status__c = 'RMA';
            current.accountid = rmaAccount;
        }
        update inboundAssetsToUpdate ;
    }
    
    
}