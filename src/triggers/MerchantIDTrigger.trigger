trigger MerchantIDTrigger on Merchant_Id__c (after insert, after update) {
    
    new MerchantIDTriggerDispatcher().run();
}