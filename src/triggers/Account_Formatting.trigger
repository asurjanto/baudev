//The trigger formats Account fields on the insert and update of account 
//  and sets certain fields on the basis of other fields
trigger Account_Formatting on Account (before insert, before update) 
{
    Account[] accountschanged = new Account[]{};
    for (Account a : trigger.new)
    {
        //Sets Account Number by removing commas in Merchant ID field
        if(a.Merchant_ID__c != null)
        {
            String tempMID = a.Merchant_ID__c.format();
            String fixedMID = tempMID.replace(',','');
            a.AccountNumber = fixedMID;
        }
            
        //Sets Trading Address Lines 1, 2 and 3 from Shipping Address
        //  The lines 1, 2 and 3 are substrings of 30 subsequent characters in ShippingStreet
        //  The logic checks for full words only and if adding a word to a line will make it longer than 30, the word is shifted to the next line
        if(a.ShippingStreet != null)
        {
            String TradingAddress = a.ShippingStreet;
            String first30FullWords =' ';
            String second30FullWords=' ';
            String third30FullWords=' ';
            Integer line = 1;

            String[] split = TradingAddress.split(' ');
    
            for (String currentWord : split)
            {
                if (first30FullWords == null || (first30FullWords.length() + currentWord.length()) <= 30)
                {
                    if(line == 1)
                    first30FullWords = first30FullWords + ' ' + currentWord;
                }
                else
                line = 2;
            
                if (second30FullWords == null ||(second30FullWords.length() + currentWord.length()) <= 30)
                {
                    if(line == 2)
                    second30FullWords = second30FullWords + ' ' + currentWord;
                }
                else
                line = 3;
            
                if (third30FullWords == null ||(third30FullWords.length() + currentWord.length()) <= 30)
                {
                    if(line == 3)
                    third30FullWords = third30FullWords + ' ' + currentWord;
                }
                else
                line = 4;
            }
            if(first30FullWords != null)
            a.Trading_Address_Line_1__c = first30FullWords;
            if(second30FullWords != null)
            a.Trading_Address_Line_2__c = second30FullWords;
            if(third30FullWords != null)
            a.Trading_Address_Line_3__c = third30FullWords;
        }
        
        //Sets the HUD Phone Number field from Phone field
        //  The format of HUD Phone Number is XXX-XXX-XXXXXX (10 numbers 2 dashes)
        //  The mobile numbers are stripped of country code information
        //  The landline numbers are formatted as per the HUD format
        //  All numbers start with a ZERO
        //  The code works for Australian numbers only
        if(a.phone != null )
        {
            a.HUD_Phone_No__c = FormatPhoneForHUD.FormatPhoneForHUD(a.phone);
        }
        else if(a.phone == null )
        a.HUD_Phone_No__c = null;
        
    }
}