trigger MerchantGroupMemberTrigger on Merchant_Group_Member__c (after insert, after update) {
    
    new MerchantGroupMemberTriggerDispatcher().run();
}