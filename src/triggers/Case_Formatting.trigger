//Formats Case Subject for following conditions
//	Case Reason = New Shipment OR Case Reason = RMA Shipment
//	Case's Account's Channel__c is one of
//		Light Storm/FM Integrated pharmacy solutions/Deliver IT/Imagatec/Leapfrog IT/CDC/Goodson Imports
trigger Case_Formatting on Case (before insert, before update) 
{
    if(trigger.new.size() == 1)
    {
        if(trigger.new[0].Reason == 'New Shipment' || trigger.new[0].Reason == 'RMA Shipment')
        {
            Account currentAcc = [select Channel__c, MCC__c from Account where id = :trigger.new[0].AccountId];
            if (currentAcc.Channel__c == 'Light Storm' || currentAcc.Channel__c == 'FM Integrated pharmacy solutions' || currentAcc.Channel__c == 'Deliver IT' || currentAcc.Channel__c == 'Imagatec' || currentAcc.Channel__c == 'Leapfrog IT' || currentAcc.Channel__c == 'CDC' || currentAcc.Channel__c == 'Goodson Imports')
            {
                //If the Case subject does not already contain '** T1 **' OR '** T2 **' OR T2
                //Add '** T1 **' to the Subject
                if (trigger.new[0].Subject.startswith('** T1 **') == FALSE && trigger.new[0].Subject.startswith('** T2 **') == FALSE && trigger.new[0].Subject.startswith('T2') == FALSE)
                {
                    trigger.new[0].Subject = '** T1 ** '  + trigger.new[0].Subject;
                }
            }
        }
    }
}