//Deletes EmailMessage for cases where:
//	Case.ApexMarker__c = Delete voicemail AND
//	EmailMessage.Subject starts with 'New message 1 in mailbox'
trigger DeleteEmailsFromCase on Case (after update) 
{
	List<Case> caseToCheck = new List<Case>();
	for (Case a : trigger.new)
	{
		if (a.ApexMarker__c == 'Delete voicemail')
		{
			caseToCheck.add(a);
		}
	}
	
	//List<Case> caseToCheck = new List<Case>([select id, ApexMarker__c from Case where id in :trigger.new AND ApexMarker__c = 'Delete voicemail']);

	List<EmailMessage> emailsToCheck = new List<EmailMessage>([select id, subject from EmailMessage where Parentid in :caseToCheck]);
	
	List<EmailMessage> emailsToDelete = new List<EmailMessage>();
	
	for (EmailMessage a : emailsToCheck)
	{
		if (a.subject.startswith('New message 1 in mailbox'))
		{
			emailsToDelete.add(a);
		}
	}
	
	if (emailsToDelete.size() > 0)
	{ 
		delete emailsToDelete;
	}

}