//Updates Number_of_merchants__c field on Integration_Product__c for Accounts
//If Merchant ID field on Account is populated, the Number_of_merchants__c field is set to 1
trigger CountRelatedAccounts on Account (after insert, after update, after delete, after undelete) 
{
    Set<Id> updateIntegrationProductIds = new Set<Id>();
    Map<Id,Integration_Product__c> updateIntegrationProduct = new Map<Id,Integration_Product__c>();
    
    // If we are inserting, updating, or undeleting, use the new ID values
    if(Trigger.isInsert || Trigger.isUpdate || Trigger.isUndelete)
        for(Account testing:Trigger.new)
        
        updateIntegrationProductIds.add(testing.Integration_Product__c);
    
    // If we are updating, some Integration Products might change, so include that as well as deletes
    if(Trigger.isUpdate || Trigger.isDelete)
        for(Account testing:Trigger.old)
        
        updateIntegrationProductIds.add(testing.Integration_Product__c);
    
    // Do not create a record for null field
    updateIntegrationProductIds.remove(null);
    
    // Create in-memory copies for all Integration Products that will be affected
    for(Id IntegrationProductId:updateIntegrationProductIds)
        updateIntegrationProduct.put(IntegrationProductId,new Integration_Product__c(id=IntegrationProductId,Number_of_merchants__c=0));
    
    // Run an optimized query that looks for all Integration Products that meet the if/then criteria
    for(Account testing:[select id,Integration_Product__c, Merchant_ID__c from Account where Integration_Product__c in :updateIntegrationProductIds])
    {
        //Check if Merchant ID field is null
        if(testing.Merchant_ID__c > 0)
        {
            updateIntegrationProduct.get(testing.Integration_Product__c).Number_of_merchants__c++;
        }
    }
    
    // Update all the Integration Products with new values.
    Database.update(updateIntegrationProduct.values());
}