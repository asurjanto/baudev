//Updates Account's Has_Task__c field to False for all Completed Tasks
trigger clearFlag on Task (After update) 
{
    
    Set<id> accountIDs = new Set<id>();
    Map<id, String> taskCategory = new Map<id, String>();
    List<Account> accountsToUpdate = new List<Account>();
    Boolean changesMade = FALSE;
    for(Task a: Trigger.new)
    {
        //check status of task to see if it is set to 'Completed'
        String status = a.status;
        if(status == 'Completed')
        {
            
            taskCategory.put(a.whatID, a.Task_Category__c);
        }
        
    }
    
    accountIDs = taskCategory.keyset();
    
    if(accountIDs.size() > 0)
    {
        accountsToUpdate = new List<Account>([select has_task__c from Account where id in :accountIDs]);
        
        
        for(Account i : accountsToUpdate)
        {
            
            // check if 'has task' box is selected.
            if (i.has_task__c == TRUE)
            {
                //unflag 'has task' box
                i.has_task__c = FALSE;
                changesMade = TRUE;
            }
            
            
        }
    }
    try
    {
        if(changesMade)
        {
            update accountsToUpdate;
        }
    }
    catch (DmlException e)
    {
        for(Task a: Trigger.new)
        { 
            String adminErrorMsg = ' There was a problem with the clearFlag trigger.\n\n';
            String errorMsg = e.getMessage();
            EmailUsers.emailAdmin(adminErrorMsg, errorMsg);
            Integer start = errorMsg.lastIndexOf('EXCEPTION');
            start = start + 10;
            String shortErrorMsg = errorMsg.substring(start);
            a.Task_Category__c.adderror('There was an error updating the account associated with this task. (<a href=\'/' + a.whatId + '\'>' + shortErrorMsg + '</a> ). If you are unsure of how to correct this yourself, please contact your administrator.' );
        }
    }
    
}