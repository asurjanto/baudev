//Inserts Terminal_Diagnosis__c and Updates Assests
//  If Case.Reason = RMA Shipment
//Updates Assets
//  If Case.Reason = Terminal returns
trigger newTerminalDiag on Case (after insert)
{
    List<Terminal_Diagnosis__c> Terminal = new List<Terminal_Diagnosis__c>();
    Asset[] assetToUpdate = new Asset[]{};
    Set<id> assetIds = new Set<id>();
   
     
    for (Case c: Trigger.new)
    {   
        if (c.Reason == 'RMA Shipment')
        {
            Terminal_Diagnosis__c terminalDiag1 =  new Terminal_Diagnosis__c();
            terminalDiag1.Case__c = c.ID;
            terminalDiag1.Asset__c = c.Terminal_1__c;
            terminalDiag1.Fault_Category__c = c.Fault_Category2__c;
            terminalDiag1.Fault_Sub_Category__c = c.Fault_Sub_Category__c;
            insert terminalDiag1;
            
            if(c.Terminal_2__c<>null)
            {
                Terminal_Diagnosis__c terminalDiag2 =  new Terminal_Diagnosis__c();
                terminalDiag2.Case__c = c.ID;
                terminalDiag2.Asset__c = c.Terminal_2__c;
                terminalDiag2.Fault_Category__c = c.Fault_Category2__c;
                terminalDiag2.Fault_Sub_Category__c = c.Fault_Sub_Category__c;
                insert terminalDiag2;
            }
            if(c.Terminal_3__c<>null)
            {
                Terminal_Diagnosis__c terminalDiag3 =  new Terminal_Diagnosis__c();
                terminalDiag3.Case__c = c.ID;
                terminalDiag3.Asset__c = c.Terminal_3__c;
                terminalDiag3.Fault_Category__c = c.Fault_Category2__c;
                terminalDiag3.Fault_Sub_Category__c = c.Fault_Sub_Category__c;
                insert terminalDiag3;
            }
            
            assetIds.add(c.Terminal_1__c);
            assetIds.add(c.Terminal_2__c);
            assetIds.add(c.Terminal_3__c);
           
            assetToUpdate = [select Status__c, ID from Asset where ID in :assetIds];
            
            for (Asset current: assetToUpdate)
            {
                current.Status__c = 'Awaiting Return';             
            }
            
            update assetToUpdate;
                
        }    
        if (c.Reason == 'Terminal returns')
        {
        
            assetIds.add(c.Terminal_1__c);
            assetIds.add(c.Terminal_2__c);
            assetIds.add(c.Terminal_3__c);
           
            assetToUpdate = [select Status__c, ID from Asset where ID in :assetIds];
            
            for (Asset current: assetToUpdate)
            {
                current.Status__c = 'Awaiting Return';             
            }
            
            update assetToUpdate;
        
      
        }   
    }       
   
}