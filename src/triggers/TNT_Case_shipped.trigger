//Updates Case.Status = Shipped, if
//  TNT_Shipment__c.Shipment_Type__c = Outbound AND
//  Case.Status is not Closed
trigger TNT_Case_shipped on TNT_Shipment__c (after insert) {

    Set<id> caseIds = new Set<id>();
    Case[] caseToUpdate = new case[]{};
    for(TNT_Shipment__c a: Trigger.new)
    {
        if (a.Shipment_Type__c == 'Outbound')
        {
            caseIds.add(a.Case__c);
        }
    }
    if (caseIds.size() > 0)
    {
        caseToUpdate = [select id, Status from Case where id in :caseIds];
    }
    
    if (caseToUpdate.size() > 0)
    {
        for (Case current: caseToUpdate)
        {
            if(current.Status != 'Closed')
            {
            current.Status = 'Shipped';
            }
        }
        update caseToUpdate;
    }
}