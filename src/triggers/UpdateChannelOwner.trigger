//Sets Account.Channel_Manager__c field with Name of the Owner of Account
trigger UpdateChannelOwner on Account(before insert, before update ){
  /**
  1. For each Account being inserted add User Id value in in Set of User Ids.
  2. Fetch all Users whose Id is in the Set.
  3. Add these fetched Users in a Map <User Id, User object>
  4. for each Account being inserted get User from the map and update the field values  
  **/  
  
  //holds User Ids
  Set<Id> setUserIds = new Set<Id>();
  
  //holds a list of Users
  List<User> lstUser = new List<User>();
  
  //holds key value pairs of User Id and User Object
  Map<Id, User> mapUserObj = new Map<Id, User>();

  //holds User object
  User UserObj;
   
  //For each Account being inserted add User Id value in in Set of User Ids.
  for(Account a: Trigger.new){
    if(a.OwnerId != null){
      setUserIds.add(a.OwnerId);
    }
  }
  
  //Fetch all Users whose Id is in the Set.
  lstUser = [select Id, Name from User where Id in :setUserIds Limit 1000];
  if(lstUser.size() == 0){
    return;  
  }
  
  //Add these fetched Users in a Map <User Id, User object>
  for(User usrObj : lstUser){
    mapUserObj.put(usrObj.Id, usrObj);  
  }
  
  //for each Account being inserted get User from the map and update the field values
  for(Account a: Trigger.new){
    //get User object
    if(a.OwnerId != null){
      if(mapUserObj.containsKey(a.OwnerId)){
        UserObj = mapUserObj.get(a.OwnerId);
        //map Account fields to User fields
        a.Channel_Manager__c = UserObj.Name;
      }  
    }
  }
}