/*************************************************************************
Author : Isha Saxena
Description: Created to link a case with Live chat Transcript
Created Date: 24 March 2017
Last modified Date: 7 April 2017
*************************************************************************/






trigger LiveChatTranscriptTrigger on LiveChatTranscript (after insert,after update)
{
    
    Set<String> chatKeys = new Set<String>();
    List<Case> casesToCreate = new List<Case>();
    //List<Case> toupdateOwner = new List<Case>();
    List<LiveChatTranscript> transcriptsToUpdate = new List<LiveChatTranscript>();
    String userId = UserInfo.getUserId();
    system.debug('########'+userId);
    
    for ( LiveChatTranscript transcript : Trigger.new ) 
    {
        if ( String.isBlank( transcript.caseId ) && String.isNotBlank(transcript.chatKey) ) 
        {
            chatKeys.add( transcript.chatKey );
            Case caseToAdd = new Case();

               // Set up any fields you want
               
               caseToAdd.chat_key__c = transcript.chatKey;
               caseToAdd.Status = 'New';
               caseToAdd.RecordTypeId = '012D00000002eWY';
               caseToAdd.Origin = 'Chat';
               caseToAdd.Subject ='Live Agent Chat support';
               casesToCreate.add(caseToAdd);
               insert casesToCreate;             
               transcriptsToUpdate.add( new LiveChatTranscript(id = transcript.id,caseId = caseToAdd.id));           
        }
    }
    
    
    System.debug( 'chatKeys=' + chatKeys );
    

    if ( chatKeys.size() > 0 ) 
    {
             // chat_key__c should be a unique, external id field
        // populated in the console once the chat has started and case created
        
        List<Case> cases = new List<Case>([SELECT id, chat_key__c FROM Case WHERE chat_key__c IN :chatKeys ORDER BY createdDate ASC]);
        // chatKey => case
        Map<String, Case> chatKeyCasesMap = new Map<String, Case>();
        for ( Case cs : cases ) 
        {
            chatKeyCasesMap.put( cs.chat_key__c, cs );
            
          
        }
        System.debug( 'chatKeyCasesMap=' + chatKeyCasesMap );       
        if ( transcriptsToUpdate.size() > 0 )
        {
            System.debug( 'transcriptsToUpdate: ' + transcriptsToUpdate );
            update transcriptsToUpdate;
        }

     }
     
     
     
 
}