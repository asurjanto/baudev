trigger Track_Lead_History on Lead (after update) 
{
    
        BusinessHours stdBusinessHours = [select id,Name from businesshours where Name = 'Marketing_Hours'];
        System.debug('######Hours'+stdBusinessHours);
        
        DateTime startDate;
        DateTime endDate;
        Double dayct=0.0;
        
        //to update the lead history list
        List<Lead_Stage_History__c> leadlist = new List<Lead_Stage_History__c>();
        
        Set<Lead_Stage_History__c> setld = new Set<Lead_Stage_History__c>();
        Set<Id> Leadset = new Set<Id>();
        
        List<Lead_Stage_History__c> leadst = new List<Lead_Stage_History__c>();
        
        for (Lead ld : Trigger.new)
        {
                Lead  Oldldt = Trigger.oldMap.get(ld.Id);
                System.debug('call1@@@'+Oldldt);
                if(Oldldt.Status != ld.Status) 
                {
                        Leadset.add(ld.id);
                }
        }
        
        leadst =[Select Id, Name,Lead__c,Business_hour__c,Days__c,Status_Entry_Date_Time__c,Stage_Change_Date__c,Status_Time__c,From_Stage__c,To_Stage__c,Owner.Id,Current_Status_Elapsed_Time__c from Lead_Stage_History__c where Lead__c in :Leadset ];
        System.debug('#####Lead Status'+leadst);
        
        For (Lead_Stage_History__c lsh : leadst)
        {
                if (lsh.To_Stage__c == null && lsh.Stage_Change_Date__c == null)
                {
                  // store lead id and it's child records
                      setld.add(lsh); 
                }
        }
        
        for (Lead_Stage_History__c lds : setld) 
        {
             
              System.debug('all1@@@@@@');
              startDate = lds.Status_Entry_Date_Time__c;
              endDate = system.now();
              lds.Stage_Change_Date__c = endDate;
              System.debug('&&&&&&Startdate'+startDate);
              System.debug('&&&&&&endDate'+endDate);
                                       
              long diff = BusinessHours.diff(stdBusinessHours.Id,startDate,endDate); // getting the difference as per business hours in milliseconds
              System.debug('&&&&&&diff'+diff);
                                       
              Long minutes = diff / (60 * 1000); // converting milliseconds to minutes
              double min = diff/60000;
              System.debug('&&&&&Daymin'+min);
              System.debug('&&&&&&min'+minutes);
              String hrs = (minutes / 60) + '.' + Math.mod(minutes, 60); // converting minutes to hours
              double  business_cal = double.valueof(hrs);
              System.debug('&&&&&&dbl'+business_cal);
              System.debug('&&&&&&hrs'+hrs);
                                        
              dayct = min/480;
              System.debug('&&&&&&days'+dayct);
              Lead_Stage_History__c lt=new Lead_Stage_History__c();
              lt.id=lds.id;
              lt.Business_hour__c= business_cal;
              
              lt.Days__c = dayct;
                                       
              system.debug('$$$$$Business hour'+lt.Business_hour__c);
              system.debug('$$$$$Business day'+lt.Days__c);
                                    
              leadlist.add(lt);
        }
       system.debug('Exited CalcBusinessHours Method'+leadlist);
        update leadlist; //update leadlist
}