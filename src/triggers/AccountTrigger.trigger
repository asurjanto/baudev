trigger AccountTrigger on Account (after update, before update) {
    
    new AccountTriggerDispatcher().run();
}