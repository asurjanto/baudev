//Creates cases for Paper Order based on different conditions
trigger newPaperCase on Case (before insert)
{
    
    List<Case> paperOrders = new List<Case>();
    Set<id> accountIds = new Set<id>();
    account[] account1 = new account[]{};
        
        
        for (Case c: Trigger.new)
    {
        accountIds.add(c.AccountID);  
        
        
        
        //xenta-xr paper order with delivery address listed in account
        if (c.Call_Category__c == 'Paper order' && c.Call_Reason__c == 'Xenta/XR Paper' && c.Delivery_Address__c == 'Trading Address')
        {
            Case paperCase =  new Case(); 
            paperCase.RecordTypeId = '01220000000ISrJ';
            paperCase.AccountId = c.AccountId;
            paperCase.Reason = 'Paper Order';
            paperCase.Origin = 'Tyro - Internal';
            paperCase.Status = 'New';
            paperCase.Subject = 'XENTA PAPER';
            paperCase.Alliance_Paper_Order__c = 'Xenta';
            paperCase.Delivery_Address_Line_1__c = c.Delivery_Address_Line_1__c;
            paperCase.Delivery_Address_Line_2__c = c.Delivery_Address_Line_2__c;
            paperCase.Delivery_Address_Line_3__c = c.Delivery_Address_Line_3__c;
            paperCase.City__c = c.City__c ;
            paperCase.State__c = c.State__c;
            paperCase.Postcode__c = c.Postcode__c;
            paperCase.Paper_Quantity__c = c.Paper_Quantity__c;
            paperCase.Delivery_Address__c = c.Delivery_Address__c;
            paperOrders.add(paperCase);
            
            
        }
        //xenta-xr paper order with custom delivery address
        else if (c.Call_Category__c == 'Paper order' && c.Call_Reason__c == 'Xenta/XR Paper' && c.Delivery_Address__c == 'Custom')
        {
            Case paperCase =  new Case(); 
            paperCase.RecordTypeId = '01220000000ISrJ';
            paperCase.AccountId = c.AccountId;
            paperCase.Reason = 'Paper Order';
            paperCase.Origin = 'Tyro - Internal';
            paperCase.Status = 'New';
            paperCase.Subject = 'XENTA PAPER';
            paperCase.Alliance_Paper_Order__c = 'Xenta';
            paperCase.Description = c.Description;
            paperCase.Paper_Quantity__c = c.Paper_Quantity__c;
            paperCase.Delivery_Address__c = c.Delivery_Address__c;
            paperOrders.add(paperCase);
            
            
        }
        //xentissimo-yoximo paper order with delivery address listed in account
        else if (c.Call_Category__c == 'Paper order' && c.Call_Reason__c == 'Xentissimo/Yoximo Paper' && c.Delivery_Address__c == 'Trading Address')
        {
            Case paperCase =  new Case(); 
            paperCase.RecordTypeId = '01220000000ISrJ';
            paperCase.AccountId = c.AccountId;
            paperCase.Reason = 'Paper Order';
            paperCase.Origin = 'Tyro - Internal';
            paperCase.Status = 'New';
            paperCase.Subject = 'XENTISSIMO PAPER';
            paperCase.Alliance_Paper_Order__c = 'Xentissimo';
            paperCase.Delivery_Address__c = 'Other';
            paperCase.Delivery_Address_Line_1__c = c.Delivery_Address_Line_1__c;
            paperCase.Delivery_Address_Line_2__c = c.Delivery_Address_Line_2__c;
            paperCase.Delivery_Address_Line_3__c = c.Delivery_Address_Line_3__c;
            paperCase.City__c = c.City__c ;
            paperCase.State__c = c.State__c;
            paperCase.Postcode__c = c.Postcode__c;
            paperCase.Paper_Quantity__c = c.Paper_Quantity__c;
            paperCase.Delivery_Address__c = c.Delivery_Address__c;
            paperOrders.add(paperCase);
            
            
        }
        //xentissimo-yoximo paper order with custom delivery address
        else if (c.Call_Category__c == 'Paper order' && c.Call_Reason__c == 'Xentissimo/Yoximo Paper' && c.Delivery_Address__c == 'Custom')
        {
            Case paperCase =  new Case(); 
            paperCase.RecordTypeId = '01220000000ISrJ';
            paperCase.AccountId = c.AccountId;
            paperCase.Reason = 'Paper Order';
            paperCase.Origin = 'Tyro - Internal';
            paperCase.Status = 'New';
            paperCase.Subject = 'XENTISSIMO PAPER';
            paperCase.Alliance_Paper_Order__c = 'Xentissimo';
            paperCase.Description = c.Description;
            paperCase.Paper_Quantity__c = c.Paper_Quantity__c;
            paperCase.Delivery_Address__c = c.Delivery_Address__c;
            paperOrders.add(paperCase);
            
            
        }
        
        
        if(paperOrders.size() > 0)
            
            insert paperOrders;
    }
}