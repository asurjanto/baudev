//Updates Account's Merchant Status = Active, If
//  Asset.Status = In Use AND
//  Asset.Account.Merchant_ID__c = 'Boarding Completed'/'Terminal Shipped'
trigger update_account_active on Asset (after update) 
{
    Set<id> accountIds = new Set<id>();
    Account[] accountsToUpdate = new Account[]{};
    for(Asset a: Trigger.new)
    {
        if (a.Status__c == 'In Use')
        {
            accountids.add(a.AccountId);
        }

    }
    
    if (accountIds.size() > 0)
    {
    accountsToUpdate = [select id, Merchant_Status__c from Account where id in :accountIds];
    }
    
    if (accountsToUpdate.size() > 0)
    {
        for (Account current: accountsToUpdate)
        {
            if(current.Merchant_Status__c == 'Boarding Completed'|| current.Merchant_Status__c == 'Terminal Shipped')
            {
            current.Merchant_Status__c = 'Active';
            }
        }
    }
    update accountsToUpdate;
}