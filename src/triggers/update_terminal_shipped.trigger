//Updates Account's Merchant Status = Terminal Shipped, if
//  Case.Reason = New Shipment AND
//  Case.Status = Shipped AND
//  Account.Merchant_Status__c != Active
trigger update_terminal_shipped on Case (before update) 
{
    Set<id> accountIds = new Set<id>();
    Set<id> XentaAccountIds = new Set<id>();
    Set<id> XentissimoAccountIds = new Set<id>();
    Account[] accountsToUpdate = new Account[]{};
        //Paper_Order__c[] paperOrders = new Paper_Order__c[]{};
        for(Case a: Trigger.new)
    {
        if (a.Reason == 'New Shipment'&& a.Status == 'Shipped')
        {
            accountids.add(a.AccountId);
        }
        ///**********************************************************************************
        ///Paper_Order__c object is deemed redundant.
        ///The code below is commented to remove the dependency of Paper_Order__c object
        ///Jira Item - SF-503
        ///**********************************************************************************
        /*if (a.Reason == 'New Shipment' && a.Status == 'Shipped' && a.Shipped_Paper__c == FALSE && a.Paper_Order__c == 'Xenta')
{
Paper_Order__c newPaperOrder = new Paper_Order__c();
newPaperOrder.Account__c = a.Accountid;
newPaperOrder.Paper_Type__c = 'Xenta';
newPaperOrder.Quantity__c = 1;
newPaperOrder.Delivery_Address__c = 'Other';
newPaperOrder.Delivery_Street_Line_1__c = a.Delivery_Address_Line_1__c;
newPaperOrder.Delivery_Street_Line_2__c = a.Delivery_Address_Line_2__c;
newPaperOrder.Delivery_Street_Line_3__c = a.Delivery_Address_Line_3__c;
newPaperOrder.City__c = a.City__c ;
newPaperOrder.State__c = a.State__c;
newPaperOrder.Postcode__c = a.Postcode__c;
paperOrders.add(newPaperOrder);
a.Shipped_Paper__c = TRUE;
}
else if (a.Reason == 'New Shipment' && a.Status == 'Shipped' && a.Shipped_Paper_Xentissimo__c == FALSE && a.Paper_Order__c == 'Xentissimo')
{
Paper_Order__c newPaperOrder = new Paper_Order__c();
newPaperOrder.Account__c = a.Accountid;
newPaperOrder.Paper_Type__c = 'Xentissimo';
newPaperOrder.Quantity__c = 1;
newPaperOrder.Delivery_Address__c = 'Other';
newPaperOrder.Delivery_Street_Line_1__c = a.Delivery_Address_Line_1__c;
newPaperOrder.Delivery_Street_Line_2__c = a.Delivery_Address_Line_2__c;
newPaperOrder.Delivery_Street_Line_3__c = a.Delivery_Address_Line_3__c;
newPaperOrder.City__c = a.City__c ;
newPaperOrder.State__c = a.State__c;
newPaperOrder.Postcode__c = a.Postcode__c;

paperOrders.add(newPaperOrder);
a.Shipped_Paper_Xentissimo__c = TRUE;           
}*/
    }
    
    /*if(paperOrders.size() > 0)
{
insert paperOrders;
}*/
    
    if (accountIds.size() > 0)
    {
        accountsToUpdate = [select id, Merchant_Status__c from Account where id in :accountIds];
    }
    
    if (accountsToUpdate.size() > 0)
    {
        for (Account current: accountsToUpdate)
        {
            if(current.Merchant_Status__c != 'Active')
            {
                current.Merchant_Status__c = 'Terminal Shipped';
            }
        }
    }
    update accountsToUpdate;
}