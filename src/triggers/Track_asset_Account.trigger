/*************************************************************************
Author : Isha Saxena
Description: Trigger is created to save the Old account value for an asset if an account changes to Tyro stock Account
Created Date: 26 May 2017
Last modified Date: 31 May 2017
// 
*************************************************************************/
trigger Track_asset_Account on Asset (after update)
{
        List<Asset> oldass= new List<Asset>();
        List<Asset> toUpdate = new List<Asset>();
        Set<Id> aset = new Set<Id>();
        System.debug('***SFDC: Trigger.old is: ' + Trigger.old);
        System.debug('***SFDC: Trigger.new is: ' + Trigger.new);
     for (Asset ast : trigger.new)
    {
        String newVal=ast.AccountId;
        System.debug('******'+newVal);
        String oldVal=trigger.oldMap.get(ast.id).AccountId;
        System.debug('******'+oldVal);
        if (newVal!=oldVal)
        {
           Asset newMem=ast.clone(true, true);
           System.debug('&&&&&&&'+newMem);
           newMem.Prior_AccountId__c=oldVal;
           toUpdate.add(newMem);
        }
   }
   
   System.debug('******'+toUpdate);
    if (toUpdate.size()>0)
    {       update toUpdate;

    }

     
           
}