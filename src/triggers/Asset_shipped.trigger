//Updates Assets for Shipment_Content__c.Shipment__r.Shipment_Status__c = Outbound/Inbound
//Sets Paper Order field on Cases
trigger Asset_shipped on Shipment_Content__c (after insert) {
    
    ID rmaAccount = [select id from Account where Name = 'Tyro Stock' limit 1].id;
    List<Shipment_Content__c> contentList = new List<Shipment_Content__c>([select id, Shipment__r.Shipment_Status__c, Shipment__r.Case__r.Accountid,Shipment__r.Case__r.id, Asset__c, Asset__r.name from Shipment_Content__c where id in :Trigger.new]);
    Map<id, Shipment_Content__c> OutboundAssetIdsMap = new Map<id, Shipment_Content__c>();
    Asset[] outboundAssetsToUpdate = new Asset[]{};
        Map<id, Shipment_Content__c> inboundAssetIdsMap = new Map<id, Shipment_Content__c>();
    Asset[] inboundAssetsToUpdate = new Asset[]{};
        Map<id, string> paperOrder = new Map<id, string>();
    
    for(Shipment_Content__c a: contentList)
    {
        //Set Paper Order collection to update case
        paperOrder.put(a.Shipment__r.Case__r.id, a.Asset__r.name);
        
        //Get list of Assets for Shipment_Content__c.Shipment__r.Shipment_Status__c = Outbound
        if(a.Shipment__r.Shipment_Status__c == 'Outbound')
        {
            ID tempassetid = a.Asset__c;
            outboundAssetIdsMap.put(tempassetid, a);
        }
        
        //Get list of Assets for Shipment_Content__c.Shipment__r.Shipment_Status__c = Inbound
        if(a.Shipment__r.Shipment_Status__c == 'Inbound')
        {
            ID tempassetid = a.Asset__c;
            inboundAssetIdsMap.put(tempassetid, a);
        }
    }
    
    Set<id> outboundAssetIds = new Set<id>(outboundAssetIdsMap.keySet());
    Set<id> inboundAssetIds = new Set<id>(inboundAssetIdsMap.keySet());
    
    if (outboundAssetIds.size() > 0)
    {
        outboundAssetsToUpdate = [select id, Accountid, Status__c from Asset where id in :outboundAssetIds];
    }
    
    if (inboundAssetIds.size() > 0)
    {
        inboundAssetsToUpdate = [select id, Accountid, Status__c from Asset where id in :inboundAssetIds];
    }
    
    //If Shipment_Content__c.Shipment__r.Shipment_Status__c is Outbound
    //	Update Asset, Set Status, Statuc__c and AccountId
    if (outboundAssetsToUpdate.size() > 0)
    {
        for (Asset current: outboundAssetsToUpdate)
        {
            current.status = 'Fleet';
            current.Status__c = 'Shipped';
            Shipment_Content__c currentContent = new Shipment_Content__c();
            currentContent = outboundAssetIdsMap.get(current.id);
            id tempAccountId = currentContent.Shipment__r.Case__r.Accountid;
            current.accountid = tempAccountId;
        }
        update outboundAssetsToUpdate;
    }
    
    //If Shipment_Content__c.Shipment__r.Shipment_Status__c is Inbound
    //	Update Asset, Set Status, Statuc__c and AccountId
    if (inboundAssetsToUpdate.size() > 0)
    {
        for (Asset current: inboundAssetsToUpdate)
        {	
            current.status = 'Stock';	
            current.Status__c = 'RMA';
            current.accountid = rmaAccount;
        }
        update inboundAssetsToUpdate;
    }
    
    
    //Update Case
    //	Set Paper Order field with appropriate Xenta or Xentissimo
    List<Case> caseList = new List<Case>([select id, Paper_Order__c, status, Reason from Case where id in :paperOrder.keyset()]);
    
    for (Case c : caseList)
    {
        if(c.reason == 'New Shipment')
        {
            string name = paperOrder.get(c.id);
            if(name == 'Xenta')
            {
                c.Paper_Order__c = 'Xenta';
            }
            else if (name == 'Xentissimo')
            {
                c.Paper_Order__c = 'Xentissimo';
            }
        }
        
    }
    update caseList;
    
    
}