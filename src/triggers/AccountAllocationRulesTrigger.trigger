trigger AccountAllocationRulesTrigger on Account_Allocation_Rules__c (after insert, after update) {
    
    new AccountAllocationRulesTriggerDispatcher().run();
}