<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>Hierarchy</customSettingsType>
    <description>Store all the Amex Configurations</description>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>Amex_Enabled_Status_API_Name__c</fullName>
        <externalId>false</externalId>
        <label>Amex Enabled Status API Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Amex_Error_Notification_Email_List__c</fullName>
        <description>Type in Email ids separated by a semi-colon. Error Emails would be sent to these addresses.</description>
        <externalId>false</externalId>
        <inlineHelpText>Type in Email ids separated by a semi-colon. Error Emails would be sent to these addresses.</inlineHelpText>
        <label>Amex Error Notification Email List</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Back_Office_Status_API_Name__c</fullName>
        <externalId>false</externalId>
        <label>Back Office Status API Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case_amex_record_type_Id__c</fullName>
        <externalId>false</externalId>
        <label>Case amex record type Id</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Created_Before_date__c</fullName>
        <description>The number of days before today, which is picked by AmexCaseCloseBatch to close the cases</description>
        <externalId>false</externalId>
        <inlineHelpText>Batch picks up Amex cases to close - this number of dates previous to today</inlineHelpText>
        <label>Created Before date</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Divisor_of_Net_Transaction_Volume__c</fullName>
        <description>The net transaction volume is divided by this number to get the Amex transaction volume</description>
        <externalId>false</externalId>
        <inlineHelpText>If Amex transaction volume is 5% of net transaction volume. This number should be 20</inlineHelpText>
        <label>Divisor of Net Transaction Volume</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Inbound_Amex_Email_CSV_Amex_MID_Header_2__c</fullName>
        <description>If the headers keep changing add the alternative header here.</description>
        <externalId>false</externalId>
        <label>Inbound Amex Email CSV Amex MID Header 2</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Inbound_Amex_Email_CSV_Amex_MID_Header__c</fullName>
        <description>Amex sends us a success email with CSV. This CSV is parsed in a class which pulls out this header values</description>
        <externalId>false</externalId>
        <inlineHelpText>Amex sends us a success email with CSV. This CSV is parsed in a class which pulls out this header values</inlineHelpText>
        <label>Inbound Amex Email CSV Amex MID Header</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Inbound_Amex_Email_CSV_Body_Identifier__c</fullName>
        <description>Amex sends us a success email with CSV. The body must contain these values in order for the class to process email.</description>
        <externalId>false</externalId>
        <inlineHelpText>Amex sends us a success email with CSV. The body must contain these values in order for the class to process email.</inlineHelpText>
        <label>Inbound Amex Email CSV Body Identifier</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Inbound_Amex_Email_CSV_MID_Header__c</fullName>
        <description>Amex sends us a success email with CSV. This CSV is parsed in a class which pulls out this header values</description>
        <externalId>false</externalId>
        <inlineHelpText>Amex sends us a success email with CSV. This CSV is parsed in a class which pulls out this header values</inlineHelpText>
        <label>Inbound Amex Email CSV MID Header</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Invitation_Email_Template__c</fullName>
        <description>Template Id used to send Email to users. Used at Amex Email Service Class</description>
        <externalId>false</externalId>
        <inlineHelpText>Template Id used to send Invitation Email to users</inlineHelpText>
        <label>Invitation Email Template</label>
        <length>18</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Micro_Segment_Ceiling__c</fullName>
        <description>The account whose Amex transaction volume is less than this number falls under micro-segment.</description>
        <externalId>false</externalId>
        <inlineHelpText>The account whose Amex transaction volume is less than this number falls under micro-segment.</inlineHelpText>
        <label>Micro Segment Ceiling</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Offer_Accepted_Status_API_Name__c</fullName>
        <externalId>false</externalId>
        <label>Offer Accepted Status API Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Offer_Sent_Status_API_Name__c</fullName>
        <externalId>false</externalId>
        <label>Offer Sent Status API Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sandbox_Test_Contact_Id__c</fullName>
        <externalId>false</externalId>
        <label>Sandbox Test Contact Id</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Small_Segment_Ceiling__c</fullName>
        <description>The accounts with amex transaction less than this number have small segment, first check would be for micro segment</description>
        <externalId>false</externalId>
        <inlineHelpText>The accounts with amex transaction less than this number have small segment, first check would be for micro segment</inlineHelpText>
        <label>Small Segment Ceiling</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Amex Configurations</label>
    <visibility>Public</visibility>
</CustomObject>
