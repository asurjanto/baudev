<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>SF-633 

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Active_Forecast__c</fullName>
        <defaultValue>false</defaultValue>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates if this is a current forecast</inlineHelpText>
        <label>Active Forecast</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Avg_Lead_To_Application_Received_Days__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Average number of days for a Lead to be converted to an application received</inlineHelpText>
        <label>Avg. LD To AR Days</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Avg_MID_Easyclaim_Revenue__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <label>Avg MID EasyClaim Revenue</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Avg_MID_Healthpoint_Revenue__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <label>Avg MID Healthpoint Revenue</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Avg_MID_Monthly_Net_TXN_Value__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Average net transaction value per Merchant ID over the past 12 months</inlineHelpText>
        <label>Avg MID Monthly Net TXN Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>CaseSafeId__c</fullName>
        <externalId>false</externalId>
        <formula>CASESAFEID(Id)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>CaseSafeId</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Channel__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Channel</label>
        <referenceTo>Channel_Agreement__c</referenceTo>
        <relationshipLabel>Channel Forecasts</relationshipLabel>
        <relationshipName>Channel_Forecasts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Closed_Won_Opportunities_Last_365_Days__c</fullName>
        <externalId>false</externalId>
        <label>Closed Won Opportunities Last 365 Days</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Forecast_MSV__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Total merchant statement alue (MSV) forecast for the period</inlineHelpText>
        <label>Forecast MSV</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Forecast_Roll_Up__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>SF-633 

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <label>Forecast Roll Up</label>
        <referenceTo>Forecast_Roll_Up__c</referenceTo>
        <relationshipLabel>Channel Forecasts</relationshipLabel>
        <relationshipName>Channel_Forecasts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Forecast_Start_Date__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Forecast period start date</inlineHelpText>
        <label>Forecast Start Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Integration_Product__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <label>Integration Product</label>
        <referenceTo>Integration_Product__c</referenceTo>
        <relationshipLabel>Channel Forecasts</relationshipLabel>
        <relationshipName>Forecaster</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>LD_To_AR_30_Days__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Percentage of Leads that are converted to application received within 30 days after creation over the past 365 days. Note, includes Opportunities created directly.</inlineHelpText>
        <label>% LD To AR - 30 Days</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>LD_To_AR_60_Days__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Percentage of Leads that are converted to application received between 31 &amp; 60 days  after creation over the past 365 days. Note, includes Opportunities created directly.</inlineHelpText>
        <label>% LD To AR - 60 Days</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>LD_To_AR_90_Days__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Percentage of Leads that are converted to application received between 61 &amp; 90 days after creation over the past 365 days. Note, includes Opportunities created directly.</inlineHelpText>
        <label>% LD To AR - 90 Days</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>LD_To_AR_90_Plus_Days__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Percentage of Leads that are converted to application received more than 90 days after creation over the past 365 days. Note, includes Opportunities created directly.</inlineHelpText>
        <label>% LD To AR - 90+ Days</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Lead_To_App_n_Rec_Conversion_Rate__c</fullName>
        <description>SF-633 

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Percentage of Leads converted to application received over the past 365 days. Note, includes Opportunities created directly.</inlineHelpText>
        <label>LD To AR Conversion Rate</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Leads_Created_Last_365_Days__c</fullName>
        <externalId>false</externalId>
        <label>Leads Created Last 365 Days</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Month_10__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of new Leads expected to be generated for the period</inlineHelpText>
        <label>Month 10</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Month_11__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of new Leads expected to be generated for the period</inlineHelpText>
        <label>Month 11</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Month_12__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of new Leads expected to be generated for the period</inlineHelpText>
        <label>Month 12</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Month_1__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of new Leads expected to be generated for the period</inlineHelpText>
        <label>Month 1</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Month_2__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of new Leads expected to be generated for the period</inlineHelpText>
        <label>Month 2</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Month_3__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of new Leads expected to be generated for the period</inlineHelpText>
        <label>Month 3</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Month_4__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of new Leads expected to be generated for the period</inlineHelpText>
        <label>Month 4</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Month_5__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of new Leads expected to be generated for the period</inlineHelpText>
        <label>Month 5</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Month_6__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of new Leads expected to be generated for the period</inlineHelpText>
        <label>Month 6</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Month_7__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of new Leads expected to be generated for the period</inlineHelpText>
        <label>Month 7</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Month_8__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of new Leads expected to be generated for the period</inlineHelpText>
        <label>Month 8</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Month_9__c</fullName>
        <description>SF-633

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of new Leads expected to be generated for the period</inlineHelpText>
        <label>Month 9</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Channel Forecast</label>
    <nameField>
        <label>Forecast Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Channel Forecasts</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Edit_Forecast</fullName>
        <active>true</active>
        <label>Edit Forecast</label>
    </recordTypes>
    <recordTypes>
        <fullName>Initiate_Forecast</fullName>
        <active>true</active>
        <label>Initiate Forecast</label>
    </recordTypes>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Forecast_Start_Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Integration_Product__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Forecast_Roll_Up__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Active_Forecast__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATED_DATE</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>LAST_UPDATE</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Forecast_Start_Date__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Integration_Product__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Forecast_Roll_Up__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Active_Forecast__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Forecast_Start_Date__c</searchFilterFields>
        <searchFilterFields>Integration_Product__c</searchFilterFields>
        <searchFilterFields>Forecast_Roll_Up__c</searchFilterFields>
        <searchFilterFields>Active_Forecast__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>FC001_Cannot_Edit_Channel_Forecast</fullName>
        <active>false</active>
        <description>Prevents a user from editing the record after approval

SF-633 

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <errorConditionFormula>TEXT(Forecast_Roll_Up__r.Status__c) = &quot;Approved&quot;</errorConditionFormula>
        <errorMessage>FC001: Forecast has been approved and can no longer be edited</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>Mass_Edit_Channel_Forecast</fullName>
        <availability>online</availability>
        <description>Launches a visualforce page which allows a user to edit multiple Channel Forecast records in a single transaction

SF-633 

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Mass Edit Channel Forecast</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>/apex/MassEditChannelForecast</url>
    </webLinks>
    <webLinks>
        <fullName>Mass_Edit_Channel_Forecast_2</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Mass Edit Channel Forecast *</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>/apex/MassEditChannelForecast_2?id={!Forecast_Data__c.ForecastId__c}</url>
    </webLinks>
    <webLinks>
        <fullName>Refresh_Actuals_Data</fullName>
        <availability>online</availability>
        <description>SF-633 

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Refresh Actuals Data</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <url>/apex/Refresh_Actuals_Data?VAR_Forecaster_Id={!Forecaster__c.Id}</url>
    </webLinks>
    <webLinks>
        <fullName>Refresh_Pipeline_Data</fullName>
        <availability>online</availability>
        <description>SF-633 

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Refresh Pipeline Data</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <url>/apex/Refresh_Pipeline_Data?VAR_Forecaster_Id={!Forecaster__c.Id}</url>
    </webLinks>
</CustomObject>
