<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>NEO-782

Table that stores recommended margin based on MCC Category &amp; Segment</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Industry__c</fullName>
        <externalId>false</externalId>
        <label>Industry</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Health</fullName>
                    <default>false</default>
                    <label>Health</label>
                </value>
                <value>
                    <fullName>Hospitality</fullName>
                    <default>false</default>
                    <label>Hospitality</label>
                </value>
                <value>
                    <fullName>Retail</fullName>
                    <default>false</default>
                    <label>Retail</label>
                </value>
                <value>
                    <fullName>Service</fullName>
                    <default>false</default>
                    <label>Service</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Margin_Lookup_Parameter__c</fullName>
        <caseSensitive>true</caseSensitive>
        <description>NEO-782</description>
        <externalId>false</externalId>
        <label>Margin Lookup Parameter</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Margin__c</fullName>
        <description>NEO-782</description>
        <externalId>false</externalId>
        <label>Margin</label>
        <precision>18</precision>
        <required>false</required>
        <scale>3</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Segment__c</fullName>
        <description>NEO-782</description>
        <externalId>false</externalId>
        <label>Segment</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>Segment</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>Sub_Industry__c</fullName>
        <externalId>false</externalId>
        <label>Sub Industry</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>Industry__c</controllingField>
            <restricted>true</restricted>
            <valueSetName>Sub_Industry</valueSetName>
            <valueSettings>
                <controllingFieldValue>Health</controllingFieldValue>
                <valueName>Allied Health, Care &amp; Hospital Services</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Health</controllingFieldValue>
                <valueName>Medical Services</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Health</controllingFieldValue>
                <valueName>Pharmacies</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Health</controllingFieldValue>
                <valueName>Veterinary Services</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hospitality</controllingFieldValue>
                <valueName>Accommodation</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hospitality</controllingFieldValue>
                <valueName>Cafes, Restaurants &amp; Takeaway Food Services</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hospitality</controllingFieldValue>
                <valueName>Pubs, Bars &amp; Liquor Store</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Retail - Automotive</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Retail - Clothing, Footwear and Personal Accessory Retailing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Retail - Electrical &amp; Electronic Goods Retailing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Retail - Food</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Retail - Hardware &amp; Furniture</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Retail - Pharmaceutical and Other Store-Based Retailing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Retail - Recreational Goods Retailing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Service - Construction</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Service - Education &amp; Training</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Service - Personal Service</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Service - Professional Services</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Service - Rental and Hiring</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Service - Sports &amp; Recreational Activities</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Service - Transport, Postal &amp; Warehousing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Wholesale Trade</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <label>Margin Guide</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Industry__c</columns>
        <columns>Sub_Industry__c</columns>
        <columns>Segment__c</columns>
        <columns>Margin__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <sharedTo>
            <role>AM_Manager</role>
            <role>HO_Corp_Sales</role>
            <role>HO_Direct_Sales</role>
            <role>System_Administrators</role>
            <role>VPSales</role>
        </sharedTo>
    </listViews>
    <nameField>
        <displayFormat>MG{00000000}</displayFormat>
        <label>Margin Guide No.</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Margin Guide</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Industry__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Segment__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Margin__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Industry__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Segment__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Margin__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Industry__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Segment__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Margin__c</lookupPhoneDialogsAdditionalFields>
        <searchResultsAdditionalFields>Industry__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Segment__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Margin__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
