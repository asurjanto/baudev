<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Asset__c</fullName>
        <externalId>false</externalId>
        <label>Asset</label>
        <referenceTo>Asset</referenceTo>
        <relationshipName>Terminal_Faults</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Case__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipName>Terminal_Faults</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Comments__c</fullName>
        <externalId>false</externalId>
        <label>Comments</label>
        <length>32000</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Fault_Category__c</fullName>
        <externalId>false</externalId>
        <label>Fault Category</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>Fautly__c</controllingField>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Power</fullName>
                    <default>false</default>
                    <label>Power</label>
                </value>
                <value>
                    <fullName>Software</fullName>
                    <default>false</default>
                    <label>Software</label>
                </value>
                <value>
                    <fullName>Hardware</fullName>
                    <default>false</default>
                    <label>Hardware</label>
                </value>
                <value>
                    <fullName>2G/3G</fullName>
                    <default>false</default>
                    <label>2G/3G</label>
                </value>
                <value>
                    <fullName>GPRS</fullName>
                    <default>false</default>
                    <label>GPRS</label>
                </value>
                <value>
                    <fullName>Wifi</fullName>
                    <default>false</default>
                    <label>Wifi</label>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>Yes</controllingFieldValue>
                <controllingFieldValue>No</controllingFieldValue>
                <controllingFieldValue>DOA</controllingFieldValue>
                <valueName>Power</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Yes</controllingFieldValue>
                <controllingFieldValue>No</controllingFieldValue>
                <controllingFieldValue>DOA</controllingFieldValue>
                <valueName>Software</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Yes</controllingFieldValue>
                <controllingFieldValue>No</controllingFieldValue>
                <controllingFieldValue>DOA</controllingFieldValue>
                <valueName>Hardware</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Yes</controllingFieldValue>
                <controllingFieldValue>No</controllingFieldValue>
                <controllingFieldValue>DOA</controllingFieldValue>
                <valueName>2G/3G</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Yes</controllingFieldValue>
                <controllingFieldValue>No</controllingFieldValue>
                <controllingFieldValue>DOA</controllingFieldValue>
                <valueName>Wifi</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <fields>
        <fullName>Fault_Sub_Category__c</fullName>
        <externalId>false</externalId>
        <label>Fault Sub-Category</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>Fault_Category__c</controllingField>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>2G/3G Config Error</fullName>
                    <default>false</default>
                    <label>2G/3G Config Error</label>
                </value>
                <value>
                    <fullName>Battery Pins</fullName>
                    <default>false</default>
                    <label>Battery Pins</label>
                </value>
                <value>
                    <fullName>Broken Printer</fullName>
                    <default>false</default>
                    <label>Broken Printer</label>
                </value>
                <value>
                    <fullName>Cant connect to Tyro</fullName>
                    <default>false</default>
                    <label>Cant connect to Tyro</label>
                </value>
                <value>
                    <fullName>Chip Reader</fullName>
                    <default>false</default>
                    <label>Chip Reader</label>
                </value>
                <value>
                    <fullName>Contactless</fullName>
                    <default>false</default>
                    <label>Contactless</label>
                </value>
                <value>
                    <fullName>Does not print</fullName>
                    <default>false</default>
                    <label>Does not print</label>
                </value>
                <value>
                    <fullName>Ethernet Port</fullName>
                    <default>false</default>
                    <label>Ethernet Port</label>
                </value>
                <value>
                    <fullName>Failed to sign on</fullName>
                    <default>false</default>
                    <label>Failed to sign on</label>
                </value>
                <value>
                    <fullName>Failed to Start</fullName>
                    <default>false</default>
                    <label>Failed to Start</label>
                </value>
                <value>
                    <fullName>Faint Print</fullName>
                    <default>false</default>
                    <label>Faint Print</label>
                </value>
                <value>
                    <fullName>GPRS Config Error</fullName>
                    <default>false</default>
                    <label>GPRS Config Error</label>
                </value>
                <value>
                    <fullName>Installation Problem</fullName>
                    <default>false</default>
                    <label>Installation Problem</label>
                </value>
                <value>
                    <fullName>Keypad</fullName>
                    <default>false</default>
                    <label>Keypad</label>
                </value>
                <value>
                    <fullName>LEDS or Sound</fullName>
                    <default>false</default>
                    <label>LEDS or Sound</label>
                </value>
                <value>
                    <fullName>Liquid Damage</fullName>
                    <default>false</default>
                    <label>Liquid Damage</label>
                </value>
                <value>
                    <fullName>Loose connector</fullName>
                    <default>false</default>
                    <label>Loose connector</label>
                </value>
                <value>
                    <fullName>Magnetic Stripe Reader</fullName>
                    <default>false</default>
                    <label>Magnetic Stripe Reader</label>
                </value>
                <value>
                    <fullName>Missing Parts (description)</fullName>
                    <default>false</default>
                    <label>Missing Parts (description)</label>
                </value>
                <value>
                    <fullName>Missing Sim</fullName>
                    <default>false</default>
                    <label>Missing Sim</label>
                </value>
                <value>
                    <fullName>NFC</fullName>
                    <default>false</default>
                    <label>NFC</label>
                </value>
                <value>
                    <fullName>No Pin</fullName>
                    <default>false</default>
                    <label>No Pin</label>
                </value>
                <value>
                    <fullName>Not charging</fullName>
                    <default>false</default>
                    <label>Not charging</label>
                </value>
                <value>
                    <fullName>Physical Broken Printer</fullName>
                    <default>false</default>
                    <label>Physical Broken Printer</label>
                </value>
                <value>
                    <fullName>Physical Damage</fullName>
                    <default>false</default>
                    <label>Physical Damage</label>
                </value>
                <value>
                    <fullName>Poor 2G/3G</fullName>
                    <default>false</default>
                    <label>Poor 2G/3G</label>
                </value>
                <value>
                    <fullName>Poor GPRS</fullName>
                    <default>false</default>
                    <label>Poor GPRS</label>
                </value>
                <value>
                    <fullName>Screen</fullName>
                    <default>false</default>
                    <label>Screen</label>
                </value>
                <value>
                    <fullName>Software ver problem (description)</fullName>
                    <default>false</default>
                    <label>Software ver problem (description)</label>
                </value>
                <value>
                    <fullName>Stuck on &apos;Terminal starting&apos;</fullName>
                    <default>false</default>
                    <label>Stuck on &apos;Terminal starting&apos;</label>
                </value>
                <value>
                    <fullName>Stuck on Tyro Payments</fullName>
                    <default>false</default>
                    <label>Stuck on Tyro Payments</label>
                </value>
                <value>
                    <fullName>Stuck on Xenta/Xentissimo starting</fullName>
                    <default>false</default>
                    <label>Stuck on Xenta/Xentissimo starting</label>
                </value>
                <value>
                    <fullName>Tampered State</fullName>
                    <default>false</default>
                    <label>Tampered State</label>
                </value>
                <value>
                    <fullName>Tip of charger stuck in side</fullName>
                    <default>false</default>
                    <label>Tip of charger stuck in side</label>
                </value>
                <value>
                    <fullName>Update Failed</fullName>
                    <default>false</default>
                    <label>Update Failed</label>
                </value>
                <value>
                    <fullName>Wifi Error</fullName>
                    <default>false</default>
                    <label>Wifi Error</label>
                </value>
                <value>
                    <fullName>Wont Power Up</fullName>
                    <default>false</default>
                    <label>Wont Power Up</label>
                </value>
                <value>
                    <fullName>Other (description)</fullName>
                    <default>false</default>
                    <label>Other (description)</label>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>2G/3G</controllingFieldValue>
                <valueName>2G/3G Config Error</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>2G/3G</controllingFieldValue>
                <controllingFieldValue>Software</controllingFieldValue>
                <controllingFieldValue>Wifi</controllingFieldValue>
                <controllingFieldValue>GPRS</controllingFieldValue>
                <valueName>Failed to sign on</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>2G/3G</controllingFieldValue>
                <controllingFieldValue>GPRS</controllingFieldValue>
                <valueName>Missing Sim</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>2G/3G</controllingFieldValue>
                <controllingFieldValue>GPRS</controllingFieldValue>
                <valueName>No Pin</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>2G/3G</controllingFieldValue>
                <valueName>Poor 2G/3G</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>2G/3G</controllingFieldValue>
                <controllingFieldValue>Power</controllingFieldValue>
                <controllingFieldValue>Software</controllingFieldValue>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <controllingFieldValue>Wifi</controllingFieldValue>
                <controllingFieldValue>GPRS</controllingFieldValue>
                <valueName>Other (description)</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Power</controllingFieldValue>
                <valueName>Battery Pins</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Power</controllingFieldValue>
                <valueName>Loose connector</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Power</controllingFieldValue>
                <valueName>Not charging</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Power</controllingFieldValue>
                <valueName>Tip of charger stuck in side</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Power</controllingFieldValue>
                <valueName>Wont Power Up</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Software</controllingFieldValue>
                <valueName>Cant connect to Tyro</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Software</controllingFieldValue>
                <valueName>Does not print</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Software</controllingFieldValue>
                <valueName>Installation Problem</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Software</controllingFieldValue>
                <valueName>Software ver problem (description)</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Software</controllingFieldValue>
                <valueName>Stuck on &apos;Terminal starting&apos;</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Software</controllingFieldValue>
                <valueName>Stuck on Tyro Payments</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Software</controllingFieldValue>
                <valueName>Update Failed</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <valueName>Broken Printer</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <valueName>Chip Reader</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <valueName>Contactless</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <valueName>Ethernet Port</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <valueName>Failed to Start</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <valueName>Faint Print</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <valueName>Keypad</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <valueName>LEDS or Sound</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <valueName>Liquid Damage</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <valueName>Magnetic Stripe Reader</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <valueName>Missing Parts (description)</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <valueName>NFC</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <valueName>Physical Broken Printer</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <valueName>Physical Damage</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <valueName>Screen</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Hardware</controllingFieldValue>
                <valueName>Tampered State</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Wifi</controllingFieldValue>
                <valueName>Wifi Error</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>GPRS</controllingFieldValue>
                <valueName>GPRS Config Error</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>GPRS</controllingFieldValue>
                <valueName>Poor GPRS</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <fields>
        <fullName>Fautly__c</fullName>
        <externalId>false</externalId>
        <label>Faulty</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Yes</fullName>
                    <default>false</default>
                    <label>Yes</label>
                </value>
                <value>
                    <fullName>No</fullName>
                    <default>false</default>
                    <label>No</label>
                </value>
                <value>
                    <fullName>DOA</fullName>
                    <default>false</default>
                    <label>DOA</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Return_Condition_Comments__c</fullName>
        <externalId>false</externalId>
        <label>Return Condition Comments</label>
        <length>32000</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Return_Condition__c</fullName>
        <externalId>false</externalId>
        <label>Return Condition</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Acceptable</fullName>
                    <default>false</default>
                    <label>Acceptable</label>
                </value>
                <value>
                    <fullName>Unacceptable</fullName>
                    <default>false</default>
                    <label>Unacceptable</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Unacceptable_Reasons__c</fullName>
        <externalId>false</externalId>
        <label>Unacceptable Reasons</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <controllingField>Return_Condition__c</controllingField>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Missing Parts</fullName>
                    <default>false</default>
                    <label>Missing Parts</label>
                </value>
                <value>
                    <fullName>Missing SIM</fullName>
                    <default>false</default>
                    <label>Missing SIM</label>
                </value>
                <value>
                    <fullName>Liquid Damage</fullName>
                    <default>false</default>
                    <label>Liquid Damage</label>
                </value>
                <value>
                    <fullName>Visible Damage</fullName>
                    <default>false</default>
                    <label>Visible Damage</label>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>Unacceptable</controllingFieldValue>
                <valueName>Missing Parts</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Unacceptable</controllingFieldValue>
                <valueName>Missing SIM</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Unacceptable</controllingFieldValue>
                <valueName>Liquid Damage</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Unacceptable</controllingFieldValue>
                <valueName>Visible Damage</valueName>
            </valueSettings>
        </valueSet>
        <visibleLines>5</visibleLines>
    </fields>
    <label>Terminal Diagnosis</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Asset__c</columns>
        <columns>Case__c</columns>
        <columns>Fautly__c</columns>
        <columns>Fault_Category__c</columns>
        <columns>Fault_Sub_Category__c</columns>
        <columns>Comments__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CREATED_DATE</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </filters>
        <label>New Today</label>
    </listViews>
    <nameField>
        <displayFormat>A-{0000000}</displayFormat>
        <label>Diagnosis #</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Terminal Diagnosis</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Terminal_Diagnosis</fullName>
        <active>true</active>
        <label>Terminal Diagnosis</label>
        <picklistValues>
            <picklist>Fault_Category__c</picklist>
            <values>
                <fullName>2G%2F3G</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Hardware</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Power</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Software</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Wifi</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Fault_Sub_Category__c</picklist>
            <values>
                <fullName>2G%2F3G Config Error</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Battery Pins</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Broken Printer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Cant connect to Tyro</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Chip Reader</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Contactless</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Does not print</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Ethernet Port</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Failed to Start</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Failed to sign on</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Faint Print</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>GPRS Config Error</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Installation Problem</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Keypad</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>LEDS or Sound</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Liquid Damage</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Loose connector</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Magnetic Stripe Reader</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Missing Parts %28description%29</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Missing Sim</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>NFC</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>No Pin</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Not charging</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Other %28description%29</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Physical Broken Printer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Physical Damage</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Poor 2G%2F3G</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Poor GPRS</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Screen</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Software ver problem %28description%29</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Stuck on %27Terminal starting%27</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Stuck on Tyro Payments</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Tampered State</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Tip of charger stuck in side</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Update Failed</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Wifi Error</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Wont Power Up</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Fautly__c</picklist>
            <values>
                <fullName>DOA</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>No</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Yes</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Return_Condition__c</picklist>
            <values>
                <fullName>Acceptable</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Unacceptable</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Unacceptable_Reasons__c</picklist>
            <values>
                <fullName>Liquid Damage</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Missing Parts</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Missing SIM</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Visible Damage</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>Asset__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Case__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Fault_Category__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Fault_Sub_Category__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Comments__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATED_DATE</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATEDBY_USER</customTabListAdditionalFields>
        <searchResultsAdditionalFields>Asset__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Case__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Fault_Category__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Fault_Sub_Category__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATED_DATE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATEDBY_USER</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>TD001_Case_and_Not_Faulty</fullName>
        <active>true</active>
        <errorConditionFormula>AND( ispickval(Fautly__c , &apos;No&apos;), Len( Case__c) &lt;1)</errorConditionFormula>
        <errorDisplayField>Case__c</errorDisplayField>
        <errorMessage>TD001: Please indicate the Case this terminal was returned from</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>TD002_Return_Condition_Reason</fullName>
        <active>true</active>
        <errorConditionFormula>AND(ispickval( Return_Condition__c,&apos;Unacceptable&apos; ), isblank( Return_Condition_Comments__c ))</errorConditionFormula>
        <errorDisplayField>Return_Condition_Comments__c</errorDisplayField>
        <errorMessage>TD002: Please comment on the return condition if return condition is &apos;Unacceptable&apos;</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
