({
	accountSelection : function(component, event, helper) {
		
		if(event.getParam("value") != null && event.getParam("value") != event.getParam("oldValue")) {
            component.set("v.ineligibleForAmex", false);
            component.set("v.noDetailsFoundAlert", false);
            component.set("v.emailcaseError", false);
            component.set("v.accountWrapper", null);
            component.set("v.eligibleMids", null);
            component.set("v.ineligibleMids", null);
            
			helper.recordSelectionEvent(component, event, helper);
		}
		
	},

	searchObjectChange: function (component, event, helper) {
        var selectedOptionValue = event.getParam("value");
        component.set("v.selectedSearchObject", selectedOptionValue);
        var objectName;
        if(selectedOptionValue == 'Accounts') {
        	component.set("v.selectedSObject", 'Account');
        	objectName = 'Account';
        }else{
        	component.set("v.selectedSObject", 'Merchant_Id__c');
        	objectName = 'Merchant_Id__c';
        }

        var objCompB = component.find('mysearchcomponent');
        
        objCompB.changeObject(objectName);
        component.set("v.accountWrapper", null);
        component.set("v.eligibleMids", null);
        component.set("v.ineligibleMids", null);
        component.set("v.ineligibleForAmex", false);
        component.set("v.noDetailsFoundAlert", false);
        component.set("v.emailcaseError", false);
    },
    
    openEmailDetailsModal : function (component, event, helper) {
        component.set("v.emailContactModal", true);
    },
    
    closeEmailDetailsModal : function (component, event, helper) {
        component.set("v.emailContactModal", false);
        component.set("v.selectedContact", null);
    },
    
    updateSelectedRows : function (component, event, helper) {
        var selectedRows = event.getParam('selectedRows');
        component.set("v.selectedMIDs", selectedRows);
        
    },
    
    contactSelection : function(component, event, helper) {
        
        var selectedItem  = event.currentTarget;
        let id = selectedItem.dataset.id;
        
        let contactList = [...document.getElementsByClassName('contactList')];
		
        contactList.forEach(contact => {
            contact.getAttribute('data-id') != id ? $A.util.removeClass(contact, "current") : $A.util.addClass(contact, "current");
        });
        
        let allContacts = component.get("v.accountWrapper.accountContactDetails");
        let selectedContactObject = allContacts.filter(contact => {return contact.Id === id });
      
        component.set("v.selectedContact", selectedContactObject[0]);
    },
    
    sendEmailToContact : function (component, event, helper) {
        helper.sendAmexEmail(component, event, helper);
    }
})