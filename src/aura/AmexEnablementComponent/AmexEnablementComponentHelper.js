({
    recordSelectionEvent : function(component, event, helper) {
        
        helper.SetSpinnerClass(component, 'slds-show');
        var selectedObject = component.get("v.selectedSearchObject");
        var action
        if(selectedObject == 'Accounts') {
            action = component.get('c.getParentAccountDetails');
            action.setParams({	
                selectedAccountId : event.getParam("value")
            });
        }else{
            action = component.get('c.getAccountOfMID');
            action.setParams({	
                selectedMid : event.getParam("value")
            });
            
        }
        
        
        
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            helper.SetSpinnerClass(component, 'slds-hide');
            if (state === 'SUCCESS') {
                var responseWrapper = JSON.parse(response.getReturnValue());
                component.set("v.accountWrapper", responseWrapper);
                
                let ineligibleMids = [];
                let eligibleMids = [];
                let preSelectedRows = [];
                let selectedMIDs = [];
                
                if(responseWrapper != null) {
                    
                    responseWrapper.accountMidDetails.forEach(elements => {
                        if(!elements.midAmexEligible || elements.amexEnabled) {
                            ineligibleMids.push(elements);
                        }else{
                            eligibleMids.push(elements);
                            selectedMIDs.push(elements);
                            preSelectedRows.push(elements.merchantId);
                            
                        }
                    });
            
            		
                    component.set("v.ineligibleMids", ineligibleMids); 
                	component.set("v.eligibleMids", eligibleMids);
                    component.set("v.selectedRows", preSelectedRows);
                    component.set("v.selectedMIDs", selectedMIDs);
                }
                
                
                if(responseWrapper == null || responseWrapper == '') {
                    component.set("v.noDetailsFoundAlert", true);
                }else if(eligibleMids.length === 0){
                    component.set("v.ineligibleForAmex", true);
                    component.set("v.ineligibleForAmexReason", 'No mid is eligible for amex transaction or all Mids are Amex Enabled');
                }else if(responseWrapper.amexTransactionVolume > 2000000) {
                    component.set("v.ineligibleForAmex", true);
                    component.set("v.ineligibleForAmexReason", 'Amex transaction volume is over 2,000,000');
                }else if(responseWrapper.amexRateMCCCode == null) {
                    component.set("v.ineligibleForAmex", true);
                    component.set("v.ineligibleForAmexReason", 'No MID MCC code qualifies for Amex');
                }else {
                    component.set("v.ineligibleForAmex", false);
                    component.set("v.ineligibleForAmexReason", '');
                }
                
            }
        });
        
        $A.enqueueAction(action); 
        
    },
    
    SetSpinnerClass: function(component, spinnerClass) {
        component.find("Id_spinner").set("v.class" , spinnerClass);
    },
    
    
    sendAmexEmail : function (component, event, helper) {
        helper.SetSpinnerClass(component, 'slds-show');
        
        let mids = component.get('v.selectedMIDs').map(element => { return element.merchantId });
        
        var action = component.get('c.sendAmexInvitation');
        action.setParams({	
            selectedAccountId : component.get("v.accountWrapper").searchedAccount.Id,
            accountName : component.get("v.accountWrapper").searchedAccount.Name,
            selectedContactJSON : JSON.stringify({"ContactData" : component.get("v.selectedContact")}),
            selectedMIDs : JSON.stringify(mids),
            amexTXNVolume : component.get("v.accountWrapper").amexTransactionVolume,
            amexRate : component.get("v.accountWrapper").amexRateCalculated
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            helper.SetSpinnerClass(component, 'slds-hide');
            if (state === 'SUCCESS') {
                if(response.getReturnValue()) {
                    if(response.getReturnValue().startsWith('500')) {
                        window.location.href = '/' + response.getReturnValue();
                    }else {
                        component.set("v.emailcaseError", true);
                        component.set("v.emailcaseErrorMessage", response.getReturnValue());
                        component.set("v.emailContactModal", false);
                    }
                }else{
                    component.set("v.emailcaseError", true);
                    component.set("v.emailcaseErrorMessage", response.getReturnValue());
                    component.set("v.emailContactModal", false);
                }
                
            }else{
                component.set("v.emailcaseError", true);
                component.set("v.emailcaseErrorMessage", response.getReturnValue());
                component.set("v.emailContactModal", false);
            }
            
        });
        
        $A.enqueueAction(action); 
    }
})