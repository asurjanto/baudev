({
    fetchData : function(component, event, helper) {

        
        var action = component.get("c.fetchRiskOpportunity");
        action.setParams({ 
            customMetadataAPIName : component.get("v.customMetadataAPIName")
        });

        //action.setStorable();
        action.setCallback(this, function(response) {
            
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var storeResponse = JSON.parse(response.getReturnValue());
                
                
                component.set("v.data", storeResponse.opportunityData);
                component.set("v.tableName", storeResponse.tableName);
                

                var currentUserId = $A.get("$SObjectType.CurrentUser.Id");
                component.set("v.filteredData", storeResponse.opportunityData.filter(opportunity => opportunity.riskAssessorId == currentUserId));


    
            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted. Please contact your system admin');
            
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Please contact your system admin. Error message: " + errors[0].message);
                    }
                } else {
                    alert("Please contact your system admin. Unknown error");
                }
            }
        });
        $A.enqueueAction(action);

    },

    startAssignToMeFlow : function(component, helper, row) {
        var flow = component.find("flow");
        debugger;
        var inputVariables = [{ name : "recordId", type : "String", value: row.Id }];
        
        helper.SetSpinnerClass(component, 'slds-show');
        flow.startFlow("OP_Assign_to_me_button_functionality", inputVariables);
    },

    sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.data");
        var reverse = sortDirection !== 'asc';
        //sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.data", data);
        },
    sortBy: function (field, reverse, primer) {
        var key = primer ? function(x) {return primer(x[field])} : function(x) {return x[field]};
        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },

    SetSpinnerClass: function(component, spinnerClass) {
        component.find("Id_spinner").set("v.class" , spinnerClass);
    },
})