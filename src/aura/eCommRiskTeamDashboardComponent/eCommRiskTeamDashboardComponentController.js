({
    init: function (component, event, helper) {
        
        helper.fetchData(component, event, helper);
        
    },

    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');

        switch (action.name) {
            case 'Assign_To_Me':
                helper.startAssignToMeFlow(component, helper, row);
                break;
            case 'delete':
                helper.removeBook(cmp, row);
                break;
        }
    },

    

    statusChange : function (component, event, helper) {
        if (event.getParam('status') === "FINISHED_SCREEN") {
            
            location.reload();
            
          //Do something
        }
        helper.SetSpinnerClass(component, 'slds-hide');
      },

      updateColumnSorting: function(component, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        // assign the latest attribute with the sorted column fieldName and sorted direction
        component.set("v.sortedBy", event.getParam("fieldName"));
        component.set("v.sortedDirection", event.getParam("sortDirection"));
        helper.sortData(component, fieldName, sortDirection);
        }

})