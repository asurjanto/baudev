({
	
    checkboxTicked : function(component, event, helper) {
        
        if(event.getSource().get("v.checked")) {
            if(event.getSource().get("v.name") == 'inputcheck1') {
                component.find("inCorrectAnswer").set("v.checked", false);
            }else{
                component.find("correctAnswer").set("v.checked", false);
            }
        }
        
        
    },
  

    navigateToPage : function(component, event, helper) {
		var buttonName = event.getSource().get("v.title");
		var cmpEvent = component.getEvent("navigationEvent");    

        if(helper.validateInputField(component, "myinputhigh") && helper.validateCheckbox(component)) {

            if(component.find("correctAnswer").get("v.checked") && component.get("v.maxWrongAttempts") >= 0) {
                component.set("v.ultraPointsEarned", component.get("v.ultraPointsEarned") + component.get("v.questionValue")[0].Score_Points__c);
                
            }

            component.get("v.answerRecord").Answer__c = component.get("v.inputValue");
            component.get("v.answerRecord").Is_Correct_Answer__c = component.find("correctAnswer").get("v.checked") ? true : false;
            component.get("v.answerRecord").Question__c = component.get("v.questionValue")[0].Question__c;
   
            cmpEvent.setParams({
                "severity" : component.get("v.severity"),
                "currentPage" : component.get("v.currentPage"),
                "pointsEarned" : component.get("v.questionValue")[0].Score_Points__c,
                "actionType" : buttonName,
                "answerGiven" : component.get("v.inputValue"),
                "validation" : component.find("correctAnswer").get("v.checked") ? true : false,
                "isSkipped" : false
            });

            cmpEvent.fire();

            component.set("v.inputValue", null);
            component.set("v.correctAnswer", false);
            component.find("correctAnswer").set("v.checked", false);
            component.find("inCorrectAnswer").set("v.checked", false);
            component.set("v.noCheckboxSelected", false);

        }else{
            if(Array.isArray(component.find("myinputhigh"))) {
                component.find("myinputhigh")[0].showHelpMessageIfInvalid();
            }else{
                component.find("myinputhigh").showHelpMessageIfInvalid();
            }
            
        }

        
	},

    skipQuestion : function(component, event, helper) {
        var buttonName = event.getSource().get("v.title");
        var cmpEvent = component.getEvent("navigationEvent");
        component.get("v.answerRecord").Question__c = component.get("v.questionValue")[0].Question__c;
        cmpEvent.setParams({
                "severity" : component.get("v.severity"),
                "currentPage" : component.get("v.currentPage"),
                "pointsEarned" : component.get("v.ultraPointsEarned"),
                "actionType" : buttonName,
                "validation" : true,
                "isSkipped" : true
            });
        cmpEvent.fire();
        component.set("v.inputValue", "");
        component.find("correctAnswer").set("v.checked", false);
        component.find("inCorrectAnswer").set("v.checked", false);
        component.set("v.noCheckboxSelected", false);
    }


})