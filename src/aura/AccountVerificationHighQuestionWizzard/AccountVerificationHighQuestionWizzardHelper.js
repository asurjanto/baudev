({
	validateInputField : function(component, fieldName) {

		var validity;
		if(Array.isArray(component.find(fieldName))) {
            validity = component.find(fieldName)[0].get("v.validity");
        }else{
            validity = component.find(fieldName).get("v.validity");
        }
		return validity.valid;
	},

	validateCheckbox : function(component) {

		if(component.find("correctAnswer").get("v.checked") || component.find("inCorrectAnswer").get("v.checked")) {
			component.set("v.noCheckboxSelected", false);
			return true;
		}else{
			component.set("v.noCheckboxSelected", true);
			return false;		
		}
		
	},
})