({

    GenerateLogRecord : function(component, event, helper) {
       
        component.find("logRecordCreator").getNewRecord(
            "Account_Identification_Log__c", // sObject type (objectApiName)
            null,      // recordTypeId
            false,     // skip cache?
            $A.getCallback(function() {
                var rec = component.get("v.logRecord");
                var error = component.get("v.logRecordError");
                if(error || (rec === null)) {
                    console.log("Error initializing record template: " + error);
                    return;
                }
                
            }) 
        );
    },

    fetchHistoryData: function (component, event, helper) {
        
        var action = component.get("c.fetchLogHistory");
        action.setParams({ 
                accountId : component.get("v.recordId")
            });

        action.setCallback(this, function(response) {
            
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var storeResponse = JSON.parse(response.getReturnValue());
                
                storeResponse.forEach((obj, index) => {
                  storeResponse[index].CreatedDate = String(Date.parse(obj.CreatedDate));
                });
                
            
                component.set("v.logHistoryonAccount", storeResponse);

        
                    
            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted. Please contact your system admin');
            
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Please contact your system admin. Error message: " + errors[0].message);
                    }
                } else {
                    alert("Please contact your system admin. Unknown error");
                }
            }
        });
        $A.enqueueAction(action);

    },


    generateAnswerRecord : function(component, helper) {

        component.find("answerRecordCreator").getNewRecord(
            "Id_Verification_Answer__c", // sObject type (objectApiName)
            null,      // recordTypeId
            false,     // skip cache?
            $A.getCallback(function() {

                var rec = component.get("v.answerRecord");
                var error = component.get("v.answerRecordError");
                if(error || (rec === null)) {
                    console.log("Error initializing record template: " + error);
                    return;
                }
                console.log("Record template initialized: " + rec.sobjectType);

            })
        );
    },

    reloadLogRecord : function(component, event, helper) {

    
        component.find("logRecordCreator").reloadRecord(true, $A.getCallback(function() {
            component.set("v.isSuspicion", false);
            helper.SetSpinnerClass(component, 'slds-hide');

            //Send email after success save of suspicion record     
            helper.sendEmailToRisk(component,event, helper);

        }));
    },

    RetrieveQuestionsFromServer : function(component, event, helper) {

        var action = component.get("c.fetchAllQuestions");
        action.setStorable();
        action.setCallback(this, function(response) {
            
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var storeResponse = JSON.parse(response.getReturnValue());
                
                component.set("v.listOfOptions", storeResponse.optionsList);
                component.set("v.listOfQuestion", storeResponse.questionList);
                    
            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted. Please contact your system admin');
            
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Please contact your system admin. Error message: " + errors[0].message);
                    }
                } else {
                    alert("Please contact your system admin. Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    RenderVerificationTile: function(component, event, helper) {

        
        var staticMap = new Map([["low", 0], ["medium", 1], ["high", 2], ["ultra", 2]]);


        var typeSelection = event.getParam("selectedSeverity");

        if(component.get("v.logRecordFields").Options_Selected__c == null) {
            component.get("v.logRecordFields").Options_Selected__c = event.getParam("selectedLabel") + ';';
        }else{
            component.get("v.logRecordFields").Options_Selected__c += event.getParam("selectedLabel") + ';';
        }

        
        this.createUpdateLogRecord(component, helper);

        component.set("v.selectedOption", event.getParam("selectedLabel"));
        
        component.set("v.verificationType", typeSelection);
        component.set("v.questionSubset", null);

        var stateMachine = component.get("v.finiteStateMachine");
        
        if(staticMap.get(typeSelection) <= staticMap.get(stateMachine.passLevel)) {
            this.openModalWithHeaderAndBody(component, $A.get("$Label.c.Success_modal_header"), $A.get("$Label.c.Success_modal_body"), true);
        }

        else if(stateMachine.history.indexOf('high fail') != -1) {
            this.openModalWithHeaderAndBody(component, $A.get("$Label.c.Failure_modal_header"), $A.get("$Label.c.Failure_modal_body"), false);      
        }

        else if(stateMachine.history.indexOf('low fail') != -1 && stateMachine.history.indexOf('medium fail') != -1) {
            this.openModalWithHeaderAndBody(component, $A.get("$Label.c.Failure_modal_header"), $A.get("$Label.c.Failure_modal_body"), false);
            //this.sendEmailToRisk(component, event, helper, false);
        }

        else if(stateMachine.history.indexOf(typeSelection + ' fail') != -1) {
            this.openModalWithHeaderAndBody(component, $A.get("$Label.c.Failure_modal_header"), $A.get("$Label.c.Failure_modal_body"), false);
            //this.sendEmailToRisk(component, event, helper, false);
        }

        else if(stateMachine.history.indexOf(typeSelection + ' pass') != -1) {
            this.openModalWithHeaderAndBody(component, $A.get("$Label.c.Success_modal_header"), $A.get("$Label.c.Success_modal_body"), true);
        }

        else if(typeSelection == 'medium') {
            if(stateMachine.history.indexOf('medium questions') == -1) {
                stateMachine.medium();
                component.set("v.finiteStateMachine", stateMachine);
                this.RetrieveQuestionsFromList(component, event, helper, 'pre medium', 0);
            }
        }

        else if(typeSelection == 'high') {

            if(stateMachine.history.indexOf('high questions') == -1) {
                stateMachine.high();
                component.set("v.finiteStateMachine", stateMachine);
                if(component.get("v.logRecordFields").Full_Name__c == null) {
                    this.RetrieveQuestionsFromList(component, event, helper, 'pre high', 0);
                }else{
                    this.RetrieveQuestionsFromList(component, event, helper, 'high', 0);
                }
                
            }
            
        }
        
        else {
            var machineFunction = typeSelection.toLowerCase();
            stateMachine[machineFunction]();
            component.set("v.finiteStateMachine", stateMachine);
            this.RetrieveQuestionsFromList(component, event, helper, typeSelection);
        }
        

    },

    RetrieveQuestionsFromList : function (component, event, helper, typeSelection, pointsEarned) {
        
        helper.SetSpinnerClass(component, 'slds-show'); 
        
        var stateMachine = component.get("v.finiteStateMachine");
        var storeResponse = component.get("v.listOfQuestion");
        
        var filteredQuestion = storeResponse.filter(function (i,n){
                        return i.Severity_Level__c === typeSelection;
        });

        if(typeSelection == 'ultra') {
            if(component.get("v.allowedSkips") < 0) {
                stateMachine.fail();
                component.set("v.finiteStateMachine", stateMachine);
                this.updateValidationStatus(component, this, 'High Fail');
                this.openModalWithHeaderAndBody(component, $A.get("$Label.c.Failure_modal_header"), $A.get("$Label.c.Failure_modal_body") , false);
                
            }

            if(pointsEarned >= component.get("v.pointsRequired") && component.get("v.highQuestionsWrongAllowed") >= 0) {
                stateMachine.pass();
                stateMachine.setPassLevel('high');
                this.updateValidationStatus(component, this, 'High Pass');
                this.openModalWithHeaderAndBody(component, $A.get("$Label.c.Success_modal_header"), $A.get("$Label.c.Success_modal_body"), true);
                
            }else if(filteredQuestion.length == 0) {

                stateMachine.fail();
                component.set("v.finiteStateMachine", stateMachine);
                this.updateValidationStatus(component, this, 'High Fail');
                this.openModalWithHeaderAndBody(component, $A.get("$Label.c.Failure_modal_header"), $A.get("$Label.c.Failure_modal_body") , false);
            }else{
                var randomNumber = Math.floor(Math.random() * filteredQuestion.length);
                component.set("v.questionSubset", filteredQuestion[randomNumber]);
                filteredQuestion = [filteredQuestion[randomNumber]];
                filteredQuestion.forEach(f => storeResponse.splice(storeResponse.findIndex(e => e.DeveloperName === f.DeveloperName),1));
                component.set("v.listOfQuestion", storeResponse);
            }
  

        }else{

            if(filteredQuestion.length == 0) {
                if(typeSelection == 'high') {
                    
                    filteredQuestion = storeResponse.filter(function (i,n){
                            return i.Severity_Level__c === 'ultra';
                        });

                    component.set("v.questionSubset", filteredQuestion[0]);
                    filteredQuestion = [filteredQuestion[0]];
                    filteredQuestion.forEach(f => storeResponse.splice(storeResponse.findIndex(e => e.DeveloperName === f.DeveloperName),1));
                    
                    component.set("v.listOfQuestion", storeResponse);
                    component.set("v.verificationType", 'ultra');
                    //Set Logic for Points Required
                    if(!component.get("v.DOBValidation")) {

                        stateMachine.fail();
                        component.set("v.finiteStateMachine", stateMachine);
                        this.updateValidationStatus(component, this, 'High Fail');
                        this.openModalWithHeaderAndBody(component, $A.get("$Label.c.Failure_modal_header"), $A.get("$Label.c.Failure_modal_body"), false);

                    }else if(component.get("v.DOBValidation") && !component.get("v.contactValidation")) {
                        component.set("v.pointsRequired", 150);
                    }else{
                        component.set("v.pointsRequired", 100);
                    }
                }else{
                    
                    if(component.get("v.validationState")) {                     
                        stateMachine.pass();
                        stateMachine.setPassLevel(typeSelection.toLowerCase());
                        component.set("v.finiteStateMachine", stateMachine);
                        this.updateValidationStatus(component, this, typeSelection.charAt(0).toUpperCase() + typeSelection.substr(1) + ' Pass');
                        this.openModalWithHeaderAndBody(component, $A.get("$Label.c.Success_modal_header"), $A.get("$Label.c.Success_modal_body"), true);
                    }else{
                        stateMachine.fail();
                        component.set("v.finiteStateMachine", stateMachine);
                        this.updateValidationStatus(component, this, typeSelection.charAt(0).toUpperCase() + typeSelection.substr(1) + ' Fail');
                        this.openModalWithHeaderAndBody(component, $A.get("$Label.c.Failure_modal_header"), $A.get("$Label.c.Failure_modal_body"), false);

                    }
                    
                }
                

            }else{
                filteredQuestion = [filteredQuestion[0]];
                //Removing the filtered element from the Main List
                filteredQuestion.forEach(f => storeResponse.splice(storeResponse.findIndex(e => e.DeveloperName === f.DeveloperName),1));
                component.set("v.listOfQuestion", storeResponse);
                component.set("v.questionSubset", filteredQuestion[0]);
            }
        }
        helper.SetSpinnerClass(component, 'slds-hide');     
    },

    HandleNavigation: function(component, event, helper) {
        var typeSelection = event.getParam("severity");
        
        var actionType = event.getParam("actionType"); 
        var pointsEarned = event.getParam("pointsEarned");
        var validationStatus = event.getParam("validation");
        var questionName = event.getParam("questionName");
        var isSkipped = event.getParam("isSkipped");
        var answerResponse = event.getParam("answerGiven");

        var stateMachine = component.get("v.finiteStateMachine");
        var twoHighQuestionsWrong = false;
        
        

        if(actionType == 'Skip'){
                
            component.set("v.allowedSkips", component.get("v.allowedSkips") - 1);

        }

        if(component.get("v.validationState") && !isSkipped) {
            component.set("v.validationState", validationStatus); 
        }


        if(typeSelection == 'high' && questionName.includes('DOB')) {
            component.set("v.DOBValidation", validationStatus);
        }else if(typeSelection == 'high' && questionName.includes('Contact Number')) {
            component.set("v.contactValidation" , validationStatus);
        }

        if(typeSelection == 'ultra' && actionType != 'Skip') {

            var wrongAllowed = component.get("v.highQuestionsWrongAllowed");
            
            if(validationStatus && wrongAllowed >= 0 ) {
                debugger;
                component.set("v.totalPointsEarned", component.get("v.totalPointsEarned") + pointsEarned);
            }else{
                component.set("v.highQuestionsWrongAllowed", wrongAllowed - 1);
            }
            
        }
        
    
        component.get("v.answerRecordFields").Skipped__c = isSkipped ? true : false;
        component.get("v.answerRecordFields").Severity_Level__c = (typeSelection == 'ultra' ? 'high' : typeSelection);
        component.get("v.answerRecordFields").Points__c = parseInt(pointsEarned);
        component.get("v.answerRecordFields").Is_Correct_Answer__c = isSkipped ? false : validationStatus;
        component.get("v.answerRecordFields").Account_Identification_Log__c = component.get("v.logRecordId");
        component.get("v.answerRecordFields").Question_Type__c = component.get("v.selectedOption");
        
        this.createAnswerRecord(component, helper);

        //NEXT BUTTON IS NOT WORKING . CHOWN -> ENTER QUEST -> ANSWER - > SENCOND QUESTION -> NOW IT IS NOT COMING
        if( stateMachine.state.includes('pre medium')) {
            if(validationStatus) {
                stateMachine.pass();
                component.get("v.logRecordFields").Full_Name__c = answerResponse;
                this.createUpdateLogRecord(component, helper);
                this.RetrieveQuestionsFromList(component, event, helper, 'medium', component.get("v.totalPointsEarned"));
            }else{
                stateMachine.fail();
                this.updateValidationStatus(component, this, 'Medium Fail');
                this.openModalWithHeaderAndBody(component, $A.get("$Label.c.Failure_modal_header"), $A.get("$Label.c.Failure_modal_body"), false);

            }
        }else if(stateMachine.state.includes('pre high')) {
            if(validationStatus) {
                stateMachine.pass();
                component.get("v.logRecordFields").Full_Name__c = answerResponse;
                this.createUpdateLogRecord(component, helper);
                this.RetrieveQuestionsFromList(component, event, helper, 'high', component.get("v.totalPointsEarned"));
            }else{
                stateMachine.fail();
                this.updateValidationStatus(component, this, 'High Fail');
                this.openModalWithHeaderAndBody(component, $A.get("$Label.c.Failure_modal_header"), $A.get("$Label.c.Failure_modal_body"), false);
            }
        }else{
            this.RetrieveQuestionsFromList(component, event, helper, typeSelection, component.get("v.totalPointsEarned"));
        }

    },

    createAnswerRecord : function(component, helper) {

        
        
        component.find("answerRecordCreator").saveRecord($A.getCallback(function(saveResult) {
            
            helper.generateAnswerRecord(component, helper);
            
            // NOTE: If you want a specific behavior(an action or UI behavior) when this action is successful 
            // then handle that in a callback (generic logic when record is changed should be handled in recordUpdated event handler)
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                // handle component related logic in event handler
            } else if (saveResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            }
        }));

        //this.generateAnswerRecord(component, helper);

    },

    createUpdateLogRecord : function(component, helper) {


        component.get("v.logRecordFields").Account__c = component.get("v.recordId");
        component.get("v.logRecordFields").Case_Support_Call__c = component.get("v.supportCaseId");
        component.find("logRecordCreator").saveRecord($A.getCallback(function(saveResult) {

            
            // NOTE: If you want a specific behavior(an action or UI behavior) when this action is successful 
            // then handle that in a callback (generic logic when record is changed should be handled in recordUpdated event handler)
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                component.set("v.logRecordId", saveResult.recordId);
                helper.SetSpinnerClass(component, 'slds-hide'); 
            } else if (saveResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            }
        }));
    }, 

    SetSpinnerClass: function(component, spinnerClass) {
    	component.find("Id_spinner").set("v.class" , spinnerClass);
	},

    updateValidationStatus : function(component, helper, passFailLevel) {
        component.get("v.logRecordFields").Validation_Status__c = passFailLevel;
        helper.createUpdateLogRecord(component, helper);
    },

    closeModalAndReset: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        //this.GenerateLogRecord(component, event, helper);

        var currentVerificationName = component.get("v.verificationType");
        var currentVerificationStatus = component.set("v.validationState", true);

        var verificationsAvailable = component.get("v.verificationsAvailable");


        component.set("v.isOpen", false);
        component.set("v.verificationType", null);
        
        //component.set("v.validationState", true);
        component.set("v.DOBValidation", false);
        component.set("v.contactValidation", false);
        
    },

    openModalWithHeaderAndBody : function (component, modalHeader, modalBody, isSuccess) {
        component.set("v.isOpen", true);
        component.set("v.validationModalHeader", modalHeader);
        component.set("v.validationModalBody", modalBody);
        if(isSuccess) {
            component.set('v.ModalStyle', 'success-modal');

        }else{
            component.set('v.ModalStyle', 'failure-modal');
        }
    },

    addArticleToCase : function (component, event, helper) {
            helper.SetSpinnerClass(component, 'slds-show');
            var action = component.get("c.attachArticleToCase");
            action.setParams({ 
                caseId : component.get("v.supportCaseId"),
                articleId : component.get("v.knowledgeArticleId")
                
            });

            action.setCallback(this, function(response) {
                

                helper.SetSpinnerClass(component, 'slds-hide');
                var state = response.getState();
                
                if (state === "SUCCESS") {
                    
                    if(response.getReturnValue().includes('Success')) {
                            
                           component.set("v.articleAttachedSuccessfully", true);
                           component.set("v.articleAlreadyPresent", false);
                           component.set("v.isArticleAttached", true);
                            
                        
                    }else{
                            component.set("v.articleAttachedSuccessfully", false);
                            component.set("v.articleAlreadyPresent", true);
                            
                        
                    }
    
                }else if (state === "INCOMPLETE") {
                    alert('Response is Incompleted. Please contact your system admin');
                
                }else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            alert("Please contact your system admin. Error message: " + errors[0].message);
                        }
                    } else {
                        alert("Please contact your system admin. Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
    },

    sendEmailToRisk : function (component, event, helper) {

        if(!component.get("v.suspicionEmailSent")) {
                        
            var action = component.get("c.sendEmailNotificationToRisk");
            action.setParams({ 
                accountId : component.get("v.recordId"),
                logRecordId : component.get("v.logRecordId")
                
            });

            action.setCallback(this, function(response) {
                
                
                var state = response.getState();
                
                if (state === "SUCCESS") {
                    
                    component.set("v.suspicionEmailSent", true);
                    
                        
                }else if (state === "INCOMPLETE") {
                    alert('Response is Incompleted. Please contact your system admin');
                
                }else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            alert("Please contact your system admin. Error message: " + errors[0].message);
                        }
                    } else {
                        alert("Please contact your system admin. Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);

        }

    }

})