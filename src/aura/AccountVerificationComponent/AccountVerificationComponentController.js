({

    doInit : function(component, event, helper) {
        helper.GenerateLogRecord(component, event, helper);
        helper.generateAnswerRecord(component, event, helper);
        helper.RetrieveQuestionsFromServer(component, event, helper);
        helper.fetchHistoryData(component, event, helper); 
    },

    closeSessionWindow : function(component, event, helper) {
        window.location.href = '/' + component.get("v.supportCaseId");
    },

    populateLogHistory : function(component, event, helper) {
      
    },

    showStateMachine : function(component, event, helper) {
      
      
   
      var fsm = new StateMachine({
        init: 'Start',
        transitions: [
          { name: 'low', from: 'Start',   to: 'low questions' },
          { name: 'medium', from: 'Start',   to: 'pre medium questions' },
          { name: 'high', from: 'Start',   to: 'pre high questions' },
          { name: 'pass', from: 'low questions',   to: 'low pass' },
          { name: 'fail', from: 'low questions',   to: 'low fail' },
          { name: 'medium', from: 'low pass',   to: 'pre medium questions' },
          { name: 'medium', from: 'low fail',   to: 'pre medium questions' },
          { name: 'pass', from: 'pre medium questions',   to: 'medium questions' },
          { name: 'fail', from: 'pre medium questions',   to: 'medium fail' },
          { name: 'pass', from: 'medium questions',   to: 'medium pass' },
          { name: 'fail', from: 'medium questions',   to: 'medium fail' },
          { name: 'low', from: 'medium fail',   to: 'low questions'},
          { name: 'high', from: 'low pass',   to: 'pre high questions' },
          { name: 'high', from: 'low fail',   to: 'pre high questions' },
          { name: 'high', from: 'medium pass',   to: 'pre high questions' },
          { name: 'high', from: 'medium fail',   to: 'pre high questions' },
          { name: 'pass', from: 'pre high questions',   to: 'high questions' },
          { name: 'fail', from: 'pre high questions',   to: 'high fail' },
          { name: 'pass', from: 'high questions',   to: 'high pass' },
          { name: 'fail', from: 'high questions',   to: 'high fail' },
        ],
        plugins: [
          new StateMachineHistory()     //  <-- plugin enabled here
        ],
        data: {
          passLevel : 'none',
          staticMap : new Map([['none', -1], ["low", 0], ["medium", 1], ["high", 2], ["ultra", 2]])
        },
        methods: {
          setPassLevel: function(level) {
            if(this.staticMap.get(level) > this.staticMap.get(this.passLevel)) {
              this.passLevel = level;
            }
          }
        }
      });


      component.set("v.finiteStateMachine", fsm);
      //Do this in console to get the .DOT file, and then see the visualization using this
      //sotware link .https://graphs.grevian.org
      StateMachineVisualize(fsm);

    },

    closeSupportCase : function(component, event, helper) {

      helper.SetSpinnerClass(component, 'slds-show');
        component.find('input_hidden_merchantid').set('v.value', component.get("v.caseMerchantId"));
        component.find('input_hidden_status').set('v.value', 'Closed');

        component.find('supportCaseGenerator').submit();
        //window.location.href = '/' + component.get("v.supportCaseId");

    },

    saveSupportCase : function(component, event, helper) {
          

          if(component.get("v.supportTicketSubject") == '') {
            component.find("caseSubject").showHelpMessageIfInvalid();
          }else if(component.get("v.supportTickedCallerName") == ''){
            component.find("callerName").showHelpMessageIfInvalid();
          }
          else{

            helper.SetSpinnerClass(component, 'slds-show');
            
            
            component.find('input_hidden_callerName').set('v.value', component.get("v.supportTickedCallerName"));
            component.find('input_hidden_account').set('v.value', component.get("v.recordId"));
            component.find('input_hidden_subject').set('v.value', component.get("v.supportTicketSubject"));
            component.find('input_hidden_merchantid').set('v.value', component.get("v.caseMerchantId"));
            component.find('input_hidden_contact').set('v.value', component.get("v.caseContactId"));
            component.find('input_hidden_reason').set('v.value', 'Log Inbound Call');
            component.find('input_hidden_status').set('v.value', 'New');
            

            component.find('supportCaseGenerator').submit();
            
          } 

    },

    supportCaseGenerated : function(component, event, helper) {
      
      
        component.set("v.isSupportTicketCreated", true);
        component.set("v.supportCaseId", event.getParams().response.id);
        
        if(event.getParams().response.fields.Status.value === 'Closed') {
          window.location.href = '/' + component.get("v.supportCaseId");
        }else{
          helper.createUpdateLogRecord(component, helper);
          helper.SetSpinnerClass(component, 'slds-hide');
        }
        
      

    },

    supportCaseError : function(component, event, helper) {
      helper.SetSpinnerClass(component, 'slds-hide');
    },

    openSuspicionModal : function(component, event, helper) {
      component.set("v.isSuspicion", true);
    },

    closeSuspicionModel : function(component, event, helper) {
      component.set("v.isSuspicion", false);
    },

    attachKnowledgeArticle : function(component, event, helper) {
      helper.addArticleToCase(component, event, helper);
    },

    updateLogRecordToBeSuspicious : function(component, event, helper) {

      helper.SetSpinnerClass(component, 'slds-show');
      if(component.get("v.logRecordFields.Id") != null) {  
        component.find('idLogGenerator').set("v.recordId", component.get("v.logRecordFields.Id"));
      }
       
      component.find('idLogGenerator').submit();

      
    },

    populateLogRecord : function(component, event, helper) {

      component.set("v.logRecordId", event.getParams().response.id);
      helper.reloadLogRecord(component, event, helper);   

    },


    displayQuestions : function(component, event, helper) {
        helper.RenderVerificationTile(component, event, helper);
    },

    navigateToNewPage : function(component, event, helper) {
      
        helper.HandleNavigation(component, event, helper);
    },

    closeModal: function(component, event, helper) {

      helper.closeModalAndReset(component, event, helper);


   }
    
})