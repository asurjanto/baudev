({
	fireComponentEvent : function(component, event, handler) {
        //change how the event is fired

        var cmpEvent = component.getEvent("severitySelectEvent");
        cmpEvent.setParams({
            "selectedSeverity" : event.getSource().get("v.value").toLowerCase(),
            "selectedLabel" : event.getSource().get("v.label")
        });
        cmpEvent.fire();
    }
})