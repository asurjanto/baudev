({
    
    fireUserSelectEvent : function(component, event, helper) {
        var compEvent = component.getEvent("userSelectEvent");
        
        compEvent.setParams({
            "selectedUserOrPOSId" : component.get("v.resourceId"),
            "componentIndex" : component.get("v.currentIndex"),
            "selectedUserOrPOSName" : component.get("v.resourceName"),
            "isActive" : component.get("v.resourceStatus")
        });
        
        compEvent.fire();

        var cardComponent = component.find("user-card");
        $A.util.addClass(cardComponent, "resource-select_active");
        $A.util.removeClass(cardComponent, "resource-select");
    },

    

    openFilteredReport : function (component, event, helper) {
        
        if(component.get("v.currentView") == 'amViewRequest') {
            //window.open('https://tyro--baudev.cs87.my.salesforce.com/00O8E000000TdqQ?pv0=' + component.get("v.resourceName") + '&pv1=' + event.target.className, '_blank');
        }else{
            //window.open('https://tyro--baudev.cs87.my.salesforce.com/00O8E000000TdqQ?pv0=' + component.get("v.resourceName") + '&pv1=' + event.target.className, '_blank');
        }

        //urlEvent.fire();
    }
})