({
	myAction : function(component, event, helper) {
	
	},

	checkboxTicked : function(component, event, helper) {
		
		if(event.getSource().get("v.checked")) {
			if(event.getSource().get("v.name") == 'inputcheck1') {
				component.find("inCorrectAnswer").set("v.checked", false);
			}else{
				component.find("correctAnswer").set("v.checked", false);
			}
		}
		
		
	},
 
	navigateToPage : function(component, event, helper) {

		var buttonName = event.getSource().get("v.title");
		var cmpEvent = component.getEvent("navigationEvent");



		if(helper.validateInputField(component, "myinput") && helper.validateCheckbox(component)) {

    		//if(component.get("v.questionValue")[0].Response_Type__c === 'Number') {
			//component.get("v.answerRecord").Answer__c = parseInt(component.get("v.inputValue"));
			
			//}else{
				
				
			//}

			//Phone attempt 2nd
			if(component.get("v.inputValuePhone2")) {
				component.get("v.answerRecord").Answer__c = component.get("v.logRecord").Answer__c + ';' + component.get("v.inputValuePhone2");
			}
			
			component.get("v.answerRecord").Is_Correct_Answer__c = component.find("correctAnswer").get("v.checked") ? true : false;
			component.get("v.answerRecord").Question__c = component.get("v.questionValue")[0].Question__c;
			component.get("v.answerRecord").Answer__c = component.get("v.inputValue");

	        cmpEvent.setParams({
	            "severity" : component.get("v.severity"),
	            "currentPage" : component.get("v.currentPage"),
	            "actionType" : buttonName,
	            "answerGiven" : component.get("v.inputValue"),
	            "questionName" : component.get("v.questionValue")[0].Label,
	            "validation" : component.find("correctAnswer").get("v.checked") ? true : false
	        });
	        
	        
	        component.set("v.inputValue", null);
			component.find("correctAnswer").set("v.checked", false);
			component.find("inCorrectAnswer").set("v.checked", false);
			component.set("v.noCheckboxSelected", false);
			cmpEvent.fire();

    	}else{
    		
    		if(Array.isArray(component.find("myinput"))) {
            	component.find("myinput")[0].showHelpMessageIfInvalid();
	        }else{
	            component.find("myinput").showHelpMessageIfInvalid();
	        }
    	}

		
	}


})