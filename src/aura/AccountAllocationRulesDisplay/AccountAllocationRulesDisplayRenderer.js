({
	rerender : function(component, helper){
        this.superRerender();
        helper.makeTable(component, helper);
    }
})