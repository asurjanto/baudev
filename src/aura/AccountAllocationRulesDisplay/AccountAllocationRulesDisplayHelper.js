({
	makeTable : function(component, helper) {
		var viewRequested = component.get("v.viewRequested");
		if(viewRequested === 'amViewRequest') {
			var isEditable = component.get("v.selectedUserPOSId") == null ? false : true;
			var tableJSON = [
	                            { label: 'Name', fieldName: 'posName', type: 'text' },
	                            { label: 'Micro 1', fieldName: 'Micro_1', type: 'boolean', editable: isEditable },
	                            { label: 'Micro 2', fieldName: 'Micro_2', type: 'boolean', editable: isEditable },
	                            { label: 'Small 1', fieldName: 'Small_1', type: 'boolean', editable: isEditable },
	                            { label: 'Small 2', fieldName: 'Small_2', type: 'boolean', editable: isEditable },
	                            { label: 'Medium 1', fieldName: 'Medium_1', type: 'boolean', editable: isEditable },
	                            { label: 'Medium 2', fieldName: 'Medium_2', type: 'boolean', editable: isEditable },
	                            { label: 'Large', fieldName: 'Large', type: 'boolean', editable: isEditable },
	                            { label: 'X Large', fieldName: 'X_Large', type: 'boolean', editable: isEditable }
							]
			component.set("v.tableHeader", tableJSON);
             
		}else{
			var isEditable = component.get("v.selectedUserPOSId") == null ? false : true;
			var tableJSON = [
	                            { label: 'Name', fieldName: 'accountManagerName', type: 'text' },
	                            { label: 'Micro 1', fieldName: 'Micro_1', type: 'boolean', editable: isEditable },
	                            { label: 'Micro 2', fieldName: 'Micro_2', type: 'boolean', editable: isEditable },
	                            { label: 'Small 1', fieldName: 'Small_1', type: 'boolean', editable: isEditable },
	                            { label: 'Small 2', fieldName: 'Small_2', type: 'boolean', editable: isEditable },
	                            { label: 'Medium 1', fieldName: 'Medium_1', type: 'boolean', editable: isEditable },
	                            { label: 'Medium 2', fieldName: 'Medium_2', type: 'boolean', editable: isEditable },
	                            { label: 'Large', fieldName: 'Large', type: 'boolean', editable: isEditable },
	                            { label: 'X Large', fieldName: 'X_Large', type: 'boolean', editable: isEditable }
							]
			component.set("v.tableHeader", tableJSON);
            
		}
	}
})