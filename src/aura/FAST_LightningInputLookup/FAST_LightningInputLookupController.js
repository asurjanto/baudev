/*
 * Author: Enrico Murru (http://enree.co, @enreeco)
 */
({
    
    /*
     * Executes the search server-side action when the c:InputLookupEvt is thrown
     */
    handleInputLookupEvt: function(component, event, helper){
        
		helper.searchAction(component, event.getParam('searchString')); 
    },
    
    /*
    	Loads the typeahead component after JS libraries are loaded
    */
    initTypeahead : function(component, event, helper){
        //first load the current value of the lookup field and then
        //creates the typeahead component
        helper.loadFirstValue(component);
    },

    reinitTypehead : function(component, event, helper){
        //first load the current value of the lookup field and then
        //creates the typeahead component
        var params = event.getParam('arguments');
        if (params) {
            var param1 = params.objectName;
            component.set("v.type", param1);
            var elements = document.getElementsByClassName("slds-combobox__input");
            elements[1].value = '';
            component.get("v.value", '');
            helper.loadFirstValue(component);
        }
    }


})