({
    doInit : function(component, event, helper) {
        helper.getAccountManagerList(component, event, helper);  
    },
    
    filterRuleList : function(component, event, helper) {

        
        component.set("v.currentUserOrPOSId", event.getParam("selectedUserOrPOSId"));
        var currentCompIndex = event.getParam("componentIndex");

        var previousCompIndex = component.get("v.previousSelectedUserCard");
        helper.getAllocationRecrods(component, event, helper, event.getParam("selectedUserOrPOSId"));    
        component.set("v.userOrPOSName", event.getParam("selectedUserOrPOSName"));
        component.set("v.isCurrentUserOrPOSActive" , event.getParam("isActive"));
        
        if(previousCompIndex != -1) {
            $A.util.removeClass(component.find('sidebar-comp')[previousCompIndex].find('user-card'), "resource-select_active");
            $A.util.addClass(component.find('sidebar-comp')[previousCompIndex].find('user-card'), "resource-select");
        }

        component.set("v.previousSelectedUserCard", currentCompIndex);
        
    },

    amViewRequest : function(component, event, helper) {
        
        component.set("v.currentView", "amViewRequest");
        component.set("v.previousSelectedUserCard", -1);
        component.set("v.userOrPOSName", null);
        component.set("v.currentUserOrPOSId", null);
        component.set("v.newSelectedSegment", null);
        component.set("v.newIntegrationProductOrUserId", null);
        component.set("v.showNewPOSonUser", false);
        component.set("v.showAlertBox", false);
        
        helper.getAccountManagerList(component, event, helper);
        
    },

    posViewRequest : function(component, event, helper) {

        component.set("v.currentView", "posViewRequest");
        component.set("v.previousSelectedUserCard", -1);
        component.set("v.userOrPOSName", null);
        component.set("v.currentUserOrPOSId", null);
        component.set("v.newSelectedSegment", null);
        component.set("v.newIntegrationProductOrUserId", null);
        component.set("v.showNewPOSonUser", false);
        component.set("v.showAlertBox", false);
        
        helper.getAccountManagerList(component, event, helper);
        

    },

    saveTable : function(component, event, helper) {
        helper.saveTableData(component, event, helper);
    },

    openNewPOSonUserModal : function(component, event, helper) {
        component.set("v.showNewPOSonUser", true);
    },

    closeNewPOSonUserModal : function(component, event, helper) {
        component.set("v.newSelectedSegment", null);
        component.set("v.newIntegrationProductOrUserId", null);
        component.set("v.showNewPOSonUser", false);
        component.set("v.showAlertBox", false);
    },

    openTransferAllModal : function(component, event, helper) { 
        component.set("v.showTransferAllModal", true);
    },

    closeTransferAllModal : function(component, event, helper) {
        component.set("v.showTransferAllModal", false);
        component.set("v.transferUserSelect", null);
    },

    transferRules : function(component, event, helper) {
        if(component.get("v.transferUserSelect") != null ) {
            helper.SetSpinnerClass(component, 'slds-show');
            component.set("v.showTransferAllModal", false);
            component.set("v.isCurrentUserOrPOSActive", true);
            helper.transferRules(component, event, helper);

        }else{
            component.set("v.showTransferAllModal", false);
        }
    },

    openExclusionModal : function(component,event, helper) {
        helper.fetchExcludedAccounts(component,event, helper);
    },

    closeExclusionModal : function(component,event, helper) {
        component.set("v.showExclusionModal", false);
    },

    saveExcludedAccounts : function(component, event, helper) {
        helper.saveExcludedAccount(component, event, helper); 
    },

    deleteExclusionRule : function(component, event, helper) {
        
        helper.deleteExclusionRule(component, event, helper);
    },

    segmentSelected : function(component, event, helper) {
        component.set("v.newSelectedSegment", component.find('select').get('v.value'));
    },

    saveNewRule : function(component, event, helper) {
        if( component.get("v.newSelectedSegment") == null ||  component.get("v.newIntegrationProductOrUserId") == null ) {
            component.set("v.showAlertBox", true);
        }else{
            helper.SetSpinnerClass(component, 'slds-show');
            component.set("v.showAlertBox", false);
            component.set("v.showNewPOSonUser", false);

            helper.upsertNewAllocationRule(component, event, helper);
            //call Helper Method
        }
    }
})