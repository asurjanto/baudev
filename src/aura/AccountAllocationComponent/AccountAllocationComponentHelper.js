({
	
    //AM View Function
    getAccountManagerList : function(component, event, helper) {
        
        helper.SetSpinnerClass(component, 'slds-show');
        var action = component.get("c.fetchManagersORpos");

        action.setParams({ 
                viewRequested : component.get("v.currentView")
        });
        
        //action.setStorable();
        action.setCallback(this, function(response) {
            
            helper.SetSpinnerClass(component, 'slds-hide');
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var storeResponse = JSON.parse(response.getReturnValue());

                component.set("v.AccountManagersList", storeResponse);
                component.set("v.currentUserOrPOSId", storeResponse[0].resourceId);
                component.set("v.userOrPOSName", storeResponse[0].resourceName);
                helper.getAllocationRecrods(component, event, helper);
    
            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted. Please contact your system admin');
            
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Please contact your system admin. Error message: " + errors[0].message);
                    }
                } else {
                    alert("Please contact your system admin. Unknown error");
                }
            }
        });
        $A.enqueueAction(action);


    },
    
    //AM View Function
    getAllocationRecrods : function(component, event, helper) {
		helper.SetSpinnerClass(component, 'slds-show');
        var action = component.get("c.fetchRulesbyPOSorAM");
        var userOrPosId = component.get("v.currentUserOrPOSId");

        action.setParams({ 
                userPOSId   : userOrPosId,
                requestView : component.get("v.currentView")

        });
        
        action.setCallback(this, function(response) {
            
            helper.SetSpinnerClass(component, 'slds-hide');
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var storeResponse = JSON.parse(response.getReturnValue());
                
                
                component.set("v.userAllocationRules", storeResponse);

                if(component.get("v.previousSelectedUserCard") == -1) {
                    $A.util.addClass(component.find('sidebar-comp')[0].find('user-card'), "resource-select_active");
                    component.set("v.previousSelectedUserCard", 0);
                }

 
            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted. Please contact your system admin');
            
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Please contact your system admin. Error message: " + errors[0].message);
                    }
                } else {
                    alert("Please contact your system admin. Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        
        
	},

    saveTableData : function(component, event, helper) {

        helper.SetSpinnerClass(component, 'slds-show');
        var action = component.get("c.saveNewAllocationRules");
        action.setParams({ 
                unsavedTableData : JSON.stringify(event.getParam("unsavedTableData")),
                userOrPosId : component.get("v.currentUserOrPOSId"),
                currentView : component.get("v.currentView")
        });

        action.setCallback(this, function(response) {
            helper.SetSpinnerClass(component, 'slds-hide');
            var state = response.getState();
            if (state === "SUCCESS") {
                

                var storeResponse = JSON.parse(response.getReturnValue());
                component.set("v.userAllocationRules", storeResponse);
                
                
                    
            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted. Please contact your system admin');
            
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Please contact your system admin. Error message: " + errors[0].message);
                    }
                } else {
                    alert("Please contact your system admin. Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    upsertNewAllocationRule : function(component, event, helper) {
        helper.SetSpinnerClass(component, 'slds-show');
        var action = component.get("c.upsertAllocationRule");

        var userId;
        var posId;
        if(component.get("v.currentView") == "amViewRequest") {
            userId = component.get("v.currentUserOrPOSId");
            posId  = component.get("v.newIntegrationProductOrUserId");
        }else{
            posId = component.get("v.currentUserOrPOSId");
            userId  = component.get("v.newIntegrationProductOrUserId");
        }

        action.setParams({ 
                userId : userId,
                posId  : posId,
                viewRequest : component.get("v.currentView"),
                Segment : component.get("v.newSelectedSegment")
                
        });

        action.setCallback(this, function(response) {
            helper.SetSpinnerClass(component, 'slds-hide');
            var state = response.getState();
            if (state === "SUCCESS") {

                component.set("v.newIntegrationProductOrUserId", null);
                component.set("v.newSelectedSegment", null);
                var storeResponse = JSON.parse(response.getReturnValue());
                component.set("v.userAllocationRules",storeResponse);
                    
            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted. Please contact your system admin');
            
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Please contact your system admin. Error message: " + errors[0].message);
                    }
                } else {
                    alert("Please contact your system admin. Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    transferRules : function(component, event, helper) {
        helper.SetSpinnerClass(component, 'slds-show');
        var action = component.get("c.transferRulesToNewUser");
        action.setParams({ 
                oldUserId : component.get("v.currentUserOrPOSId"),
                newUserId : component.get("v.transferUserSelect")
        });

        action.setCallback(this, function(response) {
            helper.SetSpinnerClass(component, 'slds-hide');
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set("v.transferUserSelect", null);
                helper.getAccountManagerList(component, event, helper);

            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted. Please contact your system admin');
            
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Please contact your system admin. Error message: " + errors[0].message);
                    }
                } else {
                    alert("Please contact your system admin. Unknown error");
                }
            }
        });
        $A.enqueueAction(action);

    },

    saveExcludedAccount : function(component, event, helper) {
        helper.SetSpinnerClass(component, 'slds-show');
        var action = component.get("c.saveCustomSetting");
        action.setParams({ 
                selectedAccountId : component.get("v.excludedAccount")
                
        });

        action.setCallback(this, function(response) {
            helper.SetSpinnerClass(component, 'slds-hide');
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set("v.excludedAccount", null);
                helper.SetSpinnerClass(component, 'slds-hide');
                helper.fetchExcludedAccounts(component, event, helper);

            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted. Please contact your system admin');
            
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Please contact your system admin. Error message: " + errors[0].message);
                    }
                } else {
                    alert("Please contact your system admin. Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    deleteExclusionRule : function(component, event, helper) {
        
        var customSettingToDelete = event.getSource().get("v.value");

        helper.SetSpinnerClass(component, 'slds-show');
        var action = component.get("c.deleteCustomSetting");
        action.setParams({ 
                customSettingId : customSettingToDelete
                
        });

        action.setCallback(this, function(response) {
            helper.SetSpinnerClass(component, 'slds-hide');
            var state = response.getState();
            if (state === "SUCCESS") {
                
                helper.SetSpinnerClass(component, 'slds-hide');
                helper.fetchExcludedAccounts(component, event, helper);
                
                

            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted. Please contact your system admin');
            
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Please contact your system admin. Error message: " + errors[0].message);
                    }
                } else {
                    alert("Please contact your system admin. Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    fetchExcludedAccounts : function(component, event, helper) {
        
        helper.SetSpinnerClass(component, 'slds-show');
        var action = component.get("c.getExcludedAccountList");
        

        action.setCallback(this, function(response) {
            helper.SetSpinnerClass(component, 'slds-hide');
            var state = response.getState();
            if (state === "SUCCESS") {
                
                helper.SetSpinnerClass(component, 'slds-hide');
                component.set("v.excludedAccountList", JSON.parse(response.getReturnValue()));
                component.set("v.showExclusionModal", true);
                

            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted. Please contact your system admin');
            
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Please contact your system admin. Error message: " + errors[0].message);
                    }
                } else {
                    alert("Please contact your system admin. Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    SetSpinnerClass: function(component, spinnerClass) {
        component.find("Id_spinner").set("v.class" , spinnerClass);
    },
})