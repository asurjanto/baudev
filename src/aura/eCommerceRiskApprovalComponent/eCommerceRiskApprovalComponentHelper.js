({
    /*
    getRecordStatus : function(component, event, helper) {

        var action = component.get("c.assignApprovalToCurrentUser");
        action.setParams({ 
            recordId : component.get("v.recordId")
        });

        action.setCallback(this, function(response) {
            
            helper.SetSpinnerClass(component, 'slds-hide');
            var state = response.getState();
            
            if (state === "SUCCESS") {

                debugger;
                var returnWrapper = JSON.parse(response.getReturnValue());
                if(!returnWrapper.isSuccess) {
                    component.set('v.showError', true);
                    component.set('v.customErrorMessage', returnWrapper.message + '. Changes made to the record would be saved.');
                }
                    
            }else if (state === "INCOMPLETE") {
                
            
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Please contact your system admin. Error message: " + errors[0].message);
                    }
                } else {
                    alert("Please contact your system admin. Unknown error");
                }
            }
        });
        $A.enqueueAction(action);



    },
    */
    SetSpinnerClass: function(component, spinnerClass) {
    	component.find("Id_spinner").set("v.class" , spinnerClass);
    },
    
    handleApprovalProcess : function(component, event, helper) {

        var action = component.get("c.actionApprovalProcess");
        action.setParams({ 
            recordId : component.get("v.recordId"),
            action : component.find("approveReject").get("v.value") === 'Approved' ? 'Approve' : 'Reject',
            comments : component.find("approveReject").get("v.value") === 'Approved' ? 'Approve' : 'Reject'
        });

        action.setCallback(this, function(response) {
            
            helper.SetSpinnerClass(component, 'slds-hide');
            var state = response.getState();
            
            if (state === "SUCCESS") {

                debugger;
                var returnWrapper = JSON.parse(response.getReturnValue());
                if(returnWrapper.isSuccess) {
                    if(window.location.host.includes('baudev')) {
                        window.parent.postMessage('Close and Refresh', 'https://tyro--baudev.cs87.my.salesforce.com');
                    }else if(window.location.host.includes('stagingfc')) {
                        window.parent.postMessage('Close and Refresh', 'https://tyro--stagingfc.cs110.my.salesforce.com');
                    }else{
                        window.parent.postMessage('Close and Refresh', 'https://tyro.my.salesforce.com');
                    }
                }else{
                    component.set('v.showError', true);
                    component.set('v.customErrorMessage', returnWrapper.message + '. Changes made to the record would be saved.');
                }

                    
            }else if (state === "INCOMPLETE") {
                
            
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Please contact your system admin. Error message: " + errors[0].message);
                    }
                } else {
                    alert("Please contact your system admin. Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})