({
    
    doInit : function (component, event, helper) {
        //helper.getRecordStatus(component, event, helper);
    },

    handleSuccess : function(component, event, helper) {
        helper.handleApprovalProcess(component,event,helper);
    },

    startSpinner : function(component, event, helper) {
        helper.SetSpinnerClass(component, 'slds-show');
    },

    stopSpinner : function(component, event, helper){
        helper.SetSpinnerClass(component, 'slds-hide');
    }
})