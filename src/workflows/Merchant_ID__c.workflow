<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AM_Notification_Customer_Terminated</fullName>
        <description>AM Notification - Customer Terminated</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Merchant_Onboarding_Alerts/AM_Notification_MID_Terminated</template>
    </alerts>
    <alerts>
        <fullName>BePoz_Application_Received_MID</fullName>
        <ccEmails>acu@tyro.com</ccEmails>
        <ccEmails>corporate-team@tyro.com</ccEmails>
        <description>BePoz - Application Received (MID) - NEO-808: DECOMMISSIONED</description>
        <protected>false</protected>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/BePOZ_AM_Handover</template>
    </alerts>
    <alerts>
        <fullName>Best_Practice_Activation_Email</fullName>
        <description>Best Practice - Activation Email - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Activation_BestPractice</template>
    </alerts>
    <alerts>
        <fullName>Best_Practice_Application_Received_MID</fullName>
        <ccEmails>acu@tyro.com</ccEmails>
        <description>Best Practice - Application Received (MID) - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/Bp_Handover_email</template>
    </alerts>
    <alerts>
        <fullName>Best_Practice_Welcome_Email</fullName>
        <description>Best Practice - Welcome Email - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Best_Practice_Welcome_Email_24_06_2015</template>
    </alerts>
    <alerts>
        <fullName>Blue_Chip_Activation_Email</fullName>
        <description>Blue Chip - Activation Email - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Activation_Blue_Chip</template>
    </alerts>
    <alerts>
        <fullName>Blue_Chip_Application_Received_MID</fullName>
        <ccEmails>acu@tyro.com</ccEmails>
        <description>Blue Chip - Application Received (MID) - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/BlueChip_Handover_email</template>
    </alerts>
    <alerts>
        <fullName>Blue_Chip_Welcome_Email</fullName>
        <description>DELETE - Blue Chip - Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Blue_Chip_Welcome_Email_24_06_15</template>
    </alerts>
    <alerts>
        <fullName>CCOS_Activation_Email</fullName>
        <description>CCOS - Activation Email - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Clinical_Computers_Welcome</template>
    </alerts>
    <alerts>
        <fullName>CCOS_Application_Received_MID</fullName>
        <ccEmails>acu@tyro.com</ccEmails>
        <description>CCOS - Application Received (MID) - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/CCOS_Handover_email</template>
    </alerts>
    <alerts>
        <fullName>CCOS_Welcome_Email</fullName>
        <description>DELETE - CCOS - Welcome Email 2</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Clinical_Computers_Activation_17_04_15</template>
    </alerts>
    <alerts>
        <fullName>CCOS_Welcome_Email_1</fullName>
        <description>CCOS - Welcome Email 1 - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/CCOS_Welcome_24_06_15</template>
    </alerts>
    <alerts>
        <fullName>CorePlus_Welcome_Email</fullName>
        <description>CorePlus - Welcome Email - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Coreplus_Welcome_Letter_13_07_2015</template>
    </alerts>
    <alerts>
        <fullName>Fedelta_Application_Received_MID</fullName>
        <ccEmails>acu@tyro.com</ccEmails>
        <description>Fedelta - Application Received (MID) - NEO-808: DECOMMISSIONED</description>
        <protected>false</protected>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Fedelta_AM_Handover</template>
    </alerts>
    <alerts>
        <fullName>Front_Desk_Activation_Email</fullName>
        <description>Front Desk - Activation Email - REDUNDANT (SF-1252)</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Front_Desk_Reconciliation_Guide_25_02_2015</template>
    </alerts>
    <alerts>
        <fullName>Front_Desk_Boarded</fullName>
        <description>Front Desk - Boarded - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Front_Desk_Welcome_Letter_24_06_2015</template>
    </alerts>
    <alerts>
        <fullName>Front_Desk_Welcome_Email</fullName>
        <description>DELETE - Front Desk - Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Front_Desk_Reconciliation_Guide_25_02_2015</template>
    </alerts>
    <alerts>
        <fullName>Generic_Application_Received_MID</fullName>
        <description>Generic - Application Received (MID) - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Retail_Hosp_handover</template>
    </alerts>
    <alerts>
        <fullName>Generic_MID_Status_Change_Alert_Channel</fullName>
        <description>Generic - MID Status Change Alert - Channel - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>Channel_Notification_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Generic_MID_Status_Change_Alert_Channel</template>
    </alerts>
    <alerts>
        <fullName>Generic_MID_Status_Change_Alert_Channel_Internal</fullName>
        <description>Generic - MID Status Change Alert - Channel - Internal</description>
        <protected>false</protected>
        <recipients>
            <field>Channel_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Generic_Internal_Merchant_ID_Status_Update</template>
    </alerts>
    <alerts>
        <fullName>Generic_MID_Status_Change_Alert_POS_Product</fullName>
        <description>Generic - MID Status Change Alert - POS Product - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>POS_Product_Notification_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Generic_MID_Status_Chg_Alert_POS_Product</template>
    </alerts>
    <alerts>
        <fullName>Generic_Pay_Table_Alert</fullName>
        <description>Generic Pay@Table Alert - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>POS_Product_Notification_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Generic_Pay_At_Table_Alert_2</template>
    </alerts>
    <alerts>
        <fullName>Generic_Welcome_Email</fullName>
        <description>Generic - Welcome Email - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/General_EFTPOS_Welcome_24_06_2015</template>
    </alerts>
    <alerts>
        <fullName>Genie_Welcome_Email</fullName>
        <description>Genie - Welcome Email - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Genie_Welcome_25_08_17</template>
    </alerts>
    <alerts>
        <fullName>H_L_ACT_Pay_Table_Alert</fullName>
        <ccEmails>nsw.support@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L ACT Pay@Table Alert - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Generic_Pay_At_Table_Alert_2</template>
    </alerts>
    <alerts>
        <fullName>H_L_MID_Status_Change_Alert_BHG</fullName>
        <ccEmails>gregl@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L - MID Status Change Alert - BHG</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/H_L_MID_Status_Change_Alert_BHG</template>
    </alerts>
    <alerts>
        <fullName>H_L_NSW_Pay_Table_Alert</fullName>
        <ccEmails>nsw.support@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L NSW Pay@Table Alert - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Generic_Pay_At_Table_Alert_2</template>
    </alerts>
    <alerts>
        <fullName>H_L_NT_Pay_Table_Alert</fullName>
        <ccEmails>jasperl@hlaustralia.com.au</ccEmails>
        <ccEmails>tonyc@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L NT Pay@Table Alert - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Generic_Pay_At_Table_Alert_2</template>
    </alerts>
    <alerts>
        <fullName>H_L_QLD_Pay_Table_Alert</fullName>
        <ccEmails>qld.admin@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L QLD Pay@Table Alert - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Generic_Pay_At_Table_Alert_2</template>
    </alerts>
    <alerts>
        <fullName>H_L_SA_Pay_Table_Alert</fullName>
        <ccEmails>jasperl@hlaustralia.com.au</ccEmails>
        <ccEmails>jerryf@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L SA Pay@Table Alert - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Generic_Pay_At_Table_Alert_2</template>
    </alerts>
    <alerts>
        <fullName>H_L_TAS_Pay_Table_Alert</fullName>
        <ccEmails>vic.orders@hlaustralia.com.au</ccEmails>
        <ccEmails>john.pycroft@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L TAS Pay@Table Alert - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Generic_Pay_At_Table_Alert_2</template>
    </alerts>
    <alerts>
        <fullName>H_L_VIC_Pay_Table_Alert</fullName>
        <ccEmails>vic.orders@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L VIC Pay@Table Alert - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Generic_Pay_At_Table_Alert_2</template>
    </alerts>
    <alerts>
        <fullName>H_L_WA_Pay_Table_Alert</fullName>
        <ccEmails>bmoar@hlaustralia.com.au</ccEmails>
        <ccEmails>wa.support@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L WA Pay@Table Alert - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Generic_Pay_At_Table_Alert_2</template>
    </alerts>
    <alerts>
        <fullName>Healthpoint_Alert</fullName>
        <ccEmails>appspecialist-team@tyro.com</ccEmails>
        <description>Healthpoint Alert - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Merchant_Onboarding_Alerts/Generic_Internal_Merchant_ID_Status_Update</template>
    </alerts>
    <alerts>
        <fullName>IdealPos_Application_Received_MID</fullName>
        <ccEmails>acu@tyro.com</ccEmails>
        <description>IdealPos - Application Received (MID) - NEO-808: DECOMMISSIONED</description>
        <protected>false</protected>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/IdealPOS_AM_Handover</template>
    </alerts>
    <alerts>
        <fullName>MID_Channel_Change_Notification</fullName>
        <description>MID Channel Change Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Channel_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>elliotk@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nkumar@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Merchant_Onboarding_Alerts/CM_Notification_MID_Channel_Change</template>
    </alerts>
    <alerts>
        <fullName>MID_Sales_Survey_to_Active_MIDs</fullName>
        <description>MID: Sales Survey to Active MIDs</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>survey@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/Post_Sales_Survey_to_Merchants</template>
    </alerts>
    <alerts>
        <fullName>Medilink_Activation_Email</fullName>
        <description>Medilink - Activation Email - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Activation_Medilink_trainingvid</template>
    </alerts>
    <alerts>
        <fullName>Medilink_Application_Received_MID</fullName>
        <ccEmails>acu@tyro.com</ccEmails>
        <description>Medilink - Application Received (MID) - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/Medilink_Handover_email</template>
    </alerts>
    <alerts>
        <fullName>Medilink_Welcome_Email</fullName>
        <description>Medilink - Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Merchant_Onboarding_Alerts/Medilink_Welcome_Email_24_06_15</template>
    </alerts>
    <alerts>
        <fullName>Merchant_ID_Closure_notification</fullName>
        <description>Merchant ID: Closure notification</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>fs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_emails/FS_Termination_Notification</template>
    </alerts>
    <alerts>
        <fullName>PPMP_Activation_Email</fullName>
        <description>PPMP - Activation Email - REDUNDANT (SF-1252)</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/PPMP_Reconciliation_Guide_17_07_2015</template>
    </alerts>
    <alerts>
        <fullName>PPMP_Boarding_Complete_MID</fullName>
        <description>PPMP - Boarding Complete (MID) - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/PPMP_Welcome_Letter_17_07_2015</template>
    </alerts>
    <alerts>
        <fullName>PPMP_Welcome_Email</fullName>
        <description>DELETE - PPMP - Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/PPMP_Reconciliation_Guide_17_07_2015</template>
    </alerts>
    <alerts>
        <fullName>PracSoft_Welcome_Email</fullName>
        <description>DELETE - PracSoft - Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/PracSoft_Activation_Email_14_04_2015</template>
    </alerts>
    <alerts>
        <fullName>Pracsoft_Activation_Email</fullName>
        <description>Pracsoft - Activation Email - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/PracSoft_Activation_Email_14_04_2015</template>
    </alerts>
    <alerts>
        <fullName>Pracsoft_Application_Received_MID</fullName>
        <ccEmails>acu@tyro.com</ccEmails>
        <description>Pracsoft - Application Received (MID) - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/PS_Handover_email</template>
    </alerts>
    <alerts>
        <fullName>Practice_2000_Application_Received_MID</fullName>
        <ccEmails>acu@tyro.com</ccEmails>
        <description>Practice 2000 - Application Received (MID) - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/P2K_Handover_email</template>
    </alerts>
    <alerts>
        <fullName>RxWorks_Boarding_Completed</fullName>
        <description>RxWorks Boarding Completed - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>POS_Product_Notification_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/RxWorks_Boarding_Completed</template>
    </alerts>
    <alerts>
        <fullName>Swiftpos_Application_Received_MID</fullName>
        <ccEmails>acu@tyro.com</ccEmails>
        <description>Swiftpos - Application Received (MID) - NEO-808: DECOMMISSIONED</description>
        <protected>false</protected>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/SwiftPOS_AM_Handover</template>
    </alerts>
    <alerts>
        <fullName>Ultimo_Dental_Boarding_Complete_MID</fullName>
        <description>Ultimo Dental - Boarding Complete (MID) - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Ultimo_Welcome_Email_17_09_2015</template>
    </alerts>
    <alerts>
        <fullName>Waiter_POS_PAD_Application_Received_MID</fullName>
        <ccEmails>acu@tyro.com</ccEmails>
        <description>Waiter POS/PAD - Application Received (MID) - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/TriniTEQ_AM_Handover</template>
    </alerts>
    <alerts>
        <fullName>Zedmed_Activation_Email</fullName>
        <description>Zedmed - Activation Email - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Zedmed_activation_email_24_06_2015</template>
    </alerts>
    <alerts>
        <fullName>Zedmed_Application_Received_MID</fullName>
        <ccEmails>acu@tyro.com</ccEmails>
        <description>Zedmed - Application Received (MID) - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/AM_Handover_email</template>
    </alerts>
    <alerts>
        <fullName>Zedmed_Welcome_Email</fullName>
        <description>DELETE - Zedmed - Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Zedmed_activation_email_24_06_2015</template>
    </alerts>
    <fieldUpdates>
        <fullName>FU_Set_Merchant_Id</fullName>
        <description>Sets Merchant Id

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <field>Name</field>
        <formula>MID__c</formula>
        <name>FU - Set Merchant Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Primary_POS_On_Account</fullName>
        <description>NEO-572</description>
        <field>Primary_Integration_Product__c</field>
        <formula>Integration_Product__r.Name</formula>
        <name>FU - Set Primary POS On Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Segment_Update_Flag_TRUE</fullName>
        <description>NEO-572</description>
        <field>Segment_Update_Flag__c</field>
        <literalValue>1</literalValue>
        <name>FU - Set Segment Update Flag - TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Termination_Date</fullName>
        <description>Sets Termination Date to TODAY()</description>
        <field>Termination_Date__c</field>
        <formula>TODAY()</formula>
        <name>FU - Set Termination Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Termination_Date_NULL</fullName>
        <field>Termination_Date__c</field>
        <name>Set Termination Date - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>E -Termination Alert</fullName>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3 OR 4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Merchant_ID__c.Status__c</field>
            <operation>equals</operation>
            <value>Tyro Terminated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Merchant_ID__c.Channel_Name__c</field>
            <operation>equals</operation>
            <value>Micros</value>
        </criteriaItems>
        <criteriaItems>
            <field>Merchant_ID__c.Channel_Name__c</field>
            <operation>equals</operation>
            <value>Wedderburn</value>
        </criteriaItems>
        <criteriaItems>
            <field>Merchant_ID__c.Channel_Name__c</field>
            <operation>equals</operation>
            <value>QCCU (Queensland Country Credit Union) ISO 15% for 5 years</value>
        </criteriaItems>
        <criteriaItems>
            <field>Merchant_ID__c.Channel_Name__c</field>
            <operation>equals</operation>
            <value>Riva Affiliated Merchants</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Generic_MID_Status_Change_Alert_Channel</name>
                <type>Alert</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Email - Notify AM On Termination</fullName>
        <actions>
            <name>AM_Notification_Customer_Terminated</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-707

Notifies Account Manager of MID termination by customer where Termination Reason = &quot;Other&quot;</description>
        <formula>ISPICKVAL(Status__c, &quot;Customer Terminated&quot;)
&amp;&amp; ISPICKVAL(Termination_Reason__c, &quot;Other&quot;)
&amp;&amp; (BEGINS(Account__r.Owner.UserRole.Name, &quot;AM&quot;) 
||  Account__r.Owner.LastName = &quot;Pulante&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Merchant Id</fullName>
        <actions>
            <name>FU_Set_Merchant_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets Merchant ID to value populated in MID.

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>ISCHANGED(MID__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Termination Date</fullName>
        <actions>
            <name>FU_Set_Termination_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Merchant_ID__c.Status__c</field>
            <operation>equals</operation>
            <value>Customer Terminated,Tyro Terminated,Migrated To New MID,Inactive (Loaner/Other)</value>
        </criteriaItems>
        <description>Sets Termnation Date to TODAY() when Status is changed to &apos;Tyro Terminated&apos; or &apos;Customer Terminated&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Termination Date - NULL</fullName>
        <actions>
            <name>Set_Termination_Date_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Merchant_ID__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Customer Terminated,Tyro Terminated,Migrated To New MID,Inactive (Loaner/Other)</value>
        </criteriaItems>
        <description>Sets Termnation Date to NULL when Status is changed from &apos;Tyro Terminated&apos; or &apos;Customer Terminated&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Primary POS On Account</fullName>
        <actions>
            <name>FU_Set_Primary_POS_On_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>NEO-572 

Sets Primary Integration Product on Account if Integration Product changed on Merchant Id</description>
        <formula>ISCHANGED(Integration_Product__c)  &amp;&amp; LEN(Account__r.Primary_Integration_Product__c) &lt;&gt; 0  &amp;&amp; Account__r.Primary_Integration_Product__c &lt;&gt; Integration_Product__r.Name</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
