<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FU_Set_Merchant_ID_To_Active</fullName>
        <description>Sets Merchant ID Status to Active

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <field>Status__c</field>
        <literalValue>Active</literalValue>
        <name>FU - Set Merchant ID To Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Merchant_ID__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CaseSafeId_On_Transaction_History</fullName>
        <description>Sets Account CaseSafeID on Transaction History record</description>
        <field>Account_CaseSafeId_3__c</field>
        <formula>CASESAFEID(Merchant_ID__r.Account__r.Id)</formula>
        <name>Set CaseSafeId On Transaction History</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>FU%3A Merchant ID Active</fullName>
        <actions>
            <name>FU_Set_Merchant_ID_To_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Transaction_History__c.Net_Transaction_Value__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Transaction_History__c.Total_Transactions__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Merchant_ID__c.Status__c</field>
            <operation>equals</operation>
            <value>Boarding Completed,Terminal Shipped</value>
        </criteriaItems>
        <description>Sets Merchant ID to Active

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TH - Set Account CaseSafeId</fullName>
        <actions>
            <name>Set_CaseSafeId_On_Transaction_History</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates Transaction History record with Account CaseSafeId</description>
        <formula>ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
