<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Survey_Result_Notification_of_negative_CSAT_Survey_Result</fullName>
        <description>Survey Result: Notification of negative CSAT Survey Result</description>
        <protected>false</protected>
        <recipients>
            <recipient>htaylor@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mparkes@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sbarber@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Automated_Email_Templates/Notification_of_Negative_CSAT_survey</template>
    </alerts>
    <rules>
        <fullName>SR%3A Notification of negative CSAT Survey Result</fullName>
        <actions>
            <name>Survey_Result_Notification_of_negative_CSAT_Survey_Result</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3 OR 4 OR 5) AND 6</booleanFilter>
        <criteriaItems>
            <field>Survey_Result__c.Question_1__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Survey_Result__c.Question_2__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Survey_Result__c.Question_3__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Survey_Result__c.Question_4__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Survey_Result__c.Question_5__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Survey_Result__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>CSAT</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
