<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>KA_How_to_Set_status_to_draft</fullName>
        <description>SF-615 - Implement Salesforce Knowledge - Field update Validation status to draft as part of the approval process</description>
        <field>ValidationStatus</field>
        <literalValue>Not validated</literalValue>
        <name>KA: How to - Set status to &quot;draft&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KA_How_to_Status_to_Approved_Exter</fullName>
        <description>SF-615 - Implement Salesforce Knowledge - Set validation status to &quot;Approved External&quot;</description>
        <field>ValidationStatus</field>
        <literalValue>Approved External</literalValue>
        <name>KA: How to: Status to Approved Exter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KA_How_to_Status_to_Internal_Approved</fullName>
        <description>SF-615 - Implement Salesforce Knowledge - Set Validation Status to &quot;Approved Internal&quot;</description>
        <field>ValidationStatus</field>
        <literalValue>Approved Internal</literalValue>
        <name>KA: How to - Status to Internal Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>KA_How_to_Publish</fullName>
        <action>Publish</action>
        <description>SF-615 - Implement Salesforce Knowledge ; Publish article as same version</description>
        <label>KA: How to : Publish</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <knowledgePublishes>
        <fullName>KA_How_to_Publish_As_New_Version</fullName>
        <action>PublishAsNew</action>
        <description>SF-615 - Implement Salesforce Knowledge . Publish a How to Article as a new version of previous article</description>
        <label>KA: How to: Publish As New Version</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
</Workflow>
