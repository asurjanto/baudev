<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>FC_Approved</fullName>
        <description>FC - Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Email_Templates/Forecast_Approved</template>
    </alerts>
    <alerts>
        <fullName>FC_Declined</fullName>
        <description>FC - Declined</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Email_Templates/Forecast_Declined</template>
    </alerts>
    <fieldUpdates>
        <fullName>FU_Approved</fullName>
        <description>SF-633 

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>FU - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Declined</fullName>
        <description>SF-633 

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <field>Status__c</field>
        <literalValue>Declined</literalValue>
        <name>FU - Declined</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Draft</fullName>
        <description>SF-633 

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <field>Status__c</field>
        <literalValue>Draft</literalValue>
        <name>FU - Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Approval_Date</fullName>
        <description>SF-633 

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <field>Approval_Date__c</field>
        <formula>TODAY()</formula>
        <name>FU - Set Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Submitted</fullName>
        <description>SF-633 

https://confluence.tyro.com/display/SF/SF-633+-+Channel+Forecasting</description>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>FU - Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
