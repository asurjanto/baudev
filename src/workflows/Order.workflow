<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>WTC_Acknowledgement_for_accessories_order</fullName>
        <description>WTC - Acknowledgement for accessories order</description>
        <protected>false</protected>
        <recipients>
            <field>BillToContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CS_Automated_Email_Templates/WTC_Acknowledgement_for_acc_order</template>
    </alerts>
    <alerts>
        <fullName>WTC_Acknowledgement_for_paid_paper_order</fullName>
        <description>WTC - Acknowledgement for paid paper order</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CS_Automated_Email_Templates/WTC_Acknowledgmt_for_Paid_Paper_order_v1</template>
    </alerts>
</Workflow>
