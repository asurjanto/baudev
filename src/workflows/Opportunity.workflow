<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Opportunity_Owner_Loan_Pre_Approval_Offer_Withdrawn</fullName>
        <description>Notify Opportunity Owner - Loan Pre Approval / Offer Withdrawn</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales/Notification_Loan_Pre_Approval_Offer_Withdrwn</template>
    </alerts>
    <alerts>
        <fullName>eCommerce_alert_to_Risk_on_resubmission</fullName>
        <description>eCommerce alert to Risk on resubmission</description>
        <protected>false</protected>
        <recipients>
            <field>eComm_Risk_Assessor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_eCommerce/eCommerce_Risk_re_approval_notification</template>
    </alerts>
    <alerts>
        <fullName>eCommerce_alert_to_application_specialists</fullName>
        <description>eCommerce alert to application specialists</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ssharma@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Application_Specialist__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Onboarding_Specialist__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_eCommerce/eCommerce_App_Specialists_Approval_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>eCommerce_opportunity_owner_stage_alert</fullName>
        <description>eCommerce opportunity owner stage alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_eCommerce/Opportunity_owner_status_alert_template</template>
    </alerts>
    <alerts>
        <fullName>eCommerce_opportunity_owner_wrong_stage_alert</fullName>
        <description>eCommerce opportunity owner wrong stage alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>athakur@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_eCommerce/Opportunity_owner_wrong_status_alert_template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Add_90_Days_To_Close_Date</fullName>
        <description>Adds 90 days to current close date</description>
        <field>CloseDate</field>
        <formula>CloseDate + 90</formula>
        <name>Add 90 Days To Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Close_Date_To_TODAY</fullName>
        <description>SF-469

Sets Close date to TODAY()</description>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>FU - Set Close Date To TODAY()</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Cls_Date_To_Pre_Appr_Expiry_Dat</fullName>
        <description>SF-1245</description>
        <field>CloseDate</field>
        <formula>Pre_Approval_Expiry_Date__c</formula>
        <name>FU - Set Cls Date To Pre Appr Expiry Dat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Loan_Amount_On_Account_NULL</fullName>
        <field>Loan_Amount__c</field>
        <name>FU - Set Loan Amount On Account - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Merchant_Statement_Value</fullName>
        <description>SF-469 

Updates Merchant Statement Value</description>
        <field>Amount</field>
        <formula>Merchant_Statement_Value__c</formula>
        <name>FU - Update Merchant Statement Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Application_Received_Date_TODAY</fullName>
        <description>NEO-567 

Sets Close Date to TODAY() when an Opportunity is closed</description>
        <field>Application_Received_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set App Rec Date - TODAY()</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Channel_Override_TRUE</fullName>
        <description>NEO-825</description>
        <field>Channel_Override__c</field>
        <literalValue>1</literalValue>
        <name>Set Channel Override - TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Close_Date_TODAY_90</fullName>
        <description>SF-1100</description>
        <field>CloseDate</field>
        <formula>TODAY() + 90</formula>
        <name>Set Close Date - TODAY + 90</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Close_date_On_Loan_Offer</fullName>
        <description>Sets Opportunity close date based on Expiry Date set by Integration</description>
        <field>CloseDate</field>
        <formula>Loan_Pre_Approval_Expiry_Date__c</formula>
        <name>Set Close Date On Loan Offer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Expiry_Date_NULL</fullName>
        <field>Loan_Pre_Approval_Expiry_Date__c</field>
        <name>Set Expiry Date - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Loan_Amount_On_Account</fullName>
        <description>SF-1090</description>
        <field>Loan_Amount__c</field>
        <formula>Loan_Amount__c</formula>
        <name>Set Loan Amount On Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Loan_Amount_On_Account_To_NULL</fullName>
        <description>SF-1090</description>
        <field>Loan_Amount__c</field>
        <name>Set Loan Amount On Account To NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Loan_Opportunity_Name</fullName>
        <field>Name</field>
        <formula>LEFT(Account.Trading_Name__c, 50) 
&amp; &quot; - &quot; 
&amp; TEXT(Loan_Product_Type__c)</formula>
        <name>Set Loan Opportunity Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Partner_Access</fullName>
        <field>Partner_Access__c</field>
        <formula>Channel_2__r.Account__r.Account_No__c</formula>
        <name>Set Partner Access</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Phone</fullName>
        <field>Phone__c</field>
        <formula>Account.Phone</formula>
        <name>Set Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pre_Bank_Merchant_TRUE</fullName>
        <description>NEO-569</description>
        <field>Pre_Bank_Merchant__c</field>
        <literalValue>1</literalValue>
        <name>Set Pre-Bank Merchant - TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Special_Bid_FALSE</fullName>
        <description>Sets Special Bid to FALSE</description>
        <field>Special_Bid__c</field>
        <literalValue>0</literalValue>
        <name>Set Special Bid - FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Terminal_Discount_FALSE</fullName>
        <description>Sets Terminal Discount to FALSE</description>
        <field>Terminal_Discount__c</field>
        <literalValue>0</literalValue>
        <name>Set Terminal Discount - FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>eComm_Bypass_validation_to_True</fullName>
        <field>Bypass_Validation_Rule__c</field>
        <literalValue>1</literalValue>
        <name>eComm Bypass validation to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>eComm_Opportunity_status_to_risk_review</fullName>
        <field>StageName</field>
        <literalValue>Risk Review</literalValue>
        <name>eComm Opportunity status to risk review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>eComm_opportunity_status_to_app_received</fullName>
        <field>StageName</field>
        <literalValue>Application Received*</literalValue>
        <name>eComm opportunity status to app received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>eComm_opportunity_status_to_rejected</fullName>
        <field>StageName</field>
        <literalValue>Declined</literalValue>
        <name>eComm opportunity status to rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Email%3A Loan Pre Approval %2F Offer Withdrawn Notification</fullName>
        <actions>
            <name>Notify_Opportunity_Owner_Loan_Pre_Approval_Offer_Withdrawn</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>SF-1101

Notifies Opportunity owner if loan pre-approval  or offer has been withdrawn</description>
        <formula>RecordType.Name = &quot;Loans&quot; &amp;&amp; ISCHANGED(StageName) &amp;&amp; (TEXT(StageName) = &quot;Loan Offer Withdrawn&quot; || TEXT(StageName) = &quot;Pre-Approval Withdrawn&quot;) &amp;&amp; Owner.Id &lt;&gt; &quot;005D0000001eVPO&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Pre Bank Merchant</fullName>
        <actions>
            <name>Set_Pre_Bank_Merchant_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Smart_Account__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Deposits</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Tyro Customer</value>
        </criteriaItems>
        <description>NEO-569

Sets field to TRUE if Smart Account Status = NULL</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Amount</fullName>
        <actions>
            <name>FU_Update_Merchant_Statement_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.HasOpportunityLineItem</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Merchant_Statement_Value__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>eCommerce</value>
        </criteriaItems>
        <description>SF-592

Sets Opportunity Amount where no line items are present</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Application Received Date - TODAY%28%29</fullName>
        <actions>
            <name>Set_Application_Received_Date_TODAY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Acquiring</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Application_Received_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>NEO-567. Sets Application Received Date to TODAY() when Opportunity is closed as won</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Close Date On Loan Offer</fullName>
        <actions>
            <name>Set_Close_date_On_Loan_Offer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Loans</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Loan Offered</value>
        </criteriaItems>
        <description>SF-1100, SF-1158

Sets Close Date on Loan Opportunity to TODAY() + 90 on Opportunity creation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Close Date To TODAY%28%29</fullName>
        <actions>
            <name>FU_Set_Close_Date_To_TODAY</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Expiry_Date_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>System Administrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>Tyro Development</value>
        </criteriaItems>
        <description>SF-469

Sets Close Date to TODAY() when an Opportunity is closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Loan Amount On Account</fullName>
        <actions>
            <name>Set_Loan_Amount_On_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Loans</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Amount__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Tyro Integration</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>equals</operation>
            <value>ayouroukelis@tyro.com</value>
        </criteriaItems>
        <description>SF-1090

Populates Loan Amount on the Account for use as a variable tag by Pardot.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Loan Amount On Account - NULL</fullName>
        <actions>
            <name>FU_Set_Loan_Amount_On_Account_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Loans</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>SF-1090

Sets Loan Amount on the Account to NULL on Opportunity closure</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Loan Close Date %2B90</fullName>
        <actions>
            <name>Set_Close_Date_TODAY_90</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Loans</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Pre-Approved</value>
        </criteriaItems>
        <description>SF-1100, SF-1158

Sets Close Date on Loan Opportunity to TODAY() + 90 on Opportunity creation</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Add_90_Days_To_Close_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Add_90_Days_To_Close_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Add_90_Days_To_Close_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>270</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>FU - Set Loan Opportunity Name</fullName>
        <actions>
            <name>Set_Loan_Opportunity_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Loans</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Product_Type__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Tyro Integration</value>
        </criteriaItems>
        <description>Sets Opportunity name when created by integration user</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FU - Update Merchant Statement Value</fullName>
        <actions>
            <name>FU_Update_Merchant_Statement_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Merchant_Statement_Value__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>eCommerce</value>
        </criteriaItems>
        <description>SF-469, SF-592 

Updates Merchant Statement Value (Amount) on Lead conversion</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Nurtured Loan - Reset Close Date</fullName>
        <actions>
            <name>FU_Set_Cls_Date_To_Pre_Appr_Expiry_Dat</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Nurtured Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Pre_Approval_Expiry_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>SF-1245

Sets Opportunity Close Date to Pre-Approval Expiry Date when Opportunity Status is set to Nurtured Opportunity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Set Key Values To NULL or FALSE</fullName>
        <actions>
            <name>Set_Special_Bid_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Terminal_Discount_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets key values to NULL or FALSE when an Opportunity is created via a clone</description>
        <formula>RecordType.DeveloperName != &apos;eCommerce&apos; &amp;&amp; ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Channel Override - TRUE</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Channel_Override__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>eCommerce</value>
        </criteriaItems>
        <description>NEO-825</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Channel_Override_TRUE</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Partner Access</fullName>
        <actions>
            <name>Set_Partner_Access</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName != &apos;eCommerce&apos; &amp;&amp; ( NOT( ISNULL(Channel_2__c) )  ||  (ISCHANGED(Channel_2__c) &amp;&amp; NOT( ISNULL(Channel_2__c) )) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Phone Number</fullName>
        <actions>
            <name>Set_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets Phone number on Opportunity creation.

Phone number on Opportunity required for use by New Voice Media dial lists</description>
        <formula>ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>eCommerce owner update on Opportunity close won</fullName>
        <actions>
            <name>eCommerce_opportunity_owner_stage_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>eCommerce</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
