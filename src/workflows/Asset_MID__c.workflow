<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FU_Set_MID_to_Active</fullName>
        <field>Status__c</field>
        <literalValue>Active</literalValue>
        <name>FU - Set MID to Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Merchant_ID__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Set MID to Active</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Status__c</field>
            <operation>equals</operation>
            <value>In Use</value>
        </criteriaItems>
        <description>Sets a MID to Active where the MID is associated with an Asset that is already &apos;In Use&apos;</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>FU_Set_MID_to_Active</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
