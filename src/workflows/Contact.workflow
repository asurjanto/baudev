<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>TEST_Email_To_Salesforce</fullName>
        <ccEmails>emailtosalesforce@v-2xjizzfry5e9hxom3fe958gs4qhd1zzktf495xeju8ozmp20ua.2-1e2ueau.eu1.le.salesforce.com</ccEmails>
        <description>TEST - Email To Salesforce</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Auto_Responses/TEST_Email_To_Salesforce</template>
    </alerts>
    <alerts>
        <fullName>Test_EMA</fullName>
        <description>Test EMA</description>
        <protected>false</protected>
        <recipients>
            <recipient>vluu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Invoice_Tyro_Payments_Limited</template>
    </alerts>
    <rules>
        <fullName>TEST - Email To Salesforce</fullName>
        <actions>
            <name>TEST_Email_To_Salesforce</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Contact.Inactive__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.CaseSafeId__c</field>
            <operation>equals</operation>
            <value>005D0000003pMJFIA2</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Tyro Development</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Task - Contact Re-Engaged</fullName>
        <actions>
            <name>Follow_Up_Required</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Sets a task for the contact owner to follow up potential sales opportunity</description>
        <formula>ISCHANGED(Re_Engaged_Date__c)
&amp;&amp; Re_Engaged_Date__c &gt; DATE(2017,05,15)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Follow_Up_Required</fullName>
        <assignedToType>accountOwner</assignedToType>
        <description>Pardot has recently updated this record due to a response to a marketing campaign

Follow up required.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Follow Up Required</subject>
    </tasks>
    <tasks>
        <fullName>Initial_Call_Tower_Prospect_List_VIC</fullName>
        <assignedToType>accountOwner</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Initial Call – Tower Prospect List (VIC)</subject>
    </tasks>
</Workflow>
