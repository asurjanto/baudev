<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FU_Update_MSV_On_OLI</fullName>
        <field>UnitPrice</field>
        <formula>Loan_Amount__c</formula>
        <name>FU - Update MSV On OLI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Loan_Product_TRUE</fullName>
        <description>NEO-462</description>
        <field>Loan_Product__c</field>
        <literalValue>1</literalValue>
        <name>Set Loan Product - TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Loan_Amount_On_Opportunity</fullName>
        <description>SF-1127</description>
        <field>Loan_Amount__c</field>
        <formula>Loan_Amount__c</formula>
        <name>Update Loan Amount On Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MSV_On_Opportunity</fullName>
        <description>SF-1127</description>
        <field>Merchant_Statement_Value__c</field>
        <formula>Merchant_Statement_Value__c</formula>
        <name>Update MSV On Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Set Loan Product - TRUE</fullName>
        <actions>
            <name>Set_Loan_Product_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Family__c</field>
            <operation>equals</operation>
            <value>Loans</value>
        </criteriaItems>
        <description>NEO-462

Sets Loan Product to TRUE if a Loan Product is selected as a line item</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Price - Loan Amount</fullName>
        <actions>
            <name>FU_Update_MSV_On_OLI</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Loan_Amount_On_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-1127, NEO-462

Updates Opportunity Loan Amount if Loan Amount is changed</description>
        <formula>(ISNEW() || ISCHANGED(Loan_Amount__c))  &amp;&amp; Opportunity.RecordType.Name = &quot;Loans&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Price - MSV</fullName>
        <actions>
            <name>Update_MSV_On_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-1127 

Updates Opportunity Loan Amount if Loan Amount is changed</description>
        <formula>ISCHANGED(Merchant_Statement_Value__c) &amp;&amp; Opportunity.RecordType.Name = &quot;Acquiring&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
