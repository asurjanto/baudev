<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>success__Update_end_to_now</fullName>
        <field>success__End_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Update end to now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>success__Team__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>success__Update_to_next_state_competitor</fullName>
        <field>success__Status__c</field>
        <name>Update to next state competitor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>NextValue</operation>
        <protected>true</protected>
        <targetObject>success__Team__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>success__Winning Score Achieved</fullName>
        <actions>
            <name>success__Update_end_to_now</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  success__Team__r.success__State__c  = 2,success__Score__c &gt;=  success__Team__r.success__Winning_Score__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
