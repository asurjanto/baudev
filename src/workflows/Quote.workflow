<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Quote_Approval_Status</fullName>
        <description>Quote - Approval Status</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Email_Templates/Quote_Approval_Status</template>
    </alerts>
    <alerts>
        <fullName>Rates_Review_Cost_Plus</fullName>
        <description>Rates Review - Cost Plus - DO NOT DELETE</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ayouroukelis@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Account_Management/Merchant_Fee_Notice_of_Change_Cost_Plus</template>
    </alerts>
    <alerts>
        <fullName>Rates_Review_Normalised</fullName>
        <description>Rates Review - Normalised - DO NOT DELETE</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ayouroukelis@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Account_Management/Merchant_Fee_Notice_of_Change_Normalised</template>
    </alerts>
    <alerts>
        <fullName>Rates_Review_Simple_Pricing_099</fullName>
        <description>Rates Review - Simple Pricing - 0.99 - DO NOT DELETE</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CS_Automated_Email_Templates/Merchant_Service_Fee_Notice_of_Change_Fixed_Rate_099</template>
    </alerts>
    <fieldUpdates>
        <fullName>FU_Set_Approval_Date</fullName>
        <field>Approval_Date__c</field>
        <formula>TODAY()</formula>
        <name>FU - Set Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Approval_Date_NULL</fullName>
        <field>Approval_Date__c</field>
        <name>FU - Set Approval Date - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Approver_1</fullName>
        <field>Approvers__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>FU - Set Approver 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Approver_2</fullName>
        <field>Approvers__c</field>
        <formula>Approvers__c &amp; &quot;; &quot; &amp;  $User.FirstName &amp; &quot; &quot; &amp;  $User.LastName</formula>
        <name>FU - Set Approver 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Approvers_To_NULL</fullName>
        <field>Approvers__c</field>
        <name>FU - Set Approvers To NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Email</fullName>
        <field>Email</field>
        <formula>Contact.Email</formula>
        <name>FU - Set Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Expiration_Date</fullName>
        <description>Sets Quote expiration date to Today() + 30</description>
        <field>ExpirationDate</field>
        <formula>TODAY() + 30</formula>
        <name>FU - Set Expiration Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Expiration_Date_NULL</fullName>
        <description>Sets expiration date to NULL</description>
        <field>ExpirationDate</field>
        <name>FU - Set Expiration Date - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Phone</fullName>
        <field>Phone</field>
        <formula>Contact.Phone</formula>
        <name>FU - Set Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Record_Type_Cost_Plus</fullName>
        <description>SF-592

Sets Quote record type to Cost Plus</description>
        <field>RecordTypeId</field>
        <lookupValue>Cost_Plus_Quote</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>FU - Set Record Type - Cost Plus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Record_Type_Rate_Review_CP</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Rate_Review_Cost_Plus</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>FU - Set Record Type - Rate Review CP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Status_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>FU - Set Status - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Status_Denied</fullName>
        <field>Status</field>
        <literalValue>Denied</literalValue>
        <name>FU - Set Status - Denied</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Status_Needs_Review</fullName>
        <field>Status</field>
        <literalValue>Needs Review</literalValue>
        <name>FU - Set Status - Needs Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Status_Pending_Approval</fullName>
        <field>Status</field>
        <literalValue>Pending Approval</literalValue>
        <name>FU - Set Status - Pending Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Status_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>FU - Set Status - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_record_Type_Rate_review_Std</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Rate_Review_Standard</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>FU - Set record Type - Rate review - Std</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Description_On_Quote</fullName>
        <field>Description</field>
        <formula>Opportunity.Description</formula>
        <name>Set Description On Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quote_Record_Type_RR_Fixed</fullName>
        <description>NEO-660: Sets Quote record type to Rate Review - Fixed Rates</description>
        <field>RecordTypeId</field>
        <lookupValue>Rate_Review_Fixed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Quote Record Type - RR Fixed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_Standard</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Standard_Quote</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type - Standard</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Auto Approve Non-Special Bid</fullName>
        <actions>
            <name>Quote_Approval_Status</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FU_Set_Approval_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Expiration_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Status_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Margin_bps__c</field>
            <operation>greaterOrEqual</operation>
            <value>0.35</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Acquiring</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Status</field>
            <operation>notEqual</operation>
            <value>Approved,Presented,Accepted</value>
        </criteriaItems>
        <description>Auto approve all non-RateReview quotes above 0.35 margin. Rate Review quote have to go through approval process.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Contact Email %26 Phone</fullName>
        <actions>
            <name>FU_Set_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-592

Sets Contact&apos;s email address and phone on the Quote</description>
        <formula>ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Void Approval Fields</fullName>
        <actions>
            <name>FU_Set_Approval_Date_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Approvers_To_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-592

Sets Approval Date and Approvers to NULL if Quote Status is changed from &quot;Approved&quot;, &quot;Presented&quot;, &quot;Accepted&quot;, &quot;Rejected&quot;</description>
        <formula>ISPICKVAL(Status, &quot;Draft&quot;) || ISPICKVAL(Status, &quot;Needs Review&quot;) || ISPICKVAL(Status, &quot;In Review&quot;) || ISPICKVAL(Status, &quot;Denied&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Record Type - Cost Plus</fullName>
        <actions>
            <name>FU_Set_Record_Type_Cost_Plus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Quote.RecordTypeId</field>
            <operation>equals</operation>
            <value>Cost Plus Rates</value>
        </criteriaItems>
        <description>SF-592

Sets Record Type to Cost Plus</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Record Type - Rate Review - Cost Plus</fullName>
        <actions>
            <name>FU_Set_Record_Type_Rate_Review_CP</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Description_On_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Quote.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rate Review - Cost Plus Rates</value>
        </criteriaItems>
        <description>SF-592

Sets Record Type to Rate Review - Cost Plus</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Record Type - Rate Review - Fixed</fullName>
        <actions>
            <name>Set_Description_On_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Quote_Record_Type_RR_Fixed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Quote.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rate Review - Fixed Rates</value>
        </criteriaItems>
        <description>NEO-639: Sets Record Type to Rate Review - Fixed</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Record Type - Rate Review - Standard</fullName>
        <actions>
            <name>FU_Set_record_Type_Rate_review_Std</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Description_On_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Quote.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rate Review - Normalised Rates</value>
        </criteriaItems>
        <description>SF-592

Sets Record Type to Rate Review - Standard</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Record Type - Standard</fullName>
        <actions>
            <name>Set_Record_Type_Standard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Quote.RecordTypeId</field>
            <operation>equals</operation>
            <value>Normalised Rates</value>
        </criteriaItems>
        <description>SF-592

Sets Record Type to Standard</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
