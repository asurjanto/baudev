<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>KA_Problem_Set_status_to_Draft</fullName>
        <field>ValidationStatus</field>
        <literalValue>Not validated</literalValue>
        <name>KA: Problem : Set  status to &quot;Draft&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KA_Problem_Status_to_Draft</fullName>
        <description>Sets the validation status to Draft</description>
        <field>ValidationStatus</field>
        <literalValue>Not validated</literalValue>
        <name>KA: Problem: Status to &quot;Draft&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Article_to_Internal_Approved</fullName>
        <description>Set article status to Internal Approved</description>
        <field>ValidationStatus</field>
        <literalValue>Approved Internal</literalValue>
        <name>Set Article to &quot;Internal Approved&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Approved_External1</fullName>
        <field>ValidationStatus</field>
        <literalValue>Approved External</literalValue>
        <name>Set Status to Approved External</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_article_to_Validated_Internal</fullName>
        <field>ValidationStatus</field>
        <literalValue>Approved Internal</literalValue>
        <name>Set article to &quot;Validated Internal&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_status_to_Approved_External</fullName>
        <description>Sets the Validation status to &quot;Approved External&quot;</description>
        <field>ValidationStatus</field>
        <literalValue>Approved External</literalValue>
        <name>Set status to &quot;Approved External&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>Publish</fullName>
        <action>Publish</action>
        <description>Lets CS agents publish draft articles and make them available for internal use</description>
        <label>Publish</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <knowledgePublishes>
        <fullName>Publish_as_New_Version</fullName>
        <action>PublishAsNew</action>
        <label>Publish as New Version</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
</Workflow>
