<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Loan_Name</fullName>
        <description>SF-1096</description>
        <field>Name</field>
        <formula>LEFT(UUID__c, 8)</formula>
        <name>Set Loan Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>LN - Set Loan Name</fullName>
        <actions>
            <name>Set_Loan_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-1096

Prevents users from updating Loan No. in the UI</description>
        <formula>ISCHANGED(Name)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
