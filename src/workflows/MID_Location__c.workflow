<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>H_L_MID_Status_Change_Alert_ACT</fullName>
        <ccEmails>XXX_nsw.support@hlaustralia.com.au</ccEmails>
        <ccEmails>XXX_tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L - MID Status Change Alert - ACT - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/H_L_MID_Status_Change_Alert</template>
    </alerts>
    <alerts>
        <fullName>H_L_MID_Status_Change_Alert_NSW</fullName>
        <ccEmails>nsw.support@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L - MID Status Change Alert - NSW - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/H_L_MID_Status_Change_Alert</template>
    </alerts>
    <alerts>
        <fullName>H_L_MID_Status_Change_Alert_NT</fullName>
        <ccEmails>tonyc@hlaustralia.com.au</ccEmails>
        <ccEmails>jasperl@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L - MID Status Change Alert - NT - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/H_L_MID_Status_Change_Alert</template>
    </alerts>
    <alerts>
        <fullName>H_L_MID_Status_Change_Alert_QLD</fullName>
        <ccEmails>qld.admin@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L - MID Status Change Alert - QLD - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/H_L_MID_Status_Change_Alert</template>
    </alerts>
    <alerts>
        <fullName>H_L_MID_Status_Change_Alert_SA</fullName>
        <ccEmails>jerryf@hlaustralia.com.au</ccEmails>
        <ccEmails>jasperl@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L - MID Status Change Alert - SA - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/H_L_MID_Status_Change_Alert</template>
    </alerts>
    <alerts>
        <fullName>H_L_MID_Status_Change_Alert_TAS</fullName>
        <ccEmails>vic.orders@hlaustralia.com.au</ccEmails>
        <ccEmails>john.pycroft@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L - MID Status Change Alert - TAS - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/H_L_MID_Status_Change_Alert</template>
    </alerts>
    <alerts>
        <fullName>H_L_MID_Status_Change_Alert_VIC</fullName>
        <ccEmails>vic.orders@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L - MID Status Change Alert - VIC - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/H_L_MID_Status_Change_Alert</template>
    </alerts>
    <alerts>
        <fullName>H_L_MID_Status_Change_Alert_WA</fullName>
        <ccEmails>bmoar@hlaustralia.com.au</ccEmails>
        <ccEmails>wa.support@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L - MID Status Change Alert - WA - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/H_L_MID_Status_Change_Alert</template>
    </alerts>
    <rules>
        <fullName>TK - New Location Added To MID</fullName>
        <actions>
            <name>TK_New_Location_Added_To_Existing_MID</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Merchant_ID__c.Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <description>Allocates a task to Customer Service to follow up new location added to an existing MID.

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>TK_New_Location_Added_To_Existing_MID</fullName>
        <assignedTo>htaylor@tyro.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new location has been added to an existing Merchant ID. Terminal shipment may be required</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New Location Added To Existing MID</subject>
    </tasks>
</Workflow>
