<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>success__Set_Reward_Status_To_Failed</fullName>
        <field>success__Reward_Status__c</field>
        <literalValue>Failed</literalValue>
        <name>Set Reward Status To Failed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Time Limit Set %28Active%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>success__Reward__c.success__Time_Limit_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>success__Reward__c.success__Reward_Status__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>success__Set_Reward_Status_To_Failed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>success__Reward__c.success__Time_Limit_Date_Time__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>success__Time Limit Set</fullName>
        <active>false</active>
        <criteriaItems>
            <field>success__Reward__c.success__Time_Limit_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
