<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Mecca_Brands_MAC_Address_Notification_New_Shipment</fullName>
        <ccEmails>itsupport@mecca.com.au</ccEmails>
        <description>Mecca Brands - MAC Address Notification - New Shipment - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CS_Automated_Email_Templates/Mecca_Brands_Ship_Content_Alert_New_Ship</template>
    </alerts>
    <alerts>
        <fullName>Mecca_Brands_MAC_Address_Notification_RMA</fullName>
        <ccEmails>itsupport@mecca.com.au</ccEmails>
        <description>Mecca Brands - MAC Address Notification - RMA - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CS_Automated_Email_Templates/Mecca_Brands_RMA_Shipment_Content_Alert</template>
    </alerts>
    <alerts>
        <fullName>Mecca_Brands_MAC_Address_Notification_RMA_Bulk_Shipment</fullName>
        <ccEmails>itsupport@mecca.com.au</ccEmails>
        <description>Mecca Brands - MAC Address Notification - RMA (Bulk Shipment) - DO NOT DE-ACTIVATE</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Mecca_Brands_RMA_Ship_Content_Alert_Bulk</template>
    </alerts>
    <fieldUpdates>
        <fullName>FU_Set_Real_Shipment_Tracking_No</fullName>
        <description>SF-662</description>
        <field>Real_Shipment_Tracking_No__c</field>
        <formula>Name</formula>
        <name>FU - Set Real Shipment Tracking #</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Startrack_Track_Trace</fullName>
        <description>SF-576</description>
        <field>TNT_Track_Trace__c</field>
        <formula>&quot;www.startrack.com.au/track-trace/?id=&quot;&amp;Name</formula>
        <name>FU - Startrack Track &amp; Trace</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_TNT_Track_Trace</fullName>
        <field>TNT_Track_Trace__c</field>
        <formula>&quot;http://www.tntexpress.com.au/InterAction/ASPs/CnmHxAS.asp?&quot; &amp;  Name</formula>
        <name>FU: TNT Track &amp; Trace</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_TNT_Track_Trace_D</fullName>
        <field>TNT_Track_Trace__c</field>
        <formula>&quot;http://www.tntexpress.com.au/InterAction/ASPs/CnmHxAS.asp?&quot; &amp; LEFT(RIGHT(Name,12),15)</formula>
        <name>FU: TNT Track &amp; Trace D</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Trim_Connote_Startrack</fullName>
        <description>SF-576</description>
        <field>Name</field>
        <formula>LEFT(Name, 12)</formula>
        <name>FU - Trim Connote - Startrack</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TNT_Trim_Connnote_BTP</fullName>
        <field>Name</field>
        <formula>&quot;BTP&quot;&amp;LEFT(RIGHT(Name,18),9)</formula>
        <name>TNT Trim Connnote BTP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TNT_Trim_Connnote_TOP</fullName>
        <field>Name</field>
        <formula>&quot;TOP&quot;&amp;LEFT(RIGHT(Name,18),9)</formula>
        <name>TNT Trim Connnote TOP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>FU - Startrack Track %26 Trace</fullName>
        <actions>
            <name>FU_Startrack_Track_Trace</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>TNT_Shipment__c.Name</field>
            <operation>startsWith</operation>
            <value>QZFZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>TNT_Shipment__c.Name</field>
            <operation>startsWith</operation>
            <value>2TY</value>
        </criteriaItems>
        <description>SF-576</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Startrack Trim Connnote</fullName>
        <actions>
            <name>FU_Trim_Connote_Startrack</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>((1 AND 2) OR (3 AND 4) OR (5 AND 6) OR (7 AND 8)) AND 9</booleanFilter>
        <criteriaItems>
            <field>TNT_Shipment__c.Name</field>
            <operation>startsWith</operation>
            <value>QZFZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>TNT_Shipment__c.Shipment_Type__c</field>
            <operation>equals</operation>
            <value>Outbound</value>
        </criteriaItems>
        <criteriaItems>
            <field>TNT_Shipment__c.Name</field>
            <operation>startsWith</operation>
            <value>QZFZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>TNT_Shipment__c.Shipment_Type__c</field>
            <operation>equals</operation>
            <value>Inbound</value>
        </criteriaItems>
        <criteriaItems>
            <field>TNT_Shipment__c.Name</field>
            <operation>startsWith</operation>
            <value>2TY</value>
        </criteriaItems>
        <criteriaItems>
            <field>TNT_Shipment__c.Shipment_Type__c</field>
            <operation>equals</operation>
            <value>Outbound</value>
        </criteriaItems>
        <criteriaItems>
            <field>TNT_Shipment__c.Name</field>
            <operation>startsWith</operation>
            <value>2TY</value>
        </criteriaItems>
        <criteriaItems>
            <field>TNT_Shipment__c.Shipment_Type__c</field>
            <operation>equals</operation>
            <value>Inbound</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Aliance XENTA Paper</value>
        </criteriaItems>
        <description>SF-576, SF-662</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A TNT Trim Connnote BTP</fullName>
        <actions>
            <name>TNT_Trim_Connnote_BTP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>((1 AND 2) OR (3 AND 4)) AND 5</booleanFilter>
        <criteriaItems>
            <field>TNT_Shipment__c.Name</field>
            <operation>startsWith</operation>
            <value>610400112925</value>
        </criteriaItems>
        <criteriaItems>
            <field>TNT_Shipment__c.Shipment_Type__c</field>
            <operation>equals</operation>
            <value>Outbound</value>
        </criteriaItems>
        <criteriaItems>
            <field>TNT_Shipment__c.Name</field>
            <operation>startsWith</operation>
            <value>610400112925</value>
        </criteriaItems>
        <criteriaItems>
            <field>TNT_Shipment__c.Shipment_Type__c</field>
            <operation>equals</operation>
            <value>Inbound</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Aliance XENTA Paper</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A TNT Trim Connnote TOP</fullName>
        <actions>
            <name>TNT_Trim_Connnote_TOP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>((1 AND 2) OR (3 AND 4)) AND 5</booleanFilter>
        <criteriaItems>
            <field>TNT_Shipment__c.Name</field>
            <operation>startsWith</operation>
            <value>610400292425</value>
        </criteriaItems>
        <criteriaItems>
            <field>TNT_Shipment__c.Shipment_Type__c</field>
            <operation>equals</operation>
            <value>Outbound</value>
        </criteriaItems>
        <criteriaItems>
            <field>TNT_Shipment__c.Name</field>
            <operation>startsWith</operation>
            <value>610400292425</value>
        </criteriaItems>
        <criteriaItems>
            <field>TNT_Shipment__c.Shipment_Type__c</field>
            <operation>equals</operation>
            <value>Inbound</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Aliance XENTA Paper</value>
        </criteriaItems>
        <description>SF-662</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TNT Track %26 Trace</fullName>
        <actions>
            <name>FU_TNT_Track_Trace</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>TNT_Shipment__c.Name</field>
            <operation>notContain</operation>
            <value>[D]</value>
        </criteriaItems>
        <criteriaItems>
            <field>TNT_Shipment__c.Name</field>
            <operation>notContain</operation>
            <value>QZFZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>TNT_Shipment__c.Name</field>
            <operation>notContain</operation>
            <value>2TY</value>
        </criteriaItems>
        <description>SF-576</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TNT Track %26 Trace %5BD%5D</fullName>
        <actions>
            <name>FU_Set_Real_Shipment_Tracking_No</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_TNT_Track_Trace_D</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TNT_Shipment__c.Name</field>
            <operation>startsWith</operation>
            <value>[D]</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
