<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Customer_Re_Engaged_Follow_up_required</fullName>
        <description>Email Alert: Customer Re-Engaged - Follow up required</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Customer_Re_engaged_Follow_up_required</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Lead_Opportunity_Status_NULL</fullName>
        <description>SF-982</description>
        <field>Lead_Opportunity_Status__c</field>
        <name>Set Lead/Opportunity Status - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Status_changed</fullName>
        <field>Status_Changed_On__c</field>
        <formula>Now()</formula>
        <name>Task Status changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>FU - Set Lead%2FOpportunity Status - NULL</fullName>
        <actions>
            <name>Set_Lead_Opportunity_Status_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Task.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.CaseSafeId_WhoId__c</field>
            <operation>startsWith</operation>
            <value>00Q</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.CaseSafe_WhatId__c</field>
            <operation>startsWith</operation>
            <value>006</value>
        </criteriaItems>
        <description>SF-982

Sets Lead/Opportunity Status to NULL when a Task is closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Status Changed</fullName>
        <actions>
            <name>Task_Status_changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ischanged( Status )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Update Triage</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Triage__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>SF-243</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify Task Creator On Completion</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
