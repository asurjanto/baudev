<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CHOWN_Notify_AM</fullName>
        <ccEmails>am@tyro.com</ccEmails>
        <description>CHOWN - Notify AM</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Account_Management/CHOWN_Progress_Notification</template>
    </alerts>
    <alerts>
        <fullName>CHOWN_Notify_Credit</fullName>
        <ccEmails>credit-team@tyro.com</ccEmails>
        <description>CHOWN - Notify Credit</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Account_Management/CHOWN_Progress_Notification</template>
    </alerts>
    <alerts>
        <fullName>CHOWN_Status_Notification</fullName>
        <description>CHOWN Status Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Account_Management/CHOWN_Progress_Notification</template>
    </alerts>
    <alerts>
        <fullName>CS_Reminder_Due</fullName>
        <ccEmails>cs@tyro.com</ccEmails>
        <description>CS Reminder Due</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Auto_Responses/CS_Request_Reminder_Due</template>
    </alerts>
    <alerts>
        <fullName>Case_Case_assigned_notification_to_merchant</fullName>
        <description>Case: Case assigned notification to merchant</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CS_Automated_Email_Templates/Case_Case_Assigned_notification_to_merchant</template>
    </alerts>
    <alerts>
        <fullName>Case_Closure_Notification_for_Merchant</fullName>
        <ccEmails>corporate-team@tyro.com</ccEmails>
        <description>Case: Closure Notification for Merchant</description>
        <protected>false</protected>
        <senderAddress>fs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_emails/FS_Termination_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_Csat_survey_for_Case_Closed</fullName>
        <description>Case: Csat survey for Case Closed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CS_Automated_Email_Templates/Case_CSAT_Survey_for_Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>Case_FS_Case_assigned_notification_to_merchant</fullName>
        <description>Case: FS Case assigned notification to merchant</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>fs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CS_Automated_Email_Templates/Case_FS_Case_Assigned_notification_to_merchant</template>
    </alerts>
    <alerts>
        <fullName>Case_Send_Termination_form_to_Merchant_after_Termination_Request</fullName>
        <description>Case:Send Termination form to Merchant after Termination Request</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>fs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_emails/FS_Account_Closure</template>
    </alerts>
    <alerts>
        <fullName>Case_Status_Update_merchant_portal</fullName>
        <description>Case: Status Update merchant portal</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Automated_Email_Templates/Case_On_Access_change_on_mechant_Portal_notify_AM</template>
    </alerts>
    <alerts>
        <fullName>Case_Termination_request_notification_to_AM</fullName>
        <description>Case: Termination request notification to AM</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>mcastillo@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_emails/Termination_Request_Merchant_ID</template>
    </alerts>
    <alerts>
        <fullName>Case_Termination_request_notification_to_am_email</fullName>
        <ccEmails>am@tyro.com</ccEmails>
        <description>Case: Termination request notification to am@tyro.com if account owner is not correctly assigned</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Support_emails/Termination_Request_Merchant_ID</template>
    </alerts>
    <alerts>
        <fullName>Case_now_owned</fullName>
        <description>Case now owned</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>cs-requests@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Auto_Responses/Case_now_owned</template>
    </alerts>
    <alerts>
        <fullName>CasenotificationforNBG</fullName>
        <description>Case notification for NBG</description>
        <protected>false</protected>
        <recipients>
            <recipient>mparkes@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Auto_Responses/NBG_case_notification</template>
    </alerts>
    <alerts>
        <fullName>Credit_Approval_Approved</fullName>
        <description>Credit Approval - Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Finance/Refund_Approved</template>
    </alerts>
    <alerts>
        <fullName>Credit_Approval_Rejected</fullName>
        <description>Credit Approval Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Finance/Refund_Request_Declined</template>
    </alerts>
    <alerts>
        <fullName>Ducor_new_Account</fullName>
        <description>Ducor: new Account</description>
        <protected>false</protected>
        <recipients>
            <recipient>rdavidson@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Auto_Responses/Ducor_new_Account_notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_Send_notification_of_Development_portal_access_request</fullName>
        <ccEmails>Integrationsupport@tyro.com</ccEmails>
        <ccEmails>corporate-team@tyro.com</ccEmails>
        <description>Email Alert: Send notification of Development portal access request</description>
        <protected>false</protected>
        <senderAddress>noreply@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CS_Automated_Email_Templates/Case_Notification_of_Development_portal_access_request</template>
    </alerts>
    <alerts>
        <fullName>Shipment_Alert_Ali_Baba</fullName>
        <ccEmails>corporate-team@tyro.com</ccEmails>
        <description>Shipment Alert - Ali Baba</description>
        <protected>false</protected>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Shipment_Alert_Ali_Baba</template>
    </alerts>
    <alerts>
        <fullName>Shipment_Alert_Mrs_Fields</fullName>
        <ccEmails>corporate-team@tyro.com</ccEmails>
        <description>Shipment Alert - Mrs Fields</description>
        <protected>false</protected>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Shipment_Alert_Mrs_Fields</template>
    </alerts>
    <alerts>
        <fullName>WTC_Acknowledgement</fullName>
        <description>WTC- Acknowledgement</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CS_Automated_Email_Templates/Web_to_Case_Acknowledgement</template>
    </alerts>
    <alerts>
        <fullName>WTC_Acknowledgement_for_paper_orders</fullName>
        <description>WTC- Acknowledgement for paper orders</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CS_Automated_Email_Templates/Web_to_case_Acknowledgement_Paper_Order</template>
    </alerts>
    <fieldUpdates>
        <fullName>Alliance_paper_case</fullName>
        <field>Subject</field>
        <formula>IF( ISPICKVAL(Alliance_Paper_Order__c, &quot;Xenta&quot; ), &quot;XENTA PAPER&quot; , &quot;XENTISSIMO PAPER&quot; )</formula>
        <name>Alliance paper case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Case_To_AM_Chown_Queue</fullName>
        <description>SF-1200</description>
        <field>OwnerId</field>
        <lookupValue>AM_CHOWN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Case To AM Chown Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Case_To_CS</fullName>
        <description>SF-1200</description>
        <field>OwnerId</field>
        <lookupValue>CSRequests</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Case To CS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Case_To_Credit_CHOWN_Queue</fullName>
        <description>SF-1200</description>
        <field>OwnerId</field>
        <lookupValue>Credit_CHOWN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Case To Credit CHOWN Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Case_To_FS_On_Hold_Queue</fullName>
        <description>SF-1330</description>
        <field>OwnerId</field>
        <lookupValue>FS_On_Hold</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Case To FS On Hold Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Case_To_FS_Queue</fullName>
        <description>SF-1200</description>
        <field>OwnerId</field>
        <lookupValue>FS_Cases</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Case To FS Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_subject_update</fullName>
        <field>Subject</field>
        <formula>&apos;Call reason - &apos; &amp;TEXT(Call_Category__c)</formula>
        <name>Call subject update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_City</fullName>
        <field>City__c</field>
        <formula>Account.ShippingCity</formula>
        <name>Case City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Delivery_Address_1</fullName>
        <field>Delivery_Address_Line_1__c</field>
        <formula>Account.Trading_Address_Line_1__c</formula>
        <name>Case Delivery Address 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Delivery_Address_2</fullName>
        <field>Delivery_Address_Line_2__c</field>
        <formula>Account.Trading_Address_Line_2__c</formula>
        <name>Case Delivery Address 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Delivery_Address_3</fullName>
        <field>Delivery_Address_Line_3__c</field>
        <formula>Account.Trading_Address_Line_3__c</formula>
        <name>Case Delivery Address 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Postcode</fullName>
        <field>Postcode__c</field>
        <formula>Account.ShippingPostalCode</formula>
        <name>Case Postcode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_State</fullName>
        <field>State__c</field>
        <formula>Account.ShippingState</formula>
        <name>Case State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Update_case_owner_to_Integration</fullName>
        <description>NEO 744 Update case owner to Integration support</description>
        <field>OwnerId</field>
        <lookupValue>Integration_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case: Update case owner to Integration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delete_voicmail</fullName>
        <field>ApexMarker__c</field>
        <literalValue>Delete voicemail</literalValue>
        <name>Delete voicmail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_MYOB_CASE_EXTRA_INFO</fullName>
        <field>Extra_Info__c</field>
        <formula>IF( LEN(Account.MYOB_Serial_Number__c) = 7,&quot;POS: RetailManager - &quot;, &quot;POS: Other - &quot; ) &amp; &quot;Terminal Type - &quot; &amp; Asset.Name</formula>
        <name>FU: MYOB CASE EXTRA INFO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Reset_Subject</fullName>
        <description>SF-569

Resets Subject to original state</description>
        <field>Subject</field>
        <formula>MID(Subject, 28, 255)</formula>
        <name>FU - Reset Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Approval_Date</fullName>
        <description>SF-541

Sets the Approval Date on the Case</description>
        <field>Approval_Date__c</field>
        <formula>TODAY()</formula>
        <name>FU - Set Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Approval_Date_To_NULL</fullName>
        <description>SF-541

Sets Approval Date to NULL</description>
        <field>Approval_Date__c</field>
        <name>FU - Set Approval Date To NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Approved_to_FALSE</fullName>
        <description>SF-541

Sets Approved to FALSE</description>
        <field>Approved__c</field>
        <literalValue>0</literalValue>
        <name>FU - Set Approved to FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Approved_to_True</fullName>
        <description>SF-541

Sets Approved to TRUE</description>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>FU - Set Approved to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Approver_1</fullName>
        <description>SF-541

Updates Approvers with Last Name of most recent Case approver</description>
        <field>Approvers__c</field>
        <formula>$User.LastName</formula>
        <name>FU - Set Approver 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Approver_2</fullName>
        <description>SF-541

Updates Approvers with Last Name of most recent Case approver</description>
        <field>Approvers__c</field>
        <formula>Approvers__c &amp; &quot;; &quot; &amp; $User.LastName</formula>
        <name>FU - Set Approver 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Approver_Id</fullName>
        <description>SF-541

Updates Approver ID with User ID of most recent Case approver</description>
        <field>Approver_ID__c</field>
        <formula>$User.Id</formula>
        <name>FU - Set Approver Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Approver_Id_To_NULL</fullName>
        <description>SF-541

Sets Approver Id to NULL</description>
        <field>Approver_ID__c</field>
        <name>FU - Set Approver Id To NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Approvers_To_NULL</fullName>
        <description>SF-541

Sets Approvers to NULL</description>
        <field>Approvers__c</field>
        <name>FU - Set Approvers To NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Case_Resume_2_Days</fullName>
        <description>SF-569

Sets Case Resume Date to TODAY() + 2</description>
        <field>Case_Resume_Date__c</field>
        <formula>TODAY() + 2</formula>
        <name>FU - Set Case Resume - 2 Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Case_Resume_Date_To_NULL</fullName>
        <description>SF-569

Sets Case resume Date to NULL</description>
        <field>Case_Resume_Date__c</field>
        <name>FU - Set Case Resume Date To NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Status_On_Hold</fullName>
        <description>SF-569

Sets Case Status to &apos;On Hold&apos;</description>
        <field>Status</field>
        <literalValue>On Hold</literalValue>
        <name>FU - Set Status On Hold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Status_To_Approved</fullName>
        <description>SF-541

Sets Case Status to Approved</description>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>FU - Set Status To Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Status_To_New</fullName>
        <description>SF-541

Sets Case Status to New</description>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>FU - Set Status To New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Status_To_Pending_Approval</fullName>
        <description>SF-541

Sets Case Status to Pending Approval</description>
        <field>Status</field>
        <literalValue>Pending Approval</literalValue>
        <name>FU - Set Status To Pending Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Status_to_Rejected</fullName>
        <description>SF-541

Sets Case Status to Rejected</description>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>FU - Set Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Case_Shipment_Date_field</fullName>
        <field>Shipment_Date__c</field>
        <formula>Now()</formula>
        <name>FU: Update Case &quot;Shipment Date&quot; field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Case_Subject</fullName>
        <description>SF-569

Updates Case Subject with &quot;ASSET ON HOLD FOR 2 DAYS&quot; + original subject</description>
        <field>Subject</field>
        <formula>&quot;ASSET ON HOLD FOR 2 DAYS - &quot; &amp;  Subject</formula>
        <name>FU - Update Case Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_AMEX_Case_Subject</fullName>
        <field>Subject</field>
        <formula>IF( CONTAINS(Subject, &apos;**AMEX**&apos;),PRIORVALUE(Subject), &apos;**AMEX**&apos;&amp;Subject)</formula>
        <name>Field Update: AMEX Case Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_GOODSONS_Case_Subject</fullName>
        <description>updates SUBJECT for GOODSONS merchants</description>
        <field>Subject</field>
        <formula>IF( CONTAINS(Subject, &apos;**GOODSONS**&apos;),PRIORVALUE(Subject), &apos;**GOODSONS - check if T1 required**&apos;&amp;Subject)</formula>
        <name>Field Update: GOODSONS Case Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_MEDICAL_Case_Subject</fullName>
        <description>updates SUBJECT for MEDICAL cases</description>
        <field>Subject</field>
        <formula>IF( CONTAINS(Subject, &apos;**MEDICAL**&apos;),PRIORVALUE(Subject), &apos;**MEDICAL**&apos;&amp;Subject)</formula>
        <name>Field Update: MEDICAL Case Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_MYOB_Case_Subject</fullName>
        <description>updates SUBJECT for MYOB cases</description>
        <field>Subject</field>
        <formula>IF( CONTAINS(Subject, &apos;**MYOB**&apos;),PRIORVALUE(Subject), &apos;**MYOB**&apos;&amp;Subject)</formula>
        <name>Field Update: MYOB Case Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_Terminal_Returns_Priority</fullName>
        <description>Raises the priority to level 2 for Terminal Return Cases</description>
        <field>Priority</field>
        <literalValue>2</literalValue>
        <name>Field Update: Terminal Returns Priority</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mailing_Address_Line_1</fullName>
        <field>Delivery_Address_Line_1__c</field>
        <formula>Account.BillingStreet</formula>
        <name>Mailing Address Line 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mailing_City</fullName>
        <field>City__c</field>
        <formula>Account.BillingCity</formula>
        <name>Mailing City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mailing_Postcode</fullName>
        <field>Postcode__c</field>
        <formula>Account.BillingPostalCode</formula>
        <name>Mailing Postcode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mailing_State</fullName>
        <field>State__c</field>
        <formula>Account.BillingState</formula>
        <name>Mailing State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Outage</fullName>
        <description>SF-316</description>
        <field>Call_Reason__c</field>
        <literalValue>Activating new terminals</literalValue>
        <name>Outage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Outage_Update</fullName>
        <description>SF-316</description>
        <field>Call_Category__c</field>
        <literalValue>Outage</literalValue>
        <name>Outage Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owned_Assets_in_case</fullName>
        <field>Special_Notes__c</field>
        <formula>IF( CONTAINS( Special_Notes__c , &apos;*** Assets are owned ***&apos;),PRIORVALUE(Special_Notes__c), &apos;*** Assets are owned ***&apos;&amp;Special_Notes__c)</formula>
        <name>Owned Assets in case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RMA_Priority_1</fullName>
        <field>Priority</field>
        <literalValue>1</literalValue>
        <name>RMA Priority 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RMA_Priority_2</fullName>
        <field>Priority</field>
        <literalValue>2</literalValue>
        <name>RMA Priority 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_City</fullName>
        <description>https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <field>City__c</field>
        <formula>Location__r.City__c</formula>
        <name>Set City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Delivery_Address_Line_1</fullName>
        <description>https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <field>Delivery_Address_Line_1__c</field>
        <formula>LEFT(Location__r.Street__c,30)</formula>
        <name>Set Delivery Address Line 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Delivery_Address_Line_2</fullName>
        <description>https://confluence.tyro.com/pages/viewpage.action?pageId=26168768</description>
        <field>Delivery_Address_Line_2__c</field>
        <formula>LEFT(Location__r.Street_2__c,30)</formula>
        <name>Set Delivery Address Line 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Delivery_Address_Line_3</fullName>
        <description>https://confluence.tyro.com/pages/viewpage.action?pageId=26168768</description>
        <field>Delivery_Address_Line_3__c</field>
        <formula>LEFT(Location__r.Street_3__c,30)</formula>
        <name>Set Delivery Address Line 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Postcode</fullName>
        <description>https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <field>Postcode__c</field>
        <formula>Location__r.Postcode__c</formula>
        <name>Set Postcode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Priority_1</fullName>
        <description>SF-1200</description>
        <field>Priority</field>
        <literalValue>1</literalValue>
        <name>Set Priority - 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_CHOWN</fullName>
        <description>SF-1200

Sets Record Type to Change Of Ownership</description>
        <field>RecordTypeId</field>
        <lookupValue>CHOWN</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type - CHOWN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_State</fullName>
        <description>https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <field>State__c</field>
        <formula>TEXT(Location__r.State__c)</formula>
        <name>Set State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status</fullName>
        <description>SF-1200</description>
        <field>Status</field>
        <literalValue>TSA Closure</literalValue>
        <name>Set Status - TSA Closure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Block_Terminal</fullName>
        <description>SF-1200</description>
        <field>Status</field>
        <literalValue>Block Terminal</literalValue>
        <name>Set Status - Block Terminal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_CHOWN_Application</fullName>
        <description>SF-1200</description>
        <field>Status</field>
        <literalValue>CHOWN Application</literalValue>
        <name>Set Status - CHOWN Application</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Closed</fullName>
        <description>SF-1330</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Set Status - Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Credit_Review</fullName>
        <description>SF1200</description>
        <field>Status</field>
        <literalValue>Credit Review</literalValue>
        <name>Set Status - Credit Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>changed_on</fullName>
        <field>Status_Changed_On__c</field>
        <formula>Now()</formula>
        <name>changed on</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Alliance paper case</fullName>
        <actions>
            <name>Alliance_paper_case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Aliance XENTA Paper</value>
        </criteriaItems>
        <description>Updates the subject of an alliance paper case</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CASE on access change in merchant portal notify AM</fullName>
        <actions>
            <name>Case_Status_Update_merchant_portal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Account.RecordType.Name = &quot;Tyro Customer&quot; &amp;&amp; Account.Owner.ProfileId = &quot;00eD00000016MWL&quot; &amp;&amp; ISPICKVAL(Call_Category__c,&quot;Passwords&quot;)  &amp;&amp; ISPICKVAL(Call_Reason__c,&quot;Extranet&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CHOWN - Close Case</fullName>
        <actions>
            <name>Set_Status_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR (1 AND 3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Change Of Ownership</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>TSA Closed,Application Abandoned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Smart_Account__c</field>
            <operation>notEqual</operation>
            <value>Requested App Access,App Enabled,TSA Active,Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Application Received</value>
        </criteriaItems>
        <description>SF-1200, SF-1330</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CHOWN - Notify AM</fullName>
        <actions>
            <name>CHOWN_Status_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Change Of Ownership</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>Account Manager</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New,Terminal Blocked,Awaiting Repayment,Loan Repaid,Loan Written Off,TSA Closed</value>
        </criteriaItems>
        <description>SF-1200, SF-1330</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CHOWN - Refer To AM - 1</fullName>
        <actions>
            <name>Assign_Case_To_AM_Chown_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Status_CHOWN_Application</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Change Of Ownership</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Loan Repaid,Loan Written Off</value>
        </criteriaItems>
        <description>SF-1200, SF-1330</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CHOWN - Refer To AM - 2</fullName>
        <actions>
            <name>Assign_Case_To_AM_Chown_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Status_CHOWN_Application</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Change Of Ownership</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Active_Loans__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Late Notification CHOWN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Terminal Blocked</value>
        </criteriaItems>
        <description>SF-1200, SF-1330</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CHOWN - Refer To AM - 3</fullName>
        <actions>
            <name>Assign_Case_To_AM_Chown_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Status_To_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Change Of Ownership</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>Account Manager</value>
        </criteriaItems>
        <description>SF-1200, SF-1330</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CHOWN - Refer To AM - 4</fullName>
        <actions>
            <name>Assign_Case_To_AM_Chown_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Status_To_New</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Record_Type_CHOWN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-1200, SF-1330</description>
        <formula>RecordType.Name = &quot;CS Request&quot; &amp;&amp; ISCHANGED(Reason) &amp;&amp; (ISPICKVAL(Reason, &quot;CHOWN&quot;) || ISPICKVAL(Reason, &quot;Late Notification CHOWN&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CHOWN - Refer To CS</fullName>
        <actions>
            <name>Assign_Case_To_CS</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Priority_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Status_Block_Terminal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4 OR 5) AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Change Of Ownership</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Late Notification CHOWN</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Account Manager</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>System Administrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Tyro Development</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Initiate CHOWN Process</value>
        </criteriaItems>
        <description>SF-1200. SF-1330</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CHOWN - Refer To Credit - 1</fullName>
        <actions>
            <name>CHOWN_Notify_Credit</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_Case_To_Credit_CHOWN_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Status_Credit_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5 OR 6) AND 7</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Change Of Ownership</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Active_Loans__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>CHOWN</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Account Manager</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>System Administrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Tyro Development</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Initiate CHOWN Process</value>
        </criteriaItems>
        <description>SF-1200, SF-1330</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CHOWN - Refer To Credit - 2</fullName>
        <actions>
            <name>CHOWN_Notify_Credit</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_Case_To_Credit_CHOWN_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Status_Credit_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Change Of Ownership</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Active_Loans__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Late Notification CHOWN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Terminal Blocked</value>
        </criteriaItems>
        <description>SF-1200, SF-1330</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CHOWN - Refer To FS %281%29</fullName>
        <actions>
            <name>Assign_Case_To_FS_On_Hold_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Status_On_Hold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Change Of Ownership</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Smart_Account__c</field>
            <operation>equals</operation>
            <value>Requested App Access,App Enabled,TSA Active,Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Application Received</value>
        </criteriaItems>
        <description>SF-1200, SF-1330</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CHOWN - Refer To FS %282%29</fullName>
        <actions>
            <name>Assign_Case_To_FS_On_Hold_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Status_On_Hold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Change Of Ownership</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Loan Repaid,Loan Written Off</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Late Notification CHOWN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Smart_Account__c</field>
            <operation>equals</operation>
            <value>Requested App Access,App Enabled,TSA Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Active_Loans__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>SF-1200, SF-1330</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CHOWN - Set Status CHOWN Application</fullName>
        <actions>
            <name>Set_Status_CHOWN_Application</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Change Of Ownership</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>notEqual</operation>
            <value>Late Notification CHOWN</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Initiate CHOWN Process</value>
        </criteriaItems>
        <description>SF-1200, SF-1330</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Call subject update</fullName>
        <actions>
            <name>Call_subject_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Support Call</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Subject%3A AMEX</fullName>
        <actions>
            <name>Field_Update_AMEX_Case_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates case subject for AMEX merchants</description>
        <formula>AND(OR(ISPICKVAL(Reason,&apos;New Shipment&apos;),ISPICKVAL(Reason,&apos;RMA Shipment&apos;)),ISPICKVAL(Account.Channel__c,&apos;Amex&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Subject%3A GOODSONS</fullName>
        <actions>
            <name>Field_Update_GOODSONS_Case_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Updates case subject for GOODSONS merchants</description>
        <formula>ISPICKVAL(Account.Channel__c, &quot;Goodson Imports&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Subject%3A MEDICAL</fullName>
        <actions>
            <name>Field_Update_MEDICAL_Case_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates case subject for MEDICAL merchants</description>
        <formula>AND(OR(ISPICKVAL(Reason,&apos;New Shipment&apos;),ISPICKVAL(Reason,&apos;RMA Shipment&apos;)),OR(ISPICKVAL(Account.Channel__c,&apos;HCN - After 27-Feb-2012&apos;),ISPICKVAL(Account.Channel__c,&apos;HCN - Pre 27-Feb-2012 (DO NOT ADD NEW SITES TO THIS CHANNEL)&apos;),Account.Integration_Product__r.Manufacturer_Account__c==&apos;HCN&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Subject%3A MYOB</fullName>
        <actions>
            <name>Field_Update_MYOB_Case_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Updates case subject for MYOB merchants</description>
        <formula>AND(OR(ISPICKVAL(Reason,&apos;New Shipment&apos;),ISPICKVAL(Reason,&apos;RMA Shipment&apos;)),OR(ISPICKVAL(Account.Channel__c,&apos;MYOB&apos;),Account.Integration_Product__r.Manufacturer_Account__c==&apos;MYOB&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case now owned</fullName>
        <actions>
            <name>Case_now_owned</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>CS Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
            <value>cs@tyro.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Escalated,New,On Hold,Shipped,Closed,Update</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Assign CS requests from Partner Portal to CS Requests queue</fullName>
        <actions>
            <name>Assign_Case_To_CS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Partner Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CS Request</value>
        </criteriaItems>
        <description>Neo -744 Partner portal</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Assign Dev Access Case to Integration Support</fullName>
        <actions>
            <name>Email_Alert_Send_notification_of_Development_portal_access_request</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Update_case_owner_to_Integration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Integration Support</value>
        </criteriaItems>
        <description>NEO-744 Assign Development portal access requests cases to the Integration Support queue and send notification email</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Send Termination Request Notification to AM when new request is created %28Account Manager  NOT correct%29</fullName>
        <actions>
            <name>Case_Termination_request_notification_to_am_email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2)AND(3 OR 4) AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Call_Category__c</field>
            <operation>equals</operation>
            <value>MID Termination</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>MID Termination</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Call_Termination_Reason__c</field>
            <operation>equals</operation>
            <value>CBA Albert,Change Of Ownership - Business Lost,Change Of Ownership - Business Retained,Change Of POS - Business Lost,Complexity Of Usage,Difficulty Of Installation,Dissatisfaction With Settlement Delay,Existing Banking Relationship Or Bank Bundling,Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Termination_Reason__c</field>
            <operation>equals</operation>
            <value>CBA Albert,Change Of Ownership - Business Lost,Change Of Ownership - Business Retained,Change Of POS - Business Lost,Complexity Of Usage,Difficulty Of Installation,Dissatisfaction With Settlement Delay,Existing Banking Relationship Or Bank Bundling,Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>notEqual</operation>
            <value>Gary Stewart,Emma Lambert,Stephanie Iacono,Alexander Waskiewicz,Clariza Pearce,Julian Montesinos,Liam Gate-Coker,Owen Behaettin,Natasha Hansen</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <description>Case: Send Termination Request Notification to AM when new request is created, and the account owner is NOT correctly assigned</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Send Termination Request Notification to AM when new request is created %28Account Manager correct%29</fullName>
        <actions>
            <name>Case_Termination_request_notification_to_AM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2)AND(3 OR 4) AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Call_Category__c</field>
            <operation>equals</operation>
            <value>MID Termination</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>MID Termination</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Call_Termination_Reason__c</field>
            <operation>equals</operation>
            <value>CBA Albert,Change Of Ownership - Business Lost,Change Of Ownership - Business Retained,Change Of POS - Business Lost,Complexity Of Usage,Difficulty Of Installation,Dissatisfaction With Settlement Delay,Existing Banking Relationship Or Bank Bundling,Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Termination_Reason__c</field>
            <operation>equals</operation>
            <value>CBA Albert,Change Of Ownership - Business Lost,Change Of Ownership - Business Retained,Change Of POS - Business Lost,Complexity Of Usage,Difficulty Of Installation,Dissatisfaction With Settlement Delay,Existing Banking Relationship Or Bank Bundling,Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>equals</operation>
            <value>Gary Stewart,Emma Lambert,Stephanie Iacono,Alexander Waskiewicz,Clariza Pearce,Julian Montesinos,Liam Gate-Coker,Owen Behaettin,Natasha Hansen</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <description>Case: Send Termination Request Notification to AM when new request is created, and the account owner is correct Account Manager</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Send Termination Request form to merchant after termination request case is saved</fullName>
        <actions>
            <name>Case_Send_Termination_form_to_Merchant_after_Termination_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Call_Reason__c</field>
            <operation>equals</operation>
            <value>Termination Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>MID Termination</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_Termination_Request_Form__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>SF-968 Termination workflow process
This workflow rule evaluates if a new case for termination request has been created with a tick in the &quot;send termination request form to merchant&quot; and proceeds to send the form to the merchant</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Terminal Returns</fullName>
        <actions>
            <name>Field_Update_Terminal_Returns_Priority</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Terminal Returns</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Update shipment date when Paper order status is set to %22Shipped%22</fullName>
        <actions>
            <name>FU_Update_Case_Shipment_Date_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Aliance XENTA Paper</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Shipped</value>
        </criteriaItems>
        <description>SF-1204 Updates shipment date field when case status is set to &quot;Shipped&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Convert CS Request To CHOWN Case</fullName>
        <actions>
            <name>CHOWN_Notify_AM</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_Case_To_AM_Chown_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Status_To_New</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Record_Type_CHOWN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CS Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>CHOWN,Late Notification CHOWN</value>
        </criteriaItems>
        <description>SF-1200

Converts a CS Request into a Change Of Ownership Case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Delete voicmail</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.ApexMarker__c</field>
            <operation>equals</operation>
            <value>Contains voicemail</value>
        </criteriaItems>
        <description>delete voicemail attachments</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Delete_voicmail</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Email%3A CS Request Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Case_Resume_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CS_Reminder_Due</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Case_Resume_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Email%3A Case assigned notification to merchant</fullName>
        <actions>
            <name>Case_Case_assigned_notification_to_merchant</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>SF-565 Notification to merchant that case has been assigned to a user different to the default Queue
The workflow rule evaluates if the status is &quot;New&quot;, the record type is &quot;CS request&quot; and the origin &quot;Email.</description>
        <formula>AND(ISPICKVAL( Origin , &quot;Email&quot;), RecordType.Name =&quot;CS Request&quot;, ISCHANGED( OwnerId ),OwnerId  &lt;&gt; &quot;00G20000001Dmay&quot;,  PRIORVALUE(OwnerId)=&quot;00G20000001Dmay&quot;,  ISPICKVAL( Status , &quot;New&quot;),NOT(CONTAINS(SuppliedEmail,&quot;@tyro.com&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A FS Case assigned notification to merchant</fullName>
        <actions>
            <name>Case_FS_Case_assigned_notification_to_merchant</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>SF-920 Implement email to case for FS@tyro.com (FS-Cases)

The workflow rule evaluates if the status is &quot;New&quot;, the record type is &quot;CS request&quot; and the origin &quot;FS Email.</description>
        <formula>AND(ISPICKVAL( Origin , &quot;FS Email&quot;), RecordType.Name =&quot;CS Request&quot;, ISCHANGED( OwnerId ),OwnerId  &lt;&gt; &quot;00GD0000005Y0J3&quot;,  PRIORVALUE(OwnerId)=&quot;00GD0000005Y0J3&quot;,  ISPICKVAL( Status , &quot;New&quot;),NOT(CONTAINS(SuppliedEmail,&quot;@tyro.com&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A NBG case notification</fullName>
        <actions>
            <name>CasenotificationforNBG</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>National Billing Group - Cabfare</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Escalated,New</value>
        </criteriaItems>
        <description>notifies NBG when a case is raised.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Web to case - Acknowledgement</fullName>
        <actions>
            <name>WTC_Acknowledgement</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Account update,Billing related,General question,Troubleshooting</value>
        </criteriaItems>
        <description>SF 566 - Acknowledgement email to customer after a case is created using the online web form</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Web to case - Acknowledgement for paper orders</fullName>
        <actions>
            <name>WTC_Acknowledgement_for_paper_orders</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Paper order</value>
        </criteriaItems>
        <description>SF 566 - Acknowledgement email to customer after a case for paper order is created using the online web form</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FU - Auto Approve refunds %3C %24200</fullName>
        <actions>
            <name>FU_Set_Approval_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Approved_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Status_To_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Finance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Credit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Charge_for__c</field>
            <operation>equals</operation>
            <value>Refund Merchant Service Fees - Revised Rates Backdated,Refund Terminal Rental</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Debit_Credit_Amount__c</field>
            <operation>lessThan</operation>
            <value>200</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>SF-541 

Sets Approved to True where refund amount is less than $200</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Reset Subject</fullName>
        <actions>
            <name>FU_Reset_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Case_Resume_Date_To_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>startsWith</operation>
            <value>ASSET ON HOLD FOR 2 DAYS -</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Shipped</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Shipment</value>
        </criteriaItems>
        <description>SF-569

Sets Subject back to original state when Case status set to Shipped</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Approval Fields To NULL</fullName>
        <actions>
            <name>FU_Set_Approval_Date_To_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Approved_to_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Approver_Id_To_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Approvers_To_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Status_To_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-541

Sets Approval related fields to NULL when a new Finance Case is created in order to avoid pulling through Approval values where a Case is Cloned</description>
        <formula>ISNEW() &amp;&amp; Debit_Credit_Amount__c &gt;= 200 &amp;&amp;    NOT(ISPICKVAL( Charge_for__c,&apos;Refund Terminal Returned&apos;)) &amp;&amp; NOT(ISPICKVAL( Charge_for__c,&apos;Non-Return of Rented Terminal&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Approval Fields To NULL 2</fullName>
        <actions>
            <name>FU_Set_Approval_Date_To_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Approved_to_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Approvers_To_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Finance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed - Rejected</value>
        </criteriaItems>
        <description>SF-541

Sets Approval related fields to NULL where a refund request is declined</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Case Resume Date %2B 2 Days</fullName>
        <actions>
            <name>FU_Set_Case_Resume_2_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Status_On_Hold</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Update_Case_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Follow_Up_HCS_Shipment_On_Hold</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>SF-569

This Workflow rule has been deactivated as the On HOld is not required anymore
SF-1185


Sets Case Resume Date, updates Case Subject, updates Case Status. 
Sends Email alert 2 days later</description>
        <formula>ISNEW()
&amp;&amp;
RecordType.Name = &quot;Shipment&quot;
&amp;&amp;
ISPICKVAL(Status, &quot;New&quot;)
&amp;&amp;
ISPICKVAL(Reason, &quot;New Shipment&quot;)
&amp;&amp;
ISPICKVAL(Account.Channel__c, &quot;HCS (Paul Zele)&quot;)
&amp;&amp;
ISNULL(Account.Merchant_ID__c) = FALSE
&amp;&amp;
ISPICKVAL(Account.Merchant_Status__c,&quot;Boarding Completed&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Take Case Off Hold</fullName>
        <active>false</active>
        <description>SF-569

Sets Case Resume Date, updates Case Subject, updates Case Status. 
Sends Email alert 2 days later</description>
        <formula>(RecordType.Name = &quot;Shipment&quot; || RecordType.Name = &quot;test Shipment&quot;)  &amp;&amp;  ISPICKVAL(Reason, &quot;New Shipment&quot;)  &amp;&amp;  NOT(ISNULL(Merchant_ID_2__c)) &amp;&amp;  Merchant_ID_2__r.Channel__r.Name = &quot;HCS (Paul Zele)&quot;  &amp;&amp; ISPICKVAL(Merchant_ID_2__r.Status__c , &quot;Boarding Completed&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>FU_Reset_Subject</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>FU_Set_Case_Resume_Date_To_NULL</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>FU_Set_Status_To_New</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>FU%3A MYOB Shipment Content</fullName>
        <actions>
            <name>FU_MYOB_CASE_EXTRA_INFO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Shipped</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>MYOB</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU%3A Paper Order Delivery Address</fullName>
        <actions>
            <name>Case_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Delivery_Address_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Delivery_Address_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Delivery_Address_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Postcode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_State</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 AND 2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Aliance XENTA Paper</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Delivery_Address_Line_1__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Delivery_Address__c</field>
            <operation>equals</operation>
            <value>Trading Address</value>
        </criteriaItems>
        <description>https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU%3A Terminal Delivery Address</fullName>
        <actions>
            <name>Set_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Delivery_Address_Line_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Delivery_Address_Line_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Delivery_Address_Line_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Postcode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_State</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>(RecordType.Name = &quot;Shipment&quot; || RecordType.Name = &quot;Aliance XENTA Paper&quot;) &amp;&amp; NOT(ISNULL(Location__c)) &amp;&amp; Alternate_Delivery_Address__c = FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Delivery Address</fullName>
        <actions>
            <name>Case_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Delivery_Address_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Delivery_Address_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Delivery_Address_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Postcode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_State</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 AND 2 AND 4) OR (3 AND 2 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Shipment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Delivery_Address_Line_1__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Aliance XENTA Paper</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Delivery_Address__c</field>
            <operation>equals</operation>
            <value>Trading Address</value>
        </criteriaItems>
        <description>De-activated as part of SCV

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Delivery Address - Mailing</fullName>
        <actions>
            <name>Mailing_Address_Line_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Mailing_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Mailing_Postcode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Mailing_State</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Shipment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Delivery_Address_Line_1__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>New Shipment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Delivery_Address__c</field>
            <operation>equals</operation>
            <value>Mailing Address</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Outage</fullName>
        <actions>
            <name>Outage_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Outage__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>SF-316</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Owned Assets in case</fullName>
        <actions>
            <name>Owned_Assets_in_case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Account.Ownership__c</field>
            <operation>equals</operation>
            <value>Owned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Own_Assests__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A RMA Priority 1</fullName>
        <actions>
            <name>RMA_Priority_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR (1 AND 3)OR (1 AND 4)OR (1 AND 5)</booleanFilter>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>RMA Shipment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Company_Name__c</field>
            <operation>contains</operation>
            <value>Mecca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Trading_Name__c</field>
            <operation>contains</operation>
            <value>Mecca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Company_Name__c</field>
            <operation>contains</operation>
            <value>Kit Cosmetics</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Trading_Name__c</field>
            <operation>contains</operation>
            <value>Kit Cosmetics</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A RMA Priority 2</fullName>
        <actions>
            <name>RMA_Priority_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>RMA Shipment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>Mecca</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Status Changed On</fullName>
        <actions>
            <name>changed_on</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ischanged( Status )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A Loaner terminal to be returned</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Loaner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>35</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Case_to_be_actioned_now</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please action the related case today.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Case.Case_Resume_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Case to be actioned now</subject>
    </tasks>
    <tasks>
        <fullName>Follow_Up_HCS_Shipment_On_Hold</fullName>
        <assignedToType>owner</assignedToType>
        <description>Case needs to be taken off Hold and shipment actioned</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Follow Up HCS Shipment On Hold</subject>
    </tasks>
</Workflow>
