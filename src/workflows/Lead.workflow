<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Tyro_Introduction</fullName>
        <ccEmails>emailtosalesforce@mvx2c8bzibap6jsjvxco3zq3kajm22ylhp0besg6oa1tym93v.2-1e2ueau.eu1.le.salesforce.com</ccEmails>
        <description>Email: Tyro Introduction</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales/Generic_Tyro_Introduction_Decision_Maker</template>
    </alerts>
    <alerts>
        <fullName>Email_Tyro_Introduction_Log_As_Activity</fullName>
        <ccEmails>emailtosalesforce@mvx2c8bzibap6jsjvxco3zq3kajm22ylhp0besg6oa1tym93v.2-1e2ueau.eu1.le.salesforce.com</ccEmails>
        <description>Email: Tyro Introduction (Log As Activity)</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Sales/Generic_Tyro_Introduction_2</template>
    </alerts>
    <alerts>
        <fullName>Fintech_Hub_Co_Dev_Application_Alert</fullName>
        <description>Fintech Hub Co-Dev Application Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>vluu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Tyro_Fintech_Hub/New_Co_Dev_Application</template>
    </alerts>
    <alerts>
        <fullName>Lead_Notification_of_new_Lead_from_Partner_Portal</fullName>
        <description>Email:  Notification of new Lead from Partner Portal</description>
        <protected>false</protected>
        <recipients>
            <field>Channel_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>dnahma@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales/Notification_of_New_EFTPOS_Lead_from_Partner_Portal</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_Lead_To_MPS_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Webform_MPS_Web_Leads</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Lead To MPS Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Lead_To_Pharmacy_Queue</fullName>
        <description>https://confluence.tyro.com/pages/viewpage.action?pageId=37896326</description>
        <field>OwnerId</field>
        <lookupValue>Web_Form_Pharmacy_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Lead To Pharmacy Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Call_Opt_Out_Date_NULL</fullName>
        <description>SF-469, SF-639 

Sets Call Opt Out Date to NULL</description>
        <field>Communication_Opt_Out_date__c</field>
        <name>FU - Set Call Opt Out Date - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Call_Opt_Out_Date_TODAY</fullName>
        <description>SF-469, SF-639

Sets the Call Opt Out Date to TODAY()</description>
        <field>Communication_Opt_Out_date__c</field>
        <formula>TODAY()</formula>
        <name>FU - Set Call Opt Out Date - TODAY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Dead_Lead_Record_Type</fullName>
        <description>SF-469

Sets Record Type to Dead Lead (required so that records are not synchronised with Pardot)</description>
        <field>RecordTypeId</field>
        <lookupValue>Dead_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>FU - Set Dead Lead Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Do_Not_Call_FALSE</fullName>
        <description>SF-469

Sets Do Not Call to  FALSE</description>
        <field>DoNotCall</field>
        <literalValue>0</literalValue>
        <name>FU - Set Do Not Call - FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Do_Not_Call_TRUE</fullName>
        <description>SF-469

Sets Do Not Call to TRUE</description>
        <field>DoNotCall</field>
        <literalValue>1</literalValue>
        <name>FU - Set Do Not Call - TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Email_Opt_Out_Date_NULL</fullName>
        <description>SF-639</description>
        <field>Email_Opt_Out_Date__c</field>
        <name>FU - Set Email Opt Out Date - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Email_Opt_Out_Date_TODAY</fullName>
        <description>SF-639</description>
        <field>Email_Opt_Out_Date__c</field>
        <formula>TODAY()</formula>
        <name>FU - Set Email Opt Out Date - TODAY</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Email_Opt_Out_To_FALSE</fullName>
        <description>SF-469

Sets Email Opt Out to FALSE</description>
        <field>HasOptedOutOfEmail</field>
        <literalValue>0</literalValue>
        <name>FU - Set Email Opt Out To FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Email_Opt_Out_To_TRUE</fullName>
        <description>SF-469

Sets Email Opt Out to TRUE</description>
        <field>HasOptedOutOfEmail</field>
        <literalValue>1</literalValue>
        <name>FU - Set Email Opt Out To TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_FTH_Co_Dev_Lead_To_Y</fullName>
        <description>SF-568</description>
        <field>Fintech_Co_Dev_Lead__c</field>
        <formula>&quot;Y&quot;</formula>
        <name>FU - Set FTH Co Dev Lead To Y</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Integration_Product_Name</fullName>
        <description>SF-469</description>
        <field>Integration_Product_Name__c</field>
        <formula>Integration_Product__r.Name</formula>
        <name>FU - Set Integration Product Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Last_Lead_Owner</fullName>
        <description>SF-469

Sets last Lead Owner</description>
        <field>Last_Owner__c</field>
        <formula>Owner:User.FirstName &amp; &quot; &quot; &amp; Owner:User.LastName</formula>
        <name>FU - Set Last Lead Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Last_Owner_NULL</fullName>
        <description>SF-469

Sets Last Owner to NULL</description>
        <field>Last_Owner__c</field>
        <name>FU - Set Last Owner - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Lead_Owner_DnB_Cold_Queue</fullName>
        <description>SF-905</description>
        <field>OwnerId</field>
        <lookupValue>DnB_Cold</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>FU - Set Lead Owner - DnB Cold Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Lead_Owner_DnB_Dead_Leads</fullName>
        <description>SF-905</description>
        <field>OwnerId</field>
        <lookupValue>DnB_Dead_Leads</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>FU - Set Lead Owner - DnB Dead Leads</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Lead_Owner_To_Recycle_Tyro</fullName>
        <description>SF-905</description>
        <field>OwnerId</field>
        <lookupValue>Recycle_Tyro</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>FU - Set Lead Owner To Recycle - Tyro</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Lead_Owner_to_Dead_Lead_Queue</fullName>
        <description>SF-469

Transfers Lead to the Dead Lead Queue when Lead Status is changed to &apos;Dead Lead&apos;</description>
        <field>OwnerId</field>
        <lookupValue>Dead_Leads</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>FU - Set Lead Owner to Dead Lead Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Lead_Owner_to_DnB_Health_Queue</fullName>
        <description>SF-905</description>
        <field>OwnerId</field>
        <lookupValue>DnB_Health_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>FU - Set Lead Owner to DnB Health Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Lead_Owner_to_DnB_Hospitality_Q</fullName>
        <description>SF-905</description>
        <field>OwnerId</field>
        <lookupValue>DnB_Hospitality_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>FU - Set Lead Owner to DnB Hospitality Q</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Lead_Owner_to_DnB_Retail_Queue</fullName>
        <description>SF-905</description>
        <field>OwnerId</field>
        <lookupValue>DnB_Retail_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>FU - Set Lead Owner to DnB Retail Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Lead_Owner_to_Do_Not_Call_Queue</fullName>
        <description>SF-469

Sets Lead Owner to Do Not Call / Email queue</description>
        <field>OwnerId</field>
        <lookupValue>Do_Not_Call</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>FU - Set Lead Owner to Do Not Call Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Lead_Owner_to_Marketing_Queue</fullName>
        <description>SF-469

Sets Lead Owner to Marketing queue</description>
        <field>OwnerId</field>
        <lookupValue>Marketing</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>FU - Set Lead Owner to Marketing Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Lead_Record_Type_Unsolicited</fullName>
        <description>NEO-615</description>
        <field>RecordTypeId</field>
        <lookupValue>Merchant_Lead_Unsolicited</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>FU - Set Lead Record Type - Unsolicited</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Lead_Status_Cold</fullName>
        <description>SF-905</description>
        <field>Status</field>
        <literalValue>Cold</literalValue>
        <name>FU - Set Lead Status - Cold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Lead_Status_Lead_Gen_Complete</fullName>
        <description>SF-905</description>
        <field>Status</field>
        <literalValue>Lead Gen: Complete</literalValue>
        <name>FU - Set Lead Status - Lead Gen Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Lead_Status_Recycle</fullName>
        <description>SF-905</description>
        <field>Status</field>
        <literalValue>Recycle</literalValue>
        <name>FU - Set Lead Status - Recycle</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Record_Redist_Queue_ID_NULL</fullName>
        <description>SF=1026</description>
        <field>Record_Distribution_Override_Queue_Id__c</field>
        <formula>&quot;--None--&quot;</formula>
        <name>FU - Set Record Redist. Queue ID - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Record_Type_To_Fintech</fullName>
        <description>SF-568</description>
        <field>RecordTypeId</field>
        <lookupValue>Fintech_Hub_Co_Dev</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>FU - Set Record Type To Fintech</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Revisit_Counter_NULL</fullName>
        <description>SF-905</description>
        <field>Revisit_Counter__c</field>
        <name>FU - Set Revisit Counter - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Revisit_Counter_To_0</fullName>
        <description>SF-905</description>
        <field>Revisit_Counter__c</field>
        <formula>0</formula>
        <name>FU - Set Revisit Counter To 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Revisit_Date_1_Year</fullName>
        <description>SF-469

Sets Revisit Date 1 year from TODAY()</description>
        <field>Revisit_Date__c</field>
        <formula>TODAY() + 365</formula>
        <name>FU - Set Revisit Date 1 Year</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Revisit_Date_87_Days</fullName>
        <description>SF-905</description>
        <field>Revisit_Date__c</field>
        <formula>TODAY() + 87</formula>
        <name>FU - Set Revisit Date - 87 Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Revisit_Date_Tomorrow</fullName>
        <field>Revisit_Date__c</field>
        <formula>TODAY() + 1</formula>
        <name>FU - Set Revisit Date - Tomorrow</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Revisit_Date_to_NULL</fullName>
        <description>SF-469

Sets Revisit Date to NULL</description>
        <field>Revisit_Date__c</field>
        <name>FU - Set Revisit Date to NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Status_Reason_Do_Not_Call</fullName>
        <description>SF-469</description>
        <field>Status_Reason__c</field>
        <literalValue>Do Not Call / Email</literalValue>
        <name>FU - Set Status Reason - Do Not Call</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Status_to_Do_Not_Contact</fullName>
        <description>SF-469

Sets Status to Marketing</description>
        <field>Status</field>
        <literalValue>Do Not Contact</literalValue>
        <name>FU - Set Status to Do Not Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Status_to_Marketing</fullName>
        <description>SF-469

Sets Status to Marketing</description>
        <field>Status</field>
        <literalValue>Marketing</literalValue>
        <name>FU - Set Status to Marketing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Trading_Name</fullName>
        <description>SF-469

Sets Trading Name when a web lead is created</description>
        <field>Trading_Name__c</field>
        <formula>Company</formula>
        <name>FU - Set Trading Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Contact_Form_POS_Product</fullName>
        <field>Contact_form_POS_PMS_system__c</field>
        <formula>Integration_Product__r.Name</formula>
        <name>FU - Update Contact Form POS Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Mobile_1</fullName>
        <field>MobilePhone</field>
        <formula>LEFT(MobilePhone, 2) 
&amp; &quot; &quot; 
&amp; MID(MobilePhone, 3, 4) 
&amp; &quot; &quot; 
&amp; RIGHT(MobilePhone, 4)</formula>
        <name>FU - Update Mobile  - 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Mobile_2</fullName>
        <field>MobilePhone</field>
        <formula>LEFT(MobilePhone, 4) 
&amp; &quot; &quot; 
&amp; MID(MobilePhone, 5, 3) 
&amp; &quot; &quot; 
&amp; RIGHT(MobilePhone, 3)</formula>
        <name>FU - Update Mobile - 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Phone_1</fullName>
        <field>Phone</field>
        <formula>LEFT(Phone, 2)
&amp; &quot; &quot;
&amp; MID(Phone, 3, 4)
&amp; &quot; &quot;
&amp; RIGHT(Phone, 4)</formula>
        <name>FU - Update Phone - 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Phone_2</fullName>
        <field>Phone</field>
        <formula>LEFT(Phone, 4) 
&amp; &quot; &quot; 
&amp; MID(Phone, 5, 3) 
&amp; &quot; &quot; 
&amp; RIGHT(Phone, 3)</formula>
        <name>FU - Update Phone - 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_State</fullName>
        <description>SF-469

Updates State on standard address field</description>
        <field>State</field>
        <formula>TEXT(State__c)</formula>
        <name>FU: Update State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Status_Reason</fullName>
        <description>SF-469

Sets Status Reason to &apos;Was On Do Not Call List&apos;</description>
        <field>Status_Reason__c</field>
        <literalValue>Was On Do Not Call List</literalValue>
        <name>FU - Update Status Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Status_Reason_Do_Not_Call</fullName>
        <description>SF-469

Updates status reason to Do Not Call / Email</description>
        <field>Status_Reason__c</field>
        <literalValue>Do Not Call</literalValue>
        <name>FU - Update Status Reason - Do Not Call</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>First_Action_Date_Time</fullName>
        <description>SF-436</description>
        <field>First_Action_Date_Time__c</field>
        <formula>now()</formula>
        <name>First Action Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_ABN_Format</fullName>
        <description>Sets ABN format as 00 000 000 000</description>
        <field>ABN_ABRN__c</field>
        <formula>LEFT(ABN_ABRN__c, 2)
&amp; &quot; &quot; 
&amp; MID(ABN_ABRN__c, 3, 3)  
&amp; &quot; &quot;
&amp; MID(ABN_ABRN__c, 6, 3)  
&amp; &quot; &quot;
&amp; RIGHT(ABN_ABRN__c, 3)</formula>
        <name>Set ABN Format</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Channel_Override_TRUE</fullName>
        <description>NEO-825</description>
        <field>Channel_Override__c</field>
        <literalValue>1</literalValue>
        <name>Set Channel Override - TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Merchant_Statement_Value_To_0</fullName>
        <field>Monthly_Transaction_Volume__c</field>
        <formula>0</formula>
        <name>Set Merchant Statement Value To $0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reason_For_Remove_From_DNCR_NULL</fullName>
        <description>Sets Reason For Removal From Do Not Call Register To NULL</description>
        <field>Reason_For_Removal_From_DNCR__c</field>
        <name>Set Reason For Remove From DNCR - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_Merchant_Lead</fullName>
        <description>NEO-615</description>
        <field>RecordTypeId</field>
        <lookupValue>Tyro_Merchant</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type - Merchant Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_MQL</fullName>
        <description>Sets Status to Marketing Qualified Lead</description>
        <field>Status</field>
        <literalValue>Marketing Qualified Lead</literalValue>
        <name>Set Status = MQL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Reason_None</fullName>
        <description>Sets Status Reason to --None--</description>
        <field>Status_Reason__c</field>
        <literalValue>--None--</literalValue>
        <name>Set Status Reason - None</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Reason_None_2</fullName>
        <field>Status_Reason__c</field>
        <literalValue>--None--</literalValue>
        <name>Set Status Reason - &quot;--None--&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Transfer_Lead_To_Abandoned_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>EFTPOS_Refresh_Sole_Trader_Abandoned</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Transfer Lead To Abandoned Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Transfer_Lead_To_EFTPOS_Refresh_ST_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>EFTPOS_Refresh_Sole_Trader_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Transfer Lead To EFTPOS Refresh ST Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_lead_desc</fullName>
        <field>Description</field>
        <formula>&quot;the workflow rule works&quot;</formula>
        <name>update lead desc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Assign Web Lead To MPS Queue</fullName>
        <actions>
            <name>Assign_Lead_To_MPS_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3) AND (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Lead.Integration_Product_Name__c</field>
            <operation>equals</operation>
            <value>WaiterMate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Integration_Product_Name__c</field>
            <operation>equals</operation>
            <value>a0J2000000094DIEAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Contact_form_POS_PMS_system__c</field>
            <operation>equals</operation>
            <value>WaiterMate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Source__c</field>
            <operation>equals</operation>
            <value>Digital Marketing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Digital Marketing</value>
        </criteriaItems>
        <description>Assigns web leads to MPS queue

SF-727</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Web Lead To Pharmacy Queue</fullName>
        <actions>
            <name>Assign_Lead_To_Pharmacy_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Lead.Industry</field>
            <operation>equals</operation>
            <value>Retail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sub_Industry__c</field>
            <operation>equals</operation>
            <value>Pharmacy</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Source__c</field>
            <operation>equals</operation>
            <value>Web Lead</value>
        </criteriaItems>
        <description>Assigns web leads to Pharmacy queue

https://confluence.tyro.com/pages/viewpage.action?pageId=37896326</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EFTPOS Refresh Sole Trader - FC Complete</fullName>
        <actions>
            <name>Transfer_Lead_To_EFTPOS_Refresh_ST_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Campaign_Code__c</field>
            <operation>equals</operation>
            <value>EFTPOS Refresh | Sole Trader | Apps Online</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.FC_Complete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Moves EFTPOS Refresh Sole Trader leads that have completed the online application into queue for onboarding</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Fintech Hub Web Lead Alert</fullName>
        <actions>
            <name>Fintech_Hub_Co_Dev_Application_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Fintech Hub - Co Dev</value>
        </criteriaItems>
        <description>SF-568

Sends an alert email to stakeholders when a new Fintech Hub Co-Dev application is submitted</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FU - Mobile Number Format - 1</fullName>
        <actions>
            <name>FU_Update_Mobile_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Enforces mobile number format</description>
        <formula>(BEGINS(MobilePhone, &quot;02&quot;)  || BEGINS(MobilePhone, &quot;03&quot;)  || BEGINS(MobilePhone, &quot;07&quot;)  || BEGINS(MobilePhone, &quot;08&quot;))  &amp;&amp; LEN(MobilePhone) = 10</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Mobile Number Format - 2</fullName>
        <actions>
            <name>FU_Update_Mobile_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Enforces mobile phone number format</description>
        <formula>(BEGINS(MobilePhone, &quot;1300&quot;)  || BEGINS(MobilePhone, &quot;1800&quot;)  || BEGINS(MobilePhone, &quot;04&quot;))  &amp;&amp; LEN(MobilePhone) = 10</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Phone Number Format - 1</fullName>
        <actions>
            <name>FU_Update_Phone_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Enforces phone number format</description>
        <formula>(BEGINS(Phone, &quot;02&quot;)  || BEGINS(Phone, &quot;03&quot;)  || BEGINS(Phone, &quot;07&quot;)  || BEGINS(Phone, &quot;08&quot;))  &amp;&amp; LEN(Phone) = 10</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Phone Number Format - 2</fullName>
        <actions>
            <name>FU_Update_Phone_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Enforces phone number format</description>
        <formula>(BEGINS(Phone, &quot;1300&quot;)  || BEGINS(Phone, &quot;1800&quot;)  || BEGINS(Phone, &quot;04&quot;)) &amp;&amp; LEN(Phone) = 10</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Call Opt Outs - FALSE</fullName>
        <actions>
            <name>FU_Set_Call_Opt_Out_Date_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Do_Not_Call_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Last_Owner_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Lead_Owner_to_Marketing_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Revisit_Date_to_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>SF-469, SF-639, SF-643

Sets Call Opt Out values to NULL</description>
        <formula>(RecordType.Name = &quot;Merchant Lead&quot;  ||RecordType.Name = &quot;Merchant Lead - Unsolicited&quot;)  &amp;&amp; NOT(ISPICKVAL(Status_Reason__c, &quot;Do Not Call&quot;))  &amp;&amp; (Owner:Queue.QueueName = &quot;Do Not Call&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Communication Opt Outs - TRUE</fullName>
        <actions>
            <name>FU_Set_Call_Opt_Out_Date_TODAY</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Do_Not_Call_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Email_Opt_Out_To_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Last_Lead_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Lead_Owner_to_Do_Not_Call_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Revisit_Date_1_Year</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Status_to_Do_Not_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Status_to_Marketing</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Update_Status_Reason_Do_Not_Call</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>SF-469

Sets Do Not Call &amp; Email Opt Out to TRUE, sets Communication Opt Out Date to TODAY(), sets Lead Owner to Do Not Call / Email Queue, sets Status Reason to Do Not Call / Email</description>
        <formula>(ISPICKVAL(Status_Reason__c, &quot;Do Not Call / Email&quot;) || DoNotCall = TRUE || HasOptedOutOfEmail = TRUE) &amp;&amp;  NOT(CONTAINS(Owner:User.UserRole.Name,&quot;ISO - GWS&quot;))  &amp;&amp;  NOT(CONTAINS(Owner:User.UserRole.Name,&quot;ISO - HCS&quot;))  &amp;&amp;  NOT(CONTAINS(Owner:User.UserRole.Name,&quot;ISO - MPS&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Email Opt Out Date</fullName>
        <actions>
            <name>FU_Set_Email_Opt_Out_Date_TODAY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-639 

Sets Email Opt Out Date to TODAY()</description>
        <formula>HasOptedOutOfEmail = TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Email Opt Out Date To NULL</fullName>
        <actions>
            <name>FU_Set_Email_Opt_Out_Date_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-639 

Sets Email Opt Out Date to NULL</description>
        <formula>ISCHANGED(HasOptedOutOfEmail)  &amp;&amp;  HasOptedOutOfEmail = FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Integration Product Name</fullName>
        <actions>
            <name>FU_Set_Integration_Product_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Update_Contact_Form_POS_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>SF-469

Updates Integration Product Name With name of related Integration Product</description>
        <formula>NOT(ISNEW()) &amp;&amp; ISNULL(Integration_Product__c) = FALSE  &amp;&amp; IsConverted = FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Last Owner - NULL</fullName>
        <actions>
            <name>FU_Set_Last_Owner_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>notEqual</operation>
            <value>Marketing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>notEqual</operation>
            <value>Recycle - GWS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>notEqual</operation>
            <value>Recycle - HCS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>notEqual</operation>
            <value>Recycle - MPS</value>
        </criteriaItems>
        <description>SF-469

Sets Last Owner to NULL</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Lead Owner to Dead Lead Queue</fullName>
        <actions>
            <name>FU_Set_Dead_Lead_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Lead_Owner_to_Dead_Lead_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Revisit_Counter_To_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Revisit_Date_to_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.Purchased_List_Source__c</field>
            <operation>notEqual</operation>
            <value>DnB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Disqualified Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Dead Lead</value>
        </criteriaItems>
        <description>SF-469

Assigns the Lead to the Dead Leads queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Lead Owner to DnB Cold Queue</fullName>
        <actions>
            <name>FU_Set_Revisit_Date_87_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>SF-905

Assigns the Lead to the Marketing queue</description>
        <formula>(RecordType.Name = &quot;Merchant Lead&quot;  || RecordType.Name = &quot;Merchant Lead - Unsolicited&quot;)  &amp;&amp; ISPICKVAL(Status,&quot;Marketing&quot;) = FALSE &amp;&amp; ISPICKVAL(Status,&quot;Dead Lead&quot;) = FALSE &amp;&amp; DoNotCall = FALSE &amp;&amp; Email = &quot;&quot; &amp;&amp; ISPICKVAL(Purchased_List_Source__c, &quot;DnB&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Lead Owner to Do Not Call Queue</fullName>
        <actions>
            <name>FU_Set_Call_Opt_Out_Date_TODAY</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Do_Not_Call_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Last_Lead_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Lead_Owner_to_Do_Not_Call_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Status_to_Marketing</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Update_Status_Reason_Do_Not_Call</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>SF-469, SF-639, SF-643


Assigns the Lead to the Do Not Call queue</description>
        <formula>(RecordType.Name = &quot;Merchant Lead&quot;  ||RecordType.Name = &quot;Merchant Lead - Unsolicited&quot;)  &amp;&amp; ((ISPICKVAL(Status,&quot;Marketing&quot;)  &amp;&amp; ISPICKVAL(Status_Reason__c,&quot;Do Not Call&quot;)  || DoNotCall = TRUE)  &amp;&amp; NOT(CONTAINS(Owner_Role__c,&quot;GWS&quot;))  &amp;&amp; NOT(CONTAINS(Owner_Role__c,&quot;HCS&quot;))  &amp;&amp; NOT(CONTAINS(Owner_Role__c,&quot;MPS&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Lead Owner to Marketing Queue</fullName>
        <actions>
            <name>FU_Set_Last_Lead_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Lead_Owner_to_Marketing_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Status_to_Marketing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>SF-469, SF-639, SF-643, SF-905

Assigns the Lead to the Marketing queue</description>
        <formula>(RecordType.Name = &quot;Merchant Lead&quot;  ||RecordType.Name = &quot;Merchant Lead - Unsolicited&quot;)  &amp;&amp; (ISPICKVAL(Status,&quot;Recycle&quot;)  || ISPICKVAL(Status,&quot;Marketing&quot;)) &amp;&amp; NOT(ISPICKVAL(Status_Reason__c,&quot; Do Not Call&quot;))  &amp;&amp; DoNotCall = FALSE &amp;&amp; NOT(CONTAINS(Owner:User.UserRole.Name,&quot;ISO - GWS&quot;))  &amp;&amp; NOT(CONTAINS(Owner:User.UserRole.Name,&quot;ISO - HCS&quot;))  &amp;&amp; NOT(CONTAINS(Owner:User.UserRole.Name,&quot;ISO - MPS&quot;)) &amp;&amp; Email &lt;&gt; &quot;&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Lead Owner to Recycle Queue</fullName>
        <actions>
            <name>FU_Set_Lead_Owner_To_Recycle_Tyro</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Lead_Status_Recycle</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Revisit_Counter_To_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>SF-905

Assigns the Lead to the Recycle queue</description>
        <formula>(RecordType.Name = &quot;Merchant Lead&quot;  ||RecordType.Name = &quot;Merchant Lead - Unsolicited&quot;)  &amp;&amp; (ISPICKVAL(Status,&quot;Recycle&quot;)  || ISPICKVAL(Status,&quot;Marketing&quot;)) &amp;&amp; NOT(ISPICKVAL(Status_Reason__c,&quot; Do Not Call&quot;))  &amp;&amp; DoNotCall = FALSE &amp;&amp; NOT(CONTAINS(Owner:User.UserRole.Name,&quot;ISO - GWS&quot;))  &amp;&amp; NOT(CONTAINS(Owner:User.UserRole.Name,&quot;ISO - HCS&quot;))  &amp;&amp; NOT(CONTAINS(Owner:User.UserRole.Name,&quot;ISO - MPS&quot;)) &amp;&amp; Email = &quot;&quot; &amp;&amp; ISPICKVAL(Purchased_List_Source__c, &quot;DnB&quot;) = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Lead Queue Override Id - NULL</fullName>
        <actions>
            <name>FU_Set_Record_Redist_Queue_ID_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-1026

Sets Record Distribution Override Queue Id to NULL</description>
        <formula>BEGINS(Owner:User.Id, &quot;005&quot;)   &amp;&amp; BEGINS(Record_Distribution_Override_Queue_Id__c, &quot;00G&quot;) &amp;&amp; Record_Distribution_Override_Queue_Id__c &lt;&gt; &quot;00GD0000001dFjVMAU&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Lead Record Type - FTH</fullName>
        <actions>
            <name>FU_Set_FTH_Co_Dev_Lead_To_Y</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Record_Type_To_Fintech</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Web_Lead_Form__c</field>
            <operation>equals</operation>
            <value>Tyro Fintech Hub Visitor</value>
        </criteriaItems>
        <description>SF-568

Sets Lead Record Type to Fintech Hub</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Lead Record Type - Solicited</fullName>
        <actions>
            <name>Set_Record_Type_Merchant_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Quick Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web Chat</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>notEqual</operation>
            <value>Lead Gen: Partial,Lead Gen: Complete</value>
        </criteriaItems>
        <description>NEO-615

Sets Lead Record Type to Solicited</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Lead Record Type - Unsolicited</fullName>
        <actions>
            <name>FU_Set_Lead_Record_Type_Unsolicited</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Trading_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Quick Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>notEqual</operation>
            <value>Web Chat</value>
        </criteriaItems>
        <description>NEO-615

Sets Lead Record Type to Unsolicited</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set MSV to %240</fullName>
        <actions>
            <name>Set_Merchant_Statement_Value_To_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Digital Marketing,Web Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Source__c</field>
            <operation>equals</operation>
            <value>Digital Marketing,Web Lead</value>
        </criteriaItems>
        <description>Sets Merchant Statement Value to $0 when a Web Lead is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Revisit Date %2B90 Days</fullName>
        <actions>
            <name>FU_Set_Lead_Status_Recycle</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Revisit_Date_87_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>SF-905

Sets Revisit Date to TODAY() + 90</description>
        <formula>RecordType.Name = &quot;Merchant Lead - Unsolicited&quot; &amp;&amp; (ISPICKVAL(Status,&quot;Recycle&quot;)  || ISPICKVAL(Status,&quot;Marketing&quot;)) &amp;&amp; NOT(ISPICKVAL(Status_Reason__c,&quot; Do Not Call&quot;))  &amp;&amp; DoNotCall = FALSE &amp;&amp; Email = &quot;&quot; &amp;&amp; ISPICKVAL(LeadSource, &quot;Purchased List&quot;)   &amp;&amp; ISPICKVAL(Purchased_List_Source__c, &quot;DnB&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Revisit Date - 2 Days</fullName>
        <active>false</active>
        <description>SF-905

Sets revisit date to TODAY() + 2 if no email, address has been obtained</description>
        <formula>ISPICKVAL(LeadSource, &quot;Purchased List&quot;)  &amp;&amp; ISPICKVAL(Purchased_List_Source__c , &quot;DnB&quot;)  &amp;&amp; Email = &quot;&quot;  &amp;&amp; ISPICKVAL(Status, &quot;Lead Gen: Partial&quot;) &amp;&amp; (Revisit_Counter__c &lt; 6 || ISNULL(Revisit_Counter__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Status As MQL</fullName>
        <actions>
            <name>Set_Status_MQL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Digital Marketing,Web Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Source__c</field>
            <operation>equals</operation>
            <value>Digital Marketing,Web Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>notEqual</operation>
            <value>Marketing Qualified Lead</value>
        </criteriaItems>
        <description>Sets Lead Status to Marketing Qualified Lead</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Status Reason - None</fullName>
        <actions>
            <name>Set_Status_Reason_None</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Lead Gen: Partial,Lead Gen: Complete,Marketing Qualified Lead,Sales Accepted Lead,Sales Qualified Lead,Converted As Duplicate</value>
        </criteriaItems>
        <description>Sets Status Reason to --None-- to overcome a Pardot limitation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Trading Name</fullName>
        <actions>
            <name>FU_Set_Trading_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Merchant Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Digital Marketing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Source__c</field>
            <operation>equals</operation>
            <value>Digital Marketing</value>
        </criteriaItems>
        <description>SF-469, NEO-761

Sets Trading Name when a web lead is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FU - TFR Lead To DnB Dead Leads Queue</fullName>
        <actions>
            <name>FU_Set_Dead_Lead_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Lead_Owner_DnB_Dead_Leads</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Revisit_Counter_To_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Revisit_Date_to_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Lead.Purchased_List_Source__c</field>
            <operation>equals</operation>
            <value>DnB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Disqualified Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Dead Lead</value>
        </criteriaItems>
        <description>SF-905

Assigns the Lead to the dead lead queue</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU%3A Update State</fullName>
        <actions>
            <name>FU_Update_State</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>SF-469

Updates State on standard address field based on inputs from custom address fields</description>
        <formula>IsConverted = FALSE &amp;&amp; ISCHANGED(State__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A First action date%2Ftime</fullName>
        <actions>
            <name>First_Action_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>SF-436</description>
        <formula>ISPICKVAL(PRIORVALUE( Status ), &quot;open&quot;) &amp;&amp; ISCHANGED( Status )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Format ABN</fullName>
        <actions>
            <name>Set_ABN_Format</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Formats ABN as 00 000 000 000</description>
        <formula>LEN(ABN_ABRN__c) = 11</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LD%3A Send notification to Partner Managers when partner portal lead is submitted</fullName>
        <actions>
            <name>Lead_Notification_of_new_Lead_from_Partner_Portal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Partners</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Web_Lead_Form__c</field>
            <operation>equals</operation>
            <value>Partner Portal</value>
        </criteriaItems>
        <description>Send notification to Partner Managers when partner portal lead is submitted</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Call Opt Out Date</fullName>
        <actions>
            <name>FU_Set_Call_Opt_Out_Date_TODAY</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Reason_For_Remove_From_DNCR_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.DoNotCall</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Merchant Lead,Dead Lead,Merchant Lead - Unsolicited</value>
        </criteriaItems>
        <description>Sets Call Opt Out Date to TODAY()</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Channel Override - TRUE</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>NEO-825</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Channel_Override_TRUE</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Status Reason</fullName>
        <actions>
            <name>Set_Status_Reason_None_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Lead Gen: Partial,Lead Gen: Complete,Marketing Qualified Lead,Sales Accepted Lead,Sales Qualified Lead,Converted As Duplicate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LastModifiedById</field>
            <operation>equals</operation>
            <value>PARDOT INTEGRATION</value>
        </criteriaItems>
        <description>Sets Status Reason to &quot;--None--&quot; when updated by Pardot</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TFR DnB Lead To Marketing</fullName>
        <actions>
            <name>FU_Set_Lead_Owner_to_Marketing_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Revisit_Counter_To_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Revisit_Date_to_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>SF-905

Transfers a DnB Lead to the Marketing queue where the Lead is not sales ready and an email address has been obtained</description>
        <formula>ISPICKVAL(LeadSource, &quot;Purchased List&quot;)  &amp;&amp; ISPICKVAL(Purchased_List_Source__c , &quot;DnB&quot;)  &amp;&amp; Email &lt;&gt; &quot;&quot;  &amp;&amp; ISPICKVAL(Status, &quot;Marketing&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TFR Lead - DNB Health</fullName>
        <actions>
            <name>FU_Set_Lead_Owner_to_DnB_Health_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Revisit_Counter_To_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Revisit_Date_Tomorrow</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-905

Transfers the Lead to the DnB Health Queue</description>
        <formula>ISPICKVAL(LeadSource, &quot;Purchased List&quot;) &amp;&amp; ISPICKVAL(Purchased_List_Source__c , &quot;DnB&quot;) &amp;&amp; (ISPICKVAL(Industry, &quot;Health &amp; Beauty&quot;) || ISPICKVAL(Industry, &quot;Healthcare&quot;) || ISPICKVAL(Industry, &quot;Health Industry - Allied&quot;) || ISPICKVAL(Industry, &quot;Health Industry - Primary&quot;) || ISPICKVAL(Industry, &quot;Veterinary&quot;)) &amp;&amp; Email &lt;&gt; &quot;&quot; &amp;&amp; Phone &lt;&gt; &quot;&quot; &amp;&amp; Integration_Product__c &lt;&gt; &quot;&quot; &amp;&amp; Integration_Product__r.Name &lt;&gt; &quot;Unknown POS&quot; &amp;&amp; Integration_Product__r.Name &lt;&gt; &quot;Unknown POS Product&quot; &amp;&amp; Integration_Product__r.Name &lt;&gt; &quot;No POS Product&quot; &amp;&amp; RecordType.Name = &quot;Merchant Lead&quot; &amp;&amp; ISPICKVAL(Status, &quot;Lead Gen: Complete&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TFR Lead - DNB Hospitality</fullName>
        <actions>
            <name>FU_Set_Lead_Owner_to_DnB_Hospitality_Q</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Revisit_Counter_To_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Revisit_Date_Tomorrow</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-905

Transfers the Lead to the DnB Hospitality Queue</description>
        <formula>ISPICKVAL(LeadSource, &quot;Purchased List&quot;)  &amp;&amp; ISPICKVAL(Purchased_List_Source__c , &quot;DnB&quot;)  &amp;&amp; (ISPICKVAL(Industry, &quot;Food &amp; Beverage&quot;) || ISPICKVAL(Industry, &quot;Hospitality&quot;) || ISPICKVAL(Industry, &quot;Entertainment&quot;)) &amp;&amp; Email &lt;&gt; &quot;&quot; &amp;&amp; Phone &lt;&gt; &quot;&quot; &amp;&amp; Integration_Product__c &lt;&gt; &quot;&quot; &amp;&amp; Integration_Product__r.Name &lt;&gt; &quot;Unknown POS&quot; &amp;&amp; Integration_Product__r.Name &lt;&gt; &quot;Unknown POS Product&quot; &amp;&amp; Integration_Product__r.Name &lt;&gt; &quot;No POS Product&quot; &amp;&amp; RecordType.Name = &quot;Merchant Lead&quot; &amp;&amp; ISPICKVAL(Status, &quot;Lead Gen: Complete&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TFR Lead - DNB Retail</fullName>
        <actions>
            <name>FU_Set_Lead_Owner_to_DnB_Retail_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Revisit_Counter_To_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Revisit_Date_Tomorrow</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-905

Transfers the Lead to the DnB Retail Queue</description>
        <formula>ISPICKVAL(LeadSource, &quot;Purchased List&quot;)  &amp;&amp; ISPICKVAL(Purchased_List_Source__c , &quot;DnB&quot;)  &amp;&amp; ISPICKVAL(Industry, &quot;Health &amp; Beauty&quot;) = FALSE &amp;&amp; ISPICKVAL(Industry, &quot;Healthcare&quot;) = FALSE &amp;&amp; ISPICKVAL(Industry, &quot;Health Industry - Allied&quot;) = FALSE &amp;&amp; ISPICKVAL(Industry, &quot;Health Industry - Primary&quot;) = FALSE &amp;&amp; ISPICKVAL(Industry, &quot;Food &amp; Beverage&quot;) = FALSE &amp;&amp; ISPICKVAL(Industry, &quot;Hospitality&quot;) = FALSE &amp;&amp; ISPICKVAL(Industry, &quot;Entertainment&quot;) = FALSE &amp;&amp; ISPICKVAL(Industry, &quot;Veterinary&quot;) = FALSE &amp;&amp; Email &lt;&gt; &quot;&quot; &amp;&amp; Phone &lt;&gt; &quot;&quot; &amp;&amp; Integration_Product__c &lt;&gt; &quot;&quot; &amp;&amp; Integration_Product__r.Name &lt;&gt; &quot;Unknown POS&quot; &amp;&amp; Integration_Product__r.Name &lt;&gt; &quot;Unknown POS Product&quot; &amp;&amp; Integration_Product__r.Name &lt;&gt; &quot;No POS Product&quot; &amp;&amp; RecordType.Name = &quot;Merchant Lead&quot; &amp;&amp; ISPICKVAL(Status, &quot;Lead Gen: Complete&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>new_web_lead</fullName>
        <assignedTo>ncause@tyro.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>new web lead</subject>
    </tasks>
</Workflow>
