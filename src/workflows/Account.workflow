<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Abaki_Account_Update</fullName>
        <ccEmails>tyroeasyclaim@abaki.com</ccEmails>
        <description>Abaki Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/Abaki_Terminal_Shipped</template>
    </alerts>
    <alerts>
        <fullName>Account_Closure_Notification_to_Merchant</fullName>
        <description>Account: Closure Notification to Merchant</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>fs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support_emails/FS_Termination_Notification</template>
    </alerts>
    <alerts>
        <fullName>Alibaba_Parent_Account_Welcome_Email</fullName>
        <ccEmails>alan@alibaba.com.au</ccEmails>
        <description>Alibaba Parent Account Welcome Email</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/Ali_baba_parent</template>
    </alerts>
    <alerts>
        <fullName>AmexDinersReminder</fullName>
        <description>Amex/Diners Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>CS</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Auto_Responses/AMEX_Diners_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Application_Received_BePoz</fullName>
        <description>Application Received - BePoz - SF-656</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/BePOZ_AM_Handover</template>
    </alerts>
    <alerts>
        <fullName>Application_Received_Fedelta</fullName>
        <description>Application Received - Fedelta - SF-659</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Fedelta_AM_Handover</template>
    </alerts>
    <alerts>
        <fullName>Application_Received_IdealPos</fullName>
        <description>Application Received - IdealPos - SF-655</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/IdealPOS_AM_Handover</template>
    </alerts>
    <alerts>
        <fullName>Application_Received_Retail_Hospitality</fullName>
        <description>Application Received - Retail &amp; Hospitality - SF-618</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Retail_Hosp_handover</template>
    </alerts>
    <alerts>
        <fullName>Application_Received_Swiftpos</fullName>
        <description>Application Received - Swiftpos - SF-658</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/SwiftPOS_AM_Handover</template>
    </alerts>
    <alerts>
        <fullName>Application_Received_TriniTeq</fullName>
        <description>Application Received - TriniTeq - SF-657</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/TriniTEQ_AM_Handover</template>
    </alerts>
    <alerts>
        <fullName>BEpoz_Account_Update</fullName>
        <ccEmails>support@bepoz.com.au</ccEmails>
        <description>BEpoz Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/BEpoz_Merchant_Status_is_updated</template>
    </alerts>
    <alerts>
        <fullName>BEpoz_Account_Update_Application_received</fullName>
        <ccEmails>support@bepoz.com.au</ccEmails>
        <description>BEpoz Account Update - Application received - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <recipient>aho@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/BEpoz_Merchant_Status_is_set_to_Application_Received</template>
    </alerts>
    <alerts>
        <fullName>B_E_Account_Update</fullName>
        <ccEmails>info@b-e.com.au</ccEmails>
        <description>B&amp;E Account Update</description>
        <protected>false</protected>
        <recipients>
            <recipient>ncause@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/B_E_Account_Update</template>
    </alerts>
    <alerts>
        <fullName>BeirutHellenic_Account_Update</fullName>
        <ccEmails>ebanking@beiruthellenic.com.au</ccEmails>
        <ccEmails>Alex.Giaourtas@beiruthellenic.com.au</ccEmails>
        <description>Beirut Hellenic Account Update</description>
        <protected>false</protected>
        <recipients>
            <recipient>ahaddad@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dsmith.hospitality@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/BeirutHellenic_Account_Update</template>
    </alerts>
    <alerts>
        <fullName>Bepoz_Pay_table</fullName>
        <ccEmails>support@bepoz.com.au</ccEmails>
        <description>Bepoz Pay@table - REDUNDANT</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/Bepoz_pay_table</template>
    </alerts>
    <alerts>
        <fullName>Best_Practice_Application_Received</fullName>
        <description>Best Practice Application Received - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/Bp_Handover_email</template>
    </alerts>
    <alerts>
        <fullName>Best_Practice_Ops_Merchant_Status_is_Active</fullName>
        <description>Best Practice Ops Merchant Status is Active - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Activation_BestPractice</template>
    </alerts>
    <alerts>
        <fullName>Best_Practice_Ops_Merchant_Status_is_Boarded</fullName>
        <description>Best Practice Ops Merchant Status is Boarded</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/Medicare_Easyclaim_Welcome_Email_BP</template>
    </alerts>
    <alerts>
        <fullName>Blue_Chip_Application_Received</fullName>
        <description>Blue Chip Application Received - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/BlueChip_Handover_email</template>
    </alerts>
    <alerts>
        <fullName>Blue_Chip_Welcome_Email</fullName>
        <description>Blue Chip Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Activation_Blue_Chip</template>
    </alerts>
    <alerts>
        <fullName>CCOS_Application_Received</fullName>
        <description>CCOS Application Received - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/CCOS_Handover_email</template>
    </alerts>
    <alerts>
        <fullName>CCOS_welcome_email</fullName>
        <description>CCOS welcome email - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Clinical_Computers_Welcome</template>
    </alerts>
    <alerts>
        <fullName>CCOS_welcome_email_2</fullName>
        <description>CCOS welcome email 2</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/CCOS_Welcome_Email_October</template>
    </alerts>
    <alerts>
        <fullName>Carbonelle_Consulting</fullName>
        <ccEmails>info@carbonelle.com.au</ccEmails>
        <description>Carbonelle Consulting</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/Carbonelle_Terminal_Shipped</template>
    </alerts>
    <alerts>
        <fullName>Channel_changed_email_alert</fullName>
        <description>Channel changed email alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales/Channel_change_notification</template>
    </alerts>
    <alerts>
        <fullName>Cheesecake_Shop_Ops_Status_Update</fullName>
        <description>Cheesecake Shop Ops Status Update</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/TCS_Account_Updated</template>
    </alerts>
    <alerts>
        <fullName>Clinical_Computers</fullName>
        <ccEmails>info@clinicalcomputers.com.au</ccEmails>
        <description>Clinical Computers shipment alert</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/Clinical_Computers_Terminal_Shipped</template>
    </alerts>
    <alerts>
        <fullName>Coreplus_Terminal_Shipped</fullName>
        <description>CorePlus - Terminal Shipped</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/Coreplus_Welcome_Letter_31_03_2015</template>
    </alerts>
    <alerts>
        <fullName>Credit_check_notice</fullName>
        <description>Credit check notice</description>
        <protected>false</protected>
        <recipients>
            <recipient>rdeguzman@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rdemel@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Auto_Responses/Credit_check_notice</template>
    </alerts>
    <alerts>
        <fullName>Ducor_new_Account</fullName>
        <ccEmails>accountsrec@ducor.com.au</ccEmails>
        <description>Ducor: new Account</description>
        <protected>false</protected>
        <recipients>
            <recipient>rdavidson@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Auto_Responses/Ducor_new_Account_notification</template>
    </alerts>
    <alerts>
        <fullName>Easyclaim_application_inactivty</fullName>
        <description>Easyclaim application inactivty</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Auto_Responses/Easyclaim_application_inactivty</template>
    </alerts>
    <alerts>
        <fullName>Email_Dale_on_Ops_Merchant_Status_Change</fullName>
        <description>Email Dale on Ops Merchant Status Change</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales/Ops_Merchant_Status_Update</template>
    </alerts>
    <alerts>
        <fullName>Email_Fred_Health</fullName>
        <ccEmails>sales@fred.com.au</ccEmails>
        <description>Email Fred Health</description>
        <protected>false</protected>
        <senderAddress>support@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/Fred_Health</template>
    </alerts>
    <alerts>
        <fullName>Fedelta_Pay_table</fullName>
        <ccEmails>rohan_musgrave@fedeltapos.com</ccEmails>
        <description>Fedelta Pay@table - SF-458 - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <recipient>ncause@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/Fedelta_pay_table</template>
    </alerts>
    <alerts>
        <fullName>Front_Desk_Active</fullName>
        <description>Front Desk - Active - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Front_Desk_Reconciliation_Guide_25_02_2015</template>
    </alerts>
    <alerts>
        <fullName>Front_Desk_Ops_Merchant_Status_is_Boarded</fullName>
        <description>Front Desk Ops Merchant Status is Boarded</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Front_Desk_Welcome_Letter_24_06_2015</template>
    </alerts>
    <alerts>
        <fullName>GWS_Agencies_Account_Update</fullName>
        <ccEmails>gstewart@tyro.com</ccEmails>
        <description>GWS Agencies Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/GWS_Agencies_Terminal_Shipped</template>
    </alerts>
    <alerts>
        <fullName>HCNSiteIDcheck</fullName>
        <description>HCN Site ID check</description>
        <protected>false</protected>
        <recipients>
            <recipient>aobrien@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jellerm@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Auto_Responses/HCN_Site_ID_Check</template>
    </alerts>
    <alerts>
        <fullName>HCNsoftwareupgraderequest</fullName>
        <description>HCN software upgrade request</description>
        <protected>false</protected>
        <recipients>
            <recipient>rdavidson@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Auto_Responses/HCN_software_upgrade_request</template>
    </alerts>
    <alerts>
        <fullName>HCS_Terminal_Shipped</fullName>
        <ccEmails>******pzele@tyro.com</ccEmails>
        <description>HCS Terminal Shipped</description>
        <protected>false</protected>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/HCS_Terminal_Shipped</template>
    </alerts>
    <alerts>
        <fullName>H_L_BHG_Account_Update</fullName>
        <ccEmails>gregl@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L BHG Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/H_L_Account_Update</template>
    </alerts>
    <alerts>
        <fullName>H_L_NSW_Account_Update</fullName>
        <ccEmails>nsw.support@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L NSW Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/H_L_Account_Update</template>
    </alerts>
    <alerts>
        <fullName>H_L_NSW_Account_Update_Application_received</fullName>
        <ccEmails>nsw.support@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L NSW Account Update - Application received</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/H_L_Account_Update_Application_received</template>
    </alerts>
    <alerts>
        <fullName>H_L_NSW_Pay_table</fullName>
        <ccEmails>nsw.support@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L NSW_Pay@table - REDUNDANT</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/H_L_pay_table</template>
    </alerts>
    <alerts>
        <fullName>H_L_NT_Account_Update</fullName>
        <ccEmails>tonyc@hlaustralia.com.au</ccEmails>
        <ccEmails>jasperl@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L NT Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/H_L_Account_Update</template>
    </alerts>
    <alerts>
        <fullName>H_L_NT_Account_Update_Application_receieved</fullName>
        <ccEmails>tonyc@hlaustralia.com.au</ccEmails>
        <ccEmails>jasperl@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L NT Account Update - Application received</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/H_L_Account_Update_Application_received</template>
    </alerts>
    <alerts>
        <fullName>H_L_NT_Pay_table</fullName>
        <ccEmails>jasperl@hlaustralia.com.au</ccEmails>
        <ccEmails>tonyc@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L NT_Pay@table - REDUNDANT</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/H_L_pay_table</template>
    </alerts>
    <alerts>
        <fullName>H_L_QLD_Account_Update</fullName>
        <ccEmails>qld.admin@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L QLD Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/H_L_Account_Update</template>
    </alerts>
    <alerts>
        <fullName>H_L_QLD_Account_Update_Application_received</fullName>
        <ccEmails>qld.admin@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L QLD Account Update Application received</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/H_L_Account_Update_Application_received</template>
    </alerts>
    <alerts>
        <fullName>H_L_QLD_Pay_table</fullName>
        <ccEmails>qld.admin@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L QLD_Pay@table - REDUNDANT</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/H_L_pay_table</template>
    </alerts>
    <alerts>
        <fullName>H_L_SA_Account_Update</fullName>
        <ccEmails>jerryf@hlaustralia.com.au</ccEmails>
        <ccEmails>jasperl@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L SA Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/H_L_Account_Update</template>
    </alerts>
    <alerts>
        <fullName>H_L_SA_Account_Update_Application_received</fullName>
        <ccEmails>jerryf@hlaustralia.com.au</ccEmails>
        <ccEmails>jasperl@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L SA Account Update - Application received</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/H_L_Account_Update_Application_received</template>
    </alerts>
    <alerts>
        <fullName>H_L_SA_Pay_table</fullName>
        <ccEmails>jasperl@hlaustralia.com.au</ccEmails>
        <ccEmails>jerryf@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L SA_Pay@table - REDUNDANT</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/H_L_pay_table</template>
    </alerts>
    <alerts>
        <fullName>H_L_TAS_Account_Update</fullName>
        <ccEmails>vic.orders@hlaustralia.com.au</ccEmails>
        <ccEmails>john.pycroft@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L TAS Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/H_L_Account_Update</template>
    </alerts>
    <alerts>
        <fullName>H_L_TAS_Account_Update_Application_received</fullName>
        <ccEmails>vic.orders@hlaustralia.com.au</ccEmails>
        <ccEmails>john.pycroft@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L TAS Account Update - Application received</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/H_L_Account_Update_Application_received</template>
    </alerts>
    <alerts>
        <fullName>H_L_TAS_Pay_table</fullName>
        <ccEmails>vic.orders@hlaustralia.com.au</ccEmails>
        <ccEmails>john.pycroft@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L TAS_Pay@table - REDUNDANT</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/H_L_pay_table</template>
    </alerts>
    <alerts>
        <fullName>H_L_VIC_Account_Update</fullName>
        <ccEmails>vic.orders@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L VIC Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/H_L_Account_Update</template>
    </alerts>
    <alerts>
        <fullName>H_L_VIC_Account_Update_Application_received</fullName>
        <ccEmails>vic.orders@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L VIC Account Update - Application received</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/H_L_Account_Update_Application_received</template>
    </alerts>
    <alerts>
        <fullName>H_L_VIC_Pay_table</fullName>
        <ccEmails>vic.orders@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L VIC_Pay@table - REDUNDANT</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/H_L_pay_table</template>
    </alerts>
    <alerts>
        <fullName>H_L_WA_Account_Update</fullName>
        <ccEmails>bmoar@hlaustralia.com.au</ccEmails>
        <ccEmails>wa.support@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L WA Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/H_L_Account_Update</template>
    </alerts>
    <alerts>
        <fullName>H_L_WA_Account_Update_Application_received</fullName>
        <ccEmails>bmoar@hlaustralia.com.au</ccEmails>
        <ccEmails>wa.support@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L WA Account Update - Application received</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/H_L_Account_Update_Application_received</template>
    </alerts>
    <alerts>
        <fullName>H_L_WA_Pay_table</fullName>
        <ccEmails>bmoar@hlaustralia.com.au</ccEmails>
        <ccEmails>wa.support@hlaustralia.com.au</ccEmails>
        <ccEmails>tyro@hlaustralia.com.au</ccEmails>
        <description>H&amp;L WA_Pay@table - REDUNDANT</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/H_L_pay_table</template>
    </alerts>
    <alerts>
        <fullName>Ideal_POS_table</fullName>
        <ccEmails>louis.badham@idealpos.com.au</ccEmails>
        <description>Ideal POS @table - SF-422 - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <recipient>ncause@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/Ideal_POS_pay_table</template>
    </alerts>
    <alerts>
        <fullName>Imagatec_Pay_table</fullName>
        <ccEmails>Thomas@imagatec.com.au</ccEmails>
        <description>Imagatec_Pay@table - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <recipient>apulante@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dsmith.hospitality@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/Imagatec_pay_table</template>
    </alerts>
    <alerts>
        <fullName>Initiate_termination_procedure_for_CS</fullName>
        <ccEmails>cs-requests@tyro.com</ccEmails>
        <ccEmails>am@tyro.com</ccEmails>
        <description>Initiate termination procedure for CS</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Auto_Responses/Merchant_is_terminating</template>
    </alerts>
    <alerts>
        <fullName>InvalidABN</fullName>
        <description>Invalid ABN</description>
        <protected>false</protected>
        <recipients>
            <recipient>rdavidson@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Auto_Responses/Invalid_or_incomplete_ABN</template>
    </alerts>
    <alerts>
        <fullName>MPSAlertEmail</fullName>
        <ccEmails>info@mpsystems.com.au</ccEmails>
        <ccEmails>info@merchantpayments.com.au</ccEmails>
        <description>MPS Alert Email</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/MPS_Account_Status_Update</template>
    </alerts>
    <alerts>
        <fullName>MYOB_Account_Update</fullName>
        <ccEmails>eftpos@myob.com</ccEmails>
        <ccEmails>cs@tyro.com</ccEmails>
        <description>MYOB Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Archive/MYOB_Account_Update</template>
    </alerts>
    <alerts>
        <fullName>Mackbron_Account_Update</fullName>
        <ccEmails>john@mackbron.com.au</ccEmails>
        <ccEmails>cs@tyro.com</ccEmails>
        <description>Mackbron Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Auto_Responses/Mackbron_Account_Update</template>
    </alerts>
    <alerts>
        <fullName>Medilink_Account_Activated</fullName>
        <ccEmails>support@medilink.com.au</ccEmails>
        <description>Medilink Account Activated - REDUNDANT</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/Medilink_Account_Activated</template>
    </alerts>
    <alerts>
        <fullName>Medilink_Account_Update</fullName>
        <ccEmails>support@medilink.com.au</ccEmails>
        <description>Medilink Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/Medilink_Terminal_Shipped</template>
    </alerts>
    <alerts>
        <fullName>Medilink_Application_Received</fullName>
        <description>Medilink Application Received - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/Medilink_Handover_email</template>
    </alerts>
    <alerts>
        <fullName>Merchant_Group_Terminal_Shipped_Alert</fullName>
        <description>Merchant Group - Terminal Shipped Alert</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CS_Automated_Email_Templates/Merchant_Group_Terminal_Shipped_Alert</template>
    </alerts>
    <alerts>
        <fullName>Micros_Account_Update</fullName>
        <ccEmails>apausgateway@micros.com</ccEmails>
        <description>Micros Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/Micros_Account_Update</template>
    </alerts>
    <alerts>
        <fullName>Mrs_Fields_Parent_Account_Welcome_Email</fullName>
        <ccEmails>sarahd@cookiecorp.com.au</ccEmails>
        <ccEmails>michaelw@cookiecorp.com.au</ccEmails>
        <description>Mrs Fields Parent Account Welcome Email</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/Mrs_Fields_parent</template>
    </alerts>
    <alerts>
        <fullName>POSACTIVE_Terminal_Shipped_alert</fullName>
        <ccEmails>mrm@posactive.com.au</ccEmails>
        <description>POSACTIVE Terminal shipped email alert</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>knasri@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/POSACTIVE_Activated</template>
    </alerts>
    <alerts>
        <fullName>PPMP_Active</fullName>
        <description>PPMP - Active - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/PPMP_Reconciliation_Guide_17_07_2015</template>
    </alerts>
    <alerts>
        <fullName>PPMP_Boarding_Complete</fullName>
        <description>PPMP - Boarding Complete</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/PPMP_Welcome_Letter_17_07_2015</template>
    </alerts>
    <alerts>
        <fullName>Pracsoft_Application_Received</fullName>
        <description>Pracsoft Application Received - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/PS_Handover_email</template>
    </alerts>
    <alerts>
        <fullName>Pracsoft_Welcome_Email</fullName>
        <description>Pracsoft Welcome Email - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/Activation_PracSoft</template>
    </alerts>
    <alerts>
        <fullName>Practice_2000_Application_Received</fullName>
        <description>Practice 2000 Application Received - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/P2K_Handover_email</template>
    </alerts>
    <alerts>
        <fullName>PureCommAccountAlert</fullName>
        <ccEmails>tyroupdates@pure-commerce.com</ccEmails>
        <ccEmails>fnyssens@tyro.com</ccEmails>
        <description>Pure Comm Account Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>rdavidson@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Archive/Pure_Comm_Account_Email</template>
    </alerts>
    <alerts>
        <fullName>QCCU_Account_Alert</fullName>
        <ccEmails>merchant.banking@qccu.com.au</ccEmails>
        <description>QCCU - Account Alert</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/QCCU_Merchant_Status_is_updated</template>
    </alerts>
    <alerts>
        <fullName>Revel_Pay_table</fullName>
        <ccEmails>ausupport@revelsystems.com</ccEmails>
        <description>Revel pay@table - SF-420 - REDUNDANT</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/Revel_pay_table</template>
    </alerts>
    <alerts>
        <fullName>Riva_Account_Update</fullName>
        <ccEmails>support@riva.com.au</ccEmails>
        <description>Riva - Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/Riva_Account_Update</template>
    </alerts>
    <alerts>
        <fullName>Riva_Pay_table</fullName>
        <ccEmails>support@riva.com.au</ccEmails>
        <description>Riva Pay@table - SF-554 - REDUNDANT</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/Riva_pay_table</template>
    </alerts>
    <alerts>
        <fullName>SMB_Account_Created</fullName>
        <ccEmails>support@smbconsultants.com.au</ccEmails>
        <description>SMB Account Created</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/SMB_Account_Update</template>
    </alerts>
    <alerts>
        <fullName>SMB_Account_Updated</fullName>
        <ccEmails>support@smbconsultants.com.au</ccEmails>
        <description>SMB Account Updated</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/SMB_Account_Update</template>
    </alerts>
    <alerts>
        <fullName>SMB_Consultants_shipment_alert</fullName>
        <ccEmails>team@smbconsultants.com.au</ccEmails>
        <description>SMB Consultants shipment alert</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/SMB_Consultants</template>
    </alerts>
    <alerts>
        <fullName>Ultimo_Dental_Ops_Merchant_Status_is_Boarded</fullName>
        <description>Ultimo Dental - Merchant Status is Boarded</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Ultimo_Welcome_Email_17_09_2015</template>
    </alerts>
    <alerts>
        <fullName>Update_account_owner_of_easyclaim_status</fullName>
        <description>Update account owner of easyclaim status</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Auto_Responses/Easyclaim_status_update</template>
    </alerts>
    <alerts>
        <fullName>Vectron_Account_Update</fullName>
        <ccEmails>bepoz@vectron.com.au</ccEmails>
        <description>Vectron Account Update</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/Vectron_Merchant_Status_is_updated</template>
    </alerts>
    <alerts>
        <fullName>Vectron_Account_Update_Application_received</fullName>
        <ccEmails>bepoz@vectron.com.au</ccEmails>
        <description>Vectron Account Update - Application received</description>
        <protected>false</protected>
        <recipients>
            <recipient>aho@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/Vectron_Merchant_Status_is_set_to_Application_Received</template>
    </alerts>
    <alerts>
        <fullName>Vectron_Pay_table</fullName>
        <ccEmails>bepoz@vectron.com.au</ccEmails>
        <description>Vectron Pay@table</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/Vectron_pay_table</template>
    </alerts>
    <alerts>
        <fullName>Wedderburn</fullName>
        <ccEmails>eftposintegrations@wedderburn.com.au</ccEmails>
        <description>Wedderburn</description>
        <protected>false</protected>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Retail/Wedderburn_Merchant_Status_is_updated</template>
    </alerts>
    <alerts>
        <fullName>Welcome_email_for_Medilink</fullName>
        <description>Welcome email for Medilink</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Merchant_Onboarding_Alerts/Activation_Medilink_trainingvid</template>
    </alerts>
    <alerts>
        <fullName>Welcome_email_for_integrated_medical_merchants</fullName>
        <description>Welcome email for integrated medical merchants</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Archive/Medicare_Easyclaim_Welcome_Email_10_10_13</template>
    </alerts>
    <alerts>
        <fullName>ZedMed_Application_Received</fullName>
        <description>ZedMed Application Received - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Secondary_e_mail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/AM_Handover_email</template>
    </alerts>
    <alerts>
        <fullName>ZedMed_Email_Alert</fullName>
        <description>ZedMed Email Alert status is Active - REDUNDANT</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/Activation_Zedmed</template>
    </alerts>
    <alerts>
        <fullName>ZedMed_Support_Alert</fullName>
        <description>ZedMed Support Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>acu@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/ZedMed_Support_Alert</template>
    </alerts>
    <alerts>
        <fullName>ZedMed_User_Email_notification</fullName>
        <description>ZedMed User Email notification</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>am@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Medical/Medicare_Easyclaim_Welcome_Email_Zedmed</template>
    </alerts>
    <alerts>
        <fullName>new_terminal</fullName>
        <description>new terminal information</description>
        <protected>false</protected>
        <recipients>
            <field>Merchant_Admin_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/General_Welcome_Email_October</template>
    </alerts>
    <fieldUpdates>
        <fullName>Channel_HCN</fullName>
        <field>Channel__c</field>
        <literalValue>HCN - Health Communications Network</literalValue>
        <name>Channel HCN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Channel_old_value_field_update</fullName>
        <description>Update the channel old value field with the previous channel value</description>
        <field>Channel_Old_Value__c</field>
        <formula>PRIORVALUE(Channel__c)</formula>
        <name>Channel old value field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Credit_Approved</fullName>
        <field>Merchant_Status__c</field>
        <literalValue>Credit Check Completed</literalValue>
        <name>Credit Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EasyClaim_installation_status_changed_on</fullName>
        <field>EasyClaim_Installation_status_changed_on__c</field>
        <formula>now()</formula>
        <name>EasyClaim installation status changed on</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Chg_Record_Type_To_Customer</fullName>
        <description>SF-469

Sets Record Type to &apos;Customer&apos;</description>
        <field>RecordTypeId</field>
        <lookupValue>Tyro_Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>FU - Chg Record Type To Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Migrated_to_New_MID</fullName>
        <description>If merchant is migrated to new MID then update Terminate Reason field.</description>
        <field>Terminate_Reason__c</field>
        <literalValue>Migrated to New MID</literalValue>
        <name>FU: Migrated to New MID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Record_Type_To_Partner_Account</fullName>
        <description>SF-832

Sets Account Record Type to Partner</description>
        <field>RecordTypeId</field>
        <lookupValue>Partner</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>FU - Set Record Type To Partner Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Trading_City</fullName>
        <description>SF-469

Set Trading City on Lead Conversion</description>
        <field>ShippingCity</field>
        <formula>BillingCity</formula>
        <name>FU - Set Trading City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Trading_Country</fullName>
        <description>SF-469

Set Trading Country on Lead Conversion</description>
        <field>ShippingCountry</field>
        <formula>BillingCountry</formula>
        <name>FU - Set Trading Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Trading_Postcode</fullName>
        <description>SF-469

Set Trading Postcode on Lead Conversion</description>
        <field>ShippingPostalCode</field>
        <formula>BillingPostalCode</formula>
        <name>FU - Set Trading Postcode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Trading_State</fullName>
        <description>SF-469

Set Trading State on Lead Conversion</description>
        <field>ShippingState</field>
        <formula>BillingState</formula>
        <name>FU - Set Trading State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Trading_Street</fullName>
        <description>SF-469

Set Trading Street on Lead Conversion</description>
        <field>ShippingStreet</field>
        <formula>BillingStreet</formula>
        <name>FU - Set Trading Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Tick_the_Paper_Discount_checkbox</fullName>
        <field>Paper_discount_approved__c</field>
        <literalValue>1</literalValue>
        <name>FU: Tick the Paper Discount checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Untick_the_paper_discount_checkbox</fullName>
        <field>Paper_discount_approved__c</field>
        <literalValue>0</literalValue>
        <name>FU: Untick the paper discount checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Approved_Paper_Discount_Field</fullName>
        <description>Updates the %Discount Approved</description>
        <field>Discount_Paper_Approved__c</field>
        <formula>Discount_Paper__c</formula>
        <name>FU: Update Approved Paper Discount Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Compliance_approval_date</fullName>
        <field>Compliance_Approval_Date__c</field>
        <formula>Today()</formula>
        <name>FU:Update Compliance approval date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Paper_Discount_Approval</fullName>
        <field>Paper_Discount_Approval_Date__c</field>
        <formula>Today()</formula>
        <name>FU: Update Paper Discount Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_update_Medicare_Easyclaim</fullName>
        <field>Medicare_Easyclaim_TXN_updated_on__c</field>
        <formula>NOW()</formula>
        <name>Field update: Medicare/Easyclaim</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Hud_Phone</fullName>
        <field>HUD_Phone_No__c</field>
        <formula>SUBSTITUTE (Phone, &quot; &quot;,&quot;&quot;)</formula>
        <name>Hud Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Index_MID</fullName>
        <field>AccountNumber</field>
        <formula>TEXT( Merchant_ID__c )</formula>
        <name>Index MID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MCC</fullName>
        <field>MCC__c</field>
        <formula>MCC_Description__r.MCC_Code__c</formula>
        <name>MCC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Merchant_Active</fullName>
        <field>Merchant_Status__c</field>
        <literalValue>Active</literalValue>
        <name>Merchant Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rollout_link</fullName>
        <field>Rollout_Link__c</field>
        <formula>&quot;https://rollout.tyro.com/apply/continue.htm?key=&quot;&amp; Registration_Key__c</formula>
        <name>Rollout link</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_status</fullName>
        <field>Sales_Merchant_Status__c</field>
        <literalValue>Application received</literalValue>
        <name>Sales status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Primary_Integration_Product_NULL</fullName>
        <field>Primary_Integration_Product__c</field>
        <name>Set Primary Integration Product - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Segment_NULL</fullName>
        <field>Segment__c</field>
        <name>Set Segment - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Terminal_Arrived</fullName>
        <field>Easyclaim_Installation_Status__c</field>
        <literalValue>Terminal arrived</literalValue>
        <name>Terminal Arrived</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Discount_Paper_Approved</fullName>
        <field>Discount_Paper_Approved__c</field>
        <formula>Discount_Paper__c</formula>
        <name>Update %Discount Paper Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Tyro_Alert_Field_for_Best_Practic</fullName>
        <description>SF-363</description>
        <field>Tyro_Alert__c</field>
        <formula>&quot;Best Practice Multi-Merchant account. Integ. Eftpos not currently available, refer to Multi-Merchant Guide for Easyclaim workflow.&quot;</formula>
        <name>Update Tyro Alert Field for Best Practic</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updatechangedondateandtime</fullName>
        <field>Alert_changed_on__c</field>
        <formula>Now()</formula>
        <name>Update changed on date and time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>application_received_date</fullName>
        <field>Application_received_date__c</field>
        <formula>DATE(YEAR(Merchant_Create_Date__c),MONTH(Merchant_Create_Date__c), DAY(Merchant_Create_Date__c)-7)</formula>
        <name>application received date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>easyclaim_date_time</fullName>
        <field>EasyClaim_Application_status_changed_on__c</field>
        <formula>NOW()</formula>
        <name>easyclaim date time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>easyclaim_enabled</fullName>
        <field>Medicare_Easyclaim__c</field>
        <literalValue>1</literalValue>
        <name>easyclaim enabled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Acc%3A Update time of compliance approval for Partner</fullName>
        <actions>
            <name>FU_Update_Compliance_approval_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(Compliance_Approved__c ),  Compliance_Approved__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Check ABN</fullName>
        <actions>
            <name>InvalidABN</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>and(LEN(ABN__c)&gt;0,  MOD(

Value(MID(Text(Value(ABN__c) - 10000000000),1,1))*10+
Value(mid(Text(Value(ABN__c) - 10000000000),2,1))*1+
Value(mid(Text(Value(ABN__c) - 10000000000),3,1))*3+
Value(mid(Text(Value(ABN__c) - 10000000000),4,1))*5+
Value(mid(Text(Value(ABN__c) - 10000000000),5,1))*7+
Value(mid(Text(Value(ABN__c) - 10000000000),6,1))*9+
Value(mid(Text(Value(ABN__c) - 10000000000),7,1))*11+
Value(mid(Text(Value(ABN__c) - 10000000000),8,1))*13+
Value(mid(Text(Value(ABN__c) - 10000000000),9,1))*15+
Value(mid(Text(Value(ABN__c) - 10000000000),10,1))*17+
Value(mid(Text(Value(ABN__c) - 10000000000),11,1))*19
,89) &lt;&gt;0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Churned Account</fullName>
        <actions>
            <name>Set_Primary_Integration_Product_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Segment_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Churned</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit Approved</fullName>
        <actions>
            <name>Credit_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>notEqual</operation>
            <value>Active,Migrated to new MID,Boarding Completed,Terminal Shipped,Credit Check Completed,Suspended,Inactive (loaner/other),Customer Terminated,Tyro Terminated,Declined by Tyro</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Credit_Check_Aproval__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>De-activated as part of SCV

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMAIL%3A POSACTIVE Terminal Shipped Email</fullName>
        <actions>
            <name>POSACTIVE_Terminal_Shipped_alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>Integration_Product__c = &quot;a0JD0000004xnhC&quot; &amp;&amp; ISPICKVAL(Merchant_Status__c, &quot;Terminal Shipped&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Fred Health</fullName>
        <actions>
            <name>Email_Fred_Health</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to Fred Health whenever an account ops merchant status changes to &quot;Terminal Shipped&quot; and the POS product is &quot;Fred.Retail&quot;</description>
        <formula>Integration_Product__r.Manufacturer_Account__r.Name = &quot;Fred IT Group Pty Ltd&quot; &amp;&amp;  ISPICKVAL(Merchant_Status__c, &apos;Terminal Shipped&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A AMEX%2FDiners Reminder</fullName>
        <actions>
            <name>AmexDinersReminder</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>If a customer has amex/diners and is borded, this reminds CS to send information to AMEX/Diners</description>
        <formula>OR(

AND(LEN(AMEX_Mer__c)&gt;0,ISPICKVAL(Merchant_Status__c, &quot;Boarding Completed&quot;)),

AND(LEN( Diners_Merchant_No__c)&gt;0,ISPICKVAL(  Merchant_Status__c, &quot;Boarding Completed&quot;))
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Abaki Terminal Shipped</fullName>
        <actions>
            <name>Abaki_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISPICKVAL(Channel__c, &quot;Abaki (10% MAF, 25% EC, PMS Marketing Agreement)&quot;) &amp;&amp; ISPICKVAL(Merchant_Status__c, &quot;Terminal Shipped&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Ali Baba Parent</fullName>
        <actions>
            <name>Alibaba_Parent_Account_Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Terminal Shipped</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ParentId</field>
            <operation>equals</operation>
            <value>Ali Baba Corporate</value>
        </criteriaItems>
        <description>SF 271

Replaced by process: CA - Shipment Alerts

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Andrew When Channel Changes</fullName>
        <actions>
            <name>Channel_changed_email_alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Channel_old_value_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>send email to Andrew when Channel changes

De-activated as part of SCV

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>ISCHANGED(Channel__c) &amp;&amp;  NOT(ISPICKVAL(PRIORVALUE (Channel__c),&quot;&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A B%26E Account Alert</fullName>
        <actions>
            <name>B_E_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Terminal Shipped,Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>B&amp;E (ISO 10%)</value>
        </criteriaItems>
        <description>Email to B&amp;E Contacts when an account has been updated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A BEpoz Account Alert</fullName>
        <actions>
            <name>BEpoz_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Replaced by process: MID - Status Notifications - External

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>AND( ISCHANGED(Merchant_Status__c), ISPICKVAL(Channel__c, &quot;BEpoz (PMA 15%)&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A BEpoz Account Alert - Application received</fullName>
        <actions>
            <name>BEpoz_Account_Update_Application_received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(
ISPICKVAL(Sales_Merchant_Status__c,&quot;Application received&quot;), ISPICKVAL(Channel__c , &quot;BEpoz (PMA 15%)&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Beirut Helenic Bank</fullName>
        <actions>
            <name>BeirutHellenic_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to Beirut Hellenic Bank when an account has been updated

Replaced by process: MID - Status Notifications - External

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>and( ischanged( Merchant_Status__c ), ispickval( Channel__c , &quot;Beirut Hellenic Bank&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Bepoz_Pay%40table</fullName>
        <actions>
            <name>Bepoz_Pay_table</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Mobile_Integration__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>BEpoz (PMA 15%)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Boarding Completed</value>
        </criteriaItems>
        <description>SF-297</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Best Practice Application Received</fullName>
        <actions>
            <name>Best_Practice_Application_Received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-440</description>
        <formula>Integration_Product__r.Id = &quot;a0J20000000rV10&quot;
&amp;&amp; 
Additional_Account__c = False 
&amp;&amp; 
ISPICKVAL( Sales_Merchant_Status__c, &quot;Application Received&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Best Practice Ops Merchant Status is Boarded</fullName>
        <actions>
            <name>Best_Practice_Ops_Merchant_Status_is_Boarded</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-357</description>
        <formula>Integration_Product__c = &quot;a0J20000000rV10&quot; &amp;&amp;  ISPICKVAL(Merchant_Status__c, &quot;Boarding Completed&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Best Practice Ops Merchant Status is active</fullName>
        <actions>
            <name>Best_Practice_Ops_Merchant_Status_is_Active</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-356</description>
        <formula>Integration_Product__c = &quot;a0J20000000rV10&quot; &amp;&amp;  ISPICKVAL(Merchant_Status__c, &quot;Active&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Blue Chip Application Received</fullName>
        <actions>
            <name>Blue_Chip_Application_Received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-430</description>
        <formula>Integration_Product__r.Id = &quot;a0J20000000UGJz&quot; 
&amp;&amp; 
Additional_Account__c = False 
&amp;&amp; 
ISPICKVAL( Sales_Merchant_Status__c, &quot;Application Received&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A CCOS Application Received</fullName>
        <actions>
            <name>CCOS_Application_Received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-432</description>
        <formula>Integration_Product__c = &quot;a0JD000000DmCVu&quot; 
&amp;&amp; 
Additional_Account__c = False 
&amp;&amp; 
ISPICKVAL( Sales_Merchant_Status__c, &quot;Application Received&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A CCOS welcome email</fullName>
        <actions>
            <name>CCOS_welcome_email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>Clinical Computers (no contract as yet)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <description>SF-270</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A CCOS welcome email 2</fullName>
        <actions>
            <name>CCOS_welcome_email_2</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-289</description>
        <formula>ISPICKVAL(Merchant_Status__c, &quot;Terminal Shipped&quot;) &amp;&amp;  Integration_Product__c = &quot;a0JD000000DmCVu&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Carbonelle Consulting Terminal Shipped</fullName>
        <actions>
            <name>Carbonelle_Consulting</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Lead_Source__c</field>
            <operation>equals</operation>
            <value>Carbonelle</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Terminal Shipped</value>
        </criteriaItems>
        <description>SF-321

De-activated as part of SCV

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Clinical Computers</fullName>
        <actions>
            <name>Clinical_Computers</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>Clinical Computers (no contract as yet)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Terminal Shipped</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Dale on Ops Merchant Status Change</fullName>
        <actions>
            <name>Email_Dale_on_Ops_Merchant_Status_Change</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Active,Boarding Completed,Terminal Shipped,Tyro Terminated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>Imagatec,Vectron (PMA 15%),Retail Express,H&amp;L,Ideal POS Direct,BEpoz (PMA 15%)</value>
        </criteriaItems>
        <description>Replaced by process: MID - Status Notifications - Internal

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Front Desk Ops Merchant Status is Boarded</fullName>
        <actions>
            <name>Front_Desk_Ops_Merchant_Status_is_Boarded</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-401</description>
        <formula>Integration_Product__c = &quot;a0J2000000094D3&quot; &amp;&amp;  ISPICKVAL(Merchant_Status__c, &quot;Boarding Completed&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A GWS Agencies Terminal Shipped</fullName>
        <actions>
            <name>GWS_Agencies_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>GWS Agencies</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Terminal Shipped</value>
        </criteriaItems>
        <description>SF-352</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L BHG Account Alert</fullName>
        <actions>
            <name>H_L_BHG_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to H&amp;L BHGContacts when an account has been updated

Replaced by process: MID - H&amp;L Status Notifications - BHG

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>and( ischanged( Merchant_Status__c ), ispickval( Channel__c , &quot;H&amp;L&quot;), CONTAINS(Name,  &quot;Bavarian Hospitality Group&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L NSW Account Alert</fullName>
        <actions>
            <name>H_L_NSW_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to H&amp;L NSW Contacts when an account has been updated

Replaced by process: MID - H&amp;L Status Notifications x State
Replaced by flow: MID - H&amp;L MID Status Notifications

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>and( ischanged( Merchant_Status__c ), ispickval( Channel__c , &quot;H&amp;L&quot;), ShippingState = &quot;NSW&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L NSW Account Alert - Application received</fullName>
        <actions>
            <name>H_L_NSW_Account_Update_Application_received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to H&amp;L NSW Contacts when an account has &apos;Application received&apos; status</description>
        <formula>and(ispickval( Sales_Merchant_Status__c , &quot;Application received&quot;), ispickval( Channel__c , &quot;H&amp;L&quot;),  ShippingState = &quot;NSW&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L NSW_Pay%40table</fullName>
        <actions>
            <name>H_L_NSW_Pay_table</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Mobile_Integration__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>H&amp;L</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Boarding Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingState</field>
            <operation>equals</operation>
            <value>NSW</value>
        </criteriaItems>
        <description>SF-290</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L NT Account Alert</fullName>
        <actions>
            <name>H_L_NT_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to H&amp;L NT when an account has been updated

Replaced by process: MID - H&amp;L Status Notifications x State
Replaced by flow: MID - H&amp;L MID Status Notifications

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>and( ischanged( Merchant_Status__c ), ispickval( Channel__c , &quot;H&amp;L&quot;), ShippingState = &quot;NT&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L NT Account Alert - Application received</fullName>
        <actions>
            <name>H_L_NT_Account_Update_Application_receieved</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to H&amp;L NT when an account has &apos;Application received&apos; status</description>
        <formula>and(ispickval( Sales_Merchant_Status__c , &quot;Application received&quot;), ispickval( Channel__c , &quot;H&amp;L&quot;),  ShippingState = &quot;NT&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L NT_Pay%40table</fullName>
        <actions>
            <name>H_L_NT_Pay_table</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Mobile_Integration__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>H&amp;L</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Boarding Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingState</field>
            <operation>equals</operation>
            <value>NT</value>
        </criteriaItems>
        <description>SF-295</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L QLD Account Alert</fullName>
        <actions>
            <name>H_L_QLD_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to H&amp;L QLDContacts when an account has been updated

Replaced by process: MID - H&amp;L Status Notifications x State
Replaced by flow: MID - H&amp;L MID Status Notifications

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>and( ischanged( Merchant_Status__c ), ispickval( Channel__c , &quot;H&amp;L&quot;), ShippingState = &quot;QLD&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L QLD Account Alert - Application received</fullName>
        <actions>
            <name>H_L_QLD_Account_Update_Application_received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to H&amp;L QLDContacts when an account has &apos;Application received&apos; status</description>
        <formula>and(ispickval( Sales_Merchant_Status__c , &quot;Application received&quot;), ispickval( Channel__c , &quot;H&amp;L&quot;),  ShippingState = &quot;QLD&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L QLD_Pay%40table</fullName>
        <actions>
            <name>H_L_QLD_Pay_table</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Mobile_Integration__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>H&amp;L</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Boarding Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingState</field>
            <operation>equals</operation>
            <value>QLD</value>
        </criteriaItems>
        <description>SF-291</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L SA Account Alert</fullName>
        <actions>
            <name>H_L_SA_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to H&amp;L SA Contacts when an account has been updated

Replaced by process: MID - H&amp;L Status Notifications x State
Replaced by flow: MID - H&amp;L MID Status Notifications

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>and( ischanged( Merchant_Status__c ), ispickval( Channel__c , &quot;H&amp;L&quot;), ShippingState = &quot;SA&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L SA Account Alert - Application received</fullName>
        <actions>
            <name>H_L_SA_Account_Update_Application_received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to H&amp;L SA Contacts when an account has &apos;Application received&apos; status</description>
        <formula>and(ispickval( Sales_Merchant_Status__c , &quot;Application received&quot;), ispickval( Channel__c , &quot;H&amp;L&quot;),  ShippingState = &quot;SA&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L SA_Pay%40table</fullName>
        <actions>
            <name>H_L_SA_Pay_table</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Mobile_Integration__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>H&amp;L</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Boarding Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingState</field>
            <operation>equals</operation>
            <value>SA</value>
        </criteriaItems>
        <description>SF-294</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L TAS Account Alert</fullName>
        <actions>
            <name>H_L_TAS_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to H&amp;L TAS when an account has been updated

Replaced by process: MID - H&amp;L Status Notifications x State
Replaced by flow: MID - H&amp;L MID Status Notifications

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>and( ischanged( Merchant_Status__c ), ispickval( Channel__c , &quot;H&amp;L&quot;), ShippingState = &quot;TAS&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L TAS Account_Application_received Alert</fullName>
        <actions>
            <name>H_L_TAS_Account_Update_Application_received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to H&amp;L TAS when sales status is &apos;Application received&apos;</description>
        <formula>and(ispickval( Sales_Merchant_Status__c , &quot;Application received&quot;), ispickval( Channel__c , &quot;H&amp;L&quot;), ShippingState = &quot;TAS&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L TAS_Pay%40table</fullName>
        <actions>
            <name>H_L_TAS_Pay_table</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Mobile_Integration__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>H&amp;L</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Boarding Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingState</field>
            <operation>equals</operation>
            <value>TAS</value>
        </criteriaItems>
        <description>SF-293</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L VIC Account Alert</fullName>
        <actions>
            <name>H_L_VIC_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to H&amp;L VIC when an account has been updated

Replaced by process: MID - H&amp;L Status Notifications x State
Replaced by flow: MID - H&amp;L MID Status Notifications

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>and( ischanged( Merchant_Status__c ), ispickval( Channel__c , &quot;H&amp;L&quot;), ShippingState = &quot;VIC&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L VIC Account Alert - Application received</fullName>
        <actions>
            <name>H_L_VIC_Account_Update_Application_received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to H&amp;L VIC when an account has &apos;Application received&apos; status</description>
        <formula>and(ispickval( Sales_Merchant_Status__c , &quot;Application received&quot;), ispickval( Channel__c , &quot;H&amp;L&quot;),  ShippingState = &quot;VIC&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L VIC_Pay%40table</fullName>
        <actions>
            <name>H_L_VIC_Pay_table</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Mobile_Integration__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>H&amp;L</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Boarding Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingState</field>
            <operation>equals</operation>
            <value>VIC</value>
        </criteriaItems>
        <description>SF-292</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L WA Account Alert</fullName>
        <actions>
            <name>H_L_WA_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to H&amp;L WA when an account has updated status

Replaced by process: MID - H&amp;L Status Notifications x State
Replaced by flow: MID - H&amp;L MID Status Notifications

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>and( ischanged( Merchant_Status__c ), ispickval( Channel__c , &quot;H&amp;L&quot;), ShippingState = &quot;WA&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L WA Account Alert - Application received</fullName>
        <actions>
            <name>H_L_WA_Account_Update_Application_received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to H&amp;L WA when an account has &apos;Application received&apos; status</description>
        <formula>and(ispickval( Sales_Merchant_Status__c , &quot;Application received&quot;), ispickval( Channel__c , &quot;H&amp;L&quot;), ShippingState = &quot;WA&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A H%26L WA_Pay%40table</fullName>
        <actions>
            <name>H_L_WA_Pay_table</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Mobile_Integration__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>H&amp;L</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Boarding Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingState</field>
            <operation>equals</operation>
            <value>WA</value>
        </criteriaItems>
        <description>SF-290</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A HCS Terminal Shipped</fullName>
        <actions>
            <name>HCS_Terminal_Shipped</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>HCS (Paul Zele)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Terminal Shipped</value>
        </criteriaItems>
        <description>SF-447</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Imagatec_Pay%40table</fullName>
        <actions>
            <name>Imagatec_Pay_table</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Mobile_Integration__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>Imagatec</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Boarding Completed</value>
        </criteriaItems>
        <description>SF-288</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A MPS Account Alert</fullName>
        <actions>
            <name>MPSAlertEmail</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to info@mpsystems.com.au when an account has

Replaced by process: MID - Status Notifications - External

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>and( ischanged( Merchant_Status__c ), ispickval( Channel__c , &quot;MPS - Merchant Payment Systems&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A MYOB Account alert</fullName>
        <actions>
            <name>MYOB_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Updates channel manager about changed in Ops Merchant Status

Replaced by process: MID - Status Notifications - External

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>AND(ISCHANGED(Merchant_Status__c),ISPICKVAL(Channel__c,&quot;MYOB&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Mackbron Account alert</fullName>
        <actions>
            <name>Mackbron_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Updates channel manager about changed in Ops Merchant Status

Replaced by process: MID - Status Notifications - External

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>and( ischanged( Merchant_Status__c ), ispickval( Channel__c , &quot;Mackbron&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Medilink Account Activated</fullName>
        <actions>
            <name>Medilink_Account_Activated</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>&quot;Medilink (10% MAF, 25% EC, PMS Marketing Agreement)&quot;</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Medilink Application Received</fullName>
        <actions>
            <name>Medilink_Application_Received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-431</description>
        <formula>Integration_Product__r.Id = &quot;a0J20000002dr8L&quot; 
&amp;&amp; 
Additional_Account__c = False 
&amp;&amp; 
ISPICKVAL( Sales_Merchant_Status__c, &quot;Application Received&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Medilink Terminal Shipped</fullName>
        <actions>
            <name>Medilink_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISPICKVAL(Channel__c, &quot;Medilink (10% MAF, 25% EC, PMS Marketing Agreement)&quot;) &amp;&amp; ISPICKVAL(Merchant_Status__c, &quot;Terminal Shipped&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Micros Account Alert</fullName>
        <actions>
            <name>Micros_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Active,Boarding Completed,Terminal Shipped,Tyro Terminated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>Micros</value>
        </criteriaItems>
        <description>SF-274</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Mrs Fields Parent</fullName>
        <actions>
            <name>Mrs_Fields_Parent_Account_Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Terminal Shipped</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ParentId</field>
            <operation>equals</operation>
            <value>Cookies Australia Pty Ltd.</value>
        </criteriaItems>
        <description>SF-275

Replaced by process: CA - Shipment Alerts

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A New Terminal Information</fullName>
        <actions>
            <name>new_terminal</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Replaced by Merchant Onboarding - Shipment Flow

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>ISPICKVAL(Merchant_Status__c,&apos;Terminal Shipped&apos;) &amp;&amp;  NOT(OR(  Integration_Product__c = &apos;a0J20000000UGJz&apos;,  Integration_Product__c = &apos;a0J20000000Ua5q&apos;,  Integration_Product__c = &apos;a0J20000002dr8L&apos;,  Integration_Product__c = &apos;a0J2000000094E7&apos;,  Integration_Product__c = &apos;a0JD000000DocZO&apos;,  Integration_Product__c = &apos;a0J2000000094D3&apos;,  Integration_Product__c = &apos;a0JD000000IYmJI&apos;,  Integration_Product__c = &apos;a0J20000000rV10&apos;,  Integration_Product__c = &apos;a0J20000000lvao&apos;  ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Email%3A Pracsoft Application Received</fullName>
        <actions>
            <name>Pracsoft_Application_Received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-429</description>
        <formula>Integration_Product__r.Id = &quot;a0J2000000094E7&quot; 
&amp;&amp; 
Additional_Account__c = False 
&amp;&amp; 
ISPICKVAL( Sales_Merchant_Status__c, &quot;Application Received&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Practice 2000 Application Received</fullName>
        <actions>
            <name>Practice_2000_Application_Received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-433</description>
        <formula>Integration_Product__r.Id  = &quot;a0J20000000Ua5q&quot;
&amp;&amp;  
Additional_Account__c = False
&amp;&amp;
ISPICKVAL( Sales_Merchant_Status__c, &quot;Application Received&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Pure Comm Account Alert</fullName>
        <actions>
            <name>PureCommAccountAlert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to tyroupdates@pure-commerce.com

Replaced by process: MID - Status Notifications - External

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>and( ischanged( Merchant_Status__c ), ispickval( Channel__c , &quot;Purecom&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A SMB Account Alert - Account Updated</fullName>
        <actions>
            <name>SMB_Account_Updated</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to SMB Contacts when an account has &apos;updated&apos; status

Replaced by process: MID - Status Notifications - External

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>and( ischanged( Merchant_Status__c ), ispickval( Channel__c , &quot;SMB Consultants&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A SMB Account Alert - Application received</fullName>
        <actions>
            <name>SMB_Account_Created</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Email to SMB Contacts when an account has &apos;Application received&apos; status</description>
        <formula>and(ispickval( Sales_Merchant_Status__c , &quot;Application received&quot;), ispickval( Channel__c , &quot;SMB Consultants&quot;),  Channel_Number__c  = &quot;SMB&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A SMB Consultants</fullName>
        <actions>
            <name>SMB_Consultants_shipment_alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>SMB Consultants (10% PRA Continious)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Terminal Shipped</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A The Cheesecake Shop</fullName>
        <actions>
            <name>Cheesecake_Shop_Ops_Status_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Trading_Name_EFTPOS_Receipt__c</field>
            <operation>contains</operation>
            <value>Cheesecake</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Active,Terminal Shipped,Boarding Completed</value>
        </criteriaItems>
        <description>Email to TCS when an account has been updated

Replaced by process: MID - Status Notifications - Internal

https://confluence.tyro.com/pages/viewpage.action?pageId=26168768</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Vectron Account Alert</fullName>
        <actions>
            <name>Vectron_Account_Update</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Replaced by process: MID - Status Notifications - External

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>AND( ISCHANGED(Merchant_Status__c ), ISPICKVAL(Channel__c , &quot;Vectron (PMA 15%)&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Vectron Account Alert - Application received</fullName>
        <actions>
            <name>Vectron_Account_Update_Application_received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(
ISPICKVAL(Sales_Merchant_Status__c,&quot;Application received&quot;), 
ISPICKVAL(Channel__c,&quot;Vectron (PMA 15%)&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Vectron_Pay%40table</fullName>
        <actions>
            <name>Vectron_Pay_table</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Mobile_Integration__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>Vectron (PMA 15%)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Boarding Completed</value>
        </criteriaItems>
        <description>SF-298</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Wedderburn Account Alert</fullName>
        <actions>
            <name>Wedderburn</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Channel__c</field>
            <operation>equals</operation>
            <value>Wedderburn</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Active,Boarding Completed,Terminal Shipped,Tyro Terminated</value>
        </criteriaItems>
        <description>SF-333</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Welcome email for BlueChip</fullName>
        <actions>
            <name>Blue_Chip_Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-279</description>
        <formula>ISPICKVAL(Merchant_Status__c,&quot;Active&quot;) &amp;&amp; Integration_Product__c = &quot;a0J20000000UGJz&quot; &amp;&amp; NOT(ISNULL(Merchant_ID__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Welcome email for Medilink</fullName>
        <actions>
            <name>Welcome_email_for_Medilink</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-280</description>
        <formula>ISPICKVAL(Merchant_Status__c,&quot;Active&quot;) &amp;&amp; Integration_Product__c = &quot;a0J20000002dr8L&quot; &amp;&amp; NOT(ISNULL(Merchant_ID__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Welcome email for PracSoft</fullName>
        <actions>
            <name>Pracsoft_Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-281</description>
        <formula>ISPICKVAL(Merchant_Status__c,&quot;Active&quot;) &amp;&amp; Integration_Product__c = &quot;a0J2000000094E7&quot; &amp;&amp; NOT(ISNULL(Merchant_ID__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Welcome email for integrated medical merchants</fullName>
        <actions>
            <name>Welcome_email_for_integrated_medical_merchants</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>A PMS specific welcome email for integrated Medical merchants is generated through Salesforce when the ops:merchant status is changed to &apos;Boarded&apos;</description>
        <formula>ISPICKVAL(Merchant_Status__c,&apos;Terminal Shipped&apos;) &amp;&amp;
OR(Integration_Product__c = &apos;a0J20000000UGJz&apos;, Integration_Product__c = &apos;a0J20000000Ua5q&apos;, Integration_Product__c = &apos;a0J20000002dr8L&apos;, Integration_Product__c = &apos;a0J2000000094E7&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A ZedMed Application Received</fullName>
        <actions>
            <name>ZedMed_Application_Received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-441</description>
        <formula>Integration_Product__r.Id = &quot;a0J20000000lvao&quot; 
&amp;&amp; 
Additional_Account__c = False 
&amp;&amp; 
ISPICKVAL( Sales_Merchant_Status__c, &quot;Application Received&quot; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A ZedMed Support Alert</fullName>
        <actions>
            <name>ZedMed_Support_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-366</description>
        <formula>Integration_Product__c = &apos;a0J20000000lvao&apos; &amp;&amp; ISPICKVAL( Merchant_Status__c, &quot;Terminal Shipped&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A ZedMed User Email</fullName>
        <actions>
            <name>ZedMed_User_Email_notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-364</description>
        <formula>Integration_Product__c = &quot;a0J20000000lvao&quot; &amp;&amp;  ISPICKVAL(Merchant_Status__c,&quot;Boarding Completed&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A ZedMed email alert status changed to active</fullName>
        <actions>
            <name>ZedMed_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>SF-365</description>
        <formula>Integration_Product__c = &apos;a0J20000000lvao&apos; &amp;&amp; ISPICKVAL( Merchant_Status__c, &quot;Active&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Change To Partner Record Type</fullName>
        <actions>
            <name>FU_Set_Record_Type_To_Partner_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Allows a user to convert a Prospect account into a Partner account</description>
        <formula>Convert_To_Partner_Account__c = TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Chg Record Type To Customer</fullName>
        <actions>
            <name>FU_Chg_Record_Type_To_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Boarding Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Tyro Prospect</value>
        </criteriaItems>
        <description>SF-469

Changes Record Type to Customer upon &apos;Boarding Completed&apos;


Included in Merchant Onboarding flows

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Trading Addess</fullName>
        <actions>
            <name>FU_Set_Trading_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Trading_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Trading_Postcode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Trading_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_Trading_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-469

Set Trading Address on Lead Conversion</description>
        <formula>ISNEW() &amp;&amp; RecordType.Name = &quot;Tyro Prospect&quot; &amp;&amp;  ISBLANK(ShippingStreet) &amp;&amp;  ISBLANK(ShippingCity) &amp;&amp;  ISBLANK(ShippingState) &amp;&amp;  ISBLANK(ShippingPostalCode) &amp;&amp;  ISBLANK(ShippingCountry)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU%3A Migrated to New MID</fullName>
        <actions>
            <name>FU_Migrated_to_New_MID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Merchant has been migrated to a new MID do the following...

Managed on Merchant ID record with picklist dependencies &amp; termination flows

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>ISPICKVAL(Merchant_Status__c ,&quot;Migrated to new MID&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A AUTO - MCC</fullName>
        <actions>
            <name>MCC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>De-activated as part of SCV

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>len(MCC_Description__c)&gt;0</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Account Active</fullName>
        <actions>
            <name>Merchant_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 AND 3)or (2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Account.Net_Txn_Value__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Num_TXN_Last_Month__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Boarding Completed,Terminal Shipped</value>
        </criteriaItems>
        <description>Replaced by workflow rule FU: Merchant ID Active on Transaction History object

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Auto update alert change date</fullName>
        <actions>
            <name>Updatechangedondateandtime</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ischanged( Tyro_Alert__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Channel HCN</fullName>
        <actions>
            <name>Channel_HCN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Industry</field>
            <operation>equals</operation>
            <value>Health Industry - Primary</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A EasyClaim installation status changed on</fullName>
        <actions>
            <name>EasyClaim_installation_status_changed_on</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>De-activated as part of SCV

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>ischanged( Easyclaim_Installation_Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Rollout link</fullName>
        <actions>
            <name>Rollout_link</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>De-activated as part of SCV

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>len( Registration_Key__c ) &gt; 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Terminal Arrived</fullName>
        <active>false</active>
        <booleanFilter>(1 AND 2 AND 4) or (1 AND 3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Terminal Shipped</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.EasyClaim_Application_status__c</field>
            <operation>equals</operation>
            <value>Ready for credit checking,Boarded,Ready for boarding,Waiting for DDR authorisation/s</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Free_Install__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Easyclaim_Installation_Status__c</field>
            <operation>notEqual</operation>
            <value>3.94 Installed,Installation booked,Training booked</value>
        </criteriaItems>
        <description>De-activated as part of SCV

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>EasyClaim_installation_status_changed_on</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Terminal_Arrived</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Field Update%3A easyclaim application date time</fullName>
        <actions>
            <name>Update_account_owner_of_easyclaim_status</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>easyclaim_date_time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>De-activated as part of SCV

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>ISCHANGED(EasyClaim_Application_status__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field update%3A Application received</fullName>
        <actions>
            <name>Sales_status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>application_received_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Account.Application_received_date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Create_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Active,Boarding Completed,Terminal Shipped,Credit Check Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lock Merchant Statement Value</fullName>
        <active>false</active>
        <description>Lock this field when merchant ID is not null</description>
        <formula>ISNULL(Merchant_ID__c) = False</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Merchant_status_shipped</fullName>
        <active>false</active>
        <formula>ISPICKVAL(Merchant_Status__c, &quot;Terminal Shipped&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OUTBOUND%3A Ducor%3A New Account notification</fullName>
        <actions>
            <name>Ducor_new_Account</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 AND 2 AND 4) or (1 AND 3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Boarding Completed,Credit Check Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Create_Date__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Credit_Check_Aproval__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Trading_Address_Line_1__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OUTBOUND%3A Initiate Termination Procedure for CS</fullName>
        <actions>
            <name>Initiate_termination_procedure_for_CS</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>NOT(ISBLANK(Merchant_Terminate_Date__c)) &amp;&amp; 
NOT(ISPICKVAL(Terminate_Reason__c, &quot;Change of Ownership - business retained&quot;)) &amp;&amp;
NOT(ISPICKVAL(Terminate_Reason__c, &quot;Merchant Group - member lost&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A Create task for account Manager on status change</fullName>
        <active>false</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>Sales - Medical,Sales,Sales Manager</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>contains</operation>
            <value>Active,Terminal Shipped</value>
        </criteriaItems>
        <description>Create a task for the account manager when status of account changes

De-activated as part of SCV

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Please_call_merchant</name>
                <type>Task</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Task%3A Easyclaim non Active %5BPS 2010%5D</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Easyclaim_Installation_Status__c</field>
            <operation>equals</operation>
            <value>Install &amp; Training Complete - Not active</value>
        </criteriaItems>
        <description>Reminder to follow up with merchant as to why they are still not trading after having both installation and training completed. Part of 2010 Pracsoft Campaign</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Merchant_still_not_trading</name>
                <type>Task</type>
            </actions>
            <timeLength>21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Task%3A Install not complete %5BPS 2010%5D</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Easyclaim_Installation_Status__c</field>
            <operation>equals</operation>
            <value>Training Complete - Not Installed</value>
        </criteriaItems>
        <description>Reminder to complete installation as part of 2010 Pracsoft Campaign.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Task%3A Stuck on Signed up %5BPS 2010%5D</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Easyclaim_Installation_Status__c</field>
            <operation>equals</operation>
            <value>Signed Up</value>
        </criteriaItems>
        <description>Reminder to continue  installation and/or training as part of 2010 Pracsoft Campaign.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>28</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <timeLength>42</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Task%3A Terminal Not Activated</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Terminal Shipped</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Customer_not_called_to_activate_terminal</name>
                <type>Task</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Task%3A Termination Reason FollowUp</fullName>
        <actions>
            <name>Merchant_has_been_terminate_Please_record_Terminated_Reason</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Merchant_Status__c</field>
            <operation>equals</operation>
            <value>Migrated to new MID,Tyro Terminated,Customer Terminated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Terminate_Reason__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>De-activated as part of SCV

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A Training not complete %5BPS 2010%5D</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Easyclaim_Installation_Status__c</field>
            <operation>equals</operation>
            <value>Install Complete - Not Trained</value>
        </criteriaItems>
        <description>Reminder to complete training as part of 2010 Pracsoft Campaign.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Merchant_still_requires_Training_Installation</name>
                <type>Task</type>
            </actions>
            <timeLength>21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>account%2EvarianceReportUpdate</fullName>
        <active>false</active>
        <formula>ISCHANGED( Num_TXN_Last_Month__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Customer_not_called_to_activate_terminal</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please find out why merchant is still not active</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Customer not called to activate terminal</subject>
    </tasks>
    <tasks>
        <fullName>Easyclaim_boarding_inactivity</fullName>
        <assignedToType>owner</assignedToType>
        <description>A merchant has not moved forward in their application for more then 7 days</description>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Account.EasyClaim_Application_status_changed_on__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Easyclaim boarding inactivity</subject>
    </tasks>
    <tasks>
        <fullName>Initial_Call_Tower_Prospect_List_VIC</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Account.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Initial Call – Tower Prospect List (VIC)</subject>
    </tasks>
    <tasks>
        <fullName>Merchant_has_been_terminate_Please_record_Terminated_Reason</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Merchant has been terminated. Please record &quot;Terminate Reason&quot;</subject>
    </tasks>
    <tasks>
        <fullName>Merchant_still_not_trading</fullName>
        <assignedToType>owner</assignedToType>
        <description>Merchant has had training and installation but has been inactive for more than 2 weeks</description>
        <dueDateOffset>15</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Merchant still not trading</subject>
    </tasks>
    <tasks>
        <fullName>Merchant_still_requires_Training_Installation</fullName>
        <assignedToType>owner</assignedToType>
        <description>Investigate why merchant has not yet received both training and installation.</description>
        <dueDateOffset>43</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Merchant still requires Training/Installation</subject>
    </tasks>
    <tasks>
        <fullName>Please_call_merchant</fullName>
        <assignedToType>owner</assignedToType>
        <description>This task is automatically created from Salesforce when merchant status becomes active.</description>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Please call merchant on due date for referrals</subject>
    </tasks>
    <tasks>
        <fullName>Practice_has_been_idle_for_6_weeks_PS2010</fullName>
        <assignedToType>owner</assignedToType>
        <description>Practice has had the Easyclaim Installation Status &apos;Signed Up&apos; for more than  6 weeks and has not progressed further. Please investigate.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Practice has been idle for 6 weeks</subject>
    </tasks>
    <tasks>
        <fullName>Practice_has_not_completed_installation_PS2010</fullName>
        <assignedToType>owner</assignedToType>
        <description>Practice has not completed installation and it has been more than 3 weeks since they have had training.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Practice has not completed installation</subject>
    </tasks>
    <tasks>
        <fullName>Practice_has_not_completed_training_PS2010</fullName>
        <assignedToType>owner</assignedToType>
        <description>Practice has not completed training and it has been more than 3 weeks since they have completed installation.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Practice has not completed training</subject>
    </tasks>
    <tasks>
        <fullName>Practice_has_still_not_performed_transactions_PS2010</fullName>
        <assignedToType>owner</assignedToType>
        <description>Practice has completed installation and training more than 3 weeks ago and has still not performed and transactions.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Practice has still not performed transactions.</subject>
    </tasks>
</Workflow>
