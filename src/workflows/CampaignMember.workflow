<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Qualified_Web_Lead_Alert_Health</fullName>
        <description>Qualified Web Lead Alert - Health</description>
        <protected>false</protected>
        <recipients>
            <recipient>HO_Corp_Sales</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>TL_3_Direct_Sales</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales/Qualified_Web_Lead</template>
    </alerts>
    <alerts>
        <fullName>Qualified_Web_Lead_Alert_Hospitality</fullName>
        <description>Qualified Web Lead Alert - Hospitality</description>
        <protected>false</protected>
        <recipients>
            <recipient>HO_Direct_Sales</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>TL_1_Direct_Sales</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales/Qualified_Web_Lead</template>
    </alerts>
    <alerts>
        <fullName>Qualified_Web_Lead_Alert_MPS</fullName>
        <description>Qualified Web Lead Alert - MPS</description>
        <protected>false</protected>
        <recipients>
            <recipient>ISO_MPS</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales/Qualified_Web_Lead</template>
    </alerts>
    <alerts>
        <fullName>Qualified_Web_Lead_Alert_Retail</fullName>
        <description>Qualified Web Lead Alert - Retail</description>
        <protected>false</protected>
        <recipients>
            <recipient>TL_2_Direct_Sales</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales/Qualified_Web_Lead</template>
    </alerts>
    <fieldUpdates>
        <fullName>FU_Lead_Conversion_Date</fullName>
        <field>Lead_Conversion_Date__c</field>
        <formula>TODAY()</formula>
        <name>FU - Lead Conversion Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Campaign_Mbr_1st_Assoc_Type_CONTACT</fullName>
        <description>Sets Campaign Member type on first association to Contact</description>
        <field>First_Associated_As__c</field>
        <literalValue>Contact</literalValue>
        <name>Set Campaign Mbr 1st Assoc Type: CONTACT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Campaign_Mbr_1st_Assoc_Type_LEAD</fullName>
        <field>First_Associated_As__c</field>
        <literalValue>Lead</literalValue>
        <name>Set Campaign Mbr 1st Assoc Type: LEAD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Email%3A Qualified Web Lead Alert - Health</fullName>
        <actions>
            <name>Qualified_Web_Lead_Alert_Health</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 AND 2) OR (1 AND 3 AND 4) OR (1 AND 5 AND 6)</booleanFilter>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Website Wizard</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Industry</field>
            <operation>equals</operation>
            <value>Health &amp; Beauty,Health Industry - Allied,Health Industry - Primary,Veterinary,Healthcare</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Industry</field>
            <operation>equals</operation>
            <value>Retail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sub_Industry__c</field>
            <operation>equals</operation>
            <value>Pharmacy</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Account_Industry__c</field>
            <operation>equals</operation>
            <value>Retail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Account_Sub_Industry__c</field>
            <operation>equals</operation>
            <value>Pharmacy</value>
        </criteriaItems>
        <description>Email alert to Channel Managers / BDMs

https://confluence.tyro.com/pages/viewpage.action?pageId=37896326</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Qualified Web Lead Alert - Hospitality</fullName>
        <actions>
            <name>Qualified_Web_Lead_Alert_Hospitality</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Website Wizard</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Industry</field>
            <operation>equals</operation>
            <value>Hospitality</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Account_Industry__c</field>
            <operation>equals</operation>
            <value>Hospitality</value>
        </criteriaItems>
        <description>Email alert to Channel Managers / BDMs

https://confluence.tyro.com/pages/viewpage.action?pageId=37896326</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Qualified Web Lead Alert - MPS</fullName>
        <actions>
            <name>Qualified_Web_Lead_Alert_MPS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Website Wizard</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Contact_form_POS_PMS_system__c</field>
            <operation>equals</operation>
            <value>WaiterMate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Integration_Product_Name__c</field>
            <operation>equals</operation>
            <value>WaiterMate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Integration_Product_Name__c</field>
            <operation>equals</operation>
            <value>a0J2000000094DIEAY</value>
        </criteriaItems>
        <description>Email alert to ISO

https://confluence.tyro.com/pages/viewpage.action?pageId=37896326</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Qualified Web Lead Alert - Retail</fullName>
        <actions>
            <name>Qualified_Web_Lead_Alert_Retail</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 AND 2) OR (1 AND 3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Website Wizard</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Industry</field>
            <operation>equals</operation>
            <value>Retail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Account_Industry__c</field>
            <operation>equals</operation>
            <value>Retail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Account_Sub_Industry__c</field>
            <operation>notEqual</operation>
            <value>Pharmacy</value>
        </criteriaItems>
        <description>Email alert to Channel Managers / BDMs

https://confluence.tyro.com/pages/viewpage.action?pageId=37896326</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FU - Lead Conversion Date</fullName>
        <actions>
            <name>FU_Lead_Conversion_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sets the date that a Lead is converted to a Contact</description>
        <formula>ISCHANGED(LeadId)
&amp;&amp; ISNULL(LeadId)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Member Type - CONTACT</fullName>
        <actions>
            <name>Set_Campaign_Mbr_1st_Assoc_Type_CONTACT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.CaseSafeId__c</field>
            <operation>startsWith</operation>
            <value>003</value>
        </criteriaItems>
        <description>Sets Campaign Member first association type to CONTACT</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Member Type - LEAD</fullName>
        <actions>
            <name>Set_Campaign_Mbr_1st_Assoc_Type_LEAD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.CaseSafeId__c</field>
            <operation>startsWith</operation>
            <value>00Q</value>
        </criteriaItems>
        <description>Sets Campaign Member first association type to LEAD</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
