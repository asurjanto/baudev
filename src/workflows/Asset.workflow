<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Asset_Written_off</fullName>
        <description>Asset Written off</description>
        <protected>false</protected>
        <recipients>
            <recipient>phaig@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rdeguzman1@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Auto_Responses/Email_Asset_written_off</template>
    </alerts>
    <alerts>
        <fullName>Asset_status_changed_from_to_written_off</fullName>
        <ccEmails>finance-team@tyro.com</ccEmails>
        <description>Asset status changed from/to written off</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Finance/Asset_status_changed_from_to_written_off</template>
    </alerts>
    <alerts>
        <fullName>dev_terminal_reminder_for_CS</fullName>
        <ccEmails>cs@tyro.com</ccEmails>
        <description>dev terminal reminder for CS</description>
        <protected>false</protected>
        <senderAddress>cs-requests@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Auto_Responses/Dev_terminal</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Changed_On</fullName>
        <field>Account_Changed_On__c</field>
        <formula>NOW()</formula>
        <name>Account Changed On</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_Customer_Defaulted</fullName>
        <field>Check_CD__c</field>
        <literalValue>1</literalValue>
        <name>Asset Customer Defaulted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asset_Name</fullName>
        <description>SF-304</description>
        <field>Name</field>
        <formula>Product2.Name</formula>
        <name>Asset Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BOtounassigned</fullName>
        <field>BackOffice_Status__c</field>
        <literalValue>Unassigned</literalValue>
        <name>BO to unassigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Customer_Charged_asset_not_Return</fullName>
        <field>Check_CC_del__c</field>
        <literalValue>1</literalValue>
        <name>Check Customer Charged asset not Return</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_awaiting_return</fullName>
        <field>Check_AR__c</field>
        <literalValue>1</literalValue>
        <name>Check awaiting return</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dev_Return_Date</fullName>
        <field>Dev_Return_Date__c</field>
        <formula>today()</formula>
        <name>Dev Return Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Activation_Date_NULL</fullName>
        <description>Sets Activation Date to NULL</description>
        <field>Activation_Due_Date__c</field>
        <name>FU - Set Activation Date - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Awaiting_Return_Date</fullName>
        <field>Awaiting_Return_Date_Set__c</field>
        <formula>TODAY()</formula>
        <name>FU - Set Awaiting Return Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_Awaiting_Return_Date_NULL</fullName>
        <description>SF-636</description>
        <field>Awaiting_Return_Date_Set__c</field>
        <name>FU - Set Awaiting Return Date - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Set_merchant_Admin_Email_NULL</fullName>
        <description>SF-636</description>
        <field>Merchant_Admin_Email__c</field>
        <name>FU - Set merchant Admin Email - NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>History</fullName>
        <field>History__c</field>
        <formula>text(day(today())) &amp; &quot;-&quot; &amp; text(month(today()))  &amp;  &quot;-&quot; &amp; text(year(today())) &amp; &quot;: &quot; &amp; Account.Name &amp; &quot; with Status &quot; &amp;  text(Status__c)  &amp; &quot; by &quot;  &amp; $User.FirstName &amp; &quot; &quot; &amp;  $User.LastName &amp; BR() &amp; History__c</formula>
        <name>History</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MAC_upper_case</fullName>
        <field>MAC_Address__c</field>
        <formula>UPPER(  MAC_Address__c )</formula>
        <name>MAC upper case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PreviousAccount</fullName>
        <field>Previous_Account__c</field>
        <formula>priorvalue( TempValue__c)</formula>
        <name>Previous Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Previous_Account_2</fullName>
        <field>Previous_Account_2__c</field>
        <formula>priorvalue( Previous_Account__c )</formula>
        <name>Previous Account 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RMA_check</fullName>
        <field>Check_RMA__c</field>
        <literalValue>1</literalValue>
        <name>RMA check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Old_Serial_Number</fullName>
        <field>Old_Serial_Number_or_MAC_address__c</field>
        <formula>PRIORVALUE( SerialNumber )</formula>
        <name>Record Old Serial Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Repaired_terminal</fullName>
        <field>Is_repaired__c</field>
        <literalValue>Yes</literalValue>
        <name>Repaired terminal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StatustoInStock</fullName>
        <field>Status</field>
        <literalValue>In Stock</literalValue>
        <name>Status to In Stock</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Taxi_terminal_ticked</fullName>
        <field>Taxi_terminal_del__c</field>
        <literalValue>1</literalValue>
        <name>Taxi terminal ticked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_changed</fullName>
        <field>Status_Changed_On__c</field>
        <formula>now()</formula>
        <name>Update Status changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Asset Awaiting Return check</fullName>
        <actions>
            <name>Check_awaiting_return</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Return</value>
        </criteriaItems>
        <description>Related to Asset recovery mechanism NEO-512.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Asset Customer Defaulted</fullName>
        <actions>
            <name>Asset_Customer_Defaulted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Status__c</field>
            <operation>equals</operation>
            <value>Not Returned/Customer Defaulted</value>
        </criteriaItems>
        <description>Related to Asset recovery mechanism NEO-512. Check Customer Defaulted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Asset In Outstanding RMA status</fullName>
        <actions>
            <name>RMA_check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Status__c</field>
            <operation>equals</operation>
            <value>Outstanding RMA</value>
        </criteriaItems>
        <description>Related to Asset recovery mechanism NEO-512.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Asset customer charged asset</fullName>
        <actions>
            <name>Check_Customer_Charged_asset_not_Return</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Status__c</field>
            <operation>equals</operation>
            <value>Not Returned/Customer Charged</value>
        </criteriaItems>
        <description>Check Customer Charged. Related to Asset recovery mechanism NEO-512</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Asset written off</fullName>
        <actions>
            <name>Asset_Written_off</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Status__c</field>
            <operation>equals</operation>
            <value>Written Off</value>
        </criteriaItems>
        <description>Notifies Peter Haig when someone marks an asset as written off</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Asset%3A Status changed from%2Fto written off</fullName>
        <actions>
            <name>Asset_status_changed_from_to_written_off</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>SF 690 alert email that is sent when a terminals status is changed from/to &apos;Written off/Fleet&apos;</description>
        <formula>AND(ISCHANGED(Status__c), OR( ISPICKVAL(Status__c , &quot;Written Off/Fleet&quot;), ISPICKVAL(PRIORVALUE(Status__c),&quot;Written Off/Fleet&quot;) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Activation Date To NULL</fullName>
        <actions>
            <name>FU_Set_Activation_Date_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Activation_Due_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Status</field>
            <operation>equals</operation>
            <value>Stock</value>
        </criteriaItems>
        <description>SF-580

Sets Activation Date to NULL when Asset is transferred back to stock</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FU - Set Awaiting Terminal Return - NULL</fullName>
        <actions>
            <name>FU_Set_Awaiting_Return_Date_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Set_merchant_Admin_Email_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Status__c</field>
            <operation>notEqual</operation>
            <value>Awaiting Return,Outstanding RMA</value>
        </criteriaItems>
        <description>SF-636</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Asset Name</fullName>
        <actions>
            <name>Asset_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-304</description>
        <formula>NOT($Profile.Name = &quot;System Administrator&quot;) &amp;&amp;
Name !=  Product2.Name</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Dev Return Date</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 and 3</booleanFilter>
        <criteriaItems>
            <field>Asset.Name</field>
            <operation>startsWith</operation>
            <value>Dev,DEV</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
            <value>Tyro Stock</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>ATOS,Moneyswitch,MoneySwitch</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>dev_terminal_reminder_for_CS</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Dev_Return_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Field Update%3A Previous Account</fullName>
        <actions>
            <name>Account_Changed_On</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PreviousAccount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Previous_Account_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( AccountId )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Status Changed</fullName>
        <actions>
            <name>History</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>or(ISCHANGED( AccountId ),ISCHANGED( Status__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MAC upper case</fullName>
        <actions>
            <name>MAC_upper_case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>MAC_Address__c != UPPER( MAC_Address__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Record Old Serial Number</fullName>
        <actions>
            <name>Record_Old_Serial_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Banksys sometimes change the serial numbers of terminals. This field records the previous one if changed.</description>
        <formula>ISCHANGED( SerialNumber)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Recovery action taken</fullName>
        <active>false</active>
        <formula>OR(ISPICKVAL( Debt_Recovery_Action__c ,&quot;Yes&quot;),  ISPICKVAL(Recovery_Letter_Sent__c,&quot;Yes&quot;)) &amp;&amp;  ISCHANGED(Status)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Repaired terminal</fullName>
        <actions>
            <name>Repaired_terminal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.History__c</field>
            <operation>contains</operation>
            <value>Atos Worldline SA/NV,Banksys</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Taxi terminal ticked</fullName>
        <actions>
            <name>Taxi_terminal_ticked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.History__c</field>
            <operation>contains</operation>
            <value>National Billing Group</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Dev_Terminal_Due_Back</fullName>
        <assignedTo>rdavidson@tyro.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>This terminal has been shipped out 30 days ago and needs to be returned.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Dev Terminal Due Back</subject>
    </tasks>
</Workflow>
