<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>KA_Gen_Question_Set_status_to_draft</fullName>
        <description>SF-615 - Implement Salesforce Knowledge ; Internal approval step for General Question Article Type</description>
        <field>ValidationStatus</field>
        <literalValue>Not validated</literalValue>
        <name>KA: Gen Question - Set status to &quot;draft&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KA_Gen_Question_Status_to_Approved_Ext</fullName>
        <description>SF-615 - Implement Salesforce Knowledge ; Set Validation Status to Approved External for General Questions Article Type</description>
        <field>ValidationStatus</field>
        <literalValue>Approved External</literalValue>
        <name>KA: Gen Question: Status to Approved Ext</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KA_Gen_Question_Status_to_Approved_Int</fullName>
        <description>SF-615 - Implement Salesforce Knowledge ; Set Validation Status to &quot;Approved Internal&quot; for General Questions article type</description>
        <field>ValidationStatus</field>
        <literalValue>Approved Internal</literalValue>
        <name>KA: Gen Question: Status to Approved Int</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>KA_Gen_Question_Publish</fullName>
        <action>PublishAsNew</action>
        <description>Publish edited articles as new versions</description>
        <label>KA: Gen Question: Publish</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <knowledgePublishes>
        <fullName>KA_Gen_Question_Publish_As_New_Version</fullName>
        <action>PublishAsNew</action>
        <description>SF-615 - Implement Salesforce Knowledge General Question : Publish As New Version</description>
        <label>KA: Gen Question: Publish As New Version</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
</Workflow>
