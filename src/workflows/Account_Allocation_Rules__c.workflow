<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FU_Set_Allocation_Rule_Name</fullName>
        <description>NEO-572: Used to ensure that a rule cannot be duplicated</description>
        <field>Rule_Allocation_Name__c</field>
        <formula>TEXT(Segment__c) &amp; &quot;_&quot; &amp; Integration_Product__r.CaseSafeId__c</formula>
        <name>FU - Set Allocation Rule Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Rule Allocation Name</fullName>
        <actions>
            <name>FU_Set_Allocation_Rule_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Allocation_Rules__c.Segment__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>NEO-572</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
