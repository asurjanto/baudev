<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FU_New_Rates_Cost_Plus</fullName>
        <description>SF-592

Converts numeric values to text for quote presentation purposes</description>
        <field>New_Rates_2__c</field>
        <formula>CASE(Product2.Name, 

&quot;01 - Merchant Acquiring Fee (MAF)&quot;, 
TEXT(FLOOR(MSF_PC_OF_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_PC_OF_Trans_2__c - FLOOR(MSF_PC_OF_Trans_2__c))*1000),3) &amp; &quot;%&quot;, 

&quot;03 - EFTPOS: Transactions &lt; $15&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - floor(MSF_Per_Trans_2__c))*1000),3), 

&quot;04 - EFTPOS: Transactions = or &gt; $15&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - floor(MSF_Per_Trans_2__c))*1000),3), 

&quot;05 - EFTPOS: Cash Out&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - floor(MSF_Per_Trans_2__c))*1000),3), 

&quot;10 - Yomani&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - floor(MSF_Per_Trans_2__c))*100),2), 

&quot;11 - Yoximo&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - floor(MSF_Per_Trans_2__c))*100),2), 

&quot;12 - CSC Service Fee&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - floor(MSF_Per_Trans_2__c))*100),2), 

NULL)</formula>
        <name>FU - New Rates - Cost Plus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_New_Rates_Cost_Plus_2</fullName>
        <field>New_Rates_2__c</field>
        <formula>IF(Fixed_Or_Percent__c = &quot;%&quot;,
TEXT(FLOOR(MSF_PC_OF_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_PC_OF_Trans_2__c - FLOOR(MSF_PC_OF_Trans_2__c))*1000),3) &amp; &quot;%&quot;,

IF(Fixed_Or_Percent__c = &quot;$&quot;,
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - floor(MSF_Per_Trans_2__c))*1000),3), 

CASE(Product2.Name, 
&quot;10 - Yomani&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - floor(MSF_Per_Trans_2__c))*100),2), 

&quot;11 - Yoximo&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - floor(MSF_Per_Trans_2__c))*100),2), 

&quot;12 - CSC Service Fee&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - floor(MSF_Per_Trans_2__c))*100),2), 

NULL)))</formula>
        <name>FU - New Rates - Cost Plus - 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Current_Rates</fullName>
        <description>SF-592

Converts numeric values to text for quote presentation purposes</description>
        <field>Current_Rates_2__c</field>
        <formula>CASE(Product2.Name, 

&quot;01 - VISA/Mastercard Domestic Consumer Credit Card&quot;, 
IF(ISNULL(Current_MSF_PC_Of_Trans__c), NULL, 
(TEXT(FLOOR(Current_MSF_PC_Of_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_PC_Of_Trans__c - FLOOR(Current_MSF_PC_Of_Trans__c))*1000),3) &amp; &quot;%&quot;)), 

&quot;02 - VISA/Mastercard Domestic Premium &amp; Commercial Card&quot;, 
IF(ISNULL(Current_MSF_PC_Of_Trans__c), NULL, 
(TEXT(FLOOR(Current_MSF_PC_Of_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_PC_Of_Trans__c - FLOOR(Current_MSF_PC_Of_Trans__c))*1000),3) &amp; &quot;%&quot;)), 

&quot;03 - VISA/Mastercard International Card&quot;, 
IF(ISNULL(Current_MSF_PC_Of_Trans__c), NULL, 
(TEXT(FLOOR(Current_MSF_PC_Of_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_PC_Of_Trans__c - FLOOR(Current_MSF_PC_Of_Trans__c))*1000),3) &amp; &quot;%&quot;)),

&quot;04 - VISA/MasterCard Domestic Consumer Debit Card&quot;, 
IF(ISNULL(Current_MSF_Per_Trans__c), NULL,
(&quot;$&quot; &amp; TEXT(FLOOR(Current_MSF_Per_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*1000),3))), 

&quot;05 - Mastercard Debit Card Micropayments &lt;= $15&quot;, 
IF(ISNULL(Current_MSF_Per_Trans__c), NULL,
(&quot;$&quot; &amp; TEXT(FLOOR(Current_MSF_Per_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*1000),3))),

&quot;06 - Union Pay International Card&quot;, 
IF(ISNULL(Current_MSF_PC_Of_Trans__c), NULL, 
(TEXT(FLOOR(Current_MSF_PC_Of_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_PC_Of_Trans__c - FLOOR(Current_MSF_PC_Of_Trans__c))*1000),3) &amp; &quot;%&quot;)), 

&quot;07 - EFTPOS Debit Card - Purchase&quot;,
IF(ISNULL(Current_MSF_Per_Trans__c), NULL, 
(&quot;$&quot; &amp; TEXT(FLOOR(Current_MSF_Per_Trans__c)) &amp;&quot;.&quot;&amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*1000),3))), 

&quot;08 - EFTPOS Debit Card - Purchase &lt; $15&quot;, 
IF(ISNULL(Current_MSF_Per_Trans__c), NULL,
(&quot;$&quot; &amp; TEXT(FLOOR(Current_MSF_Per_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*1000),3))),

&quot;09 - EFTPOS Debit Card - Cashout/Purchase &amp; Cashout&quot;, 
IF(ISNULL(Current_MSF_Per_Trans__c), NULL,
(&quot;$&quot; &amp; TEXT(FLOOR(Current_MSF_Per_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*1000),3))), 

&quot;10 - Yomani&quot;, 
IF(ISNULL(Current_MSF_Per_Trans__c), NULL,
(&quot;$&quot; &amp; TEXT(FLOOR(Current_MSF_Per_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*100),2))),

&quot;11 - Yoximo&quot;, 
IF(ISNULL(Current_MSF_Per_Trans__c), NULL,
(&quot;$&quot; &amp; TEXT(FLOOR(Current_MSF_Per_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*100),2))),

&quot;12 - CSC Service Fee&quot;, 
IF(ISNULL(Current_MSF_Per_Trans__c), NULL,
(&quot;$&quot; &amp; TEXT(FLOOR(Current_MSF_Per_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*100),2))),

&quot;01 - Merchant Acquiring Fee (MAF)&quot;, 
IF(ISNULL(Current_MSF_PC_Of_Trans__c), NULL,
(TEXT(FLOOR(Current_MSF_PC_Of_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_PC_Of_Trans__c - FLOOR(Current_MSF_PC_Of_Trans__c))*1000),3) &amp; &quot;%&quot;)), 

&quot;03 - EFTPOS: Transactions &lt; $15&quot;, 
IF(ISNULL(Current_MSF_Per_Trans__c), NULL,
(&quot;$&quot; &amp; TEXT(FLOOR(Current_MSF_Per_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*1000),3))),

&quot;04 - EFTPOS: Transactions = or &gt; $15&quot;, 
IF(ISNULL(Current_MSF_Per_Trans__c), NULL,
(&quot;$&quot; &amp; TEXT(FLOOR(Current_MSF_Per_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*1000),3))),

&quot;05 - EFTPOS: Cash Out&quot;, 
IF(ISNULL(Current_MSF_Per_Trans__c), NULL,
(&quot;$&quot; &amp; TEXT(FLOOR(Current_MSF_Per_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*1000),3))), 

NULL)</formula>
        <name>FU - Update Current Rates</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_Current_Rates_2</fullName>
        <description>SF-592 

Converts numeric values to text for quote presentation purposes</description>
        <field>Current_Rates_2__c</field>
        <formula>IF(Current_Fixed_Or_Percent__c = &quot;%&quot;,  
(TEXT(FLOOR(Current_MSF_PC_Of_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_PC_Of_Trans__c - FLOOR(Current_MSF_PC_Of_Trans__c))*1000),3) &amp; &quot;%&quot;),

IF(Current_Fixed_Or_Percent__c = &quot;$&quot;,
(&quot;$&quot; &amp; TEXT(FLOOR(Current_MSF_Per_Trans__c)) &amp;&quot;.&quot;&amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*1000),3)), 

CASE(Product2.Name, 

&quot;10 - Yomani&quot;, 
IF(ISNULL(Current_MSF_Per_Trans__c), NULL, 
(&quot;$&quot; &amp; TEXT(FLOOR(Current_MSF_Per_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*100),2))), 

&quot;11 - Yoximo&quot;, 
IF(ISNULL(Current_MSF_Per_Trans__c), NULL, 
(&quot;$&quot; &amp; TEXT(FLOOR(Current_MSF_Per_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*100),2))), 

&quot;12 - CSC Service Fee&quot;, 
IF(ISNULL(Current_MSF_Per_Trans__c), NULL, 
(&quot;$&quot; &amp; TEXT(FLOOR(Current_MSF_Per_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*100),2))), 

&quot;13 - Monthly Base Fee&quot;, 
IF(ISNULL(Current_MSF_Per_Trans__c), NULL, 
(&quot;$&quot; &amp; TEXT(FLOOR(Current_MSF_Per_Trans__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*100),2))), 

&quot;14 - First Terminal Included&quot;, 
IF(ISNULL(Current_MSF_Per_Trans__c), NULL, 
(&quot;-$&quot; &amp; TEXT(ABS(FLOOR(Current_MSF_Per_Trans__c))) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + Current_MSF_Per_Trans__c - FLOOR(Current_MSF_Per_Trans__c))*100),2))), 

NULL)))</formula>
        <name>FU - Update Current Rates - 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_New_Rates</fullName>
        <description>SF-592 

Converts numeric values to text for quote presentation purposes</description>
        <field>New_Rates_2__c</field>
        <formula>CASE(Product2.Name, 

&quot;01 - VISA/Mastercard Domestic Consumer Credit Card&quot;, 
TEXT(FLOOR(MSF_PC_OF_Trans_2__c)) &amp;&quot;.&quot;&amp; RIGHT(TEXT((1 + MSF_PC_OF_Trans_2__c - FLOOR(MSF_PC_OF_Trans_2__c))*1000),3) &amp; &quot;%&quot;, 

&quot;02 - VISA/Mastercard Domestic Premium &amp; Commercial Card&quot;, 
TEXT(FLOOR(MSF_PC_OF_Trans_2__c)) &amp;&quot;.&quot;&amp; RIGHT(TEXT((1 + MSF_PC_OF_Trans_2__c - FLOOR(MSF_PC_OF_Trans_2__c))*1000),3) &amp; &quot;%&quot;, 

&quot;03 - VISA/Mastercard International Card&quot;, 
TEXT(FLOOR(MSF_PC_OF_Trans_2__c)) &amp;&quot;.&quot;&amp; RIGHT(TEXT((1 + MSF_PC_OF_Trans_2__c - FLOOR(MSF_PC_OF_Trans_2__c))*1000),3) &amp; &quot;%&quot;, 

&quot;04 - VISA/MasterCard Domestic Consumer Debit Card&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - FLOOR(MSF_Per_Trans_2__c))*1000),3), 

&quot;05 - Mastercard Debit Card Micropayments &lt;= $15&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - FLOOR(MSF_Per_Trans_2__c))*1000),3), 

&quot;06 - Union Pay International Card&quot;, 
TEXT(FLOOR(MSF_PC_OF_Trans_2__c)) &amp;&quot;.&quot;&amp; RIGHT(TEXT((1 + MSF_PC_OF_Trans_2__c - FLOOR(MSF_PC_OF_Trans_2__c))*1000),3) &amp; &quot;%&quot;, 

&quot;07 - EFTPOS Debit Card - Purchase&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - FLOOR(MSF_Per_Trans_2__c))*1000),3), 

&quot;08 - EFTPOS Debit Card - Purchase &lt; $15&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - FLOOR(MSF_Per_Trans_2__c))*1000),3), 

&quot;09 - EFTPOS Debit Card - Cashout/Purchase &amp; Cashout&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - FLOOR(MSF_Per_Trans_2__c))*1000),3), 

&quot;10 - Yomani&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - FLOOR(MSF_Per_Trans_2__c))*100),2), 

&quot;11 - Yoximo&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - FLOOR(MSF_Per_Trans_2__c))*100),2), 

&quot;12 - CSC Service Fee&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - FLOOR(MSF_Per_Trans_2__c))*100),2), 

NULL)</formula>
        <name>FU - Update New Rates</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Update_New_Rates_2</fullName>
        <description>SF-592 

Converts numeric values to text for quote presentation purposes</description>
        <field>New_Rates_2__c</field>
        <formula>IF(Fixed_Or_Percent__c = &quot;%&quot;, 
TEXT(FLOOR(MSF_PC_OF_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_PC_OF_Trans_2__c - FLOOR(MSF_PC_OF_Trans_2__c))*1000),3) &amp; &quot;%&quot;, 

IF(Fixed_Or_Percent__c = &quot;$&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - floor(MSF_Per_Trans_2__c))*1000),3), 


CASE(Product2.Name, 

&quot;10 - Yomani&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - FLOOR(MSF_Per_Trans_2__c))*100),2), 

&quot;11 - Yoximo&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - FLOOR(MSF_Per_Trans_2__c))*100),2), 

&quot;12 - CSC Service Fee&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - FLOOR(MSF_Per_Trans_2__c))*100),2),

&quot;13 - Monthly Base Fee&quot;, 
&quot;$&quot; &amp; TEXT(FLOOR(MSF_Per_Trans_2__c)) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - FLOOR(MSF_Per_Trans_2__c))*100),2),

&quot;14 - First Terminal Included&quot;, 
&quot;-$&quot; &amp; TEXT(ABS(FLOOR(MSF_Per_Trans_2__c))) &amp; &quot;.&quot; &amp; RIGHT(TEXT((1 + MSF_Per_Trans_2__c - FLOOR(MSF_Per_Trans_2__c))*100),2),

NULL)))</formula>
        <name>FU - Update New Rates - 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Use</fullName>
        <description>SF-592</description>
        <field>Fixed_Or_Percent__c</field>
        <formula>&quot;$&quot;</formula>
        <name>Set Use $</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Use_Percent</fullName>
        <description>SF-592</description>
        <field>Fixed_Or_Percent__c</field>
        <formula>&quot;%&quot;</formula>
        <name>Set Use %</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>FU - Proposed Set Use %24</fullName>
        <actions>
            <name>Set_Use</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4</booleanFilter>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>equals</operation>
            <value>03 - EFTPOS: Transactions &lt; $15</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>equals</operation>
            <value>04 - EFTPOS: Transactions = or &gt; $15</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>equals</operation>
            <value>05 - EFTPOS: Cash Out</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>equals</operation>
            <value>09 - EFTPOS Debit Card - Cashout/Purchase &amp; Cashout</value>
        </criteriaItems>
        <description>SF-592</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Proposed Set Use %25</fullName>
        <actions>
            <name>Set_Use_Percent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-592</description>
        <formula>PricebookEntry.Product2.Name = &quot;01 - Merchant Acquiring Fee (MAF)&quot; || (Terms_Of_Trade__r.Rate_Id__c = &quot;BR Custom&quot; &amp;&amp; (PricebookEntry.Product2.Name = &quot;01 - VISA/Mastercard Domestic Consumer Credit Card&quot; || PricebookEntry.Product2.Name = &quot;02 - VISA/Mastercard Domestic Premium &amp; Commercial Card&quot; || PricebookEntry.Product2.Name = &quot;03 - VISA/Mastercard International Card&quot; || PricebookEntry.Product2.Name = &quot;04 - VISA/MasterCard Domestic Consumer Debit Card&quot; || PricebookEntry.Product2.Name = &quot;05 - Mastercard Debit Card Micropayments &lt;= $15&quot; || PricebookEntry.Product2.Name = &quot;06 - Union Pay International Card&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Update Proposed %24 %2F %25</fullName>
        <actions>
            <name>Set_Use_Percent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Converts numeric values to text for quote presentation purposes</description>
        <formula>(Quote.RecordTypeId = &quot;012D0000000YTdi&quot;   || Quote.RecordTypeId = &quot;012D0000000YTdg&quot;) &amp;&amp;  Terms_Of_Trade__r.Name = &quot;BR Custom&quot;   &amp;&amp; (ISNEW()   || ISCHANGED(MSF_Per_Trans_2__c)   || ISCHANGED(MSF_PC_OF_Trans_2__c))   || (MSF_Per_Trans_2__c = 0   &amp;&amp; MSF_PC_OF_Trans_2__c = 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Update Rates - Cost Plus</fullName>
        <actions>
            <name>FU_New_Rates_Cost_Plus_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Update_Current_Rates_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-592 

Converts numeric values to text for quote presentation purposes</description>
        <formula>(Quote.RecordTypeId = &quot;012D0000000YTdc&quot;    || Quote.RecordTypeId = &quot;012D0000000YTde&quot;)    &amp;&amp; (ISNEW()    || ISCHANGED(MSF_Per_Trans_2__c)    || ISCHANGED(MSF_PC_OF_Trans_2__c))   || (MSF_Per_Trans_2__c = 0   &amp;&amp; MSF_PC_OF_Trans_2__c = 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FU - Update Rates - Standard</fullName>
        <actions>
            <name>FU_Update_Current_Rates_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FU_Update_New_Rates_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SF-592 

Converts numeric values to text for quote presentation purposes</description>
        <formula>(Quote.RecordTypeId = &quot;012D0000000YTdi&quot;   || Quote.RecordTypeId = &quot;012D0000000YTdg&quot; || Quote.RecordTypeId = &quot;012D0000000YUai&quot; || Quote.RecordTypeId = &quot;012D0000000YUdc&quot;)   &amp;&amp; (ISNEW()   || ISCHANGED(MSF_Per_Trans_2__c)   || ISCHANGED(MSF_PC_OF_Trans_2__c))   || (MSF_Per_Trans_2__c = 0   &amp;&amp; MSF_PC_OF_Trans_2__c = 0)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
