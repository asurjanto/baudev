<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Margin_Lookup_Parameter</fullName>
        <description>NEO-782</description>
        <field>Margin_Lookup_Parameter__c</field>
        <formula>TEXT(Sub_Industry__c) &amp; &quot;_&quot; &amp; TEXT(Segment__c)</formula>
        <name>Set Margin Lookup Parameter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Margin Lookup Parameter</fullName>
        <actions>
            <name>Set_Margin_Lookup_Parameter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Margin_Guide__c.Sub_Industry__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>NEO-782</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
