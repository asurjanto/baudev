<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>MPSShipmentAlertEmail</fullName>
        <description>MPS Shipment Alert Email</description>
        <protected>false</protected>
        <recipients>
            <type>partnerUser</type>
        </recipients>
        <recipients>
            <recipient>jhallis@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/MPS_Case_Status_Update</template>
    </alerts>
    <alerts>
        <fullName>Mecca_Case_alert</fullName>
        <ccEmails>itsupport@cosmeticscubed.com.au</ccEmails>
        <description>Mecca Case alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>rdavidson@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>cs@tyro.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Archive/Mecca_IT_Support</template>
    </alerts>
    <fieldUpdates>
        <fullName>FU_AAE_Track_Trace</fullName>
        <field>AAE_Track_Trace__c</field>
        <formula>&quot;https://trackandtrace.aae.com.au/&quot; &amp;  Name</formula>
        <name>FU: AAE Track &amp; Trace</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trim_Connnote</fullName>
        <field>Name</field>
        <formula>Right( left(Name,9),8)</formula>
        <name>Trim Connnote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AAE Track %26 Trace</fullName>
        <actions>
            <name>FU_AAE_Track_Trace</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>SFDC_Shipment__c.Name</field>
            <operation>contains</operation>
            <value>2GL,U9L</value>
        </criteriaItems>
        <criteriaItems>
            <field>SFDC_Shipment__c.Shipment_Status__c</field>
            <operation>equals</operation>
            <value>Inbound</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email%3A Mecca Shipments</fullName>
        <actions>
            <name>Mecca_Case_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 or 2</booleanFilter>
        <criteriaItems>
            <field>Case.Trading_Name__c</field>
            <operation>startsWith</operation>
            <value>Mecca</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Trading_Name__c</field>
            <operation>startsWith</operation>
            <value>Kit Cosmetics</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Update%3A Trim Connnote</fullName>
        <actions>
            <name>Trim_Connnote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR (3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>SFDC_Shipment__c.Name</field>
            <operation>startsWith</operation>
            <value>AU9</value>
        </criteriaItems>
        <criteriaItems>
            <field>SFDC_Shipment__c.Shipment_Status__c</field>
            <operation>equals</operation>
            <value>Outbound</value>
        </criteriaItems>
        <criteriaItems>
            <field>SFDC_Shipment__c.Name</field>
            <operation>startsWith</operation>
            <value>A2G</value>
        </criteriaItems>
        <criteriaItems>
            <field>SFDC_Shipment__c.Shipment_Status__c</field>
            <operation>equals</operation>
            <value>Outbound</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
