<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_on_Receiving_Incoming_SMS</fullName>
        <description>Email Alert on Receiving Incoming SMS</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>smagicinteract__SMS_Magic_Email_Templates/smagicinteract__Default_SMS_Magic_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_on_Receiving_Incoming_SMS_for_Contact</fullName>
        <description>Email Alert on Receiving Incoming SMS for Contact</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>smagicinteract__SMS_Magic_Email_Templates/SMS_Magic_Email_Template_for_Contact</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_on_Receiving_Incoming_SMS_for_Lead</fullName>
        <description>Email Alert on Receiving Incoming SMS for Lead</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>smagicinteract__SMS_Magic_Email_Templates/SMS_Magic_Email_Template_for_Lead</template>
    </alerts>
</Workflow>
