<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Idea_notification</fullName>
        <description>Idea notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>rdavidson@tyro.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Auto_Responses/Ideas</template>
    </alerts>
    <rules>
        <fullName>Email%3A Idea notification</fullName>
        <actions>
            <name>Idea_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Idea.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
