<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>StatusChangeNewTask</fullName>
        <actions>
            <name>POS_Product_State_Change</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(Product_status__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>POS_Product_State_Change</fullName>
        <assignedTo>acu@tyro.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Integration_Product__c.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>POS Product State Change</subject>
    </tasks>
</Workflow>
