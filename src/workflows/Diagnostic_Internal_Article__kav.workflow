<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>KA_Internal_Article_Status_to_draft</fullName>
        <description>SF-615 - Implement Salesforce Knowledge</description>
        <field>ValidationStatus</field>
        <literalValue>Not validated</literalValue>
        <name>KA: Internal Article -  Status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>KA_Internal_Publish_Article</fullName>
        <action>Publish</action>
        <description>SF-615 - Implement Salesforce Knowledge. Publish article action</description>
        <label>KA: Internal : Publish Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
</Workflow>
