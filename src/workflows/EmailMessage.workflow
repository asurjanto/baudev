<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Escalatecase</fullName>
        <field>Status</field>
        <literalValue>Escalated</literalValue>
        <name>Escalate case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reopen_Case_when_new_email_is_received</fullName>
        <description>SF 565 https://confluence.tyro.com/pages/viewpage.action?pageId=34441832</description>
        <field>Status</field>
        <literalValue>Reopened</literalValue>
        <name>Case:  Reopen when new email is received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Origin_Email</fullName>
        <field>Origin</field>
        <literalValue>Email</literalValue>
        <name>Set Case Origin - Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Email_To_Task_Record_Type</fullName>
        <description>SF-1144</description>
        <field>RecordTypeId</field>
        <lookupValue>Email_To_Task</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Email To Task Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Case Email To Task</fullName>
        <actions>
            <name>Set_Case_Origin_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Email_To_Task_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.ToAddress</field>
            <operation>equals</operation>
            <value>ayouroukelis@h-phdx8tw7t0f2r2vsfz4gcl7odeagxqob5yxb6qet3z43xva08.5e-8aq8uaa.cs84.case.sandbox.salesforce.com</value>
        </criteriaItems>
        <description>SF-1144</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Message%3A Reopen case after receiving new email to case</fullName>
        <actions>
            <name>Reopen_Case_when_new_email_is_received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>SF-745 Rule to reopen case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Case</fullName>
        <actions>
            <name>Escalatecase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Subject</field>
            <operation>startsWith</operation>
            <value>Booked</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
