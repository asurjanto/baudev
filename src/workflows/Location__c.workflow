<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FU_Set_Location_Name</fullName>
        <description>Sets Location Name

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <field>Name</field>
        <formula>LEFT(
(Street__c 
&amp; &quot; &quot; &amp; Street_2__c
&amp; &quot; &quot; &amp; Street_3__c
&amp; &quot; &quot; &amp; City__c 
&amp; &quot; &quot; &amp; TEXT(State__c) 
&amp; &quot; &quot; &amp; Postcode__c), 
80)</formula>
        <name>FU - Set Location Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>FU - Set Location Name</fullName>
        <actions>
            <name>FU_Set_Location_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets Location Name

https://confluence.tyro.com/display/SF/Single+Customer+View</description>
        <formula>ISNEW() || ISCHANGED(Street__c)  || ISCHANGED(Street_2__c) || ISCHANGED(Street_3__c) || ISCHANGED(City__c)  || ISCHANGED(State__c)  || ISCHANGED(Postcode__c)  || ISCHANGED(Country__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
