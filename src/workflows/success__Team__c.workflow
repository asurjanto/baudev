<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>success__Set_About_To_Start_To_True</fullName>
        <field>success__Start_Notification_Sent__c</field>
        <literalValue>1</literalValue>
        <name>Set About To Start To True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>true</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>success__Set_Closing_Stages_To_True</fullName>
        <field>success__End_Notification_Sent__c</field>
        <literalValue>1</literalValue>
        <name>Set Closing Stages To True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>true</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>success__Update_to_next_state</fullName>
        <field>success__Status__c</field>
        <name>Update to next state</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>NextValue</operation>
        <protected>true</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>success__Update_to_previous_state</fullName>
        <field>success__Status__c</field>
        <name>Update to previous state</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>PreviousValue</operation>
        <protected>true</protected>
    </fieldUpdates>
    <rules>
        <fullName>Ready Competition With Start Date %28Active%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>success__Team__c.success__Status__c</field>
            <operation>equals</operation>
            <value>Ready</value>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.success__Start_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Targeted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>success__Update_to_next_state</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>success__Team__c.success__Start_Date_Time__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>success__Set_About_To_Start_To_True</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>success__Team__c.success__Notify_before_Start_Date_Time__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Running Competition With End Date %28Active%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>success__Team__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Targeted</value>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.success__End_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.success__Status__c</field>
            <operation>equals</operation>
            <value>Running</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>success__Set_Closing_Stages_To_True</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>success__Team__c.success__End_Date_Time__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>success__Update_to_next_state</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>success__Team__c.success__Notify_before_End_Date_Time__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>success__New Competition is Ready</fullName>
        <actions>
            <name>success__Update_to_next_state</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND (6 OR 7)</booleanFilter>
        <criteriaItems>
            <field>success__Team__c.success__Status__c</field>
            <operation>equals</operation>
            <value>,New</value>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.success__Start_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.success__Achievements__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.success__Competitors__c</field>
            <operation>greaterOrEqual</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Targeted</value>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.success__End_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.success__Winning_Score__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>success__Ready Competition With Start Date</fullName>
        <active>false</active>
        <criteriaItems>
            <field>success__Team__c.success__State__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.success__Start_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Targeted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>success__Ready Competition is not ready</fullName>
        <actions>
            <name>success__Update_to_previous_state</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3 OR (5 AND 6)) AND 4 AND 7</booleanFilter>
        <criteriaItems>
            <field>success__Team__c.success__Achievements__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.success__Competitors__c</field>
            <operation>lessThan</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.success__Start_Date_Time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.success__Status__c</field>
            <operation>equals</operation>
            <value>Ready</value>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.success__End_Date_Time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.success__Winning_Score__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Targeted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>success__Running Competition With End Date</fullName>
        <active>false</active>
        <criteriaItems>
            <field>success__Team__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Targeted</value>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.success__End_Date_Time__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>success__Team__c.success__State__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
