import { LightningElement, api, wire, track } from 'lwc';
import { getRecord, createRecord } from 'lightning/uiRecordApi';
import findAccountContacts from '@salesforce/apex/QuoteGenerationController.findAccountContacts';
import fetchPricingGuideline from '@salesforce/apex/QuoteGenerationController.getPricingGuideline';
import submitForApproval from '@salesforce/apex/QuoteGenerationController.submitForApproval';
import quoteRecordTypeId from '@salesforce/label/c.Standard_quote_Record_Type_Id';
import uncheckPrimaryQuote from '@salesforce/apex/QuoteGenerationController.uncheckPrimaryQuote';

export default class QuoteGenerationWebComponent extends LightningElement {

    @api recordId;
    @api selectedRateType;
    @api eCommerceFrontbookQuote;
    @track error;
    @track opportunityName; 
    @track contactOptions;
    @track showLoading = false;
    @track monthlyAccessFees;
    @track eCommerceRate;

    @track pricingData;
    
    label = {
        quoteRecordTypeId
    };

    accessFeesOptions = [ { label: '0', value: '0' } , {label: '30', value: '30' } ];
    merchantStatementOptions = [ { label : 'Yes', value : 'Yes' }, { label : 'No', value : 'No' } ];
    selectedContactData; contactData; currentAccountId; merchantStatementValue; merchantStatementProvided;

    quoteName; contact; generatedQuote; quoteDescription; 
    
    pricingGuidelineColumns  = [
        { label: 'MSV Range', fieldName: 'MSVRange', type: 'text', sortable: false, cellAttributes: { alignment: 'center' } },
        { label: 'Monthly Fee', fieldName: 'HeadlineMonthlyFee', type: 'text' , sortable: false, cellAttributes: { alignment: 'center' }},
        { label: 'eCommerce Rate', fieldName: 'HeadlinePrice', type: 'text', sortable: false, cellAttributes: { alignment: 'center' } }
    ];
    

    @wire(getRecord, {
        recordId: '$recordId', fields: ['Opportunity.Name',
        'Opportunity.AccountId',
        'Opportunity.Amount',
        'Opportunity.StageName']
    })

    selectedOpportunity({ data, error }) {
        if (data) {
            //this.opportunity = data;
            
            this.opportunityName = data.fields.Name.value;
            this.currentAccountId = data.fields.AccountId.value;
            this.merchantStatementValue = data.fields.Amount.value;
        }
        if(error) {
            console.log(error);
        }
    }

    @wire(fetchPricingGuideline, { rateType: '$selectedRateType', quoteMSV : '$merchantStatementValue' })
    priceGuideline({ data, error }) {
        if (data) {
            const allMetadata = JSON.parse(data);
            this.pricingData = allMetadata.filter(metadata =>  metadata.RateType == this.selectedRateType);
            

            this.validRange = this.pricingData.filter(segmentRecord =>  
                segmentRecord.validMSVRange == true
            );

            this.monthlyAccessFees = this.validRange[0].HeadlineMonthlyFee;
            this.eCommerceRate = this.validRange[0].HeadlinePrice;

        }
    }

    @wire(findAccountContacts, { accountId: '$currentAccountId' })
    accountContacts({ data, error }) {
        if (data) {
            this.contactData = data;
            this.contactOptions = data.map(contact => ({ label: contact.Name, value: contact.Id }));
            
        }
    }


    handleContactSelection(event) {
        var selectedContact = event.detail.value;
        this.selectedContactData = this.contactData.find(contact => contact.Id == selectedContact);
    }

    handleInputFieldChange(event) {
        const field = event.target.name;
        this[field] = event.target.value;
    }

    createQuote(event){
        if (this.validateAllFields()) {           
            this.showLoading = true;

            uncheckPrimaryQuote({
                opportunityId: this.recordId
            })
            .then(() => {
                const fields = {};
                fields['Name'] = this.quoteName;
                fields['Monthly_Access_Fees__c'] = this.monthlyAccessFees;
                fields['eCommerce_Rate__c'] = this.eCommerceRate;
                fields['ContactId'] = this.contact;
                fields['OpportunityId'] = this.recordId;
                fields['Description'] = this.quoteDescription;
                fields['RecordTypeId'] = this.label.quoteRecordTypeId;
                fields['Merchant_Statement_Provided__c'] = this.merchantStatementProvided;
                fields['Quote_MSV__c'] = this.merchantStatementValue;
                fields['Primary_Quote__c'] = true;
                fields['Rate_Type__c'] = this.selectedRateType;
                
                
                const recordInput = { apiName: 'Quote', fields };
                createRecord(recordInput)
                .then(quote => {
                    this.generatedQuote = quote;
                    
                    this.navigateToQuote();
                })
                .catch(error => {
                    this.handleTransactionErrors(error);
                });
            })
            .catch((error) => {
                this.showLoading = false;
                this.handleTransactionErrors(error);
            });

            
        }else{

        }
        
    }

    handleTransactionErrors(error) {
        if(error.body) {
            if (Array.isArray(error.body.output.errors)) {
                this.error = error.body.output.errors.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                this.error = error.body.message;
            }
        }else{
            this.error = 'Exception : ' + error;
        }
        
        this.showLoading = false;
    }

    validateAllFields() {
        return [...this.template.querySelectorAll('lightning-input'), ...this.template.querySelectorAll('lightning-combobox')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
    }

    navigateToOpportunity(event) {
        window.location.href = ('/' + this.recordId);
    }

    navigateToQuote() {
        submitForApproval({
            quoteId: this.generatedQuote.id
        })
        .then(() => {
            window.location.href = ('/' + this.generatedQuote.id);
        })
        .catch((error) => {
            this.showLoading = false;
            this.handleTransactionErrors(error);
        });
        
    }

    navigateToQuoteSelection() {
        const selectedEvent = new CustomEvent("quotetypereselection", {
            detail: true
          });

        this.dispatchEvent(selectedEvent);
    }
}