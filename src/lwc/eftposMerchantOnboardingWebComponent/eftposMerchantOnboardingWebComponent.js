import { LightningElement, api } from 'lwc';

import NAME_FIELD from '@salesforce/schema/Opportunity.Name';
import ECOMM_WEBSITE_URL from '@salesforce/schema/Opportunity.eComm_Website_URL__c';
import STAGENAME from '@salesforce/schema/Opportunity.StageName';

export default class EftposMerchantOnboardingWebComponent extends LightningElement {

    @api recordId;

    fields = [NAME_FIELD, ECOMM_WEBSITE_URL, STAGENAME ];

    constructor() {
        super();
        this.classList.add('new-class');
    }

}