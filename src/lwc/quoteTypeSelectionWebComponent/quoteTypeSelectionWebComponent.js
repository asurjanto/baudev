import { LightningElement, track, api, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';

export default class quoteTypeSelectionWebComponent extends LightningElement {
    
    @api recordId;
    @track value = '';
    @track disableButton = true;
    selectedOption;
    @track isStageNameValid = false;
    @track selectedRateType;
    @track eCommerceFrontbookQuote = false;
    @track renderQuoteCreationComponent;

    @wire(getRecord, {
        recordId: '$recordId', fields: ['Opportunity.StageName', 'Opportunity.RecordType.Name']
    })

    selectedOpportunity({ data, error }) {
        if (data) {
            //this.opportunity = data;
            this.recordTypeName = data.fields.RecordType.displayValue;
            if(this.recordTypeName == 'eCommerce Only') {
                this.renderQuoteCreationComponent =  true;
                this.selectedRateType = 'Simple Pricing';
                this.eCommerceFrontbookQuote = true;
            }else{
                this.renderQuoteCreationComponent =  false;
                
            } 
            

            this.opportunityStageName = data.fields.StageName.value;
            if(this.opportunityStageName == 'Eligible' || this.opportunityStageName == 'Application Sent' || this.opportunityStageName == 'Application Received*' ) {
                this.isStageNameValid = true;
            }else{
                this.isStageNameValid = false;
            }

            
        }
        if(error) {
            console.log(error);
        }
    }

    navigateToOpportunity(event) {
        window.location.href = ('/' + this.recordId);
    }


    quoteTypeSelected(event) {
        this.selectedOption = event.target.value;
        this.disableButton = false;
    }

    handleClick(event) {
        this.selectedRateType = this.selectedOption;
        this.renderQuoteCreationComponent = true;

    }

    handleReSelection(event) {
        this.disableButton = true;
        this.renderQuoteCreationComponent = false;
        this.selectedOption = '';
        this.value = '';
    }
}