import { LightningElement, track, api, wire } from 'lwc';
import { getRecord, createRecord, updateRecord } from 'lightning/uiRecordApi';
import { getObjectInfo, getPicklistValues } from 'lightning/uiObjectInfoApi';
import Id from '@salesforce/user/Id';
import findAccountLocations from '@salesforce/apex/eCommerceOnboardingController.findAccountLocations';

export default class ECommerceOnboardingWebComponent extends LightningElement {

    //Incoming Opportunity Record 
    @api recordId;
    currentuserId = Id;
    @track showLoading = false;
    @track error;
    @track locationOptions;
    @track showIEAPCheckbox = false;
    @track showNewLocationSection = false;
    @track showeCommerceTemplate = false;
    //@track showActivateMIDLocationSection = false;

    opportunityValidState = false;
    userValidState = false;
    //Location Input Fields
    selectedLocationData; locationCity; locationPostcode; locationState;
    locationStreet1 = ''; locationStreet2 = ''; 

    //Mid Location Fields
    ieapEnabled; classicAmexSeid; selectedMIDAMEXMerchantId; selectedMIDId; opportunityShoppingCart; opportunityProductType;
    locationData; currentAccountId; locationCAID; createdMidLocation; //previousMIDLocation //bankDepositText

    @wire(getRecord, {
        recordId: '$recordId', fields: ['Opportunity.Name',
            'Opportunity.AccountId',
            'Opportunity.eComm_Website_URL__c',
            'Opportunity.StageName',
            'Opportunity.MID__c',
            'Opportunity.eComm_MID_Location__c',
            'Opportunity.eComm_Product_Type__c',
            'Opportunity.eComm_Shopping_Cart__c',
            'Opportunity.Application_Specialist__c'
            ]
    })
    selectedOpportunity({ data, error }) {
        if (data) {
            //this.opportunity = data;
            this.currentAccountId = data.fields.AccountId.value;
            this.websiteURL = data.fields.eComm_Website_URL__c.value;
            this.selectedMIDId = data.fields.MID__c.value;
            this.opportunityShoppingCart = data.fields.eComm_Shopping_Cart__c.value;
            this.opportunityProductType  = data.fields.eComm_Product_Type__c.value;

            if(data.fields.StageName.value == 'Approved' && data.fields.Application_Specialist__c.value !== null) {
                this.opportunityValidState = true;
                if(this.userValidState) {
                    this.showeCommerceTemplate = true;
                }
            }
            /*
            if(data.fields.eComm_MID_Location__c.value !== null) {
                //this.showActivateMIDLocationSection = true;
                //this.previousMIDLocation = data.fields.eComm_MID_Location__c.value;
            }
            */
            
        }
        if(error) {
            console.log(error);
        }
    }

    @wire(getRecord, { recordId: '$currentuserId', fields: ['User.UserRole.Name'] })
    currentUserDetails( {data, error }) {
        if(data) {
             if(data.fields.UserRole.displayValue == 'Application Specialists' ) {
                this.userValidState = true;
                if(this.opportunityValidState) {
                    this.showeCommerceTemplate = true;
                }
                
             }
        }
    }

    @wire(getRecord, { recordId: '$selectedMIDId', fields: ['Merchant_ID__c.Name', 'Merchant_ID__c.Amex_Merchant_ID__c'] })
    selectedMerchantId({ data, error }) {
        if (data) {
            if (data.fields.Amex_Merchant_ID__c.value !== null) {
                this.showIEAPCheckbox = true;
                this.selectedMIDAMEXMerchantId = data.fields.Amex_Merchant_ID__c.value;
            } else {
                this.showIEAPCheckbox = false;
            }
        }
    }


    @wire(findAccountLocations, { accountId: '$currentAccountId' })
    accountLocations({ data, error }) {
        if (data) {
            this.locationData = data;
            this.locationOptions = data.map(location => ({ label: location.Name, value: location.Id }));
            if (this.locationData == undefined) {
                this.showNewLocationSection = true;
            }
        }
    }

    @wire(getObjectInfo, { objectApiName: 'Location__c' })
    locationObjectInfo;

    @wire(getPicklistValues, {
        recordTypeId: '$locationObjectInfo.data.defaultRecordTypeId',
        fieldApiName: 'Location__c.State__c'
    })
    statePicklistValues;

    handleLocationSelection(event) {
        var selectedLocation = event.detail.value;
        this.selectedLocationData = this.locationData.find(location => location.Id == selectedLocation);
    }

    handleInputFieldChange(event) {
        const field = event.target.name;
        switch (field) {
            case 'street1':
                this.locationStreet1 = event.target.value;
                break;
            case 'street2':
                this.locationStreet2 = event.target.value;
                break;
            case 'city':
                this.locationCity = event.target.value;
                break;
            case 'state':
                this.locationState = event.target.value;
                break;
            case 'postcode':
                this.locationPostcode = event.target.value;
                break;
            case 'amexieapseid':
                this.ieapEnabled = event.target.checked;
                break;
            case 'amexclassicseid':
                this.classicAmexSeid = event.target.value;
                break;
            //case 'bankdeposittext':
            //    this.bankDepositText = event.target.value;
            //    break;
            case 'CAID':
                this.locationCAID = event.target.value;
                break;
        }
    }

    createECommerceLocation(event) {
        this.clickedButtonLabel = event.target.label;
        if (this.validateAllFields()) {
            this.showLoading = true;
            if (!this.showNewLocationSection || this.selectedLocationData) {
                this.createMidLocationRecord();
            } else {
                //Create a new Location First
                const fields = {};
                fields['Street__c'] = this.locationStreet1;
                fields['Street_2__c'] = this.locationStreet2;
                fields['State__c'] = this.locationState;
                fields['City__c'] = this.locationCity;
                fields['Postcode__c'] = this.locationPostcode;
                fields['Account__c'] = this.currentAccountId;
                fields['Name'] = 'Auto-Set';
                const recordInput = { apiName: 'Location__c', fields };
                createRecord(recordInput)
                    .then(location => {
                        this.selectedLocationData = location;
                        this.createMidLocationRecord();
                    })
                    .catch(error => {
                        this.handleTransactionErrors(error);
                    });

            }
        } else {

        }
    }

    validateAllFields() {
        return [...this.template.querySelectorAll('lightning-input'), ...this.template.querySelectorAll('lightning-combobox')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);
    }

    createMidLocationRecord() {
        const fields = {};
        //Stupid from salesforce
        if (this.showNewLocationSection) {
            fields['Location__c'] = this.selectedLocationData.id;
        } else {
            fields['Location__c'] = this.selectedLocationData.Id;
        }
        //if(this.locationCAID !== null && this.locationCAID !== undefined) {
            
        //}else{
        //    fields['Status__c'] = 'InActive';
        //}
        fields['Status__c'] = 'Active';
        fields['CAID__c'] = this.locationCAID;
        fields['Merchant_ID__c'] = this.selectedMIDId;
        fields['Website__c'] = this.websiteURL;
        fields['eComm_Product_Type__c'] = this.opportunityProductType;
        fields['eComm_Shopping_Cart__c'] = this.opportunityShoppingCart;
        //fields['Bank_Deposit_Text__c'] = this.bankDepositText;
        if (this.showIEAPCheckbox && this.ieapEnabled) {
            fields['Amex_Type__c'] = 'IEAP';
            fields['Amex_Merchant_Number__c'] = this.selectedMIDAMEXMerchantId;
        } else if (this.classicAmexSeid !== undefined) {
            fields['Amex_Type__c'] = 'CLASSIC';
            fields['Amex_SEID__c'] = this.classicAmexSeid;
        } else {
            fields['Amex_Type__c'] = 'NONE';
        }
        const recordInput1 = { apiName: 'MID_Location__c', fields };
        createRecord(recordInput1)
            .then(midLocation => {
                //if(this.locationCAID !== null && this.locationCAID !== undefined) {
                    this.createdMidLocation = midLocation;
                    this.updateOpportunityToClosedWon();
                /*}else{
                    const fields = {};
                    fields['Id'] = this.recordId;
                    fields['eComm_MID_Location__c'] = midLocation.id;
                    const recordInput = { fields };
                    updateRecord(recordInput)
                    .then(() => {
                        window.location.href = ('/' + this.recordId);
                    })
                    .catch(error => {
                        this.showLoading = false;
                    });
                }
                */
            })
            .catch(error => {
                this.handleTransactionErrors(error);
            });
    }

    handleLocationToggle() {
        this.showNewLocationSection = !this.showNewLocationSection;
        this.selectedLocationData = null;
    }

    updateOpportunityToClosedWon() {
        const fields = {};
        fields['Id'] = this.recordId;
        fields['StageName'] = 'Closed Won';
        fields['eComm_MID_Location__c'] = this.createdMidLocation.id;
        const recordInput = { fields };
        updateRecord(recordInput)
            .then(() => {
                this.navigateToOpportunity();
            })
            .catch(error => {
                this.handleTransactionErrors(error);
            });

    }

    navigateToOpportunity() {
        window.location.href = ('/' + this.recordId);
    }

    handleTransactionErrors(error) {
        if(error.body) {
            if (Array.isArray(error.body.output.errors)) {
                this.error = error.body.output.errors.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                this.error = error.body.message;
            }
        }else{
            this.error = 'Exception : ' + error;
        }
        
        this.showLoading = false;
    }

    /*
    activateMIDLocationRecord() {

        
        if(this.validateAllFields()) {
            this.showLoading = true;
            const fields = {};
            fields['Id'] = this.previousMIDLocation;
            fields['CAID__c'] = this.locationCAID;
            fields['Status__c'] = 'Active';
            const recordInput = { fields };
            updateRecord(recordInput)
                .then(() => {
                    this.updateOpportunityToClosedWon();
                })
                .catch(error => {
                    this.showLoading = false;
                    this.error = 'Unknown error';
                    if (Array.isArray(error.body.output.errors)) {
                        this.error = error.body.output.errors.map(e => e.message).join(', ');
                    } else if (typeof error.body.message === 'string') {
                        this.error = error.body.message;
                    }
                });
        }
        
    }
    */

}