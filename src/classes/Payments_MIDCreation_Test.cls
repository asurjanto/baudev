@isTest
private class Payments_MIDCreation_Test {
    
    private static final String ECOMMERCE;
    
    static {
        ECOMMERCE  = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('eCommerce Only').getRecordTypeId();
    }

	private static testMethod void testMidCreation() {
        Test.startTest();
            
            Opportunity createdOpp;
            
            for(opportunity opp : [Select id, name, opportunity_no__c from opportunity where name = 'Test eComm opp' limit 1]) {
                createdOpp = opp;
            }
            
            Payments_MIDRequest newMidRequest = new Payments_MIDRequest();
            newMidRequest.merchantId = '22323';
            newMidRequest.requesterSourceId = createdOpp.opportunity_no__c;
            newMidRequest.mccCode = '1771';
            newMidRequest.tradingName = 'Test Ttrading Name';
        
            
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            
            req.requestURI = '/services/apexrest/v1/merchants';  
            req.httpMethod = 'POST';  
            req.addHeader('Content-Type', 'application/json'); 
            
            req.requestBody = Blob.valueof(JSON.serialize(newMidRequest));
            
            RestContext.request = req;
            RestContext.response = res;
            
            
            Payments_MIDCreation.createMerchantId();
            
            //Error Scenario
            Payments_MIDRequest newMidRequest1 = new Payments_MIDRequest();
            newMidRequest1.merchantId = '22323';
            newMidRequest1.requesterSourceId = 'radomno123';
            newMidRequest1.mccCode = '1771';
            newMidRequest1.tradingName = 'Test Ttrading Name';
        
            
            RestRequest req1 = new RestRequest(); 
            RestResponse res1 = new RestResponse();
            
            req1.requestURI = '/services/apexrest/v1/merchants';  
            req1.httpMethod = 'POST';  
            req1.addHeader('Content-Type', 'application/json'); 
            
            req1.requestBody = Blob.valueof(JSON.serialize(newMidRequest1));
            
            RestContext.request = req1;
            RestContext.response = res1;
            
            Payments_MIDCreation.createMerchantId();
            
            
        Test.StopTest();
	}
	
	@TestSetup
	private static void createTestData() {
	    
	    Account newTestAccount = TestUtils.createAccount('Test Account', true);
	    Opportunity newOpportunity = TestUtils.createOpportunity('Test eComm opp', 'Eligible', System.today(), false);
	    newOpportunity.RecordTypeId = ECOMMERCE;
	    newOpportunity.AccountId = newTestAccount.Id;
	    insert newOpportunity;
	    
	    testutils.createNewMCCDescription('testmcc', true, 'fnb', '1771', true);
	    
	    
	}

}