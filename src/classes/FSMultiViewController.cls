/****************************************************************
* Class Name    : FSMultiViewController
* Copyright     : Tyro Payments (c) 2018
* Description   : Fetch Case List view id
* Created By    : Arvind Thakur
* 
* Modification Log ==============================================
* Author                        Date                    JIRA-LINK
* Arvind Thakur (created)     1st July, 2018            OFS-11334
* 
* 
****************************************************************/
public with sharing class FSMultiViewController {
    
    public String FSListId {get;set;}
    public String openRequestListId {get;set;}
    
    private static String FSCasesListName;
    private static String openRequestListName;
    
    static {
        FSCasesListName = 'FS Cases';
        openRequestListName = 'My Open Requests';
    }
  
    public FSMultiViewController() {
        
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Id FROM Case LIMIT 1]));
        List<SelectOption> allViews = ssc.getListViewOptions();
        for (SelectOption so : allViews) {
            if (so.getLabel() == FSCasesListName) {
                FSListId = so.getValue().substring(0,15);
            }
            else if(so.getLabel() == openRequestListName) {
                openRequestListId = so.getValue().substring(0,15);
            }
        }
    }
  
}