/*************************************************************************
Author : Isha Saxena
Description: To update the time to first call for a lead : NEO490
Created Date: 10 April 2017
Last modified Date:  7 June 2017
*************************************************************************/

global class Batch_Time_to_first_call implements Database.Batchable<sobject>
{
    BusinessHours stdBusinessHours = [select id,Name from businesshours where Name = 'Marketing_Hours'];
            
    DateTime startDate;
    DateTime endDate;
    global List<Task> sobjList = new List<Task>();
    List<Lead> leadlist = new List<Lead>(); // to update first call date time
    List<Lead> ldlist = new List<Lead>(); // to calculate business hours
    Set<Lead> LeadId = new Set<Lead>();       

    global Database.QueryLocator start(Database.BatchableContext bc)
    {
         System.debug('Businesshours###'+stdBusinessHours);
         String myTestString = 'Outbound Call';
         String tp = 'Lead';
    
        return Database.getQueryLocator('SELECT Id,Whoid,Who.type,Trading_Name__c,Sub_Type__c,Sales_Team__c,CreatedDate FROM Task WHERE Sub_Type__c = :myTestString AND Who.type = :tp');
    }
    global void execute(Database.BatchableContext bc,List<Task> sobj)
    {
        System.debug('####Task list'+sobj);
                  if(sobj != null && sobj.size() > 1)
                   {        
                           for(Task ts : sobj)
                          {
                            if(ts.Whoid != null)
                            {
                                 lead ld = [Select Id,Name,First_Call_Date_Time__c,Created_Date_Time__c,OwnerId,CreatedDate,Time_To_First_Call__c,RecordTypeId From Lead where Id =: ts.WhoId]; 
                                 
                                 if(ld.First_Call_Date_Time__c == null)
                                {
                                   ld.First_Call_Date_Time__c = ts.CreatedDate;
                                }
                                 
                                 LeadId.add(ld);
                                 if(LeadId.add(ld))
                                 {
                                         Leadlist.add(ld);
                                 
                                 }
                             }
                             
                           }
                           update Leadlist;
                        if(LeadId.size()>1)
                        {
                           for(lead la :LeadId)
                           {
                                startDate = la.CreatedDate;
                                endDate = System.NOW();
                                if(la.First_Call_Date_Time__c != null)
                                {
                                endDate = la.First_Call_Date_Time__c;
                                }
                                long td = BusinessHours.diff(stdBusinessHours.Id,startDate,endDate); 
                                Long minutes = td / (60 * 1000);
                                System.debug('&&&&&&min'+minutes);
                                String hrs = (minutes / 60) + '.' + Math.mod(minutes, 60); // converting minutes to hours
                                double  busi_cal = double.valueof(hrs);
                                la.Time_To_First_Call__c = busi_cal;
                                ldlist.add(la);
                                update ldlist;
                               System.debug('&&&&&&Leadlist'+ldlist);
                           }
                       }
                     }
        
    }
        global void finish(Database.BatchableContext bc)
        {
           
            
        }
    
}