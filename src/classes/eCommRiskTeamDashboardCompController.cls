public with sharing class eCommRiskTeamDashboardCompController {
    public eCommRiskTeamDashboardCompController() {

    }

    @AuraEnabled
    public static string fetchRiskOpportunity(String customMetadataAPIName) {

        Opportunity_Datatable_Settings__mdt tableSettings;
        for(Opportunity_Datatable_Settings__mdt datatableSettings : [SELECT Id, Filter_by_Stage_Name__c, Record_Type_Name__c, Table_Header_JSON__c, Table_Name__c 
                                                                        FROM Opportunity_Datatable_Settings__mdt 
                                                                        WHERE DeveloperName =:customMetadataAPIName]) {
            tableSettings =  datatableSettings;                                                               
        }


        
        List<OpportunityWrapper> wrapperList = new List<OpportunityWrapper>();
        for(Opportunity Opp : [SELECT Id, Name, AccountId, Account.Name, Account_ABN__c, eComm_Website_URL__c, MID__r.Name, MID__c, eComm_Priority__c,
                                        Opportunity_No__c, StageName, eComm_Risk_Assessor__r.Name, eComm_Risk_Assessor__c, lastModifiedDate
                                    FROM Opportunity
                                    WHERE RecordType.Name =: tableSettings.Record_Type_Name__c
                                    AND StageName =: tableSettings.Filter_by_Stage_Name__c
                                     ]) {
            wrapperList.add(new OpportunityWrapper(opp));
        }

        return JSON.serialize(new ResponseWrapper(wrapperList, tableSettings.Table_Name__c, tableSettings.Table_Header_JSON__c));
    }

    public class ResponseWrapper {

        public List<OpportunityWrapper> opportunityData;
        public String tableName;
        public String tableHeader;
        public String tableHeaderJSON;

        public ResponseWrapper(List<OpportunityWrapper> oppData, String tableName, String tableHeaderJSON) {
            this.opportunityData = OppData;
            this.tableName = tableName;
            this.tableHeaderJSON = tableHeaderJSON;
        }

    }

    public class OpportunityWrapper {

        public String Id;
        public String accountName;
        public String accountId;
        public String riskAssessorName;
        public String riskAssessorId;
        public String opportunityLink;
        public String accountABN;
        public String opportunityNumber;
        public String midName;
        public String midId;
        public String websiteURL;
        public String lastModifiedDate;
        public String priority;

        public OpportunityWrapper(Opportunity opportunityObject) {

            this.Id = opportunityObject.Id;
            this.opportunityLink = '/' + opportunityObject.Id;
            this.accountId = '/' + opportunityObject.AccountId;
            this.accountName = opportunityObject.Account.Name;
            this.riskAssessorId = opportunityObject.eComm_Risk_Assessor__c;
            this.riskAssessorName = opportunityObject.eComm_Risk_Assessor__r.Name;
            this.accountABN = opportunityObject.Account_ABN__c;
            this.opportunityNumber = opportunityObject.Opportunity_No__c;
            this.midName = opportunityObject.MID__r.Name;
            this.midId = opportunityObject.MID__c;
            this.websiteURL = opportunityObject.eComm_Website_URL__c;
            this.lastModifiedDate = String.valueOf(opportunityObject.lastModifiedDate);
            this.priority = opportunityObject.eComm_Priority__c;

        }

    }
    
}