global class QuotePDFGeneratorService {
    
    Webservice static Boolean savePDFToObject(String quoteId) {

        PageReference pageReferenceForPDF = new  PageReference ('/apex/QuotePDFPage?Id=' + quoteId);
        QuoteDocument newQuoteDocument = new QuoteDocument();

        Blob renderedPDF = !Test.isRunningTest() ? pageReferenceForPDF.getContentAsPDF() : Blob.ValueOf('dummy text');
        newQuoteDocument.Document = renderedPDF;
        newQuoteDocument.QuoteId = quoteId;
        try{           
            insert newQuoteDocument;
            return true;
        }catch(Exception ex) {
            return false;
        }
        
        
    }

}