global class MerchantIdDTO {
    
    public String abn;
    public String merchantId;
    public String tradingName;
    public Boolean motoEnabled;
    public Boolean merchantTerminated;
    public String notificationEmailAddress;
    public String channelAgreementBackofficeId;
    public String integrationProductId;
    public String mccCode;
    public List<MerchantLocationInformation> midLocations;
    
    public class MerchantLocationInformation {
        
        public Location merchantLocation;
        public Boolean blocked;
        public Boolean enabled; 
        public String caid;
        public String amexIeapSeId;
        public String websiteURL;
        public String bankDepositText;
        
    }
    
    public class Location {
        
        public String streetLine1;
        public String streetLine2;
        public String suburb;
        public String postcode;
        public String state; 
        
    }
    
    
}