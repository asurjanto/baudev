@isTest
private class Test_Track_asset_Account
{
    static testMethod void Assetaccount()
    {
        account test = new account();
        test.Name = 'TestAccount';
        insert test;
        
         Product2 lProduct =  new Product2(); 
         lProduct.name = 'Test';
         lProduct.ProductCode = '1325718290';
         lProduct.Type__c = 'Production Terminal';
         lProduct.Terminal__c = 750 ;
         insert lProduct;
        
        account test1 = new account();
        test1.Name = 'TestAccount123';
        insert test1;
        
       List<Asset> lstasset =   new List<Asset>
                         {
                          new Asset(AccountId = test.id,Name = 'test1',Product2Id = lProduct.Id,Status = 'Fleet',Status__c='In Use',Hardware_Version__c = '444444',Firmware_version__c='1119987', Make__c='new testing11',General_Description__c='yeuijh11',Physical_Connections__c='98920uey11',Last_inventory_performed__c = Date.today()),
                          new Asset(AccountId = test.id,Name = 'test2',Product2Id = lProduct.Id,Status = 'Fleet',Status__c='In Use',Hardware_Version__c = '332144',Firmware_version__c='2229987', Make__c='new testing22',General_Description__c='yeuijh22',Physical_Connections__c='98920uey22',Last_inventory_performed__c = Date.today()),
                          new Asset(AccountId =  test.id,Name = 'test3',Product2Id = lProduct.Id,Status = 'Fleet',Status__c='In Use',Hardware_Version__c = '441224',Firmware_version__c='3339987', Make__c='new testing33',General_Description__c='yeuijh33',Physical_Connections__c='98920uey33',Last_inventory_performed__c = Date.today()),
                          new Asset(AccountId =  test.id,Name = 'test4',Product2Id = lProduct.Id, Status = 'Fleet',Status__c='In Use',Hardware_Version__c = '1234444',Firmware_version__c='4449987', Make__c='new testing44',General_Description__c='yeuijh44',Physical_Connections__c='98920uey44',Last_inventory_performed__c = Date.today()),
                          new Asset(AccountId =  test.id,Name = 'test5',Product2Id = lProduct.Id,Status = 'Fleet',Status__c='In Use',Hardware_Version__c = '4444898',Firmware_version__c='5559987', Make__c='new testing55',General_Description__c='yeuijh55',Physical_Connections__c='98920uey55',Last_inventory_performed__c = Date.today())
                         };  
      insert lstasset;
      System.debug('Lead after inserting: ' + lstasset);
      lstasset[0].Name = 'MikeMuller';
      lstasset[0].AccountId = test1.Id;
      lstasset[1].AccountId = test1.Id;
      lstasset[3].AccountId = test1.Id;
      lstasset[4].AccountId = test1.Id;
      update lstasset;
      System.debug('Lead after update: ' + lstasset);
    
    }
    
}