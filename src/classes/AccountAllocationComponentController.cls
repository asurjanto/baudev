public class AccountAllocationComponentController {
    
    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP; 
    private static final String TYRO_CUSTOMER;
    
   
    
    
    static {
        
        MERCHANT_GROUP  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();
        
        
        
    }
    
    @AuraEnabled
    public static String fetchRulesbyPOSorAM(String userPOSId, String requestView) {
        
        return fetchPOSSegmentWrapper(userPOSId, requestView);
        
    }
    
    public static String fetchPOSSegmentWrapper(String userPOSId, String requestView) { 
        
        Map<String, POSSegmentWrapper> allocationRules = new Map<String, POSSegmentWrapper>();
        
        String query = 'SELECT Id, Name, Account_Manager__c, Active__c, Integration_Product__c, Account_Manager__r.Name, ';
        query       += ' Integration_Product__r.Name, Segment__c '; 
        query       += ' FROM Account_Allocation_Rules__c ';
        query       += ' WHERE Active__c = true '; 
        
        if(requestView == 'amViewRequest') {
            query   += ' AND Account_Manager__c =:userPOSId ';
            query   += ' Order by Integration_Product__r.Name  ASC';
        }else{
            query   += ' AND Integration_Product__c =:userPOSId ';
            query   += ' Order by  Account_Manager__r.FirstName DESC';
        }
        
        
        for(Account_Allocation_Rules__c acRules :  Database.query(query)) {
            
            String key = acRules.Integration_Product__c + String.valueOf(acRules.Account_Manager__c);   
                      
            if(!allocationRules.containsKey(key))   {
                POSSegmentWrapper newWrapper = new POSSegmentWrapper(acRules.Account_Manager__c + '_' + acRules.Integration_Product__c, acRules.Integration_Product__r.Name, acRules.Integration_Product__c, 
                                                                        acRules.Account_Manager__c, acRules.Account_Manager__r.Name);
                allocationRules.put(key, newWrapper);
            }
            
            AccountAllocationComponentController.POSSegmentWrapper s = (AccountAllocationComponentController.POSSegmentWrapper)allocationRules.get(key);
            //s.storeRulesMap(acRules.Segment__c, acRules);
            //s.storeOtherKeys(acRules.Segment__c);
            
            Map<String,Object> obj =  (Map<String,Object>) SYSTEM.JSON.deserializeUntyped(JSON.serialize(s));
            obj.put(acRules.Segment__c, true);
            
            allocationRules.put(key , (AccountAllocationComponentController.POSSegmentWrapper)JSON.deserialize(JSON.serialize(obj), AccountAllocationComponentController.POSSegmentWrapper.class));
            
        }
        
       
        
        return JSON.serialize(allocationRules.values());
    }
    
    
    @AuraEnabled
    public static String fetchManagersORpos(String viewRequested) {
        
        Map<Id, ResourceWrapper> returnWrapper = new Map<Id, ResourceWrapper>();
        Set<Id> accountManagerIds = new Set<Id>();
        List<AggregateResult> resutsToReturn = new List<AggregateResult>();
        
        String query = '';
        if(viewRequested == 'amViewRequest') {
            
            query += ' SELECT Account_Manager__c resourceId, Account_Manager__r.Name resourceName, Account_Manager__r.IsActive userStatus';
            query += ' FROM Account_Allocation_Rules__c ';
            query += ' WHERE Active__c = true ';
            query += ' Group BY Account_Manager__r.Name, Account_Manager__c, Account_Manager__r.IsActive';
            
        }else{
            
            query += ' SELECT Integration_Product__c resourceId, Integration_Product__r.Name resourceName ';
            query += ' FROM Account_Allocation_Rules__c ';
            query += ' WHERE Active__c = true ';
            query += ' Group BY Integration_Product__r.Name, Integration_Product__c';
            
        }
        
        
        for(List<AggregateResult> aggResults : Database.query(query)) {
            
            for(AggregateResult aggr : aggResults) {
                accountManagerIds.add((Id)(aggr.get('resourceId')));
                if(viewRequested == 'amViewRequest') {
                    returnWrapper.put((Id)(aggr.get('resourceId')), new ResourceWrapper((Id)(aggr.get('resourceId')), (String)(aggr.get('resourceName')), (Boolean)(aggr.get('userStatus'))));
                }else{
                    returnWrapper.put((Id)(aggr.get('resourceId')), new ResourceWrapper((Id)(aggr.get('resourceId')), (String)(aggr.get('resourceName')), true));
                }
                
            }
        }
        
        
        if(viewRequested == 'amViewRequest') {
            for(List<AggregateResult> totalResults : [SELECT Count(Id) cnt, RecordType.Name rtName, Owner.Id userId, Owner.Name userName, Owner.Title userTitle
                                                        FROM Account 
                                                        WHERE Owner.Id IN:accountManagerIds 
                                                        AND (RecordType.Id =:TYRO_CUSTOMER OR RecordType.Id =:CORPORATE_GROUP OR RecordType.Id =:MERCHANT_GROUP)
                                                        GROUP BY RecordType.Name, Owner.Id, Owner.Name, Owner.Title]) {
                for(AggregateResult aggr : totalResults) {
                    ResourceWrapper wrapper = returnWrapper.get((Id)aggr.get('userId'));
                    wrapper.setAccountValues((String)aggr.get('rtName'), (Integer)aggr.get('cnt'));
                    returnWrapper.put((Id)aggr.get('userId'), wrapper);
                }
                             
            }
        }else{
            for(List<AggregateResult> totalResults : [SELECT Count(Id) cnt, RecordType.Name rtName, Integration_Product__c posName
                                                        FROM Account 
                                                        WHERE Integration_Product__c IN:accountManagerIds 
                                                        AND (RecordType.Id =:TYRO_CUSTOMER OR RecordType.Id =:CORPORATE_GROUP OR RecordType.Id =:MERCHANT_GROUP)
                                                        GROUP BY RecordType.Name, Integration_Product__c]) {
                for(AggregateResult aggr : totalResults) {
                        ResourceWrapper wrapper = returnWrapper.get((Id)aggr.get('posName'));
                        wrapper.setAccountValues((String)aggr.get('rtName'), (Integer)aggr.get('cnt'));
                        returnWrapper.put((Id)aggr.get('posName'), wrapper);
                }
                             
            }
            
           
        }
        

        return JSON.serialize(returnWrapper.values());
    }


    
    
    @AuraEnabled
    public static String saveNewAllocationRules(String  unsavedTableData, String userOrPosId, String currentView) {
        
        List<Account_Allocation_Rules__c> newRules = new List<Account_Allocation_Rules__c>();
        
        Map<String, POSSegmentWrapper> newDataMap = new Map<String, POSSegmentWrapper>();

        List<POSSegmentWrapper> newData = (List<POSSegmentWrapper>)JSON.deserialize(unsavedTableData, List<POSSegmentWrapper>.class);
        for(POSSegmentWrapper updatedData : newData) {
            newDataMap.put(updatedData.tableKey, updatedData);
        }
        
        Account_Allocation_Rules__c newRule; 
        for(String tableKey : newDataMap.keySet()) {
            
            Map<String,Object> obj =  (Map<String,Object>) SYSTEM.JSON.deserializeUntyped(JSON.serialize(newDataMap.get(tableKey)));
            
            
            for(String wrapperClassMember : obj.keyset()) {
                
                //Lightning Data Table will always return tablekey as a string and rest params as boolean
                //Convertion to boolan is therefore not a problem
                
                if(wrapperClassMember != 'tableKey') {
                    //This prevents the users from unchecking/deactivating rule. They can instead activate the rule on some other side.
                    //AM can transfer the rule, just not deactive it. (To Prevent any segment-POS getting let unassigned/inactive)
                    if(obj.get(wrapperClassMember) != null && Boolean.valueOf(obj.get(wrapperClassMember))){
                        
                            newRule = new Account_Allocation_Rules__c ();
                            newRule.Segment__c = (String)wrapperClassMember; 
                            newRule.Active__c = Boolean.valueOf(obj.get(wrapperClassMember));
                            newRule.Account_Manager__c = ((String)obj.get('tableKey')).split('_')[0];
                            newRule.Integration_Product__c = ((String)obj.get('tableKey')).split('_')[1];
                            newRule.Rule_Allocation_Name__c = newRule.Segment__c + '_' + newRule.Integration_Product__c;
                            newRules.add(newRule);
                    }
                }
                
            } 
            
            
        }
        
        
        Schema.SObjectField externalIdField  = Account_Allocation_Rules__c.Fields.Rule_Allocation_Name__c;
        List<Database.UpsertResult> results = Database.upsert(newRules, externalIdField, false);
        Set<Id> successIds = new Set<Id>();
        for(Database.UpsertResult upsertResults : results) {
            if(upsertResults.isSuccess()) {
                successIds.add(upsertResults.getId());
            }
        }
        
        
        
        return fetchPOSSegmentWrapper(userOrPosId, currentView);
        
        
    }
    
    
    @AuraEnabled
    public static String upsertAllocationRule(String userId, String posId, String Segment, String viewRequest) {
        
        POSSegmentWrapper newWrapper;
        Schema.SObjectField externalIdField  = Account_Allocation_Rules__c.Fields.Rule_Allocation_Name__c;
        Account_Allocation_Rules__c newRule = new Account_Allocation_Rules__c();
        newRule.Account_Manager__c = userId;
        newRule.Integration_Product__c = posId;
        newRule.Segment__c = Segment;
        newRule.Active__c = true;
        newRule.Rule_Allocation_Name__c = Segment + '_' + posId;
        
        Database.UpsertResult upsertResults = Database.upsert(newRule, externalIdField, false);
        
        if(viewRequest == 'amViewRequest') {
            return fetchPOSSegmentWrapper(userId, viewRequest);
        }else{
            return fetchPOSSegmentWrapper(posId, viewRequest);
        }
        
        
    }
    
    @AuraEnabled
    public static void saveCustomSetting(String selectedAccountId) {
        Accounts_Excluded_From_Allocation_Rules__c newExcludedAccounts = new Accounts_Excluded_From_Allocation_Rules__c();
        for(Account excludedAccount : [SELECT Id, Name FROM Account WHERE Id=:selectedAccountId]) {
            newExcludedAccounts.Excluded_Account_Id__c = excludedAccount.Id;
            newExcludedAccounts.Excluded_Account_Name__c = excludedAccount.Name;
            newExcludedAccounts.Name = excludedAccount.Id;
        }
        
        Schema.SObjectField externalIdField  = Accounts_Excluded_From_Allocation_Rules__c.Fields.Excluded_Account_Id__c;
        Database.upsert(newExcludedAccounts, externalIdField, false);
    }
    
    @AuraEnabled
    public static void deleteCustomSetting(String customSettingId) {
        Accounts_Excluded_From_Allocation_Rules__c deleteAccountRule;
        for(Accounts_Excluded_From_Allocation_Rules__c deleteRule : [SELECT Id FROM Accounts_Excluded_From_Allocation_Rules__c WHERE Id=:customSettingId]) {
            deleteAccountRule = deleteRule;
        }
        
        delete deleteAccountRule;
    }
    
    @AuraEnabled
    public static void transferRulesToNewUser(String oldUserId, String newUserId) {
        
        List<Account_Allocation_Rules__c> rulesToUpdate = new List<Account_Allocation_Rules__c>();
        for(Account_Allocation_Rules__c oldUserRules : [SELECT Id, Account_Manager__c FROM Account_Allocation_Rules__c WHERE Account_Manager__c =:oldUserId]) {
            oldUserRules.Account_Manager__c = newUserId;
            rulesToUpdate.add(oldUserRules);
        }
        
        update rulesToUpdate;
    }
    
    @AuraEnabled
    public static String  getExcludedAccountList() {
         
         List<Accounts_Excluded_From_Allocation_Rules__c> nonISORules = new List<Accounts_Excluded_From_Allocation_Rules__c>();
         for(Accounts_Excluded_From_Allocation_Rules__c allRules :  [SELECT Id,Excluded_Account_Id__c, Excluded_Account_Name__c 
                                                                        FROM Accounts_Excluded_From_Allocation_Rules__c 
                                                                        WHERE ISO_Account__c = false]) {
            nonISORules.add(allRules);
         }
         return JSON.serialize(nonISORules);
    }
    
    
    public class ResourceWrapper {
        
        public String resourceId;
        public String resourceName;
        public Boolean resourceStatus;
        public Integer merchantAccounts;
        public Integer corporateAccounts;
        public Integer tyroCustomer;
        
        public ResourceWrapper(Id resourceId, String resourceName, Boolean isActive) {
            this.resourceId = resourceId;
            this.resourceName = resourceName;
            merchantAccounts = 0;
            corporateAccounts = 0;
            tyroCustomer = 0;
            resourceStatus = isActive;
        }
        
        public void setAccountValues(String recordTypeName, Integer count) {
            if(recordTypeName == 'Merchant Group') {
                merchantAccounts = count;
            }else if(recordTypeName == 'Corporate Group') {
                corporateAccounts = count;
            }else{
                tyroCustomer = count;
            }
        }
    }
    
    
    public class POSSegmentWrapper {
        //public Map<String, Account_Allocation_Rules__c> allRules;
        public String posName;
        public Id posId;
        public Id accountManagerId;
        public String accountManagerName;
        public String tableKey;
        //public String segmentPOSkey;
        //public String segmentUserKey;
        
        public Boolean Micro_1;
        public Boolean Micro_2;
        public Boolean Small_1;
        public Boolean Small_2;
        public Boolean Medium_1;
        public Boolean Medium_2;
        public Boolean Large;
        public Boolean X_Large;
        
        
        
        public POSSegmentWrapper(String tableUID, String posName, String posId, String accountManagerId, String accountManagerName) {
            this.tableKey           = tableUID;
            this.posName            = posName;
            this.posId              = posId;
            this.accountManagerId   = accountManagerId;
            this.accountManagerName = accountManagerName;
            //allRules = new Map<String, Account_Allocation_Rules__c>();
        }
        
        
        public void storeOtherKeys(String segment) {
            //this.segmentPOSkey  = (segment + '_' + this.posId);
            //this.segmentUserKey = (segment + '_' + this.accountManagerId);
        }
        
        
        
        
    }

}