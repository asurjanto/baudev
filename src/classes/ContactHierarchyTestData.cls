public with sharing class ContactHierarchyTestData {
	
	public static void createTestHierarchy() {
			// Create Accounts with Hierarchy
		
			// Create Top Level Accounts
		
		
			Contact[] subContacts = new Contact[]{};
			Contact[] contactList = new Contact[]{};
			Contact[] subContactSideTree = new Contact[]{};
			Contact[] subContactList = new Contact[]{};
			
			Account acc = new Account();
			acc.name = 'test Account';
			Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
			acc.recordtypeid = devRecordTypeId;
			insert acc;
			
			String contactFirstName = 'Hierarchy';
			String contactLastName = 'Test';
			
			Contact parentContact = new Contact(Accountid=acc.id, FirstName=contactFirstName, LastName=contactLastName+'0', Phone='123456789', MobilePhone='987654321', Title='Manager');
			Contact subTreeParent  = new Contact();
			Contact subTreeSideParent  = new Contact();
	
	
			insert parentContact;
			parentContact = [select id, name from contact where name='Hierarchy Test0'];
			Id parentID = parentContact.id;
			System.Assert(parentID != null, 'Parent Id not found');
			
			// Create sub accounts
		
			for (Integer i=0;i<10;i++){
				subContacts.add(new Contact(Accountid=acc.id,FirstName = contactFirstName, LastName=contactLastName+i, Phone='123456789', MobilePhone='987654321', Title='Manager'));	
			}
			
			insert subContacts;
			
			contactList = [Select id, ReportsToId, name from contact where name like 'Hierarchy Test%' ORDER BY Name];
					
			for (Integer x=0;x<contactList.size();x++){
				if (contactList[x].name != 'Hierarchy Test0'){
					contactList[x].ReportsToId = parentID;
					parentID=contactList[x].Id;	
				}
			}
			
			update contactList;
			
			subTreeParent = [Select id, ReportsToId, name from contact where name = 'Hierarchy Test4'];
			parentID=subTreeParent.Id;
	
			for (Integer y=0;y<10;y++){
				subContactSideTree.add(new Contact(accountid = acc.id, FirstName = contactFirstName,LastName=contactLastName+'4.'+y, Phone='123456789', MobilePhone='987654321', Title='Manager'));	
			}
	
			insert subContactSideTree;
			
			subContactList = [Select id, ReportsToId, name from Contact where name like 'Hierarchy Test4%'];
	
			for (Integer z=1;z<subContactList.size();z++){
				subContactList[z].ReportsToId = parentID;
				parentID=ContactList[z].Id;	
			}
			
			update subContactList;
			
		}

}