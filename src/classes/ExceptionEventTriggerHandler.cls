public class ExceptionEventTriggerHandler extends TriggerHandler {
    
    
    //Private Variables
    private List<Exception_Event__e> newEventList;

    public ExceptionEventTriggerHandler() {
        
        newEventList = (List<Exception_Event__e>)Trigger.new;
        
    }
    
    
    public override void afterInsert() {
         
        List<Exception_Logs__c> exceptionLogsList = new List<Exception_Logs__c>();
        for(Exception_Event__e newException : newEventList) {
             
        
            exceptionLogsList.add(new Exception_Logs__c(
                       Exception_Type__c = newException.Exception_Type__c,
                       Line_Number__c = newException.Line_Number__c,
                       Message__c = newException.Message__c,
                       Method_Name__c = newException.Method_Name__c,
                       Payload_Body__c = newException.Payload_Body__c,
                       Process_Name__c = newException.Process_Name__c,
                       Stack_Trace__c = newException.Stack_Trace__c,
                       Status__c = 'New'
            ));
             
         }
         
         if(!exceptionLogsList.isEmpty()) {
             insert exceptionLogsList;
         }
         
         
     }
    
}