public class VFController_Sidebar_Summary {

//public Integer getUnreadLeads() {
Date beginingMonth = Date.today().toStartOfMonth();
Time startOfDay = Time.newInstance(00, 00, 00, 00);
DateTime beginMonthAndDay = DateTime.newInstance(beginingMonth, startOfDay);

public Integer getTasksCreated() 
{
    Integer count = 0;
    List<Task> initialList = new List<Task>([select subject from Task where CreatedDate >= :beginMonthAndDay AND CreatedById  = :UserInfo.getUserId() limit 1000]);
    for (Task a : initialList)
    {
        if(a.subject.startswith('Mass Email')== FALSE)
        {
            count++;
        }
    }
    return count;
}

public Integer getAccountsSigned() {
return [
select count() from Account
where Application_received_date__c >= :beginingMonth 
AND OwnerId = :UserInfo.getUserId() limit 1000
];
}

public Integer getTasksClosed() 
{
    Integer count = 0;
    List<Task> initialList = new List<Task>([select subject from Task where CreatedDate >= :beginMonthAndDay AND OwnerId = :UserInfo.getUserId() AND IsClosed = TRUE limit 1000]);
    for (Task a : initialList)
    {
        if(a.subject.startswith('Mass Email')== FALSE)
        {
            count++;
        }
    }
    return count;
}

public Integer getBoardedAccounts() {
return [
select count() from Account
where Merchant_Create_Date__c >= :beginingMonth 
AND OwnerId = :UserInfo.getUserId() limit 1000
];
}

public Integer getCallsLogged() {
return [
select count() from Case
where CreatedDate >= :beginMonthAndDay
AND CreatedById = :UserInfo.getUserId() 
AND Reason = 'Log Inbound Call' limit 1000
];
}

public Integer getCSRequests() {
return [
select count() from Case
where ClosedDate >= :beginMonthAndDay
AND OwnerId = :UserInfo.getUserId()
AND RecordTypeId = '01220000000Hc58' limit 1000
];
}

public Integer getOutboundTerminals() {
return [
select count() from Shipment_Content__c
where CreatedDate >= :beginMonthAndDay
AND CreatedById = :UserInfo.getUserId() 
AND Shipment_Type__c = 'Outbound' limit 1000
];
}

public Integer getInboundTerminals() {
return [
select count() from Shipment_Content__c
where CreatedDate >= :beginMonthAndDay
AND CreatedById = :UserInfo.getUserId() 
AND Shipment_Type__c = 'Inbound' limit 1000
];
}

public Integer getAccountsSignedDaily() {
return [
select count() from Account
where Application_received_date__c >= :Date.today()
AND OwnerId = :UserInfo.getUserId() limit 1000
];
}

public Integer getBoardedAccountsDaily() {
return [
select count() from Account
where Merchant_Create_Date__c >= :Date.today()
AND OwnerId = :UserInfo.getUserId() limit 1000
];
}

public Integer getTasksCreatedDaily() 
{
    Integer count = 0;
    List<Task> initialList = new List<Task>([select subject from Task where CreatedDate >= :Date.today() AND CreatedById  = :UserInfo.getUserId() limit 1000]);
    for (Task a : initialList)
    {
        if(a.subject.startswith('Mass Email')== FALSE)
        {
            count++;
        }
    }
    return count;
}

public Integer getTasksClosedDaily() 
{
    Integer count = 0;
    List<Task> initialList = new List<Task>([select subject from Task where CreatedDate >= :Date.today() AND OwnerId = :UserInfo.getUserId() AND IsClosed = TRUE limit 1000]);
    for (Task a : initialList)
    {
        if(a.subject.startswith('Mass Email')== FALSE)
        {
            count++;
        }
    }
    return count;
}
}