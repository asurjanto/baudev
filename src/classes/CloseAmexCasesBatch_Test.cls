@isTest(seealldata=false)
private class CloseAmexCasesBatch_Test {
    
    private static final String BO_UPDATED_STATUS;
    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP;
    private static final String TYRO_CUSTOMER;
    
    
    static {
        Amex_Configurations__c amexConfig = Amex_Configurations__c.getOrgDefaults();
        
       
        BO_UPDATED_STATUS       = amexConfig.Back_Office_Status_API_Name__c;
        MERCHANT_GROUP  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();
        
        
    }

	private static testMethod void testCloseOfCasebyBatch() {
        
        test.startTest();
            Account selectedAccount = [SELECT Id, RecordTypeId, Corporate_Group__c, Amex_IEAP_Rate__c  FROM Account WHERE Name = 'TyroCustomer'];
    	    String response = AmexEnablementComponentController.getParentAccountDetails(selectedAccount.Id);
    	    
            AmexServiceClass.AccountDetailsWrapper accountWrapper = (AmexServiceClass.AccountDetailsWrapper)JSON.deserialize(response, AmexServiceClass.AccountDetailsWrapper.class);
            system.assertEquals(accountWrapper.amexRateCalculated , 1.3);
            Map<String, Contact> selectedContactObject = new Map<String, Contact>();
            selectedContactObject.put('ContactData', accountWrapper.accountContactDetails[0]);
            
            
            
            AmexEnablementComponentController.sendAmexInvitation(   accountWrapper.searchedAccount.Id, 
                                                                    JSON.serialize(accountWrapper.searchedAccount.Name), 
                                                                    JSON.serialize(selectedContactObject), 
                                                                    JSON.serialize(new List<String> {accountWrapper.accountMidDetails[0].merchantId}),  
                                                                    JSON.serialize(accountWrapper.amexRateCalculated), 
                                                                    JSON.serialize(accountWrapper.amexTransactionVolume));
                                                                    
            List<Case> amexCase = [SELECT Id, Status, accountid 
                                            FROM Case 
                                            WHERE recordtype.name = 'Amex Ticket']; 
                                            
                                           
            List<Case> amexCaseToUpdate = new List<Case>();
            for(Case amexTicket : amexCase) {
                   
                amexTicket.Status = BO_UPDATED_STATUS; 
                amexCaseToUpdate.add(amexTicket);
            }
            
            
            update amexCaseToUpdate;
            
            
            
            
            Database.executeBatch(new CloseAmexCasesBatch());
        
        test.stoptest();
        
        
        
	}
	
	@TestSetup
	private static void createTestDate() {
        insert new Amex_Configurations__c (SetupOwnerId=UserInfo.getOrganizationId(), 
                                            Micro_Segment_Ceiling__c = 100000,
                                            Small_Segment_Ceiling__c = 1000000,
                                            Divisor_of_Net_Transaction_Volume__c = 20,
                                            Invitation_Email_Template__c = '00X8E000000JKSt',
                                            Created_Before_date__c = '1',
                                            Back_Office_Status_API_Name__c = 'Back_Office_Updated', 
                                            Case_amex_record_type_Id__c = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Amex Ticket').getRecordTypeId()
                                           );
        
        insert new Amex_MCC_Rates__c (  Name = 'TestMccData',
                                        MCC_Code__c = '3434',
                                        Micro_Rate__c = 1.3,
                                        Small_Rate__c = 1.6
                                     );
        
        Mcc_Description__c mccCode = TestUtils.createNewMCCDescription('testMCCCode', true, 'Health', '3434', true);
        
        //Tyro Customer Account
        Account tyroCustomer = TestUtils.createAccount('TyroCustomer', false);
	    tyroCustomer.RecordTypeId = TYRO_CUSTOMER;
	    insert tyroCustomer;
	    
	    Contact tyroCustomerContact = TestUtils.createContact(tyroCustomer.Id, 'tyrocustomercontact', false);
	    tyroCustomerContact.Email = 'athakur@tyro.com';
	    insert tyroCustomerContact;
	    
	    Channel_Agreement__c newCA = TestUtils.createChannelAgreement('testchannel', tyroCustomer.Id, '32424', true);
	    Integration_Product__c newPOS = TestUtils.createNewPOS('myPOS', tyroCustomer.Id,  8, true);
	    Merchant_Id__c newMid = TestUtils.createMerchantId(tyroCustomer.Id, false, null, 'Active', newPOS.Id, newCA.Id, tyroCustomerContact.Id, false);
	    newMid.Mcc_Description__c = mccCode.Id;
	    newMid.Name = '89898';
	    newMid.Mid__c = '89898';
	    insert newMid;
	    
	    Location__c newLocation = TestUtils.createNewLocation(tyroCustomer.Id, 'testLocation', '155 Clearance street floor 1-5', 'Sydney', 'NSW', 'Australia', '2148', true);
        
        TestUtils.createTransactionHistoryRecord(newMid.Id, system.today().addDays(-1), Double.valueOf(11111) , true);
        TestUtils.createTransactionHistoryRecord(newMid.Id, system.today().addMonths(-1), Double.valueOf(11231) , true);
        TestUtils.createTransactionHistoryRecord(newMid.Id, system.today().addMonths(-2), Double.valueOf(21231) , true);
	    
	}

}