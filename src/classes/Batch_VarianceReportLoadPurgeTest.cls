/*---------------------------------------------------------------------
Author:        Mohith
Company:       Tyro Payments
Description:   This is Test class for Batch_VarianceReportLoadPurge.cls
History
 <Date>       <Authors Name>     <Brief Description of Change>
11/10/2016     Mohith            Initial Draft
----------------------------------------------------------------------*/

@isTest
public class Batch_VarianceReportLoadPurgeTest {
    static testmethod void testPurge(){
        String query = 'Select v.Visa_MasterCard_Scheme_Debit__c, v.Visa_MasterCard_Premium_Commercial__c, v.Visa_MasterCard_International__c, v.Visa_MasterCard_Domestic_Consumer__c, v.Variance_Report_Date__c, v.UnionPay_international__c, v.Transaction_History_Id__c, v.Transaction_History_ID_2__c, v.Total_Real_MAF__c, v.Total_Num_TX__c, v.Total_MAF__c, v.SystemModstamp, v.Real_MAF_bps__c, v.OwnerId, v.No_TXN_Visa_MasterCard_Scheme_Debit__c, v.No_TXN_Visa_MasterCard_Premium_Comm__c, v.No_TXN_Visa_MasterCard_International__c, v.No_TXN_Visa_MasterCard_Domestic_Consumer__c, v.No_TXN_UnionPay_International__c, v.No_TXN_MasterCard_Scheme_Debit_15__c, v.No_TXN_EFTPOS_With_Cash_Out__c, v.No_TXN_EFTPOS_LT_15_No_Cash_Out__c, v.No_TXN_EFTPOS_GT_15_No_Cashout__c, v.Net_Txn_Value__c, v.Name__c, v.Name, v.Merchant_Id__c, v.Merchant_Id_Count__c, v.Medicare_Part_Claims__c, v.Medicare_PP_Claims__c, v.Medicare_Bulk_Claims__c, v.MasteCard_Scheme_Debit_LT_15__c, v.MSF_bps__c, v.MSF_Ex_AMEX__c, v.MID__c, v.MAF_bps__c, v.Last_Day_of_Month__c, v.LastViewedDate, v.LastReferencedDate, v.LastModifiedDate, v.LastModifiedById, v.IsDeleted, v.Integration_Product_Id__c, v.Id, v.Healtpoint_Claims__c, v.First_Transaction_Date__c, v.First_Easyclaim_TXN__c, v.EFTPOS_With_Cash_Out__c, v.EFTPOS_LT_15_No_Cash_Out__c, v.EFTPOS_GT_15_No_Cash_Out__c, v.CreatedDate, v.CreatedById From Variance_Report_Load__c v';
        Variance_Report_Load__c[] vr = new List<Variance_Report_Load__c>(); 
         for (Integer i=0;i<10;i++) {
           Variance_Report_Load__c v = new Variance_Report_Load__c(MID__c='500000000'+i);
            vr.add(v);
       }
       insert vr;
    
    Test.startTest();
    Batch_VarianceReportLoadPurge m = new Batch_VarianceReportLoadPurge();
    Database.executeBatch(m);
    Test.stopTest();
    
     //Verify VarianceReportLoad items got truncated
     Integer i = [SELECT COUNT() FROM Variance_Report_Load__c];
     System.assertEquals(i, 0);
    }   

}