public with sharing class CaseTerminationFlowExtension {
    
    public final Flow.Interview.Case_Termination_Workflow createCase {get; set;}
    public CaseTerminationFlowExtension(ApexPages.StandardController stdController) {}
   public String getFinishLoc() { 
         return createCase==null? 'home/home.jsp': createCase.VAR_OrderID;
    }
      public String getFinishLoc2() { 
         return createCase==null? 'home/home.jsp': createCase.VAR_Merchant_ID;
    }
     Public PageReference getFinishPageRef(){
     IF(getFinishLoc() != null)
     {
//      PageReference pageRef = new PageReference('/' + getfinishLoc());
//      PageReference pageRef = new PageReference('/' + getfinishLoc() + 'e?');
        PageReference pageRef = new PageReference('/_ui/busop/orderitem/SelectSearch?addTo=' + getfinishLoc() + '&retURL=%2F' + getfinishLoc());
        pageRef.setRedirect(true);
        return pageRef;}
        ELSE {
        PageReference pageRef = new PageReference('/'+ getfinishLoc2());
        pageRef.setRedirect(true);
        return pageRef;}
        }
}