@isTest
private class eCommRiskTeamDashboardCompCont_Test {
    
    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP;
    private static final String TYRO_CUSTOMER;
    private static final String OPPORTUNITY_ECOMMERCE;

    static {
        MERCHANT_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();
        OPPORTUNITY_ECOMMERCE = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('eCommerce Up-Sell').getRecordTypeId();
    }
    
    
	private static testMethod void testControllerClass() { 
        
        Test.starttest();
        Opportunity eCommOpp;
        User riskUserInApprovalProcess;

        for (Opportunity opp: [Select id, Name from Opportunity where name = 'ECommOpportunity'
                limit 1
            ]) {
            eCommOpp = opp;
        }

        eCommOpp.stageName = 'Application Received*';
        update eCommOpp;

        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(eCommOpp.id);
        req1.setProcessDefinitionNameOrId('eCommerce_Initial_Approval_Cycle3');

        // Submit the record to specific process
        Approval.ProcessResult approvalResult = Approval.process(req1);
        
        String response = eCommRiskTeamDashboardCompController.fetchRiskOpportunity('Backbook_eCommerce_Risk_Dashboard');
        
        Test.stoptest();
        
	}
	
	@TestSetup
    private static void createTestData() {
        
        Opportunity_Datatable_Settings__mdt customData = new Opportunity_Datatable_Settings__mdt();
        customData.DeveloperName = 'Test_Table_Data';
        customData.Filter_by_Stage_Name__c = 'Risk Review';
        customData.Record_Type_Name__c = 'eCommerce';
        customData.Table_Header_JSON__c = 'Table Header';
        customData.Table_Name__c = 'test Table';
        //insert customData;
        

        Account tyroCustomer = TestUtils.createAccount('eCommTyroCustomer', false);
        tyroCustomer.RecordTypeId = TYRO_CUSTOMER;
        insert tyroCustomer;

        Contact tyroCustomerContact = TestUtils.createContact(tyroCustomer.Id, 'tyrocustomercontact', true);
        Channel_Agreement__c newCA = TestUtils.createChannelAgreement('testchannel', tyroCustomer.Id, '32424', true);
        Integration_Product__c newPOS = TestUtils.createNewPOS('myPOS', tyroCustomer.Id, 8, true);
        Merchant_Id__c newMid = TestUtils.createMerchantId(tyroCustomer.Id, false, null, 'Active', newPOS.Id, newCA.Id, tyroCustomerContact.Id, true);
        Location__c newLocation = TestUtils.createNewLocation(tyroCustomer.Id, 'testLocation', '155 Clearance street floor 1-5', 'Sydney', 'NSW', 'Australia', '2148', true);
        Mcc_Description__c newMcc = TestUtils.createNewMCCDescription('TestClassCC', true, 'Food and X', '3333', true);

        Opportunity newECommOpportunity = TestUtils.createOpportunity('ECommOpportunity', 'Eligible', System.Today(), false);
        newECommOpportunity.recordTypeId = OPPORTUNITY_ECOMMERCE;
        newECommOpportunity.eComm_Product_Type__c = 'Simplify';
        newECommOpportunity.eComm_Shopping_Cart__c = 'Shopify';
        newECommOpportunity.MID__c = newMid.Id;
        newECommOpportunity.AccountId = tyroCustomer.Id;
        newECommOpportunity.Channel_2__c = newCA.Id;
        newECommOpportunity.eComm_MCC__c = newMcc.Id;
        newECommOpportunity.eComm_Website_URL__c = 'Test.test.com.au';
        insert newECommOpportunity;

    }

}