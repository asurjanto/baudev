public with sharing class ShipmentCaseCreateFlowExtension {
    
    public final Flow.Interview.Terminal_Return_Mass_RMA createCase {get; set;}
    public ShipmentCaseCreateFlowExtension(ApexPages.StandardController stdController) {}
    public String getFinishLoc() { 
         return createCase==null? 'home/home.jsp': createCase.VAR_OrderID; 
    }
     Public PageReference getFinishPageRef(){
//      PageReference pageRef = new PageReference('/' + getfinishLoc());
//      PageReference pageRef = new PageReference('/' + getfinishLoc() + 'e?');
        PageReference pageRef = new PageReference('/_ui/busop/orderitem/SelectSearch?addTo=' + getfinishLoc() + '&retURL=%2F' + getfinishLoc());

        pageRef.setRedirect(true);
        return pageRef;
    }
}