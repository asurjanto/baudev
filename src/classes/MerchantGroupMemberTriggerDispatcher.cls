/**********************************************************
* Class Name    : MerchantGroupMemberTriggerDispatcher
* Copyright     : Tyro Payments (c) 2018
* Description   : 
* Created By    : Arvind Thakur (29/10/2018)
* **********************************************************/
public class MerchantGroupMemberTriggerDispatcher extends TriggerHandler {
    
    //Private Variables
    private Map<Id, Merchant_Group_Member__c> newMerchantGroupMap;
    private Map<Id, Merchant_Group_Member__c> oldMerchantGroupMap;
    private List<Merchant_Group_Member__c> newMerchantGroupList;
    private List<Merchant_Group_Member__c> oldMerchantGroupList;
    
    public MerchantGroupMemberTriggerDispatcher() {
        
        newMerchantGroupMap = (Map<Id, Merchant_Group_Member__c>)Trigger.newMap;
        oldMerchantGroupMap = (Map<Id, Merchant_Group_Member__c>)Trigger.oldMap;
        newMerchantGroupList = (List<Merchant_Group_Member__c>)Trigger.new;
        oldMerchantGroupList = (List<Merchant_Group_Member__c>)Trigger.old;        
        
    }
    
    /*
    Process to recalculate Account's POS and Segment After insert of a MGM
    */
    public override void afterInsert() {
        
        Set<Id> accountSegmentAndPOSUpdate = new Set<Id>();
        
        for(Merchant_Group_Member__c newMGM : newMerchantGroupList) {
            
            accountSegmentAndPOSUpdate.add(newMGM.Merchant_Group__c);
        }
        
        if(!accountSegmentAndPOSUpdate.isEmpty()) {
            AccountsToUpdate(accountSegmentAndPOSUpdate);
        }
        
        
    }
    
    /*
    If Account or Merchant_Group of MGM is changed, then sending old and new accounts for 
    Primary integration POS and segment recalculation.
    */
    public override void afterUpdate() {
        
        Set<Id> accountSegmentAndPOSUpdate = new Set<Id>();
        
        for(Merchant_Group_Member__c newMgm : newMerchantGroupList) {
            
            //Merchant Group is moved from one parent Account to Another. Both Accounts have to be recalculated.
            if(oldMerchantGroupMap.get(newMgm.Id).Account__c != newMgm.Account__c) {
                  
                  accountSegmentAndPOSUpdate.add(newMgm.Account__c);
                  accountSegmentAndPOSUpdate.add(oldMerchantGroupMap.get(newMgm.Id).Account__c);
                  
            }
            
            if(oldMerchantGroupMap.get(newMgm.Id).Merchant_Group__c != newMgm.Merchant_Group__c) {
                  
                  accountSegmentAndPOSUpdate.add(newMgm.Merchant_Group__c);
                  accountSegmentAndPOSUpdate.add(oldMerchantGroupMap.get(newMgm.Id).Merchant_Group__c);
                  
            }
            
        }
        
        if(!accountSegmentAndPOSUpdate.isEmpty()) {
            AccountsToUpdate(accountSegmentAndPOSUpdate);
        }
    }
    
    private static void AccountsToUpdate(Set<Id> accountIds) {
        
        
        List<Account> accountsToUpdate = new List<Account>();
        Map<Id, Double> AccountTxnMap;
        Map<Id, Id> accountPrimaryPOSMap = new Map<Id, Id>();
        
        //The service class methods query with Record Type. So accounts can be merchant Group Or Corporate Group Record Type. 
        //Won't Matter
        AccountTxnMap = AccountServiceClass.getNetTransactionVolumeForCorporateGroup(accountIds);
        accountPrimaryPOSMap = AccountServiceClass.getCorporateGroupPrimaryPOS(accountIds);
        AccountTxnMap.putAll(AccountServiceClass.getNetTransactionVolumeForMerchantGroup(accountIds));
        accountPrimaryPOSMap.putAll(AccountServiceClass.getMerchantGroupPrimaryPOS(accountIds)); 
        
        Account newAccount;
        
        for(Id accountId : AccountTxnMap.keySet()) {
            
            newAccount = new Account(Id=accountId);
            if(AccountTxnMap.containsKey(accountId)) {
                newAccount.Segment__c = AccountServiceClass.findAccountSegment(AccountTxnMap.get(accountId));
            }
            if(accountPrimaryPOSMap.containsKey(accountId)) {
                newAccount.Integration_Product__c = accountPrimaryPOSMap.get(accountId);
                
            }
            accountsToUpdate.add(newAccount);
        }
        
        
        update accountsToUpdate;
        
    }
}