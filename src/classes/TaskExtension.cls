public with sharing class TaskExtension {

	public final Task currentTask;
	public ApexPages.standardController controller {get;set;}
	
	public TaskExtension(ApexPages.StandardController stdController) {
    	// constructor
    	controller = stdController;
        this.currentTask = (Task)stdController.getRecord();      
    }
    
    public List<Task> otherTask
    {
    	get
    	{
			Task temp = [select WhatId from Task where id = :currentTask.Id limit 1];
    		List<Task> taskList = [select subject, Status, WhatId, What.name, whoid, who.name, ActivityDate, Description, ReminderDateTime from Task where WhatId = :temp.WhatId ORDER BY ActivityDate DESC NULLS last limit 1000];
    		List<Task> filteredTaskList = new List<Task>();
    		for (Task a : taskList)
    		{
    			if(a.subject.startswith('Mass Email') == FALSE && a.id != currentTask.Id) 
    			{
    				filteredTaskList.add(a);
    			}
    		}
    		return filteredTaskList;
    	}
    	set;
    }
    
    public List<Task> otherTask2
    {
    	get
    	{
			Task temp = [select whoId from Task where id = :currentTask.Id limit 1];
    		List<Task> taskList = [select subject, Status, WhatId, What.name, whoid, who.name, ActivityDate, Description, ReminderDateTime from Task where WhoId = :temp.WhoId ORDER BY ActivityDate DESC NULLS last limit 1000];
    		List<Task> filteredTaskList = new List<Task>();
    		for (Task a : taskList)
    		{
    			if(a.subject.startswith('Mass Email') == FALSE && a.id != currentTask.Id) 
    			{
    				filteredTaskList.add(a);
    			}
    		}
    		return filteredTaskList;
    		
    	}
    	set;
    }
}