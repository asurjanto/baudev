@isTest(seeAllData=false)
private class MerchantGroupMemberTriggerDispatch_Test {

    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP;
    private static final String TYRO_CUSTOMER;
    
    static {
        MERCHANT_GROUP  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();
    }
    
	private static testMethod void testInsertUpdateOfMGM() {
        
        test.startTest();
            
            Account merchantGroupAccount = [SELECT Id FROM Account WHERE Name = 'MG Account' LIMIT 1];
            Account tyroCustomer = [SELECT Id FROM Account WHERE Name = 'TyroCustomer' LIMIT 1];
            Location__c MidLocation = [SELECT Id FROM Location__c WHERE Account__c =:tyroCustomer.Id LIMIT 1];
            Merchant_Id__c mid = [SELECT Id FROM Merchant_Id__c WHERE Account__c =:tyroCustomer.Id LIMIT 1];
            
            Merchant_Group_Member__c insertedMGM = TestUtils.newMerchantGroupMember(merchantGroupAccount.Id, tyroCustomer.Id,  MidLocation.Id, mid.Id, True);
            
            merchantGroupAccount = [SELECT Id, Integration_Product__c FROM Account WHERE Id=:merchantGroupAccount.Id LIMIT 1];
            
            system.assertEquals(false, merchantGroupAccount.Integration_Product__c == null);
            
            Account tyroCustomer2 = [SELECT Id FROM Account WHERE Name = 'TyroCustomer2' LIMIT 1];
            
            //Moving MGM to another Tyro Customer.
            insertedMGM.Account__c = tyroCustomer2.Id;
            update insertedMGM;
            
        test.stopTest();
	}
	
	
	
	@TestSetup
	private static void createTestData() {
	    
	    TestUtils.createAccount('Test Account123', true);
	    Id adminProfileId = TestUtils.getProfileId('System Administrator'); 
	    User testUser = TestUtils.createNewUser('ABC123', 'XYZ', adminProfileId, true);
	    
	    
	    Account tyroCustomer = TestUtils.createAccount('TyroCustomer', false);
	    tyroCustomer.RecordTypeId = TYRO_CUSTOMER;
	    insert tyroCustomer;
	    
	    Account tyroCustomer2 = TestUtils.createAccount('TyroCustomer2', false);
	    tyroCustomer2.RecordTypeId = TYRO_CUSTOMER;
	    insert tyroCustomer2;
	    
	    Account merchantGroupAccount = TestUtils.createAccount('MG Account', false);
	    merchantGroupAccount.RecordTypeId = MERCHANT_GROUP;
	    insert merchantGroupAccount;
	    
	    Contact tyroCustomerContact = TestUtils.createContact(tyroCustomer.Id, 'tyrocustomercontact', true);
	    Channel_Agreement__c newCA = TestUtils.createChannelAgreement('testchannel', tyroCustomer.Id, '32424', true);
	    Integration_Product__c newPOS = TestUtils.createNewPOS('myPOS', tyroCustomer.Id,  8, true);
	    Merchant_Id__c newMid = TestUtils.createMerchantId(tyroCustomer.Id, true, merchantGroupAccount.Id, 'Active', newPOS.Id, newCA.Id, tyroCustomerContact.Id, true);
        Location__c newLocation = TestUtils.createNewLocation(tyroCustomer.Id, 'testLocation', '155 Clearance street floor 1-5', 'Sydney', 'NSW', 'Australia', '2148', true);
        
	    
	    
	}

}