public with sharing class EmailUsers 
{

	public static void emailAdmin (String subject, String body)
	{
		String currentDate = Date.today().format();			
    	String[] toAddresses = new String[] {'jpepene@tyro.com'}; 
    	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

   		mail.setPlainTextBody(body);
		mail.setSubject(subject);
  		mail.setToAddresses(toAddresses);
  		mail.setBccSender(false);
  		mail.setUseSignature(false);
  		mail.setReplyTo('salesforceError@tyro.com');
  		mail.setSenderDisplayName('Salesforce Error');
  		mail.setSaveAsActivity(false);  
 
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	
	public static void emailMultipleUsers (String[] emailAddresses, String subject, String body)
	{
		String currentDate = Date.today().format();	
    	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

   		mail.setPlainTextBody(body);
		mail.setSubject(subject);
  		mail.setToAddresses(emailAddresses);
  		mail.setBccSender(false);
  		mail.setUseSignature(false);
  		mail.setReplyTo('AutoSalesforceMessage@tyro.com');
  		mail.setSenderDisplayName('Salesforce Message');
  		mail.setSaveAsActivity(false);  
 
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	
	public static void emailSingleUser (String emailAddress, String subject, String body)
	{
		String currentDate = Date.today().format();			
    	String[] toAddresses = new String[] {emailAddress}; 
    	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

   		mail.setPlainTextBody(body);
		mail.setSubject(subject);
  		mail.setToAddresses(toAddresses);
  		mail.setBccSender(false);
  		mail.setUseSignature(false);
  		mail.setReplyTo('AutoSalesforceMessage@tyro.com');
  		mail.setSenderDisplayName('Salesforce Message');
  		mail.setSaveAsActivity(false);  
 
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	
	static testMethod void testEmailUsers() 
	{
		//Account newAccount = new Account(Name = 'testEmailUsers Account', AMEX_Mer__c = '123456789', Merchant_ID__c = 500, site = 'Test AmexFunctions Account', ShippingStreet = '123 Test St', ShippingCity = 'Test City', ShippingState = 'NSW', ShippingPostalCode = '2000', Phone = '89071700');
		//insert newAccount;
		List<String> multUsers = new List<String>();
		multUsers.add('test@tyro.com');
		multUsers.add('test2@tyro.com');
		emailSingleUser('test2@tyro.com','test','test');
		emailMultipleUsers(multUsers,'test','test');
		emailAdmin('test','test');

	}
}