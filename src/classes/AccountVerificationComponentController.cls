/**********************************************************************
* Class Name    : AccountVerificationComponentController
* Description   : Controller class for  AccountVerificationComponent
* Created By    : Arvind Thakur
* Created Date  : 1st Septemeber, 2018 
* Modification Log
* Modified By                    Ticket Number
* Arvind (Created)                  SF-1402
* 
************************************************************************/


public class AccountVerificationComponentController {
    


    @AuraEnabled
    public static String fetchAllQuestions() {

        List<Identity_Verification_Questions__mdt> questionsList = new List<Identity_Verification_Questions__mdt>();
        List<Identity_Verification_Options__mdt> optionsList = new List<Identity_Verification_Options__mdt>();
        
        
        for(Identity_Verification_Questions__mdt questionSettings : [SELECT Label, DeveloperName, Field_Name__c, Object_Name__c, Question__c, Score_Points__c, ID_Log_Field__c,
                                                                            Severity_Level__c, System_Validation_Required__c, Response_Type__c 
                                                                        FROM Identity_Verification_Questions__mdt 
                                                                        WHERE Active__c = true]) 
        {
            questionsList.add(questionSettings);
        }
        
        
        for(Identity_Verification_Options__mdt optionsSettings : [SELECT Label, Severity_Level__c
                                                                    FROM Identity_Verification_Options__mdt ]) {
            optionsList.add(optionsSettings);
        }
        
        optionsList.sort();
        IdentitySettings newSettings = new IdentitySettings(questionsList, optionsList);
        

        return JSON.serialize(newSettings);
    }
    
    
    @AuraEnabled 
    public static String fetchLogHistory(String accountId) {
        
        Map<Id, Account_Identification_Log__c> logRecordsToReturn = new Map<Id, Account_Identification_Log__c>();
        Map<Id, Account_Identification_Log__c> caseIdlogRecordMap = new Map<Id, Account_Identification_Log__c>();
        for(Account_Identification_Log__c logRecord : [SELECT Id, Name, CreatedBy.Name, Case_Support_Call__c,CreatedDate, Suspicious__c, 
                                                                Validation_Status__c, Full_Name__c 
                                                        FROM Account_Identification_Log__c 
                                                        WHERE Account__c =: accountId order by CreatedDate desc LIMIT 3]) { 
            logRecordsToReturn.put(logRecord.Id, logRecord);
            caseIdlogRecordMap.put(logRecord.Case_Support_Call__c, logRecord);
        }
        
        for(Case logCases : [SELECT Id, Caller_Name__c FROM Case WHERE Id IN :caseIdlogRecordMap.keySet()]) {
            if(String.isBlank(caseIdlogRecordMap.get(logCases.Id).Full_Name__c)) {
                logRecordsToReturn.get(caseIdlogRecordMap.get(logCases.Id).Id).Full_Name__c = logCases.Caller_Name__c;
            }
        }
        
        return JSON.serialize(logRecordsToReturn.values());
    }
    
    
    
    
    
    @AuraEnabled
    public static void sendEmailNotificationToRisk(String accountId, String logRecordId) {
        
        List<String> emailIdList = new List<String>();
        String emailTemplateName;
        Account currentAccount;
        Account_Identification_Log__c logRecord;
        EmailTemplate useEmailTemplate;
        
        for(Account accountInProgress : [SELECT Id, Name, Account_No__c FROM Account WHERE Id =:accountId]) 
        {
            currentAccount = accountInProgress;
        }
        
        for(Account_Identification_Log__c progressLogRecord : [SELECT Id, Name FROM Account_Identification_Log__c WHERE Id =:logRecordId ]) 
        {
            logRecord = progressLogRecord;
        }
        
        
        for(Notification_Email_Addresses__mdt emailAddressObject : [SELECT Label, Email_Receivers__c, Email_Template_Unique_Name__c
                                                                    FROM Notification_Email_Addresses__mdt 
                                                                    WHERE DeveloperName = 'Suspicion_Email_Receivers']) 
        {
                
            emailIdList.addAll(emailAddressObject.Email_Receivers__c.split(';'));
            emailTemplateName = emailAddressObject.Email_Template_Unique_Name__c;
                
        }
        
        
        if(!String.isBlank(emailTemplateName))
        {
            
            for(EmailTemplate emailTemplate : [SELECT Id, Subject, HtmlValue, Body 
                                                FROM EmailTemplate 
                                                WHERE DeveloperName =: emailTemplateName]) 
            {
                
                useEmailTemplate = emailTemplate;
                
            }
            
        }
        
            
        
            
        if(!emailIdList.isEmpty() && useEmailTemplate != null) 
        
        {
                
            FAST_OutboundEmailService.sendRiskEmail(emailIdList, useEmailTemplate, currentAccount, logRecord);      
            
        }
        
    }
    
    @AuraEnabled
    public static String attachArticleToCase(String caseId, String articleId) {
        
        List<CaseArticle> existingArticle = new List<CaseArticle>();
        for(CaseArticle existingArticleOnCase : [Select Id From CaseArticle Where caseId = :caseId and KnowledgeArticleId =:articleId ]) {
            existingArticle.add(existingArticleOnCase);
        }
        
        
        if(existingArticle.isEmpty()) {
            CaseArticle newCaseArticle = new CaseArticle();
            newCaseArticle.Caseid = caseId;
            newCaseArticle.KnowledgeArticleId = articleId;
            insert newCaseArticle;
            return 'Success! Article is attached to case';
        }else{
            return 'Error! Article is already attached to this case';
        }
 
    }
    
    
    public class IdentitySettings {
        
        public List<Identity_Verification_Questions__mdt> questionList;
        public List<Identity_Verification_Options__mdt> optionsList;
        
        
        public IdentitySettings(List<Identity_Verification_Questions__mdt> listOfQuestions, List<Identity_Verification_Options__mdt> listOfOptions) {
            this.questionList = listOfQuestions;
            this.optionsList = listOfOptions;
        }
    }

}