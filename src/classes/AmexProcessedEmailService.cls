/*******************************************************************************************
Class Name  :   AmexProcessedEmailService
Created By  :   Arvind Thakur
Process     :   This class handles the incoming confirmation Email from Amex.


Desined to handle only 1 email attachment per email.
Store the email Message somewhere meaning - like on Account maybe
Read the attachment in the Email
Update the MIds with Amex Ids
Get the case related to all those mids and attach the email file to the case.
Also try and attache the email to the case.
If all the Mids mentioned on the case are amex enabled, close the case. else let the case be open.

First Search by CAID for MID Location. If not there - 
    Then search for location - use the exact format of address, 
    if location is missing then create the location

********************************************************************************************/
global class AmexProcessedEmailService implements Messaging.InboundEmailHandler {
      
      
    private static final String AMEX_MERCHANT_HEADER_NAME;
    private static final String AMEX_MERCHANT_HEADER_NAME2;
    private static final String MID_NUMBER;
    /*
    private static final String CAID_NUMBER;
    private static final String STREET1;
    private static final String STREET2;
    private static final String STREET3;
    private static final String SUBURB;
    private static final String STATE;
    private static final String POSTCODE;
    private static final String PHONE_NO;
    */
    private static final String EMAIL_SUCCESS_IN_BODY;
    private static final List<String> errorEmailList;
    
    static {
        Amex_Configurations__c amexConfig   = Amex_Configurations__c.getOrgDefaults();
        AMEX_MERCHANT_HEADER_NAME           = amexConfig.Inbound_Amex_Email_CSV_Amex_MID_Header__c;
        AMEX_MERCHANT_HEADER_NAME2          = amexConfig.Inbound_Amex_Email_CSV_Amex_MID_Header_2__c;
        MID_NUMBER                          = amexConfig.Inbound_Amex_Email_CSV_MID_Header__c;
        /*
        CAID_NUMBER                         = 'CAID';
        STREET1                             = 'Address';
        STREET2                             = 'Address2';
        STREET3                             = 'Address3';
        SUBURB                              = 'Suburb'; 
        STATE                               = 'State';
        POSTCODE                            = 'Postcode';
        PHONE_NO                            = 'Phone No';
        */
        EMAIL_SUCCESS_IN_BODY               = amexConfig.Inbound_Amex_Email_CSV_Body_Identifier__c;
        errorEmailList                      = amexConfig.Amex_Error_Notification_Email_List__c.split(';');
    }
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
        
        if(email.textAttachments != null && email.textAttachments.size() == 1 && email.plainTextBody.contains(EMAIL_SUCCESS_IN_BODY)) {
            
            
            Map<String, String> MIDAmexNumberMap = new Map<String, String>();
            Map<String, Location__c> CAIDLocationMap = new Map<String, Location__c>();
            //Map<String, String>  = new Map<String, Location__c>();
            
            //Initialize the parser the csv attachment
            FAST_CSVParser csvParser = new FAST_CSVParser(String.valueOf(email.textAttachments[0].body));
            
            //Fetch the headers of the csv and look for position of two specific columns 
            List<String> headerLine = csvParser.readLine();
            Integer positionOfAmexMerchantNo    = headerLine.indexOf(AMEX_MERCHANT_HEADER_NAME) > headerLine.indexOf(AMEX_MERCHANT_HEADER_NAME2) ? headerLine.indexOf(AMEX_MERCHANT_HEADER_NAME) : headerLine.indexOf(AMEX_MERCHANT_HEADER_NAME2);
            Integer positionOfMID               = headerLine.indexOf(MID_NUMBER);
            /*
            Integer positionOfCAID              = headerLine.indexOf(CAID_NUMBER);
            Integer positionOfAddress           = headerLine.indexOf(STREET1);
            Integer positionOfAddress2          = headerLine.indexOf(STREET2);
            Integer positionOfAddress3          = headerLine.indexOf(STREET3);
            Integer positionOfSuburb            = headerLine.indexOf(SUBURB);
            Integer positionOfState             = headerLine.indexOf(STATE);
            Integer positionOfPostcode          = headerLine.indexOf(POSTCODE);
            Integer positionOfPhoneNo           = headerLine.indexOf(PHONE_NO);
            */
            //reading the first line after header and going on with that until we read all the lines
            Location__c newTempLocation;
            List<String> csvRow = csvParser.readLine();
            while(csvRow != null && csvRow.size() > positionOfMID && csvRow.size() > positionOfAmexMerchantNo) {
                
                //Pickup only one line and if there are duplicates- ignore them. This means that the caids have been enabled, but we dont have caids in us
                if(!MIDAmexNumberMap.containsKey(csvRow[positionOfMID])) {
                    MIDAmexNumberMap.put(csvRow[positionOfMID], csvRow[positionOfAmexMerchantNo]);
                }
                
                
                /*
                newTempLocation = new Location__c();
                newTempLocation.Street__c   = csvRow[positionOfAddress];
                newTempLocation.Street_2__c = csvRow[positionOfAddress2];
                newTempLocation.Street_3__c = csvRow[positionOfAddress3];
                newTempLocation.City__c = csvRow[positionOfSuburb];
                newTempLocation.State__c = csvRow[positionOfState];
                newTempLocation.PostCode__c = csvRow[positionOfPostcode];
                newTempLocation.Phone__c = csvRow[positionOfPhoneNo];
                */
                csvRow = csvParser.readLine();
            }
            
            //fetch all the mids in the csv column and update the mids with respective amex merchant id
            if(!MIDAmexNumberMap.isEmpty()) {
                List<Merchant_Id__c> midsToUpdate = new List<Merchant_Id__c>();
                for(Merchant_Id__c merchantId : [SELECT Id, Name, MID__c//, Amex_Merchant_ID__c 
                                                    FROM Merchant_Id__c 
                                                    WHERE MID__c IN: MIDAmexNumberMap.keySet()]) {
                     
                    merchantId.Amex_Merchant_ID__c = MIDAmexNumberMap.get(merchantId.MID__c);
                    midsToUpdate.add(merchantId);
                }
                
                //send error email to users, in case the mids are not updated successfully
                if(!midsToUpdate.isEmpty()) {
                    Schema.SObjectField externalField = Merchant_Id__c.Fields.MID__c;
                    for(Database.SaveResult updateResults : Database.update(midsToUpdate, false)) {
                       if (!updateResults.isSuccess()) {
                            // Operation failed, so get all errors.               
                            for(Database.Error err : updateResults.getErrors()) {
                                //Send Error Email to specific Users.
                            }
                       }
                        
                    }
                }
            }
            
        }
      
      return null;
      
    }
      
  }