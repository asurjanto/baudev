@isTest
private class Test_EmailToCase 
{
	static testMethod void Test_EmailToCaseTrigger() 
    {
        Account newAccount = new Account(Name = 'Test_DeleteEmailsFromCase Account', Merchant_Status__c = 'Prospect');
		Account newAccount2 = new Account(Name = 'Test_DeleteEmailsFromCase Account2', Merchant_Admin_Email__c = 'user2@nottyro.com');
		insert newAccount;
		insert newAccount2;
		
		//Account tempAccountAfter = [select id from Account where Name = 'Test_DeleteEmailsFromCase Account' limit 1];
		//Account tempAccount2After = [select id from Account where Name = 'MoneySwitch Limited' limit 1];
		
		Contact newContact = new Contact(LastName = 'Test_DeleteEmailsFromCase', accountid = newAccount.id, email = 'user@nottyro.com');
		Contact newContact2 = new Contact(LastName = 'Test_DeleteEmailsFromCase2', accountid = newAccount2.id);
		insert newContact;
		
		Case tempCase = new Case(Subject = 'New message 1 in mailbox 793', status = 'New', SuppliedEmail = 'user@nottyro.com');
		Case tempCase2 = new Case(Subject = 'p1 New Shipment', status = 'New', SuppliedEmail = 'user2@nottyro.com');
		insert tempCase;
		insert tempCase2;
		
		Case tempCaseAfter = [select id, contactid, Priority, accountid from Case where id = :tempCase.id limit 1];
		Case tempCaseAfter2 = [select id, contactid, Priority, accountid from Case where id = :tempCase2.id limit 1];
		
		System.assertEquals(newcontact.id, tempCaseAfter.contactid);
		System.assertEquals(newAccount.id, tempCaseAfter.accountid);
		System.assertEquals( '2', tempCaseAfter.Priority);
		
		System.assertEquals(null, tempCaseAfter2.contactid);
		System.assertEquals(newAccount2.id, tempCaseAfter2.accountid);
		System.assertEquals('1', tempCaseAfter2.Priority);
		
		Case markDelete = [select id, contactid, ApexMarker__c, accountid from Case where id = :tempCase.id limit 1];
		//Case tempCaseAfter2 = [select id, contactid, Priority, accountid from Case where id = :tempCase2.id limit 1];
		
		markDelete.ApexMarker__c = 'Delete voicemail';
		EmailMessage newMsg = new EmailMessage(ParentId = markDelete.id, FromAddress = 'user@tyro.com', subject = 'New message 1 in mailbox 793', ToAddress = 'user2@tyro.com');
		insert newMsg;
		update markDelete;
		
		EmailMessage newMsgAfter = [select id, IsDeleted from EmailMessage where id = :newMsg.id ALL ROWS];
		
		System.assertEquals(TRUE, newMsgAfter.IsDeleted);
    }
}