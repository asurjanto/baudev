public with sharing class FormatPhoneForHUD 
{
	public static String FormatPhoneForHUD(String PhoneNumber)
	{
			String PhoneFormatted = '';
			String PhoneRaw = PhoneNumber;
			String HudFormat = '';
			
			for (Integer i = 0 ; i < PhoneRaw.length(); i++)
			{
				string currentIndex = PhoneRaw.substring(i,i+1);
				if (currentIndex == '0' || currentIndex == '1' || currentIndex == '2' || currentIndex == '3' || currentIndex == '4' || currentIndex == '5' || currentIndex == '6' || currentIndex == '7' || currentIndex == '8' || currentIndex == '9')
				{
					PhoneFormatted = PhoneFormatted + currentIndex;
				}  
			}
			
			if (PhoneFormatted.length() >= 8)
			{
				if (PhoneFormatted.startswith('61') && PhoneFormatted.length() >= 10)
				{
					PhoneFormatted = PhoneFormatted.substring(2);
				}
				else if (PhoneFormatted.startswith('6') && PhoneFormatted.length() >= 9)
				{
					PhoneFormatted = PhoneFormatted.substring(1);
				}
				
				if (PhoneFormatted.startswith('2')||PhoneFormatted.startswith('3')||PhoneFormatted.startswith('4')||PhoneFormatted.startswith('7')||PhoneFormatted.startswith('8'))
				{
					PhoneFormatted = '0'+PhoneFormatted;
				}
				
				String HudBlock1 = PhoneFormatted.substring(0,3);
				String HudBlock2 = PhoneFormatted.substring(3,6);
				String HudBlock3 = PhoneFormatted.substring(6);
				HudFormat = HudBlock1 + '-' + HudBlock2 + '-'+ HudBlock3;		
			}
			
			return HudFormat;
	}
}