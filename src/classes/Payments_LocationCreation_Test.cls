@isTest
private class Payments_LocationCreation_Test {
    
    private static final String ECOMMERCE;
    
    static {
        ECOMMERCE  = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('eCommerce Only').getRecordTypeId();
    }

	private static testMethod void testLocationCreation() {
        Test.startTest();
            
            UserRole appSpecsRole;
            for(UserRole role : [Select id, name from userrole where name = 'Application Specialists']) {
                appSpecsRole = role;
            }
            
            Id adminProfileId = TestUtils.getProfileId('System Administrator'); 
            User testUser = TestUtils.createNewUser('ABC43', 'XYZ', adminProfileId, false);
            testUser.UserRoleId = appSpecsRole.Id;
            insert testUser; 
            
            Opportunity createdOpp;
            
            system.runas(testUser) {
                for(opportunity opp : [Select id, name, opportunity_no__c from opportunity where name = 'Test eComm opp' limit 1]) {
                    createdOpp = opp;
                }
                createdOpp.Application_Specialist__c = testUser.Id;
                update createdOpp;
            
                
                //Creating a MID First
            Payments_MIDRequest newMidRequest = new Payments_MIDRequest();
            newMidRequest.merchantId = '22900';
            newMidRequest.requesterSourceId = createdOpp.opportunity_no__c;
            newMidRequest.mccCode = '1771';
            newMidRequest.tradingName = 'Test Ttrading Name';
        
            
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            
            req.requestURI = '/services/apexrest/v1/merchants';  
            req.httpMethod = 'POST';  
            req.addHeader('Content-Type', 'application/json'); 
            
            req.requestBody = Blob.valueof(JSON.serialize(newMidRequest));
            
            RestContext.request = req;
            RestContext.response = res;
            
            
            Payments_MIDCreation.createMerchantId();
            
            
            
            Payments_LocationRequest newLocation = new Payments_LocationRequest();
            newLocation.caid = '323323';
            newLocation.locationType = 'eCommerce';
            newLocation.merchantId = '22900';
            newLocation.streetLine1 = 'test';
            newLocation.streetLine2 = 'street';
            
            RestRequest req1 = new RestRequest(); 
            RestResponse res1 = new RestResponse();
            
            req1.requestURI = '/services/apexrest/v1/locations';  
            req1.httpMethod = 'POST';  
            req1.addHeader('Content-Type', 'application/json'); 
            
            req1.requestBody = Blob.valueof(JSON.serialize(newLocation));
            
            RestContext.request = req1; 
            RestContext.response = res1;
            
            
            Payments_LocationCreation.createMerchantLocation();
                
                
            }
            
        Test.StopTest();
	}
	
	@TestSetup
	private static void createTestData() {
	    
        
	    Account newTestAccount = TestUtils.createAccount('Test Account', true);
	    Opportunity newOpportunity = TestUtils.createOpportunity('Test eComm opp', 'Eligible', System.today(), false);
	    newOpportunity.RecordTypeId = ECOMMERCE;
	    newOpportunity.AccountId = newTestAccount.Id;
	    insert newOpportunity;
	    
	    testutils.createNewMCCDescription('testmcc', true, 'fnb', '1771', true);
	    
	    
	}

}