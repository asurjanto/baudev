@isTest(seeAllData=false)
private class AccountAllocationTriggerDispatcher_Test {
    
    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP;
    private static final String TYRO_CUSTOMER;
    
    static {
        MERCHANT_GROUP  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();
    }
    
	private static testMethod void insertAndUpdateAllocationRules() {
	    
	    //Insert just account with system owner.
        User testUser2 = [SELECT Id FROM User WHERE firstName = 'AccountManager12' LIMIT 1];
        User testUser =  [SELECT Id FROM User WHERE firstName = 'ABC123' LIMIT 1];
	    Account insertedAccount = [SELECT Id, Name, Segment__c, Integration_Product__c, owner.Id FROM Account WHERE Name = 'CG Account' LIMIT 1];
	    Integration_Product__c pos = [SELECT Id FROM Integration_Product__c WHERE Name = 'myPOS' LIMIT 1];
	    system.assert( insertedAccount.ownerId != testUser2.Id , true);
	    
	    //Inserting New Rule
	    Account_Allocation_Rules__c insertedRule = TestUtils.createAccountAllocationRules(testUser2.id, pos.id, 'Small_1', true);
	    insertedAccount = [SELECT Id, Name, Segment__c, Integration_Product__c, owner.Id FROM Account WHERE Name = 'CG Account' LIMIT 1];
	    system.assert(insertedAccount.ownerId == testUser2.Id , true);
	    
	    //Updating Rule's AM to some other user
	    insertedRule.Account_Manager__c = testUser.Id;
	    update insertedRule;
	    insertedAccount = [SELECT Id, Name, Segment__c, Integration_Product__c, owner.Id FROM Account WHERE Name = 'CG Account' LIMIT 1];
	    system.assert(insertedAccount.ownerId == testUser.Id , true);
	    
	}
	
	@TestSetup
	private static void createTestData() {
	    
	    Id adminProfileId = TestUtils.getProfileId('System Administrator'); 
	    User testUser = TestUtils.createNewUser('ABC123', 'XYZ', adminProfileId, true);
	        
	    User testUser2 = TestUtils.createNewUser('AccountManager12', 'AM', adminProfileId, true);
        
        Account posAccount = TestUtils.createAccount('POS Account', true);
	    Integration_Product__c newPOS = TestUtils.createNewPOS('myPOS', posAccount.Id,  8, true);
        
	    Account corporateGroupAccount = TestUtils.createAccount('CG Account', false);
	    corporateGroupAccount.RecordTypeId = CORPORATE_GROUP;
	    corporateGroupAccount.Segment__c = 'Small_1';
	    corporateGroupAccount.Integration_Product__c = newPOS.Id;
	    insert corporateGroupAccount;
	    
	    Account tyroCustomer = TestUtils.createAccount('TyroCustomer', false);
	    tyroCustomer.RecordTypeId = TYRO_CUSTOMER;
	    tyroCustomer.Corporate_Group__c = corporateGroupAccount.Id;
	    insert tyroCustomer;
	    
	    Account merchantGroupAccount = TestUtils.createAccount('MG Account', false);
	    merchantGroupAccount.RecordTypeId = MERCHANT_GROUP;
	    insert merchantGroupAccount;
	    

	    
	    Contact tyroCustomerContact = TestUtils.createContact(tyroCustomer.Id, 'tyrocustomercontact', true);
	    Channel_Agreement__c newCA = TestUtils.createChannelAgreement('testchannel', tyroCustomer.Id, '32424', true);
	    
	    Merchant_Id__c newMid = TestUtils.createMerchantId(tyroCustomer.Id, true, merchantGroupAccount.Id, 'Active', newPOS.Id, newCA.Id, tyroCustomerContact.Id, true);
        Location__c newLocation = TestUtils.createNewLocation(tyroCustomer.Id, 'testLocation', '155 Clearance street floor 1-5', 'Sydney', 'NSW', 'Australia', '2148', true);
        
        //A new MGM with TC and MG
        
	    
	    //TestUtils.createAccountAllocationRules(testUser.Id, );
	    
	}

}