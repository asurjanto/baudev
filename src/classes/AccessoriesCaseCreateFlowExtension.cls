public with sharing class AccessoriesCaseCreateFlowExtension {
    
    public final Flow.Interview.Merchant_Onboarding createCase {get; set;}
    public AccessoriesCaseCreateFlowExtension(ApexPages.StandardController stdController) {}
    public String getFinishLoc() { 
         return createCase==null? 'home/home.jsp': createCase.VAR_OrderID;
    }
      public String getFinishLoc2() { 
         return createCase==null? 'home/home.jsp': createCase.VAR_Account_ID;
    }
     Public PageReference getFinishPageRef(){
     IF(getFinishLoc() != null)
     {
//      PageReference pageRef = new PageReference('/' + getfinishLoc());
//      PageReference pageRef = new PageReference('/' + getfinishLoc() + 'e?');
        PageReference pageRef = new PageReference('/_ui/busop/orderitem/SelectSearch?addTo=' + getfinishLoc() + '&retURL=%2F' + getfinishLoc());
        pageRef.setRedirect(true);
        return pageRef;}
        ELSE {
        PageReference pageRef = new PageReference('/'+ getfinishLoc2());
        pageRef.setRedirect(true);
        return pageRef;}
        }
}