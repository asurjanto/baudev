public with sharing class eCommerceOnboardingController {
    

    public eCommerceOnboardingController() {

    }

    /*
    @AuraEnabled(cacheable=true)
    public static List<Merchant_Id__c> findAccountMIDs(String accountId){
        
        List<Merchant_Id__c> accountMids = new List<Merchant_Id__c>();
        for(Merchant_Id__c mids : [SELECT Id, Name, Amex_Merchant_ID__c
                                    FROM Merchant_Id__c 
                                    WHERE Account__c =:accountId ]) {
            accountMids.add(mids);
        }

        return accountMids;

    }
    */

    @AuraEnabled(cacheable=true)
    public static List<Location__c> findAccountLocations(String accountId) {
        List<Location__c> accountLocations = new List<Location__c>();
        for(Location__c mids : [SELECT Id, Name, Street__c, Street_2__c, State__c, City__c, Postcode__c 
                                    FROM Location__c 
                                    WHERE Account__c =:accountId ]) {
            accountLocations.add(mids);
        }

        return accountLocations;
    }
    
}