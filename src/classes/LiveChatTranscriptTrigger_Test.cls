/*************************************************************************
Author : Isha Saxena
Description: Created to cover the code of LiveChatTranscriptTrigger  
Created Date: 7 April 2017
Last modified Date: 7 April 2017
*************************************************************************/


@isTest
private class LiveChatTranscriptTrigger_Test
{
   static testMethod void validatelivechat()
  {
      

        LiveChatVisitor visitor = new LiveChatVisitor();

        insert visitor;

        Test.startTest();

        LiveChatTranscript tx = new LiveChatTranscript(
            chatKey = '123',
            liveChatVisitorId = visitor.id
        );

        insert tx;

        Test.stopTest();

        tx = [ SELECT id, caseId,liveChatVisitorId,chatKey FROM LiveChatTranscript WHERE id = :tx.id ];

        System.assertEquals(tx.liveChatVisitorId,visitor.Id);
        
   
  
  
  
  
  }
            


}