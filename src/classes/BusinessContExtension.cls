public with sharing class BusinessContExtension 
{
        public List<Document> getBusinessContDocs()
        {
                List<Document> BusinessDocs = new List<Document>();
                BusinessDocs = [Select d.Name, d.Folder.Name, d.FolderId, d.Description, d.CreatedDate, d.CreatedById From Document d where d.Folder.Name ='Business Continuity' ORDER BY d.Name];
                return BusinessDocs;
        }
}