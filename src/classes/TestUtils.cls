/**********************************************************
* Class Name    : TestUtils
* Copyright     : Tyro Payments (c) 2018
* Description   : Utilities class for all the test classes
* Created By    : Arvind Thakur
* 
* Available Methods : 
*   1.) createAccount
*   2.) createContact
*   3.) createCase
*   4.) createPortalUser
*   5.) getProfileId
*   6.) createChannelAgreement
*   7.) createLead
*   8.) createOpportunity
*   9.) createNewUser
*  10.) getrecordTypeId
*  11.) createAccountVerificationLogRecord
*  12.) createKnowledgeArticle
*  13.) createMerchantGroupMember
*  14.) createNewPOS
*  14.) createMerchantId
*  15.) createLocation
*  16.) createMIDLocation
*  17.) createAccountAllocationRules
*  18.) createTransactionHistoryRecord
*  19.) createNewMCCDescription
***********************************************************/
public class TestUtils {
    
    
    
    /**********************************************************
     * Description  : Create and returns one Account
     * 
    **********************************************************/
    public static Account createAccount(String accountName, Boolean doInsert) {
        
        Account newAccount = new Account();
        newAccount.Name = accountName;
        
        if(doInsert) {
            insert newAccount;
        }
        
        return newAccount;
        
    }
    
    
    
    
    /**********************************************************
     * Description  : Create and returns one Contact
     * 
    **********************************************************/
    public static Contact createContact(Id accountId, String lastName, Boolean doInsert) {
        
        Contact newContact = new Contact();
        newContact.LastName = lastName;
        newContact.accountId = accountId;
        
        if(doInsert) {
            insert newContact;
        }
        
        return newContact;
        
    }



    /**********************************************************
     * Description  : Create and returns a new Case
     * 
    **********************************************************/    
    public static Case createCase(String recordTypeName, String subject, String Status, Boolean doInsert) {
        
        Case newCase = new Case();
        newCase.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        newCase.subject = subject;
        newCase.Status = status;
        
        if(doInsert) {
            insert newCase;
        }
        
        return newCase;
    }
    
    
    
    
    /**********************************************************
     * Description  : Create and returns one Partner User
     * 
    **********************************************************/
    public static User createPortalUser(Id contactId, Id partnerCommunityProfileId, Boolean doInsert) {
        
        //Id p = [SELECT Id FROM profile WHERE Name =: partnerCommunityProfileName].id;
       
        
                  
        User portalUser = new User(alias = 'test123', 
                            email ='testtryoclasses@noemail.com',
                            emailencodingkey = 'UTF-8', 
                            lastname = 'Testing', 
                            languagelocalekey = 'en_US',
                            localesidkey = 'en_US', 
                            profileid = partnerCommunityProfileId, 
                            country = 'United States',
                            IsActive = true,
                            ContactId = contactId,
                            timezonesidkey = 'America/Los_Angeles', 
                            username = 'testtryoclasses@noemail.com');
       
       if(doInsert) {
           insert portalUser;
       }
       
       return portalUser;
        
    }
    
    
    
    
    
    /**********************************************************
     * Description  : Returns a profile Id
     * 
    **********************************************************/
    public static Id getProfileId(String profileName) {
        
        Id profileId;
        for(Profile retrivedProfile : [SELECT Id FROM Profile WHERE Name =: profileName]) {
            profileId = retrivedProfile.Id;
        }
        
        return profileId;
    }
    
    
    
    
    /**********************************************************
     * Description  : Returns a new Channel Agreement
     * 
    **********************************************************/
    public static Channel_Agreement__c createChannelAgreement(String channelName, Id accountId, String backOfficeId, Boolean doInsert) {
        
        Channel_Agreement__c newChannelAgreement = new Channel_Agreement__c( Name = channelName,
                                                                             Account__c = accountId,
                                                                             Backoffice_ID__c = backOfficeId);
                                                                             
        if(doInsert) {
            insert newChannelAgreement;
        }
        
        return newChannelAgreement;
        
    }




    /**********************************************************
     * Description  : Returns a new Lead
     * 
    **********************************************************/    
    public static Lead createLead(String firstName, String lastName, String leadSource, String company, String leadSubSource,   
                                    Id recordTypeId, Boolean doInsert) {
        
        Lead newlead = new Lead();
        newLead.recordTypeId = recordTypeId;
        newLead.firstname = firstName;
        newlead.lastname = lastName;
        newlead.leadsource = leadSource;
        newlead.company = company;
        newlead.lead_sub_source__c = leadSubSource;
        
        if(doInsert) {
            insert newlead;
        }
        
        return newlead;
        
    }
    
    
    
    
    /**********************************************************
     * Description  : Returns a new Opportunity
     * 
    **********************************************************/ 
    public static Opportunity createOpportunity(String opportunityName, String stageName, Date closeDate, Boolean doInsert) {
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.Name = opportunityName;
        newOpportunity.StageName = stageName;
        newOpportunity.CloseDate = closeDate;
        
        if(doInsert) {
            insert newOpportunity;
        }
        
        return newOpportunity;
    }
    
    
    
    
    /**********************************************************
     * Description  : Returns a new User
     * 
    **********************************************************/ 
    public static User createNewUser(String firstName, String lastName, Id profileId, Boolean doInsert) {
        
        String orgId=UserInfo.getOrganizationId(); 
        String dateString=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer RandomId=Integer.valueOf(Math.rint(Math.random()*1000000)); 
        String uniqueName=orgId+dateString+RandomId; 
        
        
        User testUser = new User( firstname = firstName, 
                                    lastName = lastName, 
                                    email = uniqueName + '@test' + orgId + '.org', 
                                    Username = uniqueName + '@test' + orgId + '.org',
                                    EmailEncodingKey = 'ISO-8859-1', 
                                    Alias = uniqueName.substring(18, 23), 
                                    TimeZoneSidKey = 'America/Los_Angeles', 
                                    LocaleSidKey = 'en_US', 
                                    LanguageLocaleKey = 'en_US', 
                                    ProfileId = profileId
                                    );
        if(doInsert) {
            insert testUser;
        }
        return testUser;
        
    }
    
    
    
    /**********************************************************
     * Description  : Returns a RecordType Id for any object
     * 
    **********************************************************/    
    public static Id getrecordTypeId(DescribeSObjectResult objectName, String recordTypeDeveloperName) {
        
        return objectName.getRecordTypeInfosByName().get(recordTypeDeveloperName).getRecordTypeId();
        
    }
    
    
    
    
    /**********************************************************
     * Description  : Returns a new Account_Identification_Log__c
     * 
    **********************************************************/
    public static Account_Identification_Log__c createAccountVerificationLogRecord(Id accountId, Boolean doInsert) {
        
        Account_Identification_Log__c accountIdLog = new Account_Identification_Log__c();
        accountIdLog.Account__c = accountId;
        
        if(doInsert) {
            insert accountIdLog;
        }
        
        return accountIdLog;
    }
    
    
    
    
    /**********************************************************
     * Description  : Creates a generic knowledge article
     * 
    **********************************************************/
    public static sObject createKnowledgeArticle(String articleType, String title, String summary, String urlName, Boolean doInsert ) {
        sObject KnowledgeArticleType = Schema.getGlobalDescribe().get(articleType).newSObject();
        KnowledgeArticleType.put('Title', title);
        KnowledgeArticleType.put('Summary', summary);
        KnowledgeArticleType.put('UrlName', urlName);
        KnowledgeArticleType.put('Language', 'en_US'); 
        
        if(doInsert) {
            insert KnowledgeArticleType;
        }
        
        return KnowledgeArticleType;
    }
    
    
    
    /**********************************************************
     * Description  : Creates 1 Merchant Group Member
     * 
    **********************************************************/
    public static Merchant_Group_Member__c newMerchantGroupMember(Id merchantGroupAccountId, Id accountId, Id locationId, Id mid, Boolean doInsert) {
        
        Merchant_Group_Member__c newMGM = new Merchant_Group_Member__c();
        newMGM.Account__c = accountId;
        newMGM.Merchant_Group__c = merchantGroupAccountId;
        newMGM.Location__c = locationId;
        newMGM.Merchant_ID__c = mid;
        
        if(doInsert) {
            insert newMGM;
        }
        
        return newMGM;
        
    }
    
    
    
    
    /**********************************************************
     * Description  : Creates 1 Integration POS
     * 
    **********************************************************/
    public static Integration_Product__c createNewPOS (String posName, Id manufacturerAccountId, Integer POSSize, Boolean doInsert) {
        
        Integration_Product__c POS = new Integration_Product__c();
        POS.Name = posName;
        //POS.Product_status__c = POSStatus;
        //POS.Sub_Industry_Sector__c = POSSubIndustry;
        //POS.Industry_Sector__c = POSindustry;
        POS.size_of_installed_base__c = POSSize;
        POS.Manufacturer_Account__c = manufacturerAccountId;
        
        if(doInsert) {
            insert POS;
        }
        
        return POS;
        
    }
    
    
    
    /**********************************************************
     * Description  : Creates a Merchant ID
     * 
    **********************************************************/
    public static Merchant_Id__c createMerchantId(Id tyroCustomerId, Boolean isMGM, Id MGMId, String Status, Id integrationProductId, 
                                                    Id channelAgreementId, Id contactId, Boolean doInsert) {
        
        Merchant_Id__c newMID = new Merchant_Id__c();
        newMID.Account__c = tyroCustomerId;
        newMid.Merchant_Group_Member__c = isMGM;
        newMID.Merchant_Group__c = MGMId;
        newMID.Integration_Product__c = integrationProductId;
        newMId.Channel__c = channelAgreementId;
        newMid.Contact__c = contactId;
        newMid.Status__c = Status;
        
        if(doInsert) {
            insert newMID;
        }
        
        return newMID;
    }




    /**********************************************************
     * Description  : Creates a new Location
     * 
    **********************************************************/    
    public static Location__c createNewLocation(Id accountId, String tradingName, String Street, String City, String State, 
                                                    String Country,  String postCode, Boolean doInsert) {
        
        Location__c newLocation = new Location__c();
        newLocation.Account__c = accountId ;
        newLocation.Trading_Name__c = tradingName;
        newLocation.Street__c = Street;
        newLocation.City__c = City;
        newLocation.State__c= State;
        newLocation.Country__c = Country;
        newLocation.Postcode__c = PostCode;
        
        if(doInsert) {
            insert newLocation;
        }
        return newLocation;
    }




    /**********************************************************
     * Description  : Creates a new MID Location
     * 
    **********************************************************/     
    public static Mid_Location__c createMIDLocation(Id MidId, Id locationId, Boolean doInsert) {
        
        Mid_Location__c newMidLocation = new Mid_Location__c();
        newMidLocation.Location__c = locationId;
        newMidLocation.Merchant_ID__c = MidId;
        
        if(doInsert) {
            insert newMidLocation;
        }
        
        return newMidLocation;
    }
    
    
    
    
    /**********************************************************
     * Description  : Creates a new Account Allocation Rule
     * 
    **********************************************************/
    public static Account_Allocation_Rules__c createAccountAllocationRules(String userId, String posProduct, String segment, Boolean doInsert) {
        
        Account_Allocation_Rules__c newRule = new Account_Allocation_Rules__c();
        newRule.Account_Manager__c = userId;
        newRule.Integration_Product__c = posProduct;
        newRule.Segment__c = segment;
        
        if(doInsert) {
            insert newRule;
        }
        
        return newRule;
    }




    /*************************************************************
     * Description  : Creates a new Transaction_History__c record
     * 
    *************************************************************/
    public static Transaction_History__c createTransactionHistoryRecord(String merchantId, Date varianceReportDate, Double netTransactionValue , Boolean doInsert) {
        
        Transaction_History__c newTxnHistory = new Transaction_History__c();
        newTxnHistory.Merchant_Id__c = merchantId;
        newTxnHistory.Name = varianceReportDate.year() + '/' + varianceReportDate.month();
        newTxnHistory.Variance_Report_Date__c = varianceReportDate;
        newTxnHistory.Net_Transaction_Value__c = netTransactionValue;
        
        if(doInsert) {
            insert newTxnHistory;
        }
        
        return newTxnHistory;
        
    }
    
    
    
    
    /*************************************************************
     * Description  : Creates a new Transaction_History__c record
     * 
    *************************************************************/
    public static Mcc_Description__c createNewMCCDescription(String mccName, Boolean isActive, String mccIndustry , String mccCode, Boolean doInsert) {
        
        Mcc_Description__c newMCC   = new Mcc_Description__c(Name = mccName);
        newMCC.Active__c            = isActive;
        newMCC.Industry_Vertical__c = mccIndustry;
        newMCC.MCC_Code__c          = mccCode;
        
        if(doInsert) {
            insert newMCC;
        }
        
        return newMCC;
        
    }
    
    


}