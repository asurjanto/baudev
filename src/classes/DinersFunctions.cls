public with sharing class DinersFunctions {

    ///****************************************************************
    ///Jira Item: SF-506
    ///The code below is redundant and therefore commented
    ///****************************************************************
    
	/*public static void completeDinersLoad(Account currentAccount, String emailAdd)
	{
		Blob tempLoad = createDinersLoad(currentAccount);
		emailDinersLoad(tempLoad, emailAdd);
	}
	
	public static Blob createDinersLoad(Account currentAccount)
	{
    	//Account currentAccount = [select id, site, Merchant_ID__c, ShippingStreet, ShippingState, ShippingPostalCode, ShippingCity, AMEX_Mer__c, Diners_Merchant_No__c, Phone from Account where id = '0012000000Sf7Ae' limit 1];
    	String columnHeaders = 'Trading Name,Diners Merchant No.,Merchant ID,Terminal ID,Trading Street,Trading City,Trading State,Trading Postal Code  \n';
    	
    	String DinersNo = currentAccount.Diners_Merchant_No__c.replace(',',' ');
    	String site = currentAccount.site.replace(',','-');
    	String ShippingStreet = currentAccount.ShippingStreet.replace(',','-');
    	String ShippingCity = currentAccount.ShippingCity.replace(',','-');
    	String ShippingState = currentAccount.ShippingState.replace(',','-');
    	String ShippingPostalCode = currentAccount.ShippingPostalCode.replace(',','-');
    	//String phone = currentAccount.phone.replace(',','-');
    	String MerchantID = currentAccount.AccountNumber;    	
    	
    	String accountDetails = site + ',' + DinersNo + ',' + MerchantID + ',1,' +  ShippingStreet + ',' +  ShippingCity + ',' + ShippingState + ',' + ShippingPostalCode;
    	String completeString = columnHeaders + accountDetails;
    	Blob newLoad = Blob.valueOf(completeString);
    	
    	return newLoad;
	}
	
	public static void emailDinersLoad(Blob dinersLoadFile, String emailAdd)
	{
		String currentDate = Date.today().format();
		String bodyTemplate = 'To whom it may concern,\n\nI have attached the ' + currentDate + ' terminal and Merchant update.'+
		'\n\nRegards,\n' + UserInfo.getName()+ '\n\nCustomer Support \nToll free support number 1 300 966 639' +
		'\n \ntyro payments \n125 york street \nsydney nsw 2000 \nvisit www dot tyro dot com';
		
    	Messaging.EmailFileAttachment[] fileAttachments = new Messaging.EmailFileAttachment[1];    	
    	Messaging.EmailFileAttachment load = new Messaging.EmailFileAttachment();
    	
		load.setBody(dinersLoadFile); 
		load.setFileName('Diners_Load_Request.csv'); 
    	//add attachment object to attachments array
		fileAttachments[0]=load;
        String[] toAddresses = new String[] {emailAdd}; 
    	//String CSEmail = 'cs@tyro.com';
    	//toAddresses.add(CSEmail);
    	
    	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

   		mail.setPlainTextBody(bodyTemplate);
		mail.setSubject('MoneySwitch update '+ currentDate);
  		mail.setToAddresses(toAddresses);
  		mail.setBccSender(false);
  		mail.setUseSignature(false);
  		mail.setReplyTo('cs@tyro.com');
  		mail.setSenderDisplayName('Tyro Customer Support');
  		mail.setSaveAsActivity(false);  
		mail.setFileAttachments(fileAttachments);
 
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	
	static testMethod void DinersFunctions() 
	{
		Account newAccount = new Account(Name = 'Test DinersFunctions Account', Diners_Merchant_No__c = '123456789', Merchant_ID__c = 500, site = 'Test DinersFunctions Account', ShippingStreet = '123 Test St', ShippingCity = 'Test City', ShippingState = 'NSW', ShippingPostalCode = '2000');
		insert newAccount;
		completeDinersLoad(newAccount, 'lzhang@tyro.com');		
	}*/
}