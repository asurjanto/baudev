@isTest
private class Test_newRMAShipping
{
    
    static testMethod void newRMAShipping() 
    {
        
        Product2 objProduct1 = new Product2();
        objProduct1.Name = 'Xentissimo';
        insert objProduct1;
        
        Account newAccount = new Account(Name = 'Test RMA Account');
        insert newAccount;
        
        Asset newAsset = new Asset(Name = 'Xentissimo', Account = newAccount, Accountid = newAccount.id, 
                                   //Following fields are populated to get the test working
                                   //These are required fields
                                   Hardware_Version__c = 'Hardware Version', Firmware_version__c = 'Firmware Version', Make__c = 'Make', General_Description__c = 'General Description', Physical_connections__c = 'Physical Connections', Last_inventory_performed__c = date.today(),
                                   //Following is added to cater for the validation rule
                                   Product2Id = objProduct1.Id);
        insert newAsset;
        
        Case tempCase = new Case(Accountid = newAccount.id, subject = 'Test RMA', status = 'New', Reason = 'RMA Shipment', Fault_Category__c = 'Hardware Fault', Fault_Category2__c = 'SIM', Fault_Sub_Category__c = 'No PIN', Terminal_1__c = newAsset.id, Terminal_2__c = newAsset.id, Terminal_3__c = newAsset.id);
        insert tempCase;
        
        
        
    }
}