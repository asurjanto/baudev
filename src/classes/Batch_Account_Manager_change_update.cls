global class Batch_Account_Manager_change_update implements Database.Batchable<sobject>
{
    string rid = [SELECT id from RecordType where Name ='Tyro Customer'].Id;
    string dynamicQuery = null;
    boolean flag = true;
    Integer num = 0;
    string sg = null;
    Set<Id> setId = new Set<Id>();
    List<Account> upacc = new List<Account>();
      
    List<Account_Allocation_Rules__c> sobj = new List<Account_Allocation_Rules__c>();
    Map<String,account> accmap = new map<String,account>();
    map<String,Id> mapAloocation_ownerId = new map<String,Id>();
    
    
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
       
        return Database.getQueryLocator('Select Id,OwnerId,Segment__c,Account_Allocation_Parameters__c,Corporate_Group__c,Corporate_Group__r.Key_Account__c from Account where  RecordType.Id =: rid and Key_Account__c = false and Merchant_Group_Member__c = 0 and Account_Allocation_Parameters__c != null');
        
    }
    
    global void execute(Database.BatchableContext bc,List<Account> acct)
    {
         sobj = database.query('Select Id,Name,RecordType.Id,Active__c,Account_Manager__c,Active_User__c,Integration_Product__c,Lookup_Parameters__c,Rule_Allocation_Name__c,Segment__c from Account_Allocation_Rules__c where LastModifiedDate = TODAY AND Lookup_Parameters__c <> NULL AND Active__c =: flag');
          
          for(Account_Allocation_Rules__c alc : sobj) 
         {
              if(alc.Lookup_Parameters__c != null)
              {
                    System.debug('Account Rule MAAAAAAAAAAPPPPP');
                    mapAloocation_ownerId.put(alc.Lookup_Parameters__c,alc.Account_Manager__c);
              }
          }
        System.debug('&&&&&&&&&'+mapAloocation_ownerId);
        if(mapAloocation_ownerId.size()>0)
        {        
            
            System.debug('###### Account List'+acct); 
            if(acct.size()>0)
            {   
                for (Account at :acct )
                {  
                    System.debug('ACCCOUNTMAAAAAAAAAAPPPPP');
                    accmap.put(at.Account_Allocation_Parameters__c, at);
                 }
            }
        }
        System.debug('#######mapAloocation_ownerId'+mapAloocation_ownerId);
        System.debug('#######Account Map'+accmap); 
        System.debug('testing time');        
                    
        for(Account_Allocation_Rules__c alc : sobj)
        {   
                      
                    for(account act : acct)
                  {     
                        if(act.Corporate_Group__c == null || (act.Corporate_Group__c != null && act.Corporate_Group__r.Key_Account__c == false))
                        {
                           System.debug('########DrillDown');
                           if (mapAloocation_ownerId.containskey(act.Account_Allocation_Parameters__c) && (act.Account_Allocation_Parameters__c == alc.Lookup_Parameters__c))
                            {
                                 System.debug('########DrillDown111111');
                                  system.debug(LoggingLevel.INFO, alc.Lookup_Parameters__c + ' exist');
                                  System.debug('Account Owner Id####'+act.OwnerId);
                                  System.debug('Allocation Account Manager'+alc.Account_Manager__c);
                                  act.OwnerId = mapAloocation_ownerId.get(alc.Lookup_Parameters__c);
                                  if(setId.add(act.Id))
                                  {                                      
                                     upacc.add(act);
                                  }
                              }
                              else
                              {
                                      system.debug(LoggingLevel.INFO, alc.Lookup_Parameters__c + ' does not exist');
                              }
                         }
               }
        }        

         System.debug('Updated Account List*******'+upacc);
         if(upacc.size() >0)
         {
            update upacc;
         }
          
                 
        
    }
    
    global void finish(Database.BatchableContext bc)
    {
           
            
    }

}