@isTest
public class Test_VFController_Sidebar_Summary 
{
    private static void GenerateData()
    {
        list<Account> listAccounts = new list<Account>();
        
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Account';
        objAccount1.Application_received_date__c = date.today();
        objAccount1.Merchant_Status__c = 'Prospect';
        listAccounts.add(objAccount1);
        
        Account objAccount2 = new Account();
        objAccount2.Name = 'Tyro Stock';
        objAccount2.Application_received_date__c = date.today();
        objAccount2.Merchant_Status__c = 'Prospect';
        listAccounts.add(objAccount2);
        
        insert listAccounts;
        
        Case objCase = new Case();
        objCase.Subject = 'Test Case';
        objCase.AccountId = objAccount1.Id;
        Insert objCase;
        
        list<SFDC_Shipment__c> listSFDCShipment = new list<SFDC_Shipment__c>();
        SFDC_Shipment__c objSFDCShipment = new SFDC_Shipment__c();
        for(integer counter = 0; counter < 2; counter++)
        {
            objSFDCShipment = new SFDC_Shipment__c();
            objSFDCShipment.Case__c = objCase.Id;
            objSFDCShipment.Shipment_Status__c = (Math.Mod(counter,2) == 0 ? 'Outbound' : 'Inbound');
            listSFDCShipment.add(objSFDCShipment);
        }
        insert listSFDCShipment;
        
        list<TNT_Shipment__c> listTNTShipment = new list<TNT_Shipment__c>();
        TNT_Shipment__c objTNTShipment = new TNT_Shipment__c();
        for(integer counter = 0; counter < 2; counter++)
        {
            objTNTShipment = new TNT_Shipment__c();
            objTNTShipment.Case__c = objCase.Id;
            objTNTShipment.Shipment_Type__c = (Math.Mod(counter,2) == 0 ? 'Outbound' : 'Inbound');
            listTNTShipment.add(objTNTShipment);
        }
        insert listTNTShipment;
        
        Product2 objProduct1 = new Product2();
        objProduct1.Name = 'XENTA';
        insert objProduct1;
        
        Asset objAsset = new Asset(Hardware_Version__c = 'Hardware Version', Firmware_version__c = 'Firmware Version', Make__c = 'Make', General_Description__c = 'General Description', Physical_connections__c = 'Physical Connections', Last_inventory_performed__c = date.today());
        objAsset.Name = 'AssetName';
        objAsset.SerialNumber = 'AGM1966';
        objAsset.AccountId = objAccount1.Id;
        objAsset.Product2Id = objProduct1.Id;
        insert objAsset;
        
        SFDC_Shipment__c newShipment = [select id, Case__r.accountid from SFDC_Shipment__c where Shipment_Status__c = 'Outbound' limit 1];
        TNT_Shipment__c tntShipment = [select id, Case__r.accountid from TNT_Shipment__c where Shipment_Type__c = 'Outbound' limit 1];
        Asset currentAsset = [select id from Asset where SerialNumber = 'AGM1966' limit 1];
        Shipment_Content__c newContent = new Shipment_Content__c (Asset__c = currentAsset.id, Shipment__c = newShipment.id, TNT_Shipment__c = tntShipment.Id);
        insert newContent;
        
        Task objTask = new Task(
            Subject = 'Test task', 
            OwnerId = UserInfo.getUserId(),
            WhatID = objAccount1.id, 
            Status = 'Completed');
        insert objTask;
    }
    
    private static testmethod void TestAllMethods()
    {
        Test_VFController_Sidebar_Summary.GenerateData();
        VFController_Sidebar_Summary objSidebarSummary = new VFController_Sidebar_Summary();
        objSidebarSummary.getTasksCreated();
        objSidebarSummary.getAccountsSigned();
        objSidebarSummary.getTasksClosed();
        objSidebarSummary.getBoardedAccounts();
        objSidebarSummary.getCallsLogged();
        objSidebarSummary.getCSRequests();
        objSidebarSummary.getOutboundTerminals();
        objSidebarSummary.getInboundTerminals();
        objSidebarSummary.getAccountsSignedDaily();
        objSidebarSummary.getBoardedAccountsDaily();
        objSidebarSummary.getTasksCreatedDaily();
        objSidebarSummary.getTasksClosedDaily();
    }
}