/*************************************************************************
Author : Isha Saxena
Description: To update the current status time for lead to track the status : NEO335
Created Date: 10 April 2017
Last modified Date:  10 April 2017
*************************************************************************/

@isTest 
public class Batch_TrackStatus_date_Test 
{
    
    static testMethod void testMethod1() 
    {
       List<Lead> lstLeadlist = new List<Lead>();

        List<Lead> lstLead =   new List<Lead>{
                          new Lead(Company = 'JohnMiller', LastName = 'Mike', Status = 'Open',LeadSource = 'Digital Marketing',Lead_Sub_Source__c ='Direct'),
                          new Lead(Company = 'Nike', LastName = 'John', Status = 'Open',LeadSource = 'Digital Marketing',Lead_Sub_Source__c ='Direct'),
                          new Lead(Company = 'Miles', LastName = 'Davis', Status = 'Open',LeadSource = 'Digital Marketing',Lead_Sub_Source__c ='Direct'),
                          new Lead(Company = 'Reebok', LastName = 'Hillen', Status = 'Open',LeadSource = 'Digital Marketing',Lead_Sub_Source__c ='Direct'),
                          new Lead(Company = 'Addidas', LastName = 'Shrin', Status = 'Open',LeadSource = 'Digital Marketing',Lead_Sub_Source__c ='Direct')
                         };  
        insert lstLead;  
        
        lstLead[0].Status = 'Sales Accepted Lead';
        lstLead[1].Status = 'Nurtured Lead';

        update lstLead;
        
        Test.startTest();

            Batch_TrackStatus_date obj = new Batch_TrackStatus_date();
            DataBase.executeBatch(obj,50); 
            
        Test.stopTest();
    }
}