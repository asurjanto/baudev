/******************************************************************
@Tyro Payments Limited 2003-2019 
Merchant Portal Order Accessories
Name            :  Payments_MIDCreation
Description     :  HttpGet Rest Endpoint for posting MIDs
Owners          : Payments Team, Team Salesforce. 
Change Log      : Created - Sept, 2019  @Arvind 
JIRA            : 
******************************************************************/
@RestResource(urlMapping='/v1/merchants')
global with sharing class Payments_MIDCreation {
	
    @HttpPost
    global static void createMerchantId() { 
        
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        
        
        Payments_MIDRequest midRequest = (Payments_MIDRequest) System.JSON.deserialize(request.requestBody.toString(), Payments_MIDRequest.class);
        
        Merchant_Id__c newMid = new Merchant_Id__c(); 
        newMid.MID__c = midRequest.merchantId;
        newMid.Name = midRequest.merchantId;
        newMid.Trading_Name__c = midRequest.tradingName;
    
        Opportunity relatedOpportunity;
        for(Opportunity opp : [SELECT Id, Name, AccountId, MID__c
                                FROM Opportunity 
                                WHERE Opportunity_No__c	=:midRequest.requesterSourceId ]) {
            relatedOpportunity = opp;
        }
        
        if(relatedOpportunity == null) {
            response.responseBody = Blob.valueOf('Related Opportunity Not Found');
            response.statusCode = 422;
            RestContext.response = response;
            
            //Exception logging
            ExceptionDataObject newException = new ExceptionDataObject();
            newException.setLineNumber(29)
                        .setExceptionType('Record Not Found')
                        .setMessage('Related Opportunity Not Found')
                        .setClassorProcessName('Payments_MIDCreation')
                        .setMethodOrSubProcessName('createMerchantId')
                        .setJsonPayload(request.requestBody.toString());
            SalesforceExceptionLogger.logException(newException);
        }else{
            
            newMid.Account__c = relatedOpportunity.AccountId;
            Id realtedMCCId;
            for(MCC_Description__c mcc : [SELECT Id, MCC_Code__c 
                                            FROM MCC_Description__c 
                                            WHERE MCC_Code__c =:midRequest.mccCode]) {
                realtedMCCId = mcc.Id;
            }
            
            if(realtedMCCId == null) {
                response.responseBody = Blob.valueOf('MCC Code not found');
                response.statusCode = 422;
                RestContext.response = response;
                
                //Exception Logging
                ExceptionDataObject newException = new ExceptionDataObject();
                newException.setLineNumber(43)
                            .setExceptionType('Record Not Found')
                            .setMessage('MCC Code Record Not Found')
                            .setClassorProcessName('Payments_MIDCreation')
                            .setMethodOrSubProcessName('createMerchantId')
                            .setJsonPayload(request.requestBody.toString());
                SalesforceExceptionLogger.logException(newException);
                
            }else{
                try{
                    newMid.MCC_Description__c = realtedMCCId;
                    insert newMid;
                    
                    relatedOpportunity.MID__c = newMid.Id;
                    update relatedOpportunity;
                    
                    response.responseBody = Blob.valueOf('Mid is inserted');
                    response.statusCode = 201;
                    RestContext.response = response;
                }catch(Exception ex) {
                    response.responseBody = Blob.valueOf(ex.getMessage());
                    response.statusCode = 422;
                    RestContext.response = response;
                    
                    //Exception Logging
                    ExceptionDataObject newException = new ExceptionDataObject(ex);
                    newException.setClassorProcessName('Payments_MIDCreation')
                                .setMethodOrSubProcessName('createMerchantId')
                                .setJsonPayload(request.requestBody.toString());
                    SalesforceExceptionLogger.logException(newException);
                }
            }
        }
    }
 
    
    @HttpPut
    global static void merchantUpdates() { 
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf('Update request received');
        response.statusCode = 200;
        RestContext.response = response;
        
    }
    
    
}