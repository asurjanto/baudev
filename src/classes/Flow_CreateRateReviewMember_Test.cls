@isTest
private class Flow_CreateRateReviewMember_Test {

    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP;
    private static final String TYRO_CUSTOMER;
    
    static {
        MERCHANT_GROUP  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();
    }
    
	private static testMethod void testRateReviewMemberCreation() {
        
        Account cgAccount = [SELECT Id FROM Account WHERE Name = 'CG Account' LIMIT 1];
        
        Opportunity newOpportunity = TestUtils.createOpportunity('TestCGOpp', 'Application Sent', System.today(), false);
        
        newOpportunity.AccountId = cgAccount.Id;
        insert newOpportunity;
        
        Flow_CreateRateReviewMember.createRateReviewMemberForCorporateGroups(new List<Id> {newOpportunity.Id});
	}
	
	@TestSetup
	
	private static void createTestData() {
	    
	    Account corporateGroupAccount = TestUtils.createAccount('CG Account', false);
	    corporateGroupAccount.RecordTypeId = CORPORATE_GROUP;
	    insert corporateGroupAccount;
	    
	    Account tyroCustomer = TestUtils.createAccount('TyroCustomer', false);
	    tyroCustomer.RecordTypeId = TYRO_CUSTOMER;
	    tyroCustomer.Corporate_Group__c = corporateGroupAccount.Id;
	    //tyroCustomer.Corporate_Group__c = corporateGroupAccount.Id;
	    insert tyroCustomer;
	    
	    Contact tyroCustomerContact = TestUtils.createContact(tyroCustomer.Id, 'tyrocustomercontact', true);
	    
	    Channel_Agreement__c newCA = TestUtils.createChannelAgreement('testchannel', tyroCustomer.Id, '32424', true);
	    Integration_Product__c newPOS = TestUtils.createNewPOS('myPOS', tyroCustomer.Id,  8, true);
	    Merchant_Id__c newMid2 = TestUtils.createMerchantId(tyroCustomer.Id, false, null, 'Active', newPOS.Id, newCA.Id, tyroCustomerContact.Id, true);
	    
	}

}