/*
 * Author:         Isha Saxena
 * Last Update:    October 5th, 2017
 * Version:        1.0
 * Description:    Automatically update the Lead and Opportunity Channels to Tyro direct  
 */
 

global class Batch_Update_Lead_Channel_to_Tyrodirect implements Database.Batchable<sobject>
{
    
    string rid = [SELECT id from RecordType where Name ='Merchant Lead'].Id;
    string recid1 = [SELECT id from RecordType where Name ='Merchant Lead - Unsolicited'].Id;
    string recid2 = [SELECT id from RecordType where Name ='Quick Lead'].Id;
    string recid3 = [SELECT id from RecordType where Name ='Acquiring'].Id;
    string recid4 = [SELECT id from RecordType where Name ='Dead Lead'].Id;

    
    boolean flag = false;
    Set<Id> setId = new Set<Id>();
    Set<Id> setId1 = new Set<Id>();
    List<Lead> upld = new List<Lead>();
    List<Opportunity> Upopp = new List<Opportunity>();
    List<Opportunity> sobj = new List<Opportunity>();
    Channel_Agreement__c chn = [SELECT id,Name FROM Channel_Agreement__c WHERE Name ='Tyro Direct' LIMIT 1];
    string Chid = chn.Id;
    
     String dynamicQuery = 'SELECT Id,Name,RecordType.Id,Channel_2__c,Channel_2__r.Name,Channel_2__r.Exclude_From_Lead_Expiry_Rules__c,Lead_Opportunity_Age__c,IsClosed FROM Opportunity '+
      ' WHERE RecordType.Id =: recid3 AND IsClosed =: flag AND Lead_Opportunity_Age__c > 365 AND Channel_2__r.Exclude_From_Lead_Expiry_Rules__c =: flag AND Channel_2__c !=: Chid';   
    
          
          
    
    
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        String query = 'SELECT Id,Name,IsConverted,RecordType.Id,Channel_2__c,Channel_2__r.Exclude_From_Lead_Expiry_Rules__c,Channel_2__r.Name,Lead_Age__c FROM Lead '+
            ' WHERE (RecordType.Id =: rid OR RecordType.Id =: recid1 OR RecordType.Id =: recid2 OR RecordType.Id =: recid4 ) AND  IsConverted =: flag AND Lead_Age__c > 365 AND Channel_2__r.Exclude_From_Lead_Expiry_Rules__c =: flag AND Channel_2__c !=: Chid';
         
         return Database.getQueryLocator(query);
         
    }
    
    
    global void execute(Database.BatchableContext bc,List<Lead> ld)
    {
              
         System.Debug('#### Channel Id######'+chn);
         System.Debug('#### Channel Id######'+rid);
         System.Debug('#### Channel Id######'+recid1);
         System.Debug('#### Channel Id######'+recid2);
         System.Debug('#### Channel Id######'+recid3);
         System.debug('######## lead query&&&&&&&'+ld);
         
         for(Lead ldc:ld) 
         {
              if(ldc.Channel_2__c == null || ldc.Channel_2__c != chn.Id)
              {
                    System.debug('Lead UPDATE');
                    ldc.Channel_2__c  = chn.Id ;
                      if(setId.add(ldc.Id))
                     {                                      
                                    upld.add(ldc); 
                     }
                                       
              }
          }
          
        
         if(upld.size()>0)
         {
           update upld;
         }
          
           System.debug('Updated Lead List*******'+upld);
          
          sobj = database.query(dynamicQuery);
          
          System.debug('$$$$$$Opportunity Query'+sobj);
          for(Opportunity op: sobj)
          {
              if(op.Channel_2__c == null || op.Channel_2__c != chn.Id)
              {
                  op.Channel_2__c = chn.Id;
                  if(setId1.add(op.Id))
                  {
                       Upopp.add(op);
                  }
              }
          
          }
          
          
          
         
         if(Upopp.size()>0)
         {
             update Upopp;
         } 
         
         System.debug('Updated Opportunity List*******'+Upopp);
        
            
    }

     global void finish(Database.BatchableContext bc)
    {
        
         
            
    }
}