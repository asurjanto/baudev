/********************************************************************************************
* 
* 
* 
* 
* 
* 
* 
* Test Rigourosly with Test Classes.
**********************************************************************************************/

public class UpdateAccountOwner {
     
    public static Set<String> merchantStatusPickListValues;  
         
    static {
        merchantStatusPickListValues = new Set<String>();
        Schema.DescribeFieldResult fieldResult = Merchant_Id__c.Status__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple){
			merchantStatusPickListValues.add(pickListVal.getLabel());
            //Hi
		}     

    }
    
	
    
    public static void updateChildAccountOfMerchantGroup (Map<Id, Id> newMerchantGroupWithOwnerIds) {
        
        List<Account> accountsToUpdate = new List<Account>();
        
        Map<Id, Set<Id>> merchantGroupTyroCustomer = AccountServiceClass.getTyroCustomerofMerchantGroups(newMerchantGroupWithOwnerIds.keySet(), merchantStatusPickListValues);
        
        Account newAccount;
        for(Id mgId : newMerchantGroupWithOwnerIds.keySet()) {
            if(merchantGroupTyroCustomer.containsKey(mgId)) {
                
                for(Id tcId : merchantGroupTyroCustomer.get(mgId)) {
                    
                    newAccount = new Account(Id = tcId);
                    newAccount.OwnerId = newMerchantGroupWithOwnerIds.get(mgId);
                    accountsToUpdate.add(newAccount);
                    
                }
            }
        }
        
        if(!accountsToUpdate.isEmpty()) {
            try {
                update accountsToUpdate;
            }catch(Exception ex) {
                system.debug('Class:UpdateAccountOwner Method:updateChildAccountOfMerchantGroup' + ex.getMessage());
            }
        }
        
    }
    
    //I am expecting that when I update MG under a CG, then it's trigger would fire and update all TC underneath it.
    //I might not have to explicitly write an update call on all the Tyro Customer.
    public static void updateChildAccountOfCorporateGroup (Map<Id, Id> newCorporateGroupWithOwnerIds) {
        
        List<Account> accountsToUpdate = new List<Account>();
        Map<Id, Set<Id>> corporateGroupTyroCustomers  =  AccountServiceClass.getTyroCustomerofCorporateGroup(newCorporateGroupWithOwnerIds.keySet());
        Map<Id, Set<Id>> corporateGroupMerchantGroups =  AccountServiceClass.getMerchantGroupofCorporateGroup(newCorporateGroupWithOwnerIds.keySet());
        
        Account newAccount;
        for(Id cgId : newCorporateGroupWithOwnerIds.keySet()) {
            
            if(corporateGroupTyroCustomers.containsKey(cgId)) {
                
                for(Id tcId : corporateGroupTyroCustomers.get(cgId)) {
                    
                    newAccount = new Account(Id = tcId);
                    newAccount.OwnerId = newCorporateGroupWithOwnerIds.get(cgId);
                    accountsToUpdate.add(newAccount);
                    
                }
                
            }
            
            if(corporateGroupMerchantGroups.containsKey(cgId)) {
                
                for(Id mgId : corporateGroupMerchantGroups.get(cgId)) {
                    
                    newAccount = new Account(Id = mgId);
                    newAccount.OwnerId = newCorporateGroupWithOwnerIds.get(cgId);
                    accountsToUpdate.add(newAccount);
                    
                }
                
            }
        }
        
        
        if(!accountsToUpdate.isEmpty()) {
            try {
                update accountsToUpdate;
            }catch(Exception ex) {
                system.debug('Class:UpdateAccountOwner Method:updateTyroCustomerOwners' + ex.getMessage());
            }
            
        }
        
    }
    
    /*
    public static void updateTyroCustomerOwners(Map<Id, Id> tyroCustomerNewOwnerId) {
        
        List<Account> accountsToUpdate = new List<Account>();
        Account newAccount;
        
        for(Id tyroCustomerId : tyroCustomerNewOwnerId.keySet()) {
            
            newAccount          = new Account(Id = tyroCustomerId);
            newAccount.OwnerId  = tyroCustomerNewOwnerId.get(tyroCustomerId);
            
            accountsToUpdate.add(newAccount);
        }
        
        if(!accountsToUpdate.isEmpty()) {
            try {
                update accountsToUpdate;
            }catch(Exception ex) {
                system.debug('Class:UpdateAccountOwner Method:updateTyroCustomerOwners' + ex.getMessage());
            }
            
        }
        
    }
    */
    
}