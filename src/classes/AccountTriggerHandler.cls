/*****************************************************************************************************************************
Author : Isha Saxena
Description: To update all the Account Owners referring the Account Allocation Rule if Account Manager is changed : NEO 720
Created Date: 14 August 2017
Last modified Date:  15 August 2017
******************************************************************************************************************************/
public Class AccountTriggerHandler
{
    //These variables store Trigger.oldMap and Trigger.newMap
    
    private static Boolean FirstOnBeforeInsert = false;
    private static Boolean FirstOnAfterInsert = false;
    private static Boolean FirstOnBeforeUpdate = false;
    private static Boolean FirstOnAfterUpdate = false;
    private static Boolean FirstOnBeforeDelete = false;
    private static Boolean FirstOnAfterDelete = false;
    private static Boolean FirstOnAfterUndelete = false;
    public static Map<String,Schema.RecordTypeInfo> AccRecordTypeMap = Schema.SObjectType.Account.getRecordTypeInfosByName(); 
    
     public static void OnAfterUpdate(List<Account> newList, Map<Id, Account> newMap,List<Account> oldList, Map<Id, Account> oldMap,Boolean isInsert, Boolean isUpdate)
    {
        if(FirstOnAfterUpdate) return;
               FirstOnAfterUpdate=true;
        //To Update Corporate and Merchant member Account if Corporate group or merchant group owner is updated
           try
          {
              

              
              Account_Owner_Update(newList,oldMap,isInsert,isUpdate,AccRecordTypeMap);
           }
           catch(Exception e)
           {
              System.debug('Following error occured while processing Accounts '+ e.getMessage());
           }

     }
//+++++++++++++++++++++++++++++++++++++++++++ after insert and after update trigger methods +++++++++++++++++++++++++++
/* Method Name: Account_Owner_Update
* Author Name: Isha Saxena        
* Description: 1. This method contains functionality of Account after update trigger   
* Parameters: List<Account> 
* Returns:    none
*/
public static void Account_Owner_Update(List<Account> newList, Map<Id, Account> oldMap, Boolean isInsert, Boolean isUpdate,  Map<String,Schema.RecordTypeInfo> recordTypeMap)
{
          Set<Id> corpaccIds = new Set<Id>();
          Set<Id> mercaccIds = new Set<id>();
          Set<Id> reqId = new Set<Id>();
          List<Account> accts=new List<Account>();
          Map<Id,account> accmap = new map<Id,account>();
          List<Merchant_Group_Member__c> memgp = new List<Merchant_Group_Member__c>();
          List<Account> upacc = new List<Account>();
          Id recType1 = AccRecordTypeMap.get('Tyro Customer').getRecordTypeId();
          string stat1 = 'Active';
          string stat2 = 'Boarding Completed';
          string stat3 = 'Terminal Shipped';
          system.debug('$$$$$$new List'+newList);
          system.debug('$$$$$$Old Map'+oldMap);
          system.debug('$$$$$$Is insert'+isInsert);
          system.debug('$$$$$$Is Update'+isUpdate);
          system.debug('$$$$$$Recordtype'+recordTypeMap);
          for (Account accinloop : newList)
          {
              System.debug('Account update check########'+accinloop);
              // To check whether account is an Corporate Group account
              if(accinloop.recordtypeID == recordTypeMap.get('Corporate Group').getRecordTypeId() && accinloop.Key_Account__c == true)
                { 
                      System.debug('Account update check1########'+accinloop.RecordtypeId);
                      if(accinloop.OwnerId != oldMap.get(accinloop.id).OwnerId || (oldMap.get(accinloop.id).Key_Account__c == false && accinloop.Key_Account__c == true))
                      {
                                  System.debug('Account update check2########'+accinloop.OwnerId);
                                  System.debug('Account update check3########'+oldMap.get(accinloop.id).OwnerId);
                                  accmap.put(accinloop.Id,accinloop);
                                  corpaccIds.add(accinloop.Id);
                      }            
                }
                // to check account is merchant group or not
                 if(accinloop.recordtypeID == recordTypeMap.get('Merchant Group').getRecordTypeId())
                {
                       if(accinloop.OwnerId != oldMap.get(accinloop.id).OwnerId)
                      {
                                accmap.put(accinloop.Id,accinloop);
                                mercaccIds.add(accinloop.Id);
                      }          
                
                }
          
          }
        accts = database.query('select id,OwnerId,RecordTypeId,Name,Corporate_Group__c from Account where Corporate_Group__c IN : corpaccIds and RecordtypeId =: recType1');
        System.debug('Account Corporate group List$$$$$$'+accts);
        if(accts.size() > 0)
        {
            System.debug('&&&&Listnotnull');
            for(account ac : accts)
            {
                System.debug('&&&&&& Account loop'+ac);
                System.debug('$$$$$$ CG'+ac.Corporate_Group__c );
                if(accmap.containskey(ac.Corporate_Group__c))
               {
                   account act = accmap.get(ac.Corporate_Group__c);
                   System.debug('should work now');
                    ac.OwnerId = act.OwnerId ;
                     if(reqId.add(ac.Id))
                     {                                      
                                     upacc.add(ac);
                     }
               }                 
             }
        } 
       System.debug('Account Corporate group List update$$$$$$'+upacc);
       memgp =  database.query('select Id,Name,Account__c,Account__r.RecordtypeId,Merchant_ID__c,Merchant_Group__c,OwnerId,Location__c from Merchant_Group_Member__c where Merchant_Group__c IN : mercaccIds and Account__r.RecordtypeId =: recType1 and (Merchant_ID__r.Status__c =: stat1  or Merchant_ID__r.Status__c =: stat2 or Merchant_ID__r.Status__c =: stat3)');
       System.debug('Account Merchant group List$$$$$$'+memgp);
       if(memgp.size()>0)
       {
              System.debug('Merchant group member List$$$$$$');
              for(Merchant_Group_Member__c mg : memgp)
              {
           //if 2nd map does have that key
               if(accmap.keyset().contains(mg.Merchant_Group__c))
               {
                   System.debug('Account mechant memebr Map$$$$$$'+accmap.get(mg.Merchant_Group__c));
                   System.debug('Account Merchant owner Map$$$$$$'+mg.Account__r);
                    //Your code here to create that value in the database
                    account act = accmap.get(mg.Merchant_Group__c);
                    account ac = mg.Account__r;
                    System.debug('should work now');
                    ac.OwnerId = act.OwnerId ;
                    if(reqId.add(ac.Id))
                    {                                      
                      upacc.add(ac);
                    }
                }
            }
              
       }
       
        System.debug('Account group List update$$$$$$'+upacc);
        update upacc;     
    }                                         
}