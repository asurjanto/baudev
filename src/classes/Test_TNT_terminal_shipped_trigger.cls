@isTest
private class Test_TNT_terminal_shipped_trigger {

static testMethod void testAccountTrigger(){
        

        Account newAccount = new Account(Name = 'Test Account', Merchant_Status__c = 'Prospect');
       
        insert newAccount;
        
        Case tempCase = new Case(accountid = newAccount.id, Reason = 'New Shipment', status = 'Shipped', Delivery_Address_Line_1__c = 'test street', City__c = 'sydney', State__c = 'NSW', Postcode__c = '2212');
        
        insert tempCase;
       
        TNT_Shipment__c newOutboundShipment = new TNT_Shipment__c (Case__c = tempCase.id, Shipment_Type__c = 'Outbound', Shipment_Date__c = System.today());
      
        insert newOutboundShipment;
        
    
    }   

}