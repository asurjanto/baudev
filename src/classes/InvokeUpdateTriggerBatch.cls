//Updates all records for a provided SObject
global class InvokeUpdateTriggerBatch implements Database.Batchable<sObject> 

{
 
  private String sObjectName;
 
  global InvokeUpdateTriggerBatch(String sObjectName) 
  {
    this.sObjectName = sObjectName;
  }
 
  global Database.QueryLocator start(Database.BatchableContext BC)
  {
 
    String query = 'SELECT Id FROM ' + sObjectName;
    return Database.getQueryLocator(query);   
 
  }
 
  global void execute(Database.BatchableContext BC, List<sObject> scope)
  {   
 
    //Just update the records.  That's all!
    update scope;
 
  }
 
  global void finish(Database.BatchableContext BC)
  {
 
    System.debug('Batch Process Complete');
 
  }
 
}