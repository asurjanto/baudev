@isTest
private class Test_newPaperOrder
{

    static testMethod void newPaperOrder() 
    {
     
        Product2 objProduct1 = new Product2();
        objProduct1.Name = 'XENTA';
        insert objProduct1;
        
        Account newAccount = new Account(Name = 'Test Case Account');
        insert newAccount;
        
        Asset newAsset = new Asset(Name = 'XENTA', Account = newAccount, Accountid = newAccount.id,
                                   //Following fields are populated to get the test working
                                   //These are required fields
                                   Hardware_Version__c = 'Hardware Version', Firmware_version__c = 'Firmware Version', Make__c = 'Make', General_Description__c = 'General Description', Physical_connections__c = 'Physical Connections', Last_inventory_performed__c = date.today(),
                                   //Following is added to cater for the validation rule
                                   Product2Id = objProduct1.Id);
        insert newAsset;
        
        //test creating a new Xenta paper case with delivery address
        Case tempCase = new Case(Accountid = newAccount.id, subject = 'Xenta Paper Order', status = 'New', Call_category__c = 'Paper order', Call_reason__c = 'Xenta/XR Paper', Delivery_address__c = 'Trading Address');
        insert tempCase;
    
        //test creating a new Xenta paper case with custom address
        Case tempCase2 = new Case(Accountid = newAccount.id, subject = 'Xenta Paper Order', status = 'New', Call_category__c = 'Paper order', Call_reason__c = 'Xenta/XR Paper', Delivery_address__c = 'Custom');
        insert tempCase2;
        
        //test creating a new Xentissimo paper case with delivery address
        Case tempCase3 = new Case(Accountid = newAccount.id, subject = 'Xentissimo Paper Order', status = 'New', Call_category__c = 'Paper order', Call_reason__c = 'Xentissimo/Yoximo Paper', Delivery_address__c = 'Trading Address');
        insert tempCase3;  
      
        //test creating a new Xentissimo paper case with custom address  
        Case tempCase4 = new Case(Accountid = newAccount.id, subject = 'Xentissimo Paper Order', status = 'New', Call_category__c = 'Paper order', Call_reason__c = 'Xentissimo/Yoximo Paper', Delivery_address__c = 'Custom');
        insert tempCase4;  
    }
}