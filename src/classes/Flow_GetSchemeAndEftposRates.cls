/*****
 * This class is a preplacement of Flow named QLI - Get Scheme And EFTPOS Rates - 45
 * This is created to optimise the Seach of over 200000 records
 * I am just converting the flow to a slightly better process. REMOVE THIS APEX CLASS IN FUTURE
 * This subflow is getting referenced in the following flows
 * 
 * Quote- New Quote
 * Quote-Simple Rate Review
 * Quote - Edit Quote Line Items
 * 
 * Therefore is class would be called from all these places.
 * 
 * ***/
global class Flow_GetSchemeAndEftposRates {
    
    private static final String RR_COSTPLUS;
    private static final String RR_FIXED;
    private static final String RR_NORMALISED;
    
    public static final Set<String> rejectQuoteStatus;
    
    static {
        
        RR_COSTPLUS  = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Rate Review - Cost Plus').getRecordTypeId();
        RR_FIXED = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Rate Review - Fixed').getRecordTypeId();
        RR_NORMALISED   = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Rate Review - Normalised').getRecordTypeId();
        
        rejectQuoteStatus = new Set<String> {'Draft', 'In Review', 'Needs Review'};
    }
    
    @InvocableMethod(Label='Flow_GetSchemeAndEftposRates')
    global static void setRatesInQuoteLineItems(List<QuoteData> requests) {
        
        QuoteData myQuote = requests[0];
        
        
        if(myQuote.quoteStatus == null || !rejectQuoteStatus.contains(myQuote.quoteStatus)) {
            
            //Rate_review Quotes
            if(myQuote.quoteRecordTypeId == RR_COSTPLUS || myQuote.quoteRecordTypeId == RR_FIXED  ||  myQuote.quoteRecordTypeId == RR_NORMALISED) {
                
                Map<String, QuoteLineItem> qliMap = new Map<String, QuoteLineItem>();
                Map<String, Terms_Of_Trade_2__c> tot2Map = new Map<String, Terms_Of_Trade_2__c>();
                Map<String, Terms_Of_Trade_2__c> currentTot2Map = new Map<String, Terms_Of_Trade_2__c>();
                Set<Id> productIds = new Set<Id>();
                Set<Id> termOfTradeNames = new Set<Id>();
                Set<Id> currentTermOfTradeNames = new Set<Id>();
                List<QuoteLineItem> lineItemsToUpdate = new List<QuoteLineItem>();
                
                for(QuoteLineItem qli : [SELECT Id, Terms_Of_Trade__c, Current_Terms_Of_Trade__c, Product2Id
                                            FROM QuoteLineItem 
                                            WHERE QuoteId =:myQuote.quoteId 
                                            AND Terms_Of_Trade__c != null 
                                            AND Terms_Of_Trade__r.Name != 'CP Custom']) {
                    productIds.add(qli.Product2Id);
                    termOfTradeNames.add(qli.Terms_Of_Trade__c);
                    currentTermOfTradeNames.add(qli.Current_Terms_Of_Trade__c);
                    qliMap.put(qli.Terms_Of_Trade__c + '' + qli.Product2Id, qli);
                    qliMap.put(qli.Current_Terms_Of_Trade__c + '' + qli.Product2Id, qli);
                }
                
                for(Terms_Of_Trade_2__c tot2 : [SELECT Id, Name, MSF_Based__c, MSF_Fixed_Price__c,
                                                        Apply_Merchant_Fee_Only__c, Interchange_Fee_Fixed_Price__c,
                                                        Interchange_Fee_Percent_Based__c, Use__c,MAF_Fixed_Price__c,MAF_Percent_Based__c,
                                                        Terms_Of_Trade_Name__c, Product__c 
                                                FROM Terms_Of_Trade_2__c 
                                                WHERE (Terms_Of_Trade_Name__c IN :termOfTradeNames OR Terms_Of_Trade_Name__c IN :currentTermOfTradeNames)
                                                AND Product__c IN :productIds ]) {
                    if(termOfTradeNames.contains(tot2.Terms_Of_Trade_Name__c)) {
                        tot2Map.put(tot2.Terms_Of_Trade_Name__c + '' + tot2.Product__c, tot2);
                    }else{
                        currentTot2Map.put(tot2.Terms_Of_Trade_Name__c + '' + tot2.Product__c, tot2);
                    }
                    
                    
                }
                
                for(String uniqueString : qliMap.Keyset()) {
                    if(tot2Map.containsKey(uniqueString)) {
                        Terms_Of_Trade_2__c newTOT2 = tot2Map.get(uniqueString);
                        
                        QuoteLineItem qliToUpdate = qliMap.get(uniqueString);
                        if(myQuote.quoteRecordTypeId ==  RR_COSTPLUS) {
                            qliToUpdate.Current_MSF_Per_Trans__c = 0;
                            qliToUpdate.Current_MSF_PC_Of_Trans__c = 0;
                        }else{
                            if(currentTot2Map.containsKey(uniqueString)) {
                                Terms_Of_Trade_2__c currentTOT2 = currentTot2Map.get(uniqueString);
                                qliToUpdate.Current_Fixed_Or_Percent__c = currentTOT2.Use__c ;
                                qliToUpdate.Current_MSF_PC_Of_Trans__c = currentTOT2.MSF_Based__c ;
                                qliToUpdate.Current_MSF_Per_Trans__c = currentTOT2.MSF_Fixed_Price__c ;
                            }
                        }
                            
                        qliToUpdate.Fixed_Or_Percent__c = newTOT2.Use__c ;
                        qliToUpdate.Interchange_Fee_Fixed_Price_c__c = newTOT2.Interchange_Fee_Fixed_Price__c ;
                        qliToUpdate.Interchange_Fee_Percent_Based__c = newTOT2.Interchange_Fee_Percent_Based__c ;
                        qliToUpdate.MAF_Fixed_Price_c__c = newTOT2.MAF_Fixed_Price__c ;
                        qliToUpdate.MAF_Percent_Based__c = newTOT2.MAF_Percent_Based__c ;
                        qliToUpdate.MSF_PC_OF_Trans_2__c = newTOT2.MSF_Based__c ;
                        qliToUpdate.MSF_Per_Trans_2__c = newTOT2.MSF_Fixed_Price__c ;
                        
                        lineItemsToUpdate.add(qliToUpdate);
                            
                    }
                }
                
                update lineItemsToUpdate;
                //Quote updateQuote = new Quote(Id=myQuote.quoteId, );
                
            //New Quotes
            }else{
                
                
                Map<String, QuoteLineItem> qliMap = new Map<String, QuoteLineItem>();
                Map<String, Terms_Of_Trade_2__c> tot2Map = new Map<String, Terms_Of_Trade_2__c>();
                Set<Id> productIds = new Set<Id>();
                Set<Id> termOfTradeNames = new Set<Id>();
                List<QuoteLineItem> lineItemsToUpdate = new List<QuoteLineItem>();
                
                for(QuoteLineItem qli : [SELECT Id, Terms_Of_Trade__c, Product2Id
                                            FROM QuoteLineItem 
                                            WHERE QuoteId =:myQuote.quoteId 
                                            AND Terms_Of_Trade__c != null 
                                            AND Terms_Of_Trade__r.Name != 'CP Custom']) {
                    productIds.add(qli.Product2Id);
                    termOfTradeNames.add(qli.Terms_Of_Trade__c);
                    qliMap.put(qli.Terms_Of_Trade__c + '' + qli.Product2Id, qli);
                }
                
                for(Terms_Of_Trade_2__c tot2 : [SELECT Id, Name, MSF_Based__c, MSF_Fixed_Price__c,
                                                        Apply_Merchant_Fee_Only__c, Interchange_Fee_Fixed_Price__c,
                                                        Interchange_Fee_Percent_Based__c, Use__c,MAF_Fixed_Price__c,MAF_Percent_Based__c,
                                                        Terms_Of_Trade_Name__c, Product__c 
                                                FROM Terms_Of_Trade_2__c 
                                                WHERE Terms_Of_Trade_Name__c IN :termOfTradeNames 
                                                AND Product__c IN :productIds ]) {
                    
                    tot2Map.put(tot2.Terms_Of_Trade_Name__c + '' + tot2.Product__c, tot2);
                    
                }
                
                for(String uniqueString : qliMap.Keyset()) {
                    
                    if(tot2Map.containsKey(uniqueString)) {
                        Terms_Of_Trade_2__c newTOT2 = tot2Map.get(uniqueString);
                        QuoteLineItem qliToUpdate = qliMap.get(uniqueString);
                        
                        qliToUpdate.Fixed_Or_Percent__c = newTOT2.Use__c ;
                        qliToUpdate.Interchange_Fee_Fixed_Price_c__c = newTOT2.Interchange_Fee_Fixed_Price__c ;
                        qliToUpdate.Interchange_Fee_Percent_Based__c = newTOT2.Interchange_Fee_Percent_Based__c ;
                        qliToUpdate.MAF_Fixed_Price_c__c = newTOT2.MAF_Fixed_Price__c ;
                        qliToUpdate.MAF_Percent_Based__c = newTOT2.MAF_Percent_Based__c ;
                        qliToUpdate.MSF_PC_OF_Trans_2__c = newTOT2.MSF_Based__c ;
                        qliToUpdate.MSF_Per_Trans_2__c = newTOT2.MSF_Fixed_Price__c ;
                        
                        lineItemsToUpdate.add(qliToUpdate);
                    }
                }
                
                update lineItemsToUpdate;
                
            }
        }
    }
        
    
    global class QuoteData {
        
        @InvocableVariable(required=true description='Quote Id' label='Quote Id')
        public Id quoteId;
        
        @InvocableVariable(required=false description='Status of Quote' label='Quote Status')
        public String quoteStatus;
        
        @InvocableVariable(required=true description='Quote Record Type' label='Quote Record Type')
        public String quoteRecordTypeId;
        
        
    }

}