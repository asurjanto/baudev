/*****************************************************************************************************************************
Author : Isha Saxena
Description: To update all the Account Owners referring the Account Allocation Rule if Account Manager is changed : NEO 720
Created Date: 5 October 2017
Last modified Date:  5 October 2017
******************************************************************************************************************************/


@isTest
private class Test_Batch_Update_Channel_to_Tyrodirect
{
     static testMethod void validateLeadChannelupdate()
    {
       
       string rid = [SELECT id from RecordType where Name ='Merchant Lead'].Id;
       string recid1 = [SELECT id from RecordType where Name ='Merchant Lead - Unsolicited'].Id;
       string recid2 = [SELECT id from RecordType where Name ='Quick Lead'].Id;
       string recid3 = [SELECT id from RecordType where Name ='Acquiring'].Id;
       string recid4 = [SELECT id from RecordType where Name ='Dead Lead'].Id;
       string rtypeId = [Select Id From RecordType where sObjectType='Account' and Name = 'Partner'].Id;
       
       
       User u = new User(
       ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
       LastName = 'last',
       Email = 'puser000@amamama.com',
      Username = 'puser000@amamama.com' + System.currentTimeMillis(),
      CompanyName = 'TEST',
      Title = 'title',
      Alias = 'alias',
      TimeZoneSidKey = 'America/Los_Angeles',
      EmailEncodingKey = 'UTF-8',
      LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US'
      
       );
       
       insert u;

       Datetime yesterday = Datetime.now().addDays(-1100);
        
        account pt1 = new account();
        pt1.Name = 'Partner test Account';
        pt1.Industry = 'Retail';
        pt1.RecordTypeId = rtypeId;
        insert pt1;
        Test.setCreatedDate(pt1.Id, DateTime.newInstance(2012,12,12));
      
        
        
        System.debug('Account is&&&&&&&&'+pt1);
        
        Integration_Product__c inp = new Integration_Product__c();
        inp.Name = 'Postest';
        inp.Product_status__c = 'Production';
        inp.Sub_Industry_Sector__c = 'Auto Mechanics';
        inp.Industry_Sector__c = 'Automotive';
        inp.size_of_installed_base__c = 8;
        inp.Manufacturer_Account__c = pt1.Id;
        insert inp;
        Test.setCreatedDate(inp.id,DateTime.newInstance(2012,12,12));
        
        
        
        System.debug('Product iis&&&&&&&&'+inp);
        
         Channel_Agreement__c chn = new Channel_Agreement__c();
        chn.Name ='Tyro Direct';
        chn.Account__c = pt1.id;
        chn.Backoffice_ID__c = '3413';
        insert chn;
        Test.setCreatedDate(chn.Id, DateTime.newInstance(2012,12,12));
        
        Channel_Agreement__c ch1 = new Channel_Agreement__c();
        ch1.Name = 'Test channel';
        ch1.Account__c = pt1.id;
        ch1.Backoffice_ID__c = '3223';
        ch1.Account_Manager__c = u.Id;
        insert ch1;
        Test.setCreatedDate(ch1.Id, DateTime.newInstance(2012,12,12));
        
        
       
        
        System.debug('Channel iis&&&&&&&&'+ch1);
            
        Lead l1 = new lead(Company = 'JohnMiller', Channel_2__c = ch1.id,leadSource='Partners', Lead_Sub_Source__c = 'Software Developer',Trading_Name__c='abc1', LastName = 'Mike', Status = 'Marketing Qualified Lead',RecordTypeId = rid,IsConverted = false, OwnerId = u.Id);
                          
        insert l1;  
        System.debug('Lead is&&&&&&&&'+l1);
         
    
    
        Test.setCreatedDate(l1.Id, DateTime.newInstance(2012,12,12)); 
        
        Opportunity opp = new Opportunity(leadSource='Partners', Lead_Sub_Source__c = 'Software Developer',Name='opp1',Channel_2__c = ch1.id,RecordtypeId = recid3,StageName='Verbal Yes', CloseDate = System.Today());
        insert opp;
        System.debug('Lead is&&&&&&&&'+opp);
        
        Test.setCreatedDate(opp.Id, DateTime.newInstance(2012,12,12));
         
        System.runAs(u)
        {
        System.test.startTest();
        Lead mylead = [SELECT Id, Name, CreatedDate FROM Lead 
                             WHERE Company ='JohnMiller' limit 1];
        //System.assertEquals(mylead.CreatedDate, DateTime.newInstance(2012,12,12));
     
       
        
      
       System.debug('Lead Channel after update: ' + l1);
       System.debug('Lead Channel Created Date update: ' + l1.CreatedDate);
       System.debug('Lead Channel age update: ' + l1.Lead_Age__c);
       //System.debug('Lead Channel after update: ' +  opp_update);
       
      
       Batch_Update_Lead_Channel_to_Tyrodirect  obj = new  Batch_Update_Lead_Channel_to_Tyrodirect();
       database.executebatch(obj,1);
       
       System.test.stopTest(); 
       }
    
    }
}