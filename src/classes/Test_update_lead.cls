@isTest
private class Test_update_lead {

    static testMethod void myUnitTest() 
    {
        Integration_Product__c objIntegrationProduct = new Integration_Product__c();
        objIntegrationProduct.Name = 'Test Product';
        insert objIntegrationProduct;
        
        //create new test lead with mandatory fields for leads in production, this will also test lead insert.
        Lead newLead = new Lead(leadSource='Partners', Lead_Sub_Source__c = 'Software Developer', LastName = 'Test Lead', status = 'Open', Lead_Source__c = 'Google AdWords', Company = 'test company', Trading_Name__c ='test trading name', Integration_Product__c = objIntegrationProduct.Id, Monthly_Transaction_Volume__c = Decimal.ValueOf(0), industry = 'Consultant');
        insert newLead;
         
        //Test update, delete, undelete
        Test.startTest();
        update newLead; 
        newLead.status = 'recycled';
           
        delete newLead;
        
        undelete newLead;   
        
        Test.stopTest();
    }
}