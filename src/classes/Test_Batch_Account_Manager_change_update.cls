/*****************************************************************************************************************************
Author : Isha Saxena
Description: To update all the Account Owners referring the Account Allocation Rule if Account Manager is changed : NEO 720
Created Date: 1 August 2017
Last modified Date:  1 August 2017
******************************************************************************************************************************/


@isTest
private class Test_Batch_Account_Manager_change_update
{
     static testMethod void validateAccountRules()
    {
    
              
        string rtypeId = [Select Id From RecordType where sObjectType='Account' and Name = 'Partner'].Id;
        string cusacc = [Select Id From RecordType where sObjectType='Account' and Name = 'Tyro Customer'].Id;
        string cgacc = [Select Id From RecordType where sObjectType='Account' and Name = 'Corporate Group'].Id;
        
       User u = new User(
       ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
       LastName = 'last',
       Email = 'puser000@amamama.com',
      Username = 'puser000@amamama.com' + System.currentTimeMillis(),
      CompanyName = 'TEST',
      Title = 'title',
      Alias = 'alias',
      TimeZoneSidKey = 'America/Los_Angeles',
      EmailEncodingKey = 'UTF-8',
      LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US'
      
       );
       
       insert u;

         User u1 = new User(
       ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
       LastName = 'ast',
       Email = 'puer000@amma.com',
      Username = 'puer000@amma.com' + System.currentTimeMillis(),
      CompanyName = 'TEST1',
      Title = 'title',
      Alias = 'alias',
      TimeZoneSidKey = 'America/Los_Angeles',
      EmailEncodingKey = 'UTF-8',
      LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US'
      
       );
        
        insert u1;
        
        account pt1 = new account();
        pt1.Name = 'Partner test Account';
        pt1.Industry = 'Retail';
        pt1.RecordTypeId = rtypeId;
        insert pt1;
        
        Integration_Product__c inp = new Integration_Product__c();
        inp.Name = 'Postest';
        inp.Product_status__c = 'Production';
        inp.Sub_Industry_Sector__c = 'Auto Mechanics';
        inp.Industry_Sector__c = 'Automotive';
        inp.size_of_installed_base__c = 8;
        inp.Manufacturer_Account__c = pt1.Id;
        insert inp;
        
        
        account tst = new account();
        tst.Name = 'TestaaAccount';
        tst.RecordTypeId = cusacc;
        
        tst.Industry = 'Retail';
        tst.Segment__c = 'Medium_1';
        tst.OwnerId = u.Id;
         insert tst;
        
        
        account test = new account();
        test.Name = 'TestAccount';
        test.RecordTypeId = cgacc;
        //test.Segment__c = 'Extra Large / Corporate';
        test.OwnerId = u.Id;
        insert test;
         
         
        account test1 = new account();
        test1.Name = 'TestAccount123';
        test1.RecordTypeId = cusacc;
        
        test1.Segment__c = 'Medium_2';
        test1.OwnerId = u1.Id;
        test1.Corporate_Group__c  = test.Id;
        insert test1;
        
      
        
        
        List<Account_Allocation_Rules__c> acc_rule = new List<Account_Allocation_Rules__c>
                      {
                          new Account_Allocation_Rules__c(Account_Manager__c = u.Id, Active__c = true , Integration_Product__c = inp.Id ,  Segment__c = 'Medium_1'),
                          new Account_Allocation_Rules__c(Account_Manager__c = u1.Id, Active__c = true , Integration_Product__c = inp.Id ,  Segment__c = 'Medium_2'),
                          new Account_Allocation_Rules__c(Account_Manager__c = u1.Id, Active__c = true , Integration_Product__c = inp.Id ,  Segment__c = 'Small_2'),
                          new Account_Allocation_Rules__c(Account_Manager__c = u.Id, Active__c = true , Integration_Product__c = inp.Id ,  Segment__c = 'Large'),
                          new Account_Allocation_Rules__c(Account_Manager__c = u.Id, Active__c = true , Integration_Product__c = inp.Id ,  Segment__c = 'X_Large')
                       };  
       
       insert acc_rule;
       
       System.debug('Account Allocation Rule after inserting: ' +  acc_rule);
      acc_rule[0].Account_Manager__c  = u1.Id;
      acc_rule[1].Account_Manager__c  = u.Id;
      acc_rule[2].Account_Manager__c  = u.Id;
      acc_rule[3].Account_Manager__c  = u1.Id;
      acc_rule[4].Account_Manager__c  = u1.Id;
      System.test.startTest();
      update acc_rule;
        System.debug('Account Allocation Rule after update: ' +  acc_rule);
       
       
      
       Batch_Account_Manager_change_update  obj = new  Batch_Account_Manager_change_update();
       database.executebatch(obj,10);
       
       System.test.stopTest();
       
       
    }
}