/**********************************************************************************************************************************
* Class Name    : LeadTriggerDispatcher
* Copyright     : Tyro Payments (c) 2018
* Description   : This is Dispatcher Class for Lead Trigger
* Created By    : Arvind Thakur
* 
* Do NOT write business logic in this class. Make a separate class for it.
* This is a Dispatcher class. As the name suggests route logic to appropriate class
* 
* 
* Modification Log ==============================================
* Author                                  Date                                JIRA-LINK
* Arvind Thakur (created)             28th June, 2018               Updating Team Role on lead
* Arvind Thakur (modified)            30th July, 2018               Lead Assignment Rules implementation
* 
**********************************************************************************************************************************/

public class LeadTriggerDispatcher extends TriggerHandler{
    
    
    private Map<Id, Lead> newLeadMap;
    private Map<Id, Lead> oldLeadMap;
    private List<Lead> newLeadList;
    private List<Lead> oldLeadList;





    public LeadTriggerDispatcher() {

        newLeadMap = (Map<Id, Lead>)Trigger.newMap;
        oldLeadMap = (Map<Id, Lead>)Trigger.oldMap;
        newLeadList = (List<Lead>)Trigger.new;
        oldLeadList = (List<Lead>)Trigger.old;
        
    }
    
    
    
    
    
    public override void beforeInsert() {
        
        Map<Id, List<Lead>> populateTeamNameOnLead = new Map<Id, List<Lead>>();
        Map<Id, List<Lead>> ownerIdLeadMap = new Map<Id, List<Lead>>(); 
        Map<Id, List<Lead>> leadsWithChannelAgreement = new Map<Id, List<Lead>>(); 
        
        for(Lead newLead: newLeadList) {
            
            
            //For every new lead, populate custom lead source with leadsource
            if(String.isBlank(newLead.LeadSource)) {
                newLead.Lead_Source__c = newLead.LeadSource;
            }
            
            
            
            //Populate Channel 2 From Owner's Account's Channel Agreement on Portal Created Leads
            if(newLead.Channel_2__c == null) {
                if(!ownerIdLeadMap.containsKey(newLead.ownerId)) {
                ownerIdLeadMap.put(newLead.ownerId, new List<Lead>{});
                }
                
                ownerIdLeadMap.get(newLead.ownerId).add(newLead);
            }
            //Manual Created Leads
            else {
                if(!leadsWithChannelAgreement.containsKey(newLead.Channel_2__c)) {
                leadsWithChannelAgreement.put(newLead.Channel_2__c, new List<Lead>{});
                }
                
                leadsWithChannelAgreement.get(newLead.Channel_2__c).add(newLead);
            }
            
            
            //Manual created Leads
            //Populate Owner team based on Owner role hierarchy
            if(String.valueOf(newLead.ownerId).startswith('005')) {
                if(!populateTeamNameOnLead.containsKey(newLead.ownerId)) {
                    populateTeamNameOnLead.put(newLead.ownerId, new List<Lead>{});
                }
                
                populateTeamNameOnLead.get(newLead.ownerId).add(newLead);
            }

        }
        
        if(!ownerIdLeadMap.isEmpty()) {
            AccountDetailsOnLead.populateChannelAgreementOnLead(AccountDetailsOnLead.getAccountIdLeadMap(ownerIdLeadMap));
        }
        
        if(!populateTeamNameOnLead.isEmpty()) {
            PopulateTeamLeadOpportunity.PopulateTeamOnLead(populateTeamNameOnLead, false);
            
        }
        
        if(!leadsWithChannelAgreement.isEmpty()) {
            AccountDetailsOnLead.updatePartnerAccessOnLeads(leadsWithChannelAgreement);
        }
    }
    
    
    
    
    public override void afterInsert() {
        
        List<Lead> portalLeadList = new List<Lead>();
        
        for(Lead newLead : newLeadList) { 
            
            //Only Portal Leads
            if(newLead.leadSource == 'Partners' && newLead.Web_Lead_Form__c == 'Partner Portal') {
                portalLeadList.add(newLead);
            }
             
        }
        
        if(!portalLeadList.isEmpty()) {
            assignLeadsUsingAssignmentRule(portalLeadList);
        }
        
    }
    
    
    
    
    
    
    public override void beforeUpdate() {
        
        Map<Id, List<Lead>> populateTeamNameOnLead = new Map<Id, List<Lead>>();
        Map<Id, List<Lead>> channelAgreementIdLeadList = new Map<Id, List<Lead>>();
        
        
        for(Lead oldLead : oldLeadList) {
            if( oldLead.OwnerId != newLeadMap.get(oldLead.Id).OwnerId && String.valueOf(newLeadMap.get(oldLead.Id).OwnerId).startsWith('005')) {
                if(!populateTeamNameOnLead.containsKey(newLeadMap.get(oldLead.Id).OwnerId)) {
                    populateTeamNameOnLead.put(newLeadMap.get(oldLead.Id).OwnerId, new List<Lead>{});
                }
                
                populateTeamNameOnLead.get(newLeadMap.get(oldLead.Id).OwnerId).add(newLeadMap.get(oldLead.Id));
            }
            
            if(oldLead.Channel_2__c != newLeadMap.get(oldLead.Id).Channel_2__c && newLeadMap.get(oldLead.Id).Channel_2__c != null) {
                
                if(!channelAgreementIdLeadList.containskey(newLeadMap.get(oldLead.Id).Channel_2__c)) {
                    channelAgreementIdLeadList.put(newLeadMap.get(oldLead.Id).Channel_2__c , new List<Lead>());
                }
                
                channelAgreementIdLeadList.get(newLeadMap.get(oldLead.Id).Channel_2__c).add(newLeadMap.get(oldLead.Id));
                
            }
            
            //If LeadSource is updated, then update the custom lead source with lead source.
            if(oldLead.LeadSource != newLeadMap.get(oldLead.Id).LeadSource) {
                newLeadMap.get(oldLead.Id).Lead_Source__c = newLeadMap.get(oldLead.Id).LeadSource;
            }
            
        }
        
        
        if(!populateTeamNameOnLead.isEmpty()) {
            PopulateTeamLeadOpportunity.PopulateTeamOnLead(populateTeamNameOnLead, false);
        }
        
        if(!channelAgreementIdLeadList.isEmpty()) {
            AccountDetailsOnLead.updatePartnerAccessOnLeads(channelAgreementIdLeadList);
        }
    }
    
    
    


    public override void afterUpdate() {
        
        Map<Id, List<Lead>> populateTeamNameOnLead = new Map<Id, List<Lead>>();
        
        for(Lead oldLead : oldLeadList) {
            Lead newLead = newLeadMap.get(oldLead.Id);
            if(newLead.isConverted == true && oldLead.isConverted == false) {
                if(!populateTeamNameOnLead.containsKey(newLead.OwnerId)) {
                    populateTeamNameOnLead.put(newLead.OwnerId, new List<Lead>{});
                }
                
                populateTeamNameOnLead.get(newLead.OwnerId).add(newLead);
            } 
            
        }
        
        //This is done on after update as we have to get the converted status
        if(!populateTeamNameOnLead.isEmpty()) {
            PopulateTeamLeadOpportunity.PopulateTeamOnLead(populateTeamNameOnLead, true);
        }
    }
    



    
    /*
    @Invoked from : afterInsert
    @Description  :Upsert on leads with Lead assignment rules - For portal leads only - 
                    Force kick lead assignment ruleThis method deos not work in before insert
    */
    public static void assignLeadsUsingAssignmentRule (List<Lead> newLeads) {
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule= true;          
            
            List<Lead> upsertLeads = new List<Lead>();
            for(Lead leads : newLeads) {
                Lead  newLead = new Lead(Id = leads.Id);
                newLead.setOptions(dmo);
                upsertLeads.add(newLead);
            }
            upsert upsertLeads;
   }
    
    
}