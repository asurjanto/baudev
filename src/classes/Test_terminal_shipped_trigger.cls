@isTest
private class Test_terminal_shipped_trigger {
    
    static testMethod void testAccountTrigger(){
        
        Product2 objProduct1 = new Product2();
        objProduct1.Name = 'XENTA';
        insert objProduct1;
        
        Product2 objProduct2 = new Product2();
        objProduct2.Name = 'Xentissimo';
        insert objProduct2;
        
        Account newAccount = new Account(Name = 'Test Account', Merchant_Status__c = 'Prospect');
        //Account newAccount2 = new Account(Name = 'Test Account2', Merchant_Status__c = 'Prospect');
        insert newAccount;
        //insert newAccount2;
        Case tempCase = new Case(accountid = newAccount.id, Reason = 'New Shipment', status = 'Shipped', Delivery_Address_Line_1__c = 'test street', City__c = 'sydney', State__c = 'NSW', Postcode__c = '2212');
        //Case tempCase2 = new Case(accountid = newAccount2.id, Reason = 'New Shipment', status = 'Shipped');
        insert tempCase;
        //insert tempCase2;
        SFDC_Shipment__c newOutboundShipment = new SFDC_Shipment__c (Case__c = tempCase.id, Shipment_Status__c = 'Outbound', Shipment_Date__c = System.today());
        //SFDC_Shipment__c newInboundShipment = new SFDC_Shipment__c (Case__c = tempCase.id, Shipment_Status__c = 'Inbound', Shipment_Date__c = System.today());
        insert newOutboundShipment;
        //insert newInboundShipment;
        Asset newXenta = new Asset (accountid = newAccount.id, SerialNumber = 'XXX123', name = 'Xenta', 
                                    //Following fields are populated to get the test working
                                    //These are required fields
                                    Hardware_Version__c = 'Hardware Version', Firmware_version__c = 'Firmware Version', Make__c = 'Make', General_Description__c = 'General Description', Physical_connections__c = 'Physical Connections', Last_inventory_performed__c = date.today(),
                                    //Following is added to cater for the validation rule
                                    Product2Id = objProduct1.Id);
        
        Asset newXentissimo = new Asset (accountid = newAccount.id, SerialNumber = 'XXX321', name = 'Xentissimo',
                                         //Following fields are populated to get the test working
                                         //These are required fields
                                         Hardware_Version__c = 'Hardware Version', Firmware_version__c = 'Firmware Version', Make__c = 'Make', General_Description__c = 'General Description', Physical_connections__c = 'Physical Connections', Last_inventory_performed__c = date.today(),
                                         //Following is added to cater for the validation rule
                                         Product2Id = objProduct2.Id);
        insert newXenta;
        insert newXentissimo;
        Shipment_Content__c newXentaContent = new Shipment_Content__c (Shipment__c = newOutboundShipment.id, Asset__c = newXenta.id);
        Shipment_Content__c newXentissimoContent = new Shipment_Content__c (Shipment__c = newOutboundShipment.id, Asset__c = newXentissimo.id);
        insert newXentaContent;
        
        ///**********************************************************************************
        ///Paper_Order__c object is deemed redundant.
        ///The code below is commented to remove the dependency of Paper_Order__c object
        ///Jira Item - SF-503
        ///**********************************************************************************
        
        Case updatedCase = [select accountid, Shipped_Paper__c, Paper_Order__c, Shipped_Paper_Xentissimo__c from Case where id = :tempCase.id];
        System.assertEquals('Xenta', updatedCase.Paper_Order__c);
        //System.assertEquals(TRUE, updatedCase.Shipped_Paper__c);
        //Paper_Order__c newXentaOrder = [select id, Paper_Type__c from Paper_Order__c where account__c = :updatedCase.accountid AND Paper_type__c = 'Xenta'];
        //System.assertEquals('Xenta', newXentaOrder.Paper_Type__c);
        
        insert newXentissimoContent;
        Case updatedCase2 = [select accountid, Shipped_Paper__c, Paper_Order__c, Shipped_Paper_Xentissimo__c from Case where id = :tempCase.id];
        //System.assertEquals(TRUE, updatedCase2.Shipped_Paper_Xentissimo__c);
        System.assertEquals('Xentissimo', updatedCase2.Paper_Order__c);
        //Paper_Order__c newXentissimoOrder = [select id, Paper_Type__c from Paper_Order__c where account__c = :updatedCase.accountid AND Paper_type__c = 'Xentissimo'];
        //System.assertEquals('Xentissimo', newXentissimoOrder.Paper_Type__c);
        
        //Now insert data causing an contact trigger to fire. 
        //Test.startTest();
        //update tempCase;
        //update tempCase2;
        //System.assertEquals('Prospect', newAccount.Merchant_Status__c);
        //Test.stopTest();	
    }	
    
}