@isTest
private class eCommerceOnboardingController_Test {
    private static testMethod void testOnboardingController() {
        test.startTest();

            Account createdAccount = [SELECT Id , Name from Account WHERE Name = 'testAccountLocation' limit 1 ];
            List<Location__c> locationList = eCommerceOnboardingController.findAccountLocations(String.valueOf(createdAccount.Id));
            system.assert(true, locationList.size() > 0);

        test.stopTest();
    }


    @TestSetup
    private static void makeData(){

        Account newAccount = TestUtils.createAccount('testAccountLocation', true);
        Location__c newLocation = TestUtils.createNewLocation(newAccount.Id, 'test', 'testinf 123', 'mycity', 'NSW', 
                                                    'Australia',  '2323', true);

        
    }
}