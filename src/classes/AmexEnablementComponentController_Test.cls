/**********************************************************
* Class Name    : AmexServiceClass_Test
* Copyright     : Tyro Payments (c) 2019
* Description   : Test Class for AmexServiceClass
* Created By    : Arvind Thakur (07/03/2019)
* 
* 
* **********************************************************/

@isTest(seeallData=false)
private class AmexEnablementComponentController_Test {
    
    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP;
    private static final String TYRO_CUSTOMER;
    
    static {
        MERCHANT_GROUP  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();
    }

	private static testMethod void test_tyroCustomerAccountSearch() {
	    
	    Test.startTest();
    	    Account selectedAccount = [SELECT Id, RecordTypeId, Corporate_Group__c, Amex_IEAP_Rate__c  FROM Account WHERE Name = 'TyroCustomer'];
    	    String response = AmexEnablementComponentController.getParentAccountDetails(selectedAccount.Id);
    	    
            AmexServiceClass.AccountDetailsWrapper accountWrapper = (AmexServiceClass.AccountDetailsWrapper)JSON.deserialize(response, AmexServiceClass.AccountDetailsWrapper.class);
            system.assertEquals(accountWrapper.amexRateCalculated , 1.3);
            Map<String, Contact> selectedContactObject = new Map<String, Contact>();
            selectedContactObject.put('ContactData', accountWrapper.accountContactDetails[0]);
            
            
            
            AmexEnablementComponentController.sendAmexInvitation(   accountWrapper.searchedAccount.Id, 
                                                                    JSON.serialize(accountWrapper.searchedAccount.Name), 
                                                                    JSON.serialize(selectedContactObject), 
                                                                    JSON.serialize(new List<String> {accountWrapper.accountMidDetails[0].merchantId}),  
                                                                    JSON.serialize(accountWrapper.amexRateCalculated), 
                                                                    JSON.serialize(accountWrapper.amexTransactionVolume));
                                                                    
            List<Case> amexCase = [Select id from case where recordtype.name = 'Amex Ticket'];                                                        
            system.assert(true, amexCase.size() > 0);
            
        Test.stopTest();
	}
	
	private static testMethod void test_tyroCustomerMidSearch() {
	    
	    Test.startTest();
    	    Account selectedAccount = [SELECT Id, RecordTypeId, Corporate_Group__c, Amex_IEAP_Rate__c  FROM Account WHERE Name = 'TyroCustomer'];
    	    Merchant_Id__c accountMid = [SELECT Id, Account__c FROM Merchant_Id__c WHERE Account__c =:selectedAccount.Id];
    	    String resonse = AmexEnablementComponentController.getAccountOfMID(accountMid.Id);
    	    
            AmexServiceClass.AccountDetailsWrapper accountWrapper = (AmexServiceClass.AccountDetailsWrapper)(JSON.deserialize(resonse, AmexServiceClass.AccountDetailsWrapper.class));
            system.assertEquals(accountWrapper.amexRateCalculated , 1.3);
        Test.stopTest();
	}
	
	
	
	private static testMethod void test_corporateGroupsAccountSearch() {
	    
	    Test.startTest();
    	    Account selectedAccount = [SELECT Id, RecordTypeId, Corporate_Group__c, Amex_IEAP_Rate__c  FROM Account WHERE Name = 'TyroCustomer'];
    	    Account cgAccount = [SELECT Id, RecordTypeId, Corporate_Group__c, Amex_IEAP_Rate__c  FROM Account WHERE Name = 'CG Account'];
    	    selectedAccount.Corporate_Group__c = cgAccount.Id;
    	    update selectedAccount;
    	    String resonse =  AmexEnablementComponentController.getParentAccountDetails(cgAccount.Id);
    	    
    	    
            AmexServiceClass.AccountDetailsWrapper accountWrapper = (AmexServiceClass.AccountDetailsWrapper)(JSON.deserialize(resonse, AmexServiceClass.AccountDetailsWrapper.class));
            system.assertEquals(accountWrapper.amexRateCalculated , 1.3);
            
            Merchant_Id__c accountMid = [SELECT Id, Account__c FROM Merchant_Id__c WHERE Account__c =:selectedAccount.Id];
            
            TestUtils.createTransactionHistoryRecord(accountMid.Id, system.today().addMonths(-3), Double.valueOf(112223) , true);
            TestUtils.createTransactionHistoryRecord(accountMid.Id, system.today().addMonths(-4), Double.valueOf(512223) , true);
            TestUtils.createTransactionHistoryRecord(accountMid.Id, system.today().addMonths(-5), Double.valueOf(512223) , true);
            TestUtils.createTransactionHistoryRecord(accountMid.Id, system.today().addMonths(-6), Double.valueOf(912223) , true);
            
            resonse = AmexEnablementComponentController.getParentAccountDetails(selectedAccount.Id);
            accountWrapper = (AmexServiceClass.AccountDetailsWrapper)(JSON.deserialize(resonse, AmexServiceClass.AccountDetailsWrapper.class));
            system.assertEquals(accountWrapper.amexRateCalculated , 1.6);
            
            
        Test.stopTest();
	}
	
	private static testMethod void test_corporateGroupsMidSearch() {
	    
	    Test.startTest();
    	    Account selectedAccount = [SELECT Id, RecordTypeId, Corporate_Group__c, Amex_IEAP_Rate__c  FROM Account WHERE Name = 'TyroCustomer'];
    	    Account cgAccount = [SELECT Id, RecordTypeId, Corporate_Group__c, Amex_IEAP_Rate__c  FROM Account WHERE Name = 'CG Account'];
    	    selectedAccount.Corporate_Group__c = cgAccount.Id;
    	    update selectedAccount;
    	    Merchant_Id__c accountMid = [SELECT Id, Account__c FROM Merchant_Id__c WHERE Account__c =:selectedAccount.Id];
    	    String response = AmexEnablementComponentController.getAccountOfMID(accountMid.Id);
    	    
    	    
            AmexServiceClass.AccountDetailsWrapper accountWrapper = (AmexServiceClass.AccountDetailsWrapper)(JSON.deserialize(response, AmexServiceClass.AccountDetailsWrapper.class));
            system.assertEquals(accountWrapper.amexRateCalculated , 1.3);
            
            
            TestUtils.createTransactionHistoryRecord(accountMid.Id, system.today().addMonths(-3), Double.valueOf(112223) , true);
            TestUtils.createTransactionHistoryRecord(accountMid.Id, system.today().addMonths(-4), Double.valueOf(512223) , true);
            TestUtils.createTransactionHistoryRecord(accountMid.Id, system.today().addMonths(-5), Double.valueOf(512223) , true);
            TestUtils.createTransactionHistoryRecord(accountMid.Id, system.today().addMonths(-6), Double.valueOf(912223) , true);
            
            response = AmexEnablementComponentController.getAccountOfMID(accountMid.Id);
            accountWrapper = (AmexServiceClass.AccountDetailsWrapper)(JSON.deserialize(response, AmexServiceClass.AccountDetailsWrapper.class));
            system.assertEquals(accountWrapper.amexRateCalculated , 1.6);
            
            
        Test.stopTest();
	}
	
	
	
	/*
	Create test Data
	*/
	@TestSetup
    private static void createTestData() {
        
        //Custom settings Insertions
        insert new Amex_Configurations__c (SetupOwnerId=UserInfo.getOrganizationId(), 
                                            Micro_Segment_Ceiling__c = 100000,
                                            Small_Segment_Ceiling__c = 1000000,
                                            Divisor_of_Net_Transaction_Volume__c = 20,
                                            Invitation_Email_Template__c = '00X8E000000JKSt'
                                           );
        
        insert new Amex_MCC_Rates__c (  Name = 'TestMccData',
                                        MCC_Code__c = '3434',
                                        Micro_Rate__c = 1.3,
                                        Small_Rate__c = 1.6
                                     );
        
        Mcc_Description__c mccCode = TestUtils.createNewMCCDescription('testMCCCode', true, 'Health', '3434', true);
        
        //Tyro Customer Account
        Account tyroCustomer = TestUtils.createAccount('TyroCustomer', false);
	    tyroCustomer.RecordTypeId = TYRO_CUSTOMER;
	    insert tyroCustomer;
	    
	    //Inserting Corporate Group
	    Account corporateGroupAccount = TestUtils.createAccount('CG Account', false);
	    corporateGroupAccount.RecordTypeId = CORPORATE_GROUP;
	    insert corporateGroupAccount;
	    
	    Contact tyroCustomerContact = TestUtils.createContact(tyroCustomer.Id, 'tyrocustomercontact', true);
	    Channel_Agreement__c newCA = TestUtils.createChannelAgreement('testchannel', tyroCustomer.Id, '32424', true);
	    Integration_Product__c newPOS = TestUtils.createNewPOS('myPOS', tyroCustomer.Id,  8, true);
	    Merchant_Id__c newMid = TestUtils.createMerchantId(tyroCustomer.Id, false, null, 'Active', newPOS.Id, newCA.Id, tyroCustomerContact.Id, false);
	    newMid.Mcc_Description__c = mccCode.Id;
	    insert newMid;
	    
        Location__c newLocation = TestUtils.createNewLocation(tyroCustomer.Id, 'testLocation', '155 Clearance street floor 1-5', 'Sydney', 'NSW', 'Australia', '2148', true);
        
        TestUtils.createTransactionHistoryRecord(newMid.Id, system.today().addDays(-1), Double.valueOf(11111) , true);
        TestUtils.createTransactionHistoryRecord(newMid.Id, system.today().addMonths(-1), Double.valueOf(11231) , true);
        TestUtils.createTransactionHistoryRecord(newMid.Id, system.today().addMonths(-2), Double.valueOf(21231) , true);
        
    }
}