/**************************************************************************************************
 * Name         : eCommerceRiskApprovalComponentController
 * Description  : Controller class for lightning component
 * Used         : used by  eCommerceRiskApprovalComponent
                    and quotePDFGenerationComponent.
 * 
***************************************************************************************************/


public without sharing class eCommerceRiskApprovalComponentController {
    
    public eCommerceRiskApprovalComponentController() {

    }

    @AuraEnabled
    public static string actionApprovalProcess(String recordId, String action, String comments) {
        
        Approval.ProcessWorkitemRequest approvalAction = new Approval.ProcessWorkitemRequest();
        approvalAction.setComments(comments);
        
        //Approve or Reject Record
        approvalAction.setAction(action);
        String currentUserId = UserInfo.getUserId();
        //Getting Work Item Id
        Id workItemId;
        String query = 'SELECT Id FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =: recordId ';
        if(action == 'Approve' || action == 'Reject') {
            query += 'AND actorId =: currentUserId';
        }
        //We don't need anything for Removed // This happens when approval process is recalled.
        
        
        for ( sObject pItem : Database.query(query)) {
            workItemId = (Id)pItem.Id;                                    
        }

        if(workItemId != null) {
            approvalAction.setWorkitemId(workItemId);
        }else{
            return JSON.serialize(new resultWrapper(false, 'Either is record is not in approval process or you are not authorized to approve it.', null));
        }
        

        Approval.ProcessResult result = Approval.process(approvalAction);
        Approval.unlock(recordId, false);
        
        if(result.success) {
            return JSON.serialize(new resultWrapper(true, 'Success', null));
        }else{
            return JSON.serialize(new resultWrapper(false, 'Error', result.errors));
        }
    }
    /*
    @AuraEnabled
    public static string assignApprovalToCurrentUser(String recordId) {
        
        Boolean isRecordUnderApprovalProcess = false;
        ProcessInstanceWorkItem currentWorkItem;
        Boolean isCurrentUserActor = false;
        String currentUserId;

        List<Profile> profile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        String userProfileName = profile[0].Name;

        for(ProcessInstanceWorkItem workItem : [SELECT Id, ActorId 
                                                FROM ProcessInstanceWorkItem 
                                                WHERE ProcessInstance.TargetObjectId =: recordId]) {
            isRecordUnderApprovalProcess = true;
            if(workItem.ActorId == currentUserId)   {
                isCurrentUserActor = true;
            }
            currentWorkItem = workItem;
        }

        if(isRecordUnderApprovalProcess && !isCurrentUserActor && userProfileName == 'Financial') {
            currentWorkItem.ActorId = UserInfo.getUserId();
            update currentWorkItem;
            return JSON.serialize(new resultWrapper(true, 'Success', null));
        }else{
            return JSON.serialize(new resultWrapper(false, 'Either is record is not in approval process or you are not authorized to approve it', null));
        }

    }
    */
    public class resultWrapper {

        public boolean isSuccess;
        public String message;
        public List<Database.Error> systemErrors;

        public resultWrapper(Boolean isSuccess, String message, List<Database.Error> systemErrors) {
            this.isSuccess = isSuccess;
            this.message = message;
            this.systemErrors = systemErrors;
        }
    }
}