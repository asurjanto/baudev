@isTest(SeeAllData=false)
private class Test_AccountVerificationCompController {
    
    
	private static testMethod void testAccountVerificationComponentController() {
	    
	    Test.startTest();
        AccountVerificationComponentController.fetchAllQuestions();
        
        Account insertedAccount = [SELECT Id FROM Account WHERE Name = 'TestVerificationAccount' Limit 1];
        Account_Identification_Log__c logRecord = [SELECT Id FROM Account_Identification_Log__c WHERE Account__c =: insertedAccount.Id]; 
        Case insertedCase = [SELECT Id FROM Case WHERE AccountId = :insertedAccount.Id];
        //List<FAST_InputLookupController.SearchResult> articleResults = (List<FAST_InputLookupController.SearchResult>)JSON.deserialize(FAST_InputLookupController.searchSObject('KnowledgeArticleVersion', 'Article', null, null ), List<FAST_InputLookupController.SearchResult>.Class);
        
        
        //Create Article
	    Diagnostic_Internal_Article__kav newArticle = (Diagnostic_Internal_Article__kav)TestUtils.createKnowledgeArticle('Diagnostic_Internal_Article__kav', 'TestClass Article', 'Summary Article', 'testURL', true);
        //Publish Created Artcile
        Diagnostic_Internal_Article__kav obj1 = [SELECT Id,Title,KnowledgeArticleId FROM Diagnostic_Internal_Article__kav WHERE Id =: newArticle.Id];
        KbManagement.PublishingService.publishArticle(obj1.KnowledgeArticleId, true);
        
        
        
        AccountVerificationComponentController.sendEmailNotificationToRisk(insertedAccount.Id, logRecord.Id);
        AccountVerificationComponentController.attachArticleToCase(insertedCase.Id, obj1.KnowledgeArticleId);
        AccountVerificationComponentController.fetchLogHistory(insertedAccount.Id);
        
        Test.stopTest();
	}
	
	
	@TestSetup
	public static void createTestDate() {
	    
	    Account newAccount = TestUtils.createAccount('TestVerificationAccount', true);
	    Account_Identification_Log__c logRecord = TestUtils.createAccountVerificationLogRecord(newAccount.Id, true);
	    Case newCase = TestUtils.createCase('Support Call', 'testclassCase', 'New', false);
	    newCase.AccountId = newAccount.Id;
	    newCase.Call_Category__c = 'Loans';
	    insert newCase;
	    
	    
        
	}

}