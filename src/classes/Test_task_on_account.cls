@isTest
private class Test_task_on_account 
{
    
    static testMethod void myUnitTest() 
    {
        Account beforeAccount = new Account(name = 'test Acc');
        insert beforeAccount;
        beforeAccount = [select OwnerId, has_task__c from Account where name = 'test Acc' limit 1];
        Task testTask = new Task(subject = 'Mass Email: test', OwnerId = beforeAccount.ownerID, whatID = beforeAccount.id, 
                                 Task_Category__c = 'First Call');
        insert testTask;
        Account afterAccount1 = [select OwnerId, has_task__c from Account where name = 'test Acc' limit 1];
        System.assertEquals(FALSE, afterAccount1.has_task__c );
        
        Task testTask2 = new Task(subject = 'Test task', OwnerId = beforeAccount.ownerID, whatID = beforeAccount.id, Task_Category__c = 'Successful Call'); 
        insert testTask2;
        Account afterAccount = [select OwnerId, has_task__c from Account where name = 'test Acc' limit 1];
        System.assertEquals(TRUE, afterAccount.has_task__c );
        
        testTask2.Task_Category__c = 'First Call';
        update testTask2;
        
        testTask2.Task_Category__c = 'Initial Contact Follow Up';
        update testTask2;
        
        testTask2.Task_Category__c = 'Objection Handling';
        update testTask2;
        
        testTask2.Task_Category__c = 'Application Chase';
        update testTask2;
        
        testTask2.Task_Category__c = 'Application received: Chase Info';
        update testTask2;
        
        try
        {
            testTask2.Task_Category__c = 'Account Management';
            update testTask2;
        }
        catch(Exception excep)
        {
            //system.assert(true);
        }
    }
}