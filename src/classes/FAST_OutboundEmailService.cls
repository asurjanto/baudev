/***********************************************************************************
Class Name      : FAST_OutboundEmailService
Created By      : Arvind Thakur
Description     : Generic Outbound Email Service Class.
Created Date    : 11th Septemeber, 2018
*************************************************************************************/
Global class FAST_OutboundEmailService {
    
    /*
    Description : Used to send Risk Emails Only using a specific template. 
                  Called From AccountVerificationComponentController.cls
    */
    public static void sendRiskEmail(List<String> emailList, EmailTemplate templateToUse, Account currentAccount, 
                                    Account_Identification_Log__c logRecord) { 
            
            
            List<Messaging.SingleEmailMessage> sendEmailList = new List<Messaging.SingleEmailMessage>();
            List<Messaging.SingleEmailMessage> oldEmailList  = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage singleEmail;
            
            User testUser = [SELECT Id, Email FROM User WHERE email = 'athakur@tyro.com' limit 1];
            
            Map<String, String> userMailNameMapping = new Map<String, String>();
            
            for(User systemUsers : [SELECT Id, Name, Email FROM User WHERE Email IN :emailList]) {
                
                userMailNameMapping.put(systemUsers.Email, systemUsers.Name);
                
            }
            
            
            String htmlBody = templateToUse.HtmlValue;
            String plainBody = templateToUse.Body;
            
            //Record Link
            String accountURL = URL.getSalesforceBaseUrl().toExternalForm()+'/'+currentAccount.Id;
            String logRecordURL = URL.getSalesforceBaseUrl().toExternalForm()+'/'+logRecord.Id;
            String userURL = URL.getSalesforceBaseUrl().toExternalForm()+'/'+userInfo.getUserId();
 
            htmlBody = htmlBody.replace('{!Account.Link}', '<a href="' + accountURL + '">' + currentAccount.Name + ' - ' + currentAccount.Account_No__c + '</a>');
            htmlBody = htmlBody.replace('{!Account_Verification_Record_Link}', '<a href="' + logRecordURL + '">' + logRecord.Name + '</a>');
            htmlBody = htmlBody.replace('{!Agent_Record_Name_Link}', '<a href="' + userURL + '">' + userInfo.getFirstName() + ' ' + userInfo.getLastName() + '</a>');
            
            plainBody = plainBody.replace('{!Account.Link}', currentAccount.Name + ' - ' + currentAccount.Account_No__c);
            plainBody = plainBody.replace('{!Account_Verification_Record_Link}', logRecord.Name);
            plainBody = plainBody.replace('{!Agent_Record_Name_Link}', userInfo.getUserName());
            
            
            
            Messaging.SingleEmailMessage rollbackEmail = new Messaging.SingleEmailMessage();
            rollbackEmail.setSenderDisplayName('support@salesforce.tyro.com');
            rollbackEmail.setWhatId(currentAccount.Id);
            rollbackEmail.setTemplateId (templateToUse.Id);
            rollbackEmail.setSubject('Suspicion Call Reported');
            rollbackEmail.setToAddresses(new List<String> {testUser.Email} );
            rollbackEmail.setHtmlBody(htmlBody);
            rollbackEmail.setPlainTextBody(plainBody);
            
            Savepoint sp = Database.setSavepoint();
                oldEmailList.add(rollbackEmail);
                Messaging.sendEmail(oldEmailList);
            Database.rollback(sp);
            
            List<Messaging.SingleEmailMessage> newListMsgToSend = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage referenceEmail = oldEmailList[0];
            //Messaging.sendEmail(lstMsgsToSend);
            
            
            for(String emailOfReceiver : emailList) {
                
                String contactName;
                if(userMailNameMapping.containsKey(emailOfReceiver)) {
                    contactName = userMailNameMapping.get(emailOfReceiver);
                }else{
                    contactName = 'Team';
                }

                singleEmail = new Messaging.SingleEmailMessage();
                singleEmail.setSenderDisplayName('support@salesforce.tyro.com');
                singleEmail.setWhatId(currentAccount.Id);
                singleEmail.setSubject(referenceEmail.getSubject());
                singleEmail.setToAddresses(new List<String> {emailOfReceiver});
                singleEmail.setHtmlBody(referenceEmail.getHTMLBody().replace('{!User.FullName}', contactName));
                singleEmail.setPlainTextBody(referenceEmail.getPlainTextBody().replace('{!User.FullName}', contactName));
                
                newListMsgToSend.add(singleEmail);
                
            }
            system.debug('>>>> sending out Email'  + newListMsgToSend);
            
            Messaging.sendEmail(newListMsgToSend);
    
        }  
}