public class AccountAllocationRulesTriggerDispatcher extends TriggerHandler {
    
    
    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP;
    private static final String TYRO_CUSTOMER;
    
    private static final Set<Id> excludedAccounts;
    

    static {
        MERCHANT_GROUP  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();
        
        excludedAccounts = new Set<Id>();
        for(Accounts_Excluded_From_Allocation_Rules__c allExcludedAccounts : Accounts_Excluded_From_Allocation_Rules__c.getAll().values()) {
            excludedAccounts.add(allExcludedAccounts.Excluded_Account_Id__c);
        }
    }
    
    //Private Variables
    private Map<Id, Account_Allocation_Rules__c> newAccountRulesMap;
    private Map<Id, Account_Allocation_Rules__c> oldAccountRulesMap;
    private List<Account_Allocation_Rules__c> newAccountRulesList;
    private List<Account_Allocation_Rules__c> oldAccountRulesList;
    
    //Constructor
    public AccountAllocationRulesTriggerDispatcher() {
        
        newAccountRulesMap = (Map<Id, Account_Allocation_Rules__c>)Trigger.newMap;
        oldAccountRulesMap = (Map<Id, Account_Allocation_Rules__c>)Trigger.oldMap;
        newAccountRulesList = (List<Account_Allocation_Rules__c>)Trigger.new;
        oldAccountRulesList = (List<Account_Allocation_Rules__c>)Trigger.old;        
        
    }
    
    
    //After Update Function
    public override void afterUpdate() {
        
        Map<String, Id> updatedRules = new Map<String, Id>(); 
        for(Account_Allocation_Rules__c oldRule : oldAccountRulesList) {
            
            if( (oldRule.Rule_Allocation_Name__c != newAccountRulesMap.get(oldRule.Id).Rule_Allocation_Name__c) ||
                (oldRule.Account_Manager__c != newAccountRulesMap.get(oldRule.Id).Account_Manager__c) ||
                (!oldRule.Active__c && newAccountRulesMap.get(oldRule.Id).Active__c )) {
                
                updatedRules.put(newAccountRulesMap.get(oldRule.Id).Rule_Allocation_Name__c, newAccountRulesMap.get(oldRule.Id).Account_Manager__c);
                
            }
            
        }
        
        if(!updatedRules.isEmpty()) {
            udpateAccountsWithNewRules(updatedRules);
        }
        
    }
    
    public override void afterInsert() {
        
        Map<String, Id> newRulesList = new Map<String, Id>(); 
        for(Account_Allocation_Rules__c newRules : newAccountRulesList) {
            if(newRules.Active__c) {
                newRulesList.put(newRules.Rule_Allocation_Name__c, newRules.Account_Manager__c);
            }
            
        }
        
        if(!newRulesList.isEmpty()) {
            udpateAccountsWithNewRules(newRulesList);
        }
        
    }
    
    /*
    I do not want to update all Accounts Here
    Only Accounts which are standalone TC
    Only Accounts which are corporate Groups
    Only accounts which are merchant Groups.
    This is because - The Account trigger will take care of the rest of the hierarchy.
    */
    private static void udpateAccountsWithNewRules(Map<String, Id> newRules) {
        
        Map<Id, Account> allMerchantGroups = new Map<Id, Account>();
        Map<Id, Account> allCorporateGroups = new Map<Id, Account>();
        Map<Id, Account> allTyroCustomer = new Map<Id, Account>();
        Map<Id, Account> allTyroCustomerClone = new Map<Id, Account>();
        
        
        for(Account accounts : [SELECT Id, Name, OwnerId, RecordType.Id, Account_Allocation_Parameters__c, Corporate_Group__c
                                    FROM Account 
                                    WHERE Key_Account__c = false
                                    AND Corporate_Group__r.Key_Account__c = false
                                    AND Id NOT IN:excludedAccounts
                                    AND Account_Allocation_Parameters__c IN :newRules.keySet() 
                                    AND (RecordType.Id =:TYRO_CUSTOMER OR RecordType.Id =:CORPORATE_GROUP OR RecordType.Id =:MERCHANT_GROUP) FOR UPDATE]) {
            
            if(accounts.RecordType.Id == TYRO_CUSTOMER) {
                allTyroCustomer.put(accounts.Id, accounts);
            }else if(accounts.RecordType.Id == CORPORATE_GROUP) {
                allCorporateGroups.put(accounts.Id, accounts);
            }else if(accounts.RecordType.Id == MERCHANT_GROUP) {
                allMerchantGroups.put(accounts.Id, accounts);
            }
  
        }
        
        allTyroCustomerClone.putAll(allTyroCustomer);
        
        for(Merchant_Group_Member__c merchantGroupTyroCustomer : [SELECT Id, Account__c FROM Merchant_Group_Member__c WHERE Account__c IN :allTyroCustomer.KeySet()]) {
            allTyroCustomerClone.remove(merchantGroupTyroCustomer.Account__c);
        }
        
        
        
        List<Account> accountsToUpdate = AccountServiceClass.specificAccountsUpdatesOnly(allCorporateGroups, allMerchantGroups, allTyroCustomerClone, excludedAccounts, newRules);
        
        if(!accountsToUpdate.isEmpty()) {
            try{
                update accountsToUpdate;
            }catch(Exception Ex) {
                system.debug('Error Updating Account Records At :AccountAllocationRulesTriggerDispatcher Methods:udpateAccountsWithNewRules' + ex.getMessage());
            }
            
        }

        
    }
    
    

}