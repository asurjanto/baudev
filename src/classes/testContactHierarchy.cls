@isTest 
private with sharing class testContactHierarchy {

	static testMethod void testContactHierarchy() {
		// Instanciate Page
	
		ContactHierarchyTestData.createTestHierarchy();

		Contact topContact = [Select id, name from contact where name = 'Hierarchy Test0' LIMIT 1];
		Contact middleContact = [Select id, ReportsToId, name from contact where name = 'Hierarchy Test4' LIMIT 1];
		Contact bottomContact = [Select id, ReportsToId, name from contact where name = 'Hierarchy Test9' LIMIT 1];
		Contact[] contactList = [Select id, ReportsToId, name from contact where name like 'Hierarchy Test%'];

		test.startTest();
		
		System.debug('DEBUG: ContactList size: '+contactList.size());
	
		PageReference ContactHierarchyPage = Page.ContactHierarchyPage;
		Test.setCurrentPage(ContactHierarchyPage);
		ApexPages.currentPage().getParameters().put('id', topContact.id);
	
		// Instanciate Controller
		ContactStructure controller = new ContactStructure();
		
		// Call Methodes for top account
		controller.setcurrentId(null);
		ContactStructure.ObjectStructureMap[] smt1 = new ContactStructure.ObjectStructureMap[]{};
		smt1 = controller.getObjectStructure();
		System.Assert(smt1.size()>0, 'Test failed at Top contact, no Id');

		controller.setcurrentId(String.valueOf(topContact.id));
		ContactStructure.ObjectStructureMap[] smt2 = new ContactStructure.ObjectStructureMap[]{};
		smt2 = controller.getObjectStructure();
		System.Assert(smt2.size()>0, 'Test failed at Top contact, with Id: '+smt2.size());

		//Call ObjectStructureMap methodes
		smt2[0].setnodeId('1234567890');
		smt2[0].setlevelFlag(true);
		smt2[0].setlcloseFlag(false);
		smt2[0].setnodeType('parent');
		smt2[0].setcurrentNode(false);
		smt2[0].setcontact(topContact);
		String nodeId = smt2[0].getnodeId();
		Boolean[] levelFlag = smt2[0].getlevelFlag();
		Boolean[] closeFlag = smt2[0].getcloseFlag();
		String nodeType = smt2[0].getnodeType();
		Boolean currentName = smt2[0].getcurrentNode();
		Contact smbContact = smt2[0].getcontact();


		// Call Methodes for middle account
		controller.setcurrentId(String.valueOf(middleContact.id));
		ContactStructure.ObjectStructureMap[] smm = new ContactStructure.ObjectStructureMap[]{};
		smm = controller.getObjectStructure();
		System.Assert(smm.size()>0, 'Test failed at middle contact');

		// Call Methodes for bottom account
		controller.setcurrentId(String.valueOf(bottomContact.id));
		ContactStructure.ObjectStructureMap[] smb = new ContactStructure.ObjectStructureMap[]{};
		smb = controller.getObjectStructure();
		System.Assert(smb.size()>0, 'Test failed at top contact');
		
		test.stopTest();
	}
	
}