public class AmexServiceClass {
    
    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP;
    private static final String TYRO_CUSTOMER;    
    
    private static final Integer AMEX_DIVISOR_OF_TXN_VOLUME;
    
    private static final Map<String, Double> allMCCCodeMicroRates;
    private static final Map<String, Double> allMCCCodeSmallRates;
    
    private static final Integer MICRO_SEGMENT_CEILING;
    private static final Integer SMALL_SEGMENT_CEILING;
    
    private static final String MICRO_SEGMENT;    
    private static final String SMALL_SEGMENT;
    
    
    static { 
        
        Amex_Configurations__c amexConfig = Amex_Configurations__c.getOrgDefaults();
        
        MICRO_SEGMENT = 'Micro';
        SMALL_SEGMENT = 'Small';
        
        MERCHANT_GROUP  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();

        
        MICRO_SEGMENT_CEILING = Integer.valueOf(amexConfig.Micro_Segment_Ceiling__c);
        SMALL_SEGMENT_CEILING = Integer.valueOf(amexConfig.Small_Segment_Ceiling__c);
        
        //implies 5% of Net Transaction Volume;
        AMEX_DIVISOR_OF_TXN_VOLUME  = Integer.valueOf(amexConfig.Divisor_of_Net_Transaction_Volume__c);
       
        allMCCCodeMicroRates = new Map<String, Double>();
        allMCCCodeSmallRates = new Map<String, Double>();
        
        for(Amex_MCC_Rates__c amexRates : [SELECT Id, Name, MCC_Code__c, Small_Rate__c, Micro_Rate__c
                                             FROM Amex_MCC_Rates__c]) {
            
            allMCCCodeMicroRates.put(amexRates.MCC_Code__c, amexRates.Micro_Rate__c); 
            allMCCCodeSmallRates.put(amexRates.MCC_Code__c, amexRates.Small_Rate__c); 
            
            
        }
    }
    
    
    /*
    Calculate Amex eligibility for Tyro customers
    Accounts should have RecordTypeId, Corporate_Group__c, Amex_IEAP_Rate__c 
    */
    public static Map<Id, AccountDetailsWrapper> tyroCustomerAmexEligibilityDetails(List<Account> selectedAccounts) {
        
        
        Map<Id, AccountDetailsWrapper> allAccountDetails = new Map<Id, AccountDetailsWrapper>();
        Map<Id, AccountDetailsWrapper> segmentEligibleAccounts = new Map<Id, AccountDetailsWrapper>();
        Map<Id, Set<Id>> accountMids = new Map<Id, Set<Id>>();
        Map<Id, Double> accountNetTransactionVolume = new Map<Id, Double>();
        Map<Id, AccountDetailsWrapper> finalMap = new Map<Id, AccountDetailsWrapper>();
        
        
        Set<Id> accountIds = new Set<Id>();
        for(Account thisaccount : selectedAccounts) {
            accountIds.add(thisaccount.id);
        }
        accountMids = AccountServiceClass.getAccountMerchantIds(accountIds);
        
        
        Set<Id> allMids = new Set<Id>();
        for(Set<Id> Mids : accountMids.values()) {
            allMids.addAll(Mids);
        }
        accountNetTransactionVolume = AccountServiceClass.getNetTransactionVolumeForTyroCustomers(accountMids, allMids);
        
        for(Account allAccounts : selectedAccounts) {
            
            allAccountDetails.put(allAccounts.Id, new AccountDetailsWrapper(allAccounts));
            allAccountDetails.get(allAccounts.Id).netTransactionVolume = accountNetTransactionVolume.get(allAccounts.id)*12;
            allAccountDetails.get(allAccounts.Id).amexTransactionVolume = accountNetTransactionVolume.get(allAccounts.Id)*12/AMEX_DIVISOR_OF_TXN_VOLUME;
            AccountDetailsWrapper updatedWrapper = setAmexSegment(allAccountDetails.get(allAccounts.Id));
            
            if(updatedWrapper.amexSegmentElligibility) {
                segmentEligibleAccounts.put(allAccounts.Id, updatedWrapper);
            }
            
            allAccountDetails.put(allAccounts.Id, updatedWrapper);
        }
        
        
        for(Merchant_Id__c accountMid : [SELECT Id, Account__c, Name, MCC_Description__c, MCC_Description__r.Name, MCC_Description__r.MCC_Code__c, Status__c,
                                                Amex_Enabled__c
                                        FROM Merchant_Id__c 
                                        WHERE Account__c IN: segmentEligibleAccounts.keySet()
                                        AND Status__c = 'Active']) {
            
            if(!finalMap.containsKey(accountMid.Account__c)) {
                finalMap.put(accountMid.Account__c, setAccountAndMidEligibility(segmentEligibleAccounts.get(accountMid.Account__c), accountMid));
            }else{
                finalMap.put(accountMid.Account__c, setAccountAndMidEligibility(finalMap.get(accountMid.Account__c), accountMid));
            }
             
        }
        
        
        for(Contact accountContact : [SELECT Id, AccountId, Name, Email, Phone FROM Contact WHERE AccountId IN: finalMap.keySet()]) {
            finalMap.get(accountContact.AccountId).accountContactDetails.add(accountContact);
        }
        
        
        return finalMap;
    }
    
    
    
    
        
    
    /*
    Calculate Amex eligibility for Corporate Group customers
    Accounts should have RecordTypeId, Corporate_Group__c, Amex_IEAP_Rate__c 
    */
    public static Map<Id, AccountDetailsWrapper> corporateGroupsAmexEligibilityDetails(List<Account> selectedAccounts) {
        
        
        Map<Id, AccountDetailsWrapper> allAccountDetails = new Map<Id, AccountDetailsWrapper>();
        Map<Id, AccountDetailsWrapper> segmentEligibleAccounts = new Map<Id, AccountDetailsWrapper>();
        Map<Id, Set<Id>> corporateGroupMGList = new Map<Id, Set<Id>>();
        Map<Id, Double> netTransactionVolume = new Map<Id, Double>();
        Map<Id, Id> midCGRelation = new Map<Id, Id>();
        Set<Id> allMerchantGroups = new Set<Id>();
        Set<Id> allMGMids = new Set<Id>();        
        
        
        for(Account accounts : selectedAccounts) {
            allAccountDetails.put(accounts.Id, new AccountDetailsWrapper(accounts));
        }
        corporateGroupMGList = AccountServiceClass.getMerchantGroupofCorporateGroup(allAccountDetails.KeySet());
        
        
        for(Id cgId : corporateGroupMGList.keySet()) {
            allMerchantGroups.addAll(corporateGroupMGList.get(cgId));
        }
        
        
        //Processing MID collection for all Merchant Groups underneath the Corporate Groups
        if(!corporateGroupMGList.isEmpty()) {
            Map<Id, Set<Id>> merchantGroupMIDS = AccountServiceClass.getMearchantGroupMIDRecords(allMerchantGroups);
            
            for(Id cgId : corporateGroupMGList.keySet()) {
               for(Id mgOfCg : corporateGroupMGList.get(cgId)) {
                   if(merchantGroupMIDS.containsKey(mgOfCg)) {
                       for(Id mgId : merchantGroupMIDS.get(mgOfCg)) {
                           midCGRelation.put(mgId,cgId);
                           allMGMids.add(mgId);
                           
                       }
                   }
               }
            }
        }
       
        
        //This would take in account Merchant group underneath the corporate Group as well as tyro customers.
        netTransactionVolume = AccountServiceClass.getNetTransactionVolumeForCorporateGroup(allAccountDetails.keySet());
        
        
        for(Id accountId : netTransactionVolume.keySet()) {

            allAccountDetails.get(accountId).netTransactionVolume =  netTransactionVolume.get(accountId)*12;
            allAccountDetails.get(accountId).amexTransactionVolume =  netTransactionVolume.get(accountId)*12/AMEX_DIVISOR_OF_TXN_VOLUME;
            AccountDetailsWrapper updatedWrapper = setAmexSegment(allAccountDetails.get(accountId));
            if(updatedWrapper.amexSegmentElligibility) {
                segmentEligibleAccounts.put(accountId, updatedWrapper);
            }
            allAccountDetails.put(accountId, updatedWrapper);
        }
        
        
        for(Merchant_Id__c accountMid : [SELECT Id, Name, MCC_Description__c, MCC_Description__r.Name, MCC_Description__r.MCC_Code__c, Status__c,
                                                Amex_Enabled__c, Account__r.Corporate_Group__c
                                        FROM Merchant_Id__c 
                                        WHERE (Account__r.Corporate_Group__c IN :segmentEligibleAccounts.keySet() OR Id IN:allMGMids)
                                        AND Status__c = 'Active']) {
           
            if(!allMGMids.contains(accountMid.Id)) {
                if(!allAccountDetails.containsKey(accountMid.Account__r.Corporate_Group__c)) {
                    allAccountDetails.put(accountMid.Account__c, setAccountAndMidEligibility(segmentEligibleAccounts.get(accountMid.Account__r.Corporate_Group__c), accountMid));
                }else{
                    allAccountDetails.put(accountMid.Account__c, setAccountAndMidEligibility(allAccountDetails.get(accountMid.Account__r.Corporate_Group__c), accountMid));
                }
            }else{
                //This is for Mids who belong to Merchant Groups and have no direct relation to corporate groups
                allAccountDetails.put(midCGRelation.get(accountMid.Id), setAccountAndMidEligibility(allAccountDetails.get(midCGRelation.get(accountMid.Id)), accountMid));
            }
        }
        
        
        /*
        Getting Corporate Group's contact details.
        */
        for(Contact accountContact : [SELECT Id, Name, Email, Phone, AccountId 
                                        FROM Contact 
                                        WHERE AccountId IN:allAccountDetails.keyset()]) {
            allAccountDetails.get(accountContact.AccountId).accountContactDetails.add(accountContact);        
        }
            
        
        return allAccountDetails;
    }
    


    
    /*
    Set Segment and eligibility for Account Wrappers
    */
    private static AccountDetailsWrapper setAmexSegment (AccountDetailsWrapper allAccountDetails) {
        
        if(allAccountDetails.amexTransactionVolume > 0 && allAccountDetails.amexTransactionVolume < MICRO_SEGMENT_CEILING) {
            allAccountDetails.amexSegment = MICRO_SEGMENT;
            allAccountDetails.amexSegmentElligibility = true;
        }else if(allAccountDetails.amexTransactionVolume >= MICRO_SEGMENT_CEILING && allAccountDetails.amexTransactionVolume <= SMALL_SEGMENT_CEILING) {
            allAccountDetails.amexSegment = SMALL_SEGMENT;
            allAccountDetails.amexSegmentElligibility = true;
        }else {
            allAccountDetails.amexSegmentElligibility = false;
        }
        
        return allAccountDetails;
    }
    
    
    
    
    
    
    private static AccountDetailsWrapper setAccountAndMidEligibility(AccountDetailsWrapper allAccountDetails, Merchant_Id__c accountMid) {
        
       
        MCC_Description__c MidMcc = new MCC_Description__c(
                                                        Id=accountMid.MCC_Description__c, 
                                                        Name = accountMid.MCC_Description__r.Name,
                                                        MCC_Code__c = accountMid.MCC_Description__r.MCC_Code__c);
        
        MIDDetails newMidDetail = new MIDDetails(accountMid, MidMcc);
        newMidDetail.midAmexEligible = allMCCCodeMicroRates.containsKey(accountMid.MCC_Description__r.MCC_Code__c);
        
        if(newMidDetail.midAmexEligible) {
            if(allAccountDetails.amexSegment == SMALL_SEGMENT && 
                (allAccountDetails.amexRateCalculated == null || 
                allMCCCodeSmallRates.get(accountMid.MCC_Description__r.MCC_Code__c) >  allAccountDetails.amexRateCalculated)
             ) {
                 allAccountDetails.amexRateCalculated = allAccountDetails.AccountAmexRate == null ? 
                                                        allMCCCodeSmallRates.get(accountMid.MCC_Description__r.MCC_Code__c) : 
                                                        allAccountDetails.AccountAmexRate;
                 allAccountDetails.amexRateMCCCode = accountMid.MCC_Description__r.MCC_Code__c;
             }
            else if(allAccountDetails.amexSegment == MICRO_SEGMENT && 
                (allAccountDetails.amexRateCalculated == null || 
                allMCCCodeMicroRates.get(accountMid.MCC_Description__r.MCC_Code__c) >  allAccountDetails.amexRateCalculated)
             ) {
                 allAccountDetails.amexRateCalculated = allAccountDetails.AccountAmexRate == null ? 
                                                        allMCCCodeMicroRates.get(accountMid.MCC_Description__r.MCC_Code__c) : 
                                                        allAccountDetails.AccountAmexRate;
                 allAccountDetails.amexRateMCCCode = accountMid.MCC_Description__r.MCC_Code__c;
             }
        }
        
        
        allAccountDetails.accountMidDetails.add(newMidDetail); 
        return allAccountDetails;
        
    }
    
    
    
    
    
     /*
    Wrapper class for All account Details
    */
    public class AccountDetailsWrapper { 
        
        public Account searchedAccount;
        public Double accountAmexRate;
        public Double netTransactionVolume;
        public Double amexTransactionVolume;
        public String amexSegment;
        public Boolean amexSegmentElligibility;
        public Double amexRateCalculated;
        public String amexRateMCCCode;
        public List<Contact> accountContactDetails;
        public List<MIDDetails> accountMidDetails;
        
        
        public AccountDetailsWrapper(Account searchedAccount) {
            this.searchedAccount = searchedAccount;
            accountMidDetails = new List<MIDDetails>();
            accountContactDetails = new List<Contact>();
            if(searchedAccount.RecordTypeId == TYRO_CUSTOMER && searchedAccount.Corporate_Group__c != null) {
                accountAmexRate = searchedAccount.Corporate_Group__r.Amex_IEAP_Rate__c;
            }else{
                accountAmexRate = searchedAccount.Amex_IEAP_Rate__c;
            }
        }
        
        
    }
    
    /*
    Wrapper class for All Account's MID Details
    */
    public class MIDDetails {
        
        public String merchantId;
        public String midStatus;
        public String midMccName;
        public String midMccCode;
        public Boolean amexEnabled;
        public Boolean midAmexEligible;
        
        public MIDDetails (Merchant_Id__c mid, MCC_Description__c mcc) {
            this.merchantId = mid.Name;
            this.midStatus = mid.Status__c;
            this.amexEnabled = mid.Amex_Enabled__c;
            this.midMccName = mcc.Name;
            this.midMccCode = mcc.MCC_Code__c;
        }
    }
    
    
    /*
    Wrapper class for All MCC Details
    */
    public class MCCStaticValues {
        public String mccCode;
        public String amexRate;
    }

}