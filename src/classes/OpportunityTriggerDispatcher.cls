/********************************************************************
* Class Name    : OpportunityTriggerDispatcher
* Copyright     : Tyro Payments (c) 2018
* Description   : This is Dispatcher Class for Opportunity Trigger
* Created By    : Arvind Thakur
* 
* Do NOT write business logic in this class. Make a separate class for it.
* This is a Dispatcher class. As the name suggests, route logic to appropriate class
* 
* 
* Modification Log ==============================================
* Author                        Date                    JIRA-LINK
* Arvind Thakur (created)     28th June, 2018       Updating Team Role on Opportunity
* 
* 
********************************************************************/

public class OpportunityTriggerDispatcher extends TriggerHandler{
    
    private Map<Id, Opportunity> newOpportunityMap;
    private Map<Id, Opportunity> oldOpportunityMap;
    private List<Opportunity> newOpportunityList;
    private List<Opportunity> oldOpportunityList;
    
    private static final String FB_ECOMM_RECORDTYPEID;
    
    static {
        FB_ECOMM_RECORDTYPEID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('eCommerce Only').getRecordTypeId();
    }

    public OpportunityTriggerDispatcher() {

        newOpportunityMap = (Map<Id, Opportunity>)Trigger.newMap;
        oldOpportunityMap = (Map<Id, Opportunity>)Trigger.oldMap;
        newOpportunityList = (List<Opportunity>)Trigger.new;
        oldOpportunityList = (List<Opportunity>)Trigger.old;
        
    }
    
    
    public override void beforeInsert() {
        
        Map<Id, List<Opportunity>> populateTeamNameOnOpportunity = new Map<Id, List<Opportunity>>();
        
        for(Opportunity newOpportunity: newOpportunityList) {
            if(String.valueOf(newOpportunity.ownerId).startswith('005')) {
                if(!populateTeamNameOnOpportunity.containsKey(newOpportunity.ownerId)) {
                    populateTeamNameOnOpportunity.put(newOpportunity.ownerId, new List<Opportunity>{});
                }
                
                populateTeamNameOnOpportunity.get(newOpportunity.ownerId).add(newOpportunity);
            }
        }
        
        
        if(!populateTeamNameOnOpportunity.isEmpty()) {
            PopulateTeamLeadOpportunity.PopulateTeamOnOpportunity(populateTeamNameOnOpportunity);
            
        }
        
        
    }
    
    public override void beforeUpdate() {
        Map<Id, List<Opportunity>> populateTeamNameOnOpportunity = new Map<Id, List<Opportunity>>();
        Map<Id, Opportunity> oldFBECommMap = new Map<Id, Opportunity>();
        Map<Id, Opportunity> newFBECommMap = new Map<Id, Opportunity>();
        
        for(Opportunity oldOpportunity : oldOpportunityList) {
            
            //Owner Team Update Functionality
            if( (oldOpportunity.OwnerId != newOpportunityMap.get(oldOpportunity.Id).OwnerId && String.valueOf(newOpportunityMap.get(oldOpportunity.Id).OwnerId).startsWith('005')) 
                 || newOpportunityMap.get(oldOpportunity.Id).Owner_Changed_From_Account__c) {
                if(!populateTeamNameOnOpportunity.containsKey(newOpportunityMap.get(oldOpportunity.Id).OwnerId)) {
                    populateTeamNameOnOpportunity.put(newOpportunityMap.get(oldOpportunity.Id).OwnerId, new List<Opportunity>{});
                }
                
                populateTeamNameOnOpportunity.get(newOpportunityMap.get(oldOpportunity.Id).OwnerId).add(newOpportunityMap.get(oldOpportunity.Id));
            }
            
            //FrontBook eCommerce Record Type Check
            if(oldOpportunity.RecordTypeId == FB_ECOMM_RECORDTYPEID) {
                oldFBECommMap.put(oldOpportunity.Id, oldOpportunity);
                newFBECommMap.put(oldOpportunity.Id, newOpportunityMap.get(oldOpportunity.Id));
            }
        }
        
        
        if(!populateTeamNameOnOpportunity.isEmpty()) {
            PopulateTeamLeadOpportunity.PopulateTeamOnOpportunity(populateTeamNameOnOpportunity);
        }

        if(!newFBECommMap.isEmpty()) {
            //OpportunityDispatcherFBECommerce.UpdatedFrontBookECommOpportunity(oldFBECommMap, newFBECommMap);
        }
    }
    
}