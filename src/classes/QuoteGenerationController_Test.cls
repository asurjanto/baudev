@isTest
private class QuoteGenerationController_Test {
    
    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP;
    private static final String TYRO_CUSTOMER;
    private static final String OPPORTUNITY_ECOMMERCE;
    private static final String QUOTE_STANDARD;
    

    static {
        MERCHANT_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();
        OPPORTUNITY_ECOMMERCE = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('eCommerce Up-Sell').getRecordTypeId();
        QUOTE_STANDARD = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
    }
    
	private static testMethod void testQuoteGenerationComponent() {
	    
	    Test.starttest();
            Opportunity eCommOpp;
           
    
            for (Opportunity opp: [Select id, Name, AccountId from Opportunity where name = 'ECommOpportunity'
                    limit 1
                ]) {
                eCommOpp = opp;
            }
    
            eCommOpp.stageName = 'Application Received*';
            update eCommOpp;
            
            QuoteGenerationController.findAccountContacts(eCommOpp.AccountId);
            
            Quote newQuote = new Quote();
            newQuote.Opportunityid = eCommOpp.Id;
            newQuote.Status = 'Pending Approval';
            newQuote.Name = 'newEComm Quote';
            newQuote.Primary_Quote__c = true;
            insert newQuote;
            
            Quote newQuote1 = new Quote();
            newQuote1.Opportunityid = eCommOpp.Id;
            newQuote1.Status = 'Draft';
            newQuote1.Name = 'Draft newEComm Quote';
            newQuote1.RecordTypeId = QUOTE_STANDARD;
            newQuote1.eCommerce_Rate__c = 1.3;
            newQuote1.Monthly_Access_Fees__c = 30;
            
            insert newQuote1;
            
            QuoteGenerationController.uncheckPrimaryQuote(eCommOpp.Id);
            QuoteGenerationController.submitForApproval(newQuote1.Id); 
            
            
            
	    

	}
	
	@testSetup
	private static void createTestData() {
	    
	    Account tyroCustomer = TestUtils.createAccount('eCommTyroCustomer', false);
        tyroCustomer.RecordTypeId = TYRO_CUSTOMER;
        insert tyroCustomer;

        Contact tyroCustomerContact = TestUtils.createContact(tyroCustomer.Id, 'tyrocustomercontact', true);
        Channel_Agreement__c newCA = TestUtils.createChannelAgreement('testchannel', tyroCustomer.Id, '32424', true);
        Integration_Product__c newPOS = TestUtils.createNewPOS('myPOS', tyroCustomer.Id, 8, true);
        Merchant_Id__c newMid = TestUtils.createMerchantId(tyroCustomer.Id, false, null, 'Active', newPOS.Id, newCA.Id, tyroCustomerContact.Id, true);
        Location__c newLocation = TestUtils.createNewLocation(tyroCustomer.Id, 'testLocation', '155 Clearance street floor 1-5', 'Sydney', 'NSW', 'Australia', '2148', true);
        Mcc_Description__c newMcc = TestUtils.createNewMCCDescription('TestClassCC', true, 'Food and X', '3333', true);

        Opportunity newECommOpportunity = TestUtils.createOpportunity('ECommOpportunity', 'Eligible', System.Today(), false);
        newECommOpportunity.recordTypeId = OPPORTUNITY_ECOMMERCE;
        newECommOpportunity.eComm_Product_Type__c = 'Simplify';
        newECommOpportunity.eComm_Shopping_Cart__c = 'Shopify';
        newECommOpportunity.MID__c = newMid.Id;
        newECommOpportunity.AccountId = tyroCustomer.Id;
        newECommOpportunity.Channel_2__c = newCA.Id;
        newECommOpportunity.eComm_MCC__c = newMcc.Id;
        newECommOpportunity.eComm_Website_URL__c = 'Test.test.com.au';
        insert newECommOpportunity;
        
        
        
	}

}