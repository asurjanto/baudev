/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Forecast_DataTest
{
    private static testmethod void testTrigger()
    {
        // Force the dlrs_Forecast_DataTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Forecast_Data__c());
    }
}