/****************************************************************************************************
 * Name         : AmexEnablementComponentController
 * Description  : Build for Amex
 * Created By   : Arvind Thakur Jan'19
 * 
 * Methods
 * getParentAccountDetails  - Gets parent Account detail if any, when Account is searched
 * getAccountOfMID          - Gets parent account of Mid if any, when Merchant Id is searched for   
 * sendAmexInvitation       - Creates a Case and sends Amex invitation email to the selected contact
******************************************************************************************************/

public class AmexEnablementComponentController {
    
    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP;
    private static final String TYRO_CUSTOMER;
    
    private static final String CASE_AMEX_RECORDTYPE;
    private static final String CASE_OFFER_SENT_STATUS;
    private static final String CASE_CS_QUEUE_ID;
    

    
    private static Double AccountAmexRate;
    
    
    static {
        
        MERCHANT_GROUP  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();
        
        
        Amex_Configurations__c amexConfig = Amex_Configurations__c.getOrgDefaults();
        CASE_AMEX_RECORDTYPE        = amexConfig.Case_amex_record_type_Id__c;
        CASE_OFFER_SENT_STATUS      = amexConfig.Offer_Sent_Status_API_Name__c;
        CASE_CS_QUEUE_ID            = [SELECT Id, Name FROM Group WHERE Name = 'CS Requests' AND Type = 'QUEUE' Limit 1][0].Id;
    }
    
    
    
    
    /*
    When search is done for Account Object
    */
    @AuraEnabled
    public static String getParentAccountDetails(String selectedAccountId) {
        
        
        Account selectedAccount;
        String selectedAccountRecordTypeId;
        
        
        /*
        Amex is enabled at the level where there is ABN
        Amex cannot be done at Merchant Group level
        You cannot seach for accounts at merchant group level, only tyro customer underneath the merchant group
        Tyro customer and corporate groups have ABN at account level and not merchant groups
        */
        for(Account searchedAccount : [SELECT Id, Name, RecordTypeId, Corporate_Group__r.Name,  Corporate_Group__r.Amex_IEAP_Rate__c, Corporate_Group__c, Amex_IEAP_Rate__c 
                                        FROM Account 
                                        WHERE Id=:selectedAccountId 
                                        AND (RecordTypeId =:TYRO_CUSTOMER OR RecordTypeId =:CORPORATE_GROUP)]) {
            
            
            //In case the account has previously applied for AMEX, this would be the rate.
            if(searchedAccount.RecordTypeId == TYRO_CUSTOMER && searchedAccount.Corporate_Group__c != null) {
                selectedAccountRecordTypeId = CORPORATE_GROUP;
                selectedAccount = new Account(Id = searchedAccount.Corporate_Group__c, 
                                                Name = searchedAccount.Corporate_Group__r.Name,
                                                Amex_IEAP_Rate__c = searchedAccount.Corporate_Group__r.Amex_IEAP_Rate__c,
                                                RecordTypeId = CORPORATE_GROUP);
               
            }else {
                selectedAccountRecordTypeId = searchedAccount.RecordTypeId;
                selectedAccount = searchedAccount;
                AccountAmexRate = searchedAccount.Amex_IEAP_Rate__c;
            }
        }
         
         
         
        if(selectedAccount != null) {
            
            if(!String.isBlank(selectedAccountRecordTypeId) && selectedAccountRecordTypeId == TYRO_CUSTOMER) {
                //return tyroCustomerAmexEligibilityDetails(selectedAccount);
                return JSON.serialize(AmexServiceClass.tyroCustomerAmexEligibilityDetails(new List<Account> {selectedAccount}).get(selectedAccount.Id));
            }
                
            else if(!String.isBlank(selectedAccountRecordTypeId) && selectedAccountRecordTypeId == CORPORATE_GROUP) {
                return JSON.serialize(AmexServiceClass.corporateGroupsAmexEligibilityDetails(new List<Account> {selectedAccount}).get(selectedAccount.Id));
            }
        }
        
        
        return null;
    }
    
    
    
    
    
    
    /*
    When search is done for merchant id object
    */
    @AuraEnabled
    public static String getAccountOfMID(String selectedMid) {
        
        
        
        Account selectedAccount;
        String selectedAccountRecordTypeId;
        
        
        for(Merchant_Id__c selectedMerchantId : [SELECT Id, Name, Account__c, Account__r.Name, Account__r.RecordTypeId, 
                                                        Account__r.Corporate_Group__r.Name,  Account__r.Corporate_Group__c,Amex_Enabled__c,
                                                        Account__r.Corporate_Group__r.Amex_IEAP_Rate__c, Account__r.Amex_IEAP_Rate__c
                                                    FROM Merchant_Id__c 
                                                    WHERE Id=:selectedMid 
                                                    AND Account__r.RecordTypeId !=:MERCHANT_GROUP 
                                                    AND Status__c = 'Active']) {
            
            
            if(selectedMerchantId.Account__r.Corporate_Group__c != null && selectedMerchantId.Account__r.RecordTypeId == TYRO_CUSTOMER) {
                selectedAccount = new Account(Id = selectedMerchantId.Account__r.Corporate_Group__c, 
                                                Name=selectedMerchantId.Account__r.Corporate_Group__r.Name,
                                                RecordTypeId = CORPORATE_GROUP,
                                                Amex_IEAP_Rate__c = selectedMerchantId.Account__r.Corporate_Group__r.Amex_IEAP_Rate__c);
                selectedAccountRecordTypeId = CORPORATE_GROUP;
                
            }else if(selectedMerchantId.Account__r.RecordTypeId == TYRO_CUSTOMER) {
                selectedAccount = new Account(Id = selectedMerchantId.Account__c, Name=selectedMerchantId.Account__r.Name,
                                                RecordTypeId = TYRO_CUSTOMER,
                                                Amex_IEAP_Rate__c = selectedMerchantId.Account__r.Amex_IEAP_Rate__c);
                selectedAccountRecordTypeId = TYRO_CUSTOMER;
            }
        }
        
        
        
        
        if(selectedAccountRecordTypeId == null) {
            
            for(Merchant_Group_Member__c mgm: [SELECT Id, Account__c, Account__r.RecordTypeId, Account__r.Amex_IEAP_Rate__c
                                                FROM Merchant_Group_Member__c 
                                                WHERE Merchant_ID__c =:selectedMid 
                                                AND Merchant_Id__r.Status__c = 'Active']) {
                selectedAccount = new Account(Id = mgm.Account__c, 
                                                RecordTypeId= mgm.Account__r.RecordTypeId, 
                                                Amex_IEAP_Rate__c = mgm.Account__r.Amex_IEAP_Rate__c);
                selectedAccountRecordTypeId = mgm.Account__r.RecordTypeId;
            }
            
            if(selectedAccount != null) {
                if(selectedAccountRecordTypeId == TYRO_CUSTOMER) {
                    return JSON.serialize(AmexServiceClass.tyroCustomerAmexEligibilityDetails(new List<Account> {selectedAccount}).get(selectedAccount.Id));
                }else if(selectedAccountRecordTypeId == CORPORATE_GROUP) {
                    return JSON.serialize(AmexServiceClass.corporateGroupsAmexEligibilityDetails(new List<Account> {selectedAccount}).get(selectedAccount.Id));
                }
            }
            
        }else if(selectedAccountRecordTypeId == TYRO_CUSTOMER) {
            return JSON.serialize(AmexServiceClass.tyroCustomerAmexEligibilityDetails(new List<Account> {selectedAccount}).get(selectedAccount.Id));
        }else if(selectedAccountRecordTypeId == CORPORATE_GROUP) {
            return JSON.serialize(AmexServiceClass.corporateGroupsAmexEligibilityDetails(new List<Account> {selectedAccount}).get(selectedAccount.Id));
        }
        
        
        
        return null;
        
    }
    
    
    
    
    /*
    Sending confirmation Email to Selected Contact
    @selectedAccountId = Can be Corporate Group or Tyro Customer Record Type
    */
    @AuraEnabled
    public static String sendAmexInvitation(Id selectedAccountId, String accountName, String selectedContactJSON, String selectedMIDs, String amexRate, String amexTXNVolume) {
        
        
        Map<String, Object> cObjMap = (Map<String, Object>) JSON.deserializeUntyped(selectedContactJSON);
        String cObjJson = JSON.serialize(cObjMap.get('ContactData'));
        Map<String, Object> cObjMapFurious = (Map<String, Object>) JSON.deserializeUntyped(cObjJson);
        String cObjJsonDrunk = JSON.serialize(cObjMapFurious);
        
        Contact selectedContact = (Contact)JSON.deserialize(cObjJsonDrunk, Contact.class);
        
        list<String> myMids = (List<String>)JSON.deserialize(selectedMIDs, List<String>.class);
        
        String allMids = '';
        
        for(String mid : myMids) {
            allMids += mid + ',' + amexRate + ',' + Math.round(Double.valueOf(amexTXNVolume)) + '\n';
        }
        
        
        Case amexCase = new Case();
        for(Case previousCase : [SELECT Id, Subject, Status, AccountId, ContactId 
                                    FROM Case 
                                    WHERE AccountId =:selectedAccountId 
                                    AND RecordTypeId =:CASE_AMEX_RECORDTYPE
                                    AND isClosed = false]) {
            amexCase = previousCase;
        }
         
         
        /*
        Amex ticket is updated if
            - Account has previous ticket on Offer Sent status (Implies for customer loosing track of Email or want us to send email again or send it to a new contact)
        Amex ticket is created if 
            - No other OPEN amex ticket is on the account
            - No other ticket with Offer Sent Status is on the accout        
        */
        if(amexCase.id == null || amexCase.Status == CASE_OFFER_SENT_STATUS || amexCase.Status == 'Offer Accepted') {
            
            Savepoint beforeCaseCreation;
            //Boolean contactChanged = false;Commented as part of SFDC-365
            
            amexCase.RecordTypeId = CASE_AMEX_RECORDTYPE;
            amexCase.Status = CASE_OFFER_SENT_STATUS;
            amexCase.Subject = 'Enable Amex for ' + accountName;
            amexCase.AccountId = selectedAccountId;
            amexCase.contactId = selectedContact.Id;
            /*Commented as part of SFDC-365
            if(amexCase.contactId != selectedContact.Id) {
                amexCase.contactId = selectedContact.Id;
                contactChanged = true;
            }
            */
            
            amexCase.Amex_Transaction_Volume__c = Decimal.valueOf(amexTXNVolume);
            amexCase.Amex_IEAP_Rate__c = Decimal.valueOf(amexRate);
            amexCase.Description = allMids; 
            amexCase.OwnerId = CASE_CS_QUEUE_ID;
            try{
                
                beforeCaseCreation = Database.setSavepoint();
                upsert amexCase;
                //Send this when current contact is not equal to previous contact
                //if(contactChanged) {Commented as part of SFDC-365
                    AmexEmailServiceClass.sendAmexInvitationEmail (selectedAccountId, accountName, selectedContact, amexCase);
                //}
                return String.valueOf(amexCase.Id);
                
            }catch(exception ex) {
                
                //Handle Exceptions
                system.debug('Exception :: newAmexCase' + ex.getMessage());
                Database.rollback(beforeCaseCreation);
                return 'Expection while creating case and sending Email, please contact ADMIN ' + ex.getMessage();
                
                
            }
        }else if(amexCase.Status == 'Back office Updated') {
            return 'Amex offer already accepted and is in process. Please complete previous ticket, update all the MIDs to Amex Enabled and then proceed for new ticket creation';
        }
        
        return null;
        
    }
    
   
    
}