public with sharing class AmexFunctions {
///Jira Item: SF-504
///The code below is redundant and is therefore commented

/*	public static void completeAmexLoad(Account currentAccount, String emailAdd)
	{
		Blob tempLoad = createAmexLoad(currentAccount);
		emailAmexLoad(tempLoad, emailAdd);
	}
	
	public static Blob createAmexLoad(Account currentAccount)
	{
    	//Account currentAccount = [select id, site, Merchant_ID__c, ShippingStreet, ShippingState, ShippingPostalCode, ShippingCity, AMEX_Mer__c, Diners_Merchant_No__c, Phone from Account where id = '0012000000Sf7Ae' limit 1];
    	String columnHeaders = 'AMEX Merchant No.,Amex TID,SEName,Address,Address2,Address3,Address4,Suburb,State,Postcode,Contact,Phone No,Merchant number,TID,Credit Only,Amex Owned Terminal,Request to Load,Load JCB,Comments \n';
    	String AmexNo = currentAccount.AMEX_Mer__c.replace(',',' ');
    	String site = currentAccount.site.replace(',','-');
    	String ShippingStreet = currentAccount.ShippingStreet.replace(',','-');
    	String ShippingCity = currentAccount.ShippingCity.replace(',','-');
    	String ShippingState = currentAccount.ShippingState.replace(',','-');
    	String ShippingPostalCode = currentAccount.ShippingPostalCode.replace(',','-');
    	String phone = currentAccount.phone.replace(',','-');
    	String MerchantID = currentAccount.AccountNumber;
    	
    	String accountDetails = AmexNo + ',' + ',' + site + ',' + ShippingStreet + ',,,,' + ShippingCity + ',' + ShippingState + ',' + ShippingPostalCode + ',,' + phone + ',' + MerchantID + ',1';
    	String completeString = columnHeaders + accountDetails;
    	Blob newLoad = Blob.valueOf(completeString);
    	
    	return newLoad;
	}
	
	public static void emailAmexLoad(Blob amexLoadFile, String emailAdd)
	{
		String currentDate = Date.today().format();
		String bodyTemplate = 'I have attached an update for ' + currentDate +
		'\n\nRegards,\n' + UserInfo.getName()+ '\n\nCustomer Support \nToll free support number 1 300 966 639' +
		'\n \ntyro payments \n125 york street \nsydney nsw 2000 \nvisit www dot tyro dot com';
		
    	Messaging.EmailFileAttachment[] fileAttachments = new Messaging.EmailFileAttachment[1];    	
    	Messaging.EmailFileAttachment load = new Messaging.EmailFileAttachment();
    	
		load.setBody(amexLoadFile); 
		load.setFileName('Amex_Load.csv'); 
    	//add attachment object to attachments array
		fileAttachments[0]=load;
        String[] toAddresses = new String[] {emailAdd}; 
    	//String CSEmail = 'cs@tyro.com';
    	//toAddresses.add(CSEmail);
    	
    	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

   		mail.setPlainTextBody(bodyTemplate);
		mail.setSubject('MoneySwitch Amex load requests '+ currentDate + ' MNSW');
  		mail.setToAddresses(toAddresses);
  		mail.setBccSender(false);
  		mail.setUseSignature(false);
  		mail.setReplyTo('cs@tyro.com');
  		mail.setSenderDisplayName('Tyro Customer Support');
  		mail.setSaveAsActivity(false);  
		mail.setFileAttachments(fileAttachments);
 
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	
	static testMethod void AmexFunctions() 
	{
		Account newAccount = new Account(Name = 'Test AmexFunctions Account', AMEX_Mer__c = '123456789', Merchant_ID__c = 500, site = 'Test AmexFunctions Account', ShippingStreet = '123 Test St', ShippingCity = 'Test City', ShippingState = 'NSW', ShippingPostalCode = '2000', Phone = '89071700');
		insert newAccount;
		completeAmexLoad(newAccount, 'lzhang@tyro.com');

	}*/
}