/************************************************************************************************************
* Class Name    : AccountTriggerDispatcher
* Copyright     : Tyro Payments (c) 2018
* Description   : This is Dispatcher Class for Account Trigger
* Created By    : Arvind Thakur
* 
* Do NOT write business logic in this class. Make a separate class for it.
* This is a Dispatcher class. As the name suggests, route logic to appropriate class
* 
* 
* Modification Log ======================================================================================
* Author                        Date                    JIRA-LINK/Description
* Arvind Thakur (created)     2nd July, 2018        Updating Team Role on Acccount
* Arvind Thakur (modified)    24th Oct, 2018        Removing Account_Trigger and AccountTriggerHandler
*                                                   and adding their functionality in here.
*                                                   Updating owner of childern when a corporate group or merchant
*                                                   group account owner is changed.
*                                                
* 
************************************************************************************************************/

public class AccountTriggerDispatcher extends TriggerHandler {
    
    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP;
    private static final String TYRO_CUSTOMER;
    
    private static final Set<Id> excludedAccounts;
    
    static {
        MERCHANT_GROUP  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();
        
        excludedAccounts = new Set<Id>();
        for(Accounts_Excluded_From_Allocation_Rules__c allExcludedAccounts : Accounts_Excluded_From_Allocation_Rules__c.getAll().values()) {
            excludedAccounts.add(allExcludedAccounts.Excluded_Account_Id__c);
        }
    }
    
    //Private Variables
    private Map<Id, Account> newAccountMap;
    private Map<Id, Account> oldAccountMap;
    private List<Account> newAccountList;
    private List<Account> oldAccountList;
    
    //Constructor
    public AccountTriggerDispatcher() {
        
        newAccountMap = (Map<Id, Account>)Trigger.newMap;
        oldAccountMap = (Map<Id, Account>)Trigger.oldMap;
        newAccountList = (List<Account>)Trigger.new;
        oldAccountList = (List<Account>)Trigger.old;        
        
    }
    
    
    public override void beforeUpdate() {
        
        Set<String> accountRulesKeys = new Set<String>();
        Map<Id, Account> allCorporateGroups = new Map<Id, Account>();
        Map<Id, Account> allMerchantGroups = new Map<Id, Account>();
        Map<Id, Account> allTyroCustomer = new Map<Id, Account>();
        Map<Id, Account> allTyroCustomerClone = new Map<Id, Account>();
        
        Map<Id, Account> corporateGroupsRecalculation = new Map<Id, Account>();
        Map<Id, List<Account>> corporateGroupIdChildAccounts = new Map<Id, List<Account>>();
        
        for(Account newAccount : newAccountList) {
            
            
            /*
            Only ON change of Account Segment OR POS - POS & Segment would be changed by
            Merchant Id Trigger/ Merchant Group Member Trigger Or MassUpdateAccountSegmentAndPPOS Batch 
            after transaction histry record is uploaded
            */
            if(   (oldAccountMap.get(newAccount.Id).Segment__c != newAccount.Segment__c ||
                   oldAccountMap.get(newAccount.Id).Integration_Product__c != newAccount.Integration_Product__c) && 
                  !newAccount.Key_Account__c && !excludedAccounts.contains(newAccount.Id))  {
                
                
                
                if(newAccount.RecordTypeId == TYRO_CUSTOMER) {
                    allTyroCustomer.put(newAccount.Id, newAccount);
                    accountRulesKeys.add(newAccount.Segment__c + '_' + newAccount.Integration_Product__c);
                }else if(newAccount.RecordTypeId == MERCHANT_GROUP) {
                    allMerchantGroups.put(newAccount.Id, newAccount);
                    accountRulesKeys.add(newAccount.Segment__c + '_' + newAccount.Integration_Product__c);
                }else if(newAccount.RecordTypeId == CORPORATE_GROUP) {
                    allCorporateGroups.put(newAccount.Id, newAccount);
                    accountRulesKeys.add(newAccount.Segment__c + '_' + newAccount.Integration_Product__c);
                }
            }
            
            //Do not update Tyro customers if they are part of Merchant Groups
            //This case would be handled by After Update trigger.
            
            if(newAccount.Corporate_Group__c != oldAccountMap.get(newAccount.Id).Corporate_Group__c && newAccount.Corporate_Group__c != null) {
                if(!corporateGroupIdChildAccounts.containsKey(newAccount.Corporate_Group__c)) {
                    corporateGroupIdChildAccounts.put(newAccount.Corporate_Group__c, new List<Account>());
                }
                corporateGroupIdChildAccounts.get(newAccount.Corporate_Group__c).add(newAccount);
            }
        
            
        }
        
        allTyroCustomerClone.putAll(allTyroCustomer);
        for(Merchant_Group_Member__c merchantGroupTyroCustomer : [SELECT Id, Account__c 
                                                                        FROM Merchant_Group_Member__c 
                                                                        WHERE Account__c IN :allTyroCustomer.KeySet()]) {
            allTyroCustomerClone.remove(merchantGroupTyroCustomer.Account__c);
        }
        
        if(!accountRulesKeys.isEmpty()) {
            processAccountOwners(allCorporateGroups, allMerchantGroups, allTyroCustomerClone, accountRulesKeys);
        }
        
        if(!corporateGroupIdChildAccounts.isEmpty()) {
            
            updateChildAccountOwner(corporateGroupIdChildAccounts);
            
        }

    }

    
    //After Update Function
    public override void afterUpdate() {
        
        Map<Id, Account> accountToProcess = new Map<Id, Account>();
        Map<Id, Id> corporateGroupOwnerChange = new Map<Id, Id>();
        Map<Id, Id> merchantGroupOwnerChange = new Map<Id, Id>();
        
        Set<Id> cgForQantusAndTruRatingCalculation = new Set<Id>();
        
        Set<Id> corporateGroupsRecalculation = new Set<Id>();
        
        for(Id accountId : newAccountMap.keySet()) {
            if(newAccountMap.get(accountId).ownerId != oldAccountMap.get(accountId).ownerId) {
                
                //Updating Opportunity with Owner Team
                accountToProcess.put(accountId, newAccountMap.get(accountId));
                
                /*
                Updating childern of corporate and merchant groups with same owner
                This can happen manually Or from AccountAllocationRulesTriggerDispatcher, when someone
                Changes allocation Rules.
                */
                if(newAccountMap.get(accountId).RecordTypeId == MERCHANT_GROUP) {
                    
                    merchantGroupOwnerChange.put(accountId, newAccountMap.get(accountId).ownerId);
                    
                }
                
                if(newAccountMap.get(accountId).RecordTypeId == CORPORATE_GROUP) {
                    
                    corporateGroupOwnerChange.put(accountId, newAccountMap.get(accountId).ownerId);
                    
                }
            }
            
            //Change of Corporate Groups.
            //Recalculate Segment and POS for CG Account
            if(newAccountMap.get(accountId).Corporate_Group__c != oldAccountmap.get(accountId).Corporate_Group__c) {
                corporateGroupsRecalculation.add(newAccountMap.get(accountId).Corporate_Group__c);
                corporateGroupsRecalculation.add(oldAccountMap.get(accountId).Corporate_Group__c);
                
            }
            
            
            //New Functionality To Add Count Rollup of No. of Mids with TruRating and Qantas to Corporate Group Account Level
            //All Tyro Customer with Corporate Group will be elligible because only TC have MIDs associate to them
            //Check to see if Mids with Turrating or Qantus is changed
            if(newAccountMap.get(accountId).RecordTypeId == TYRO_CUSTOMER) {
                
                //IF the Mids quanuts or trurating is changed, then we'll have to do the rollup to Corporate Group Level
                if(newAccountMap.get(accountId).Corporate_Group__c != null &&
                   (newAccountMap.get(accountId).MIDs_with_TruRating__c != oldAccountMap.get(accountId).MIDs_with_TruRating__c || 
                   newAccountMap.get(accountId).MIDs_with_Qantas_Earn__c != oldAccountMap.get(accountId).MIDs_with_Qantas_Earn__c)) {
                       
                       cgForQantusAndTruRatingCalculation.add(newAccountMap.get(accountId).Corporate_Group__c);
                       
                }
               
                //Accounting for Change of Corporate group, Will send both the corporate Groups to do the rerollup calculation
                if(newAccountMap.get(accountId).Corporate_Group__c != oldAccountMap.get(accountId).Corporate_Group__c) {
                       
                       if(newAccountMap.get(accountId).Corporate_Group__c != null) {
                           cgForQantusAndTruRatingCalculation.add(newAccountMap.get(accountId).Corporate_Group__c);
                       }
                       
                       if(oldAccountMap.get(accountId).Corporate_Group__c != null) {
                           cgForQantusAndTruRatingCalculation.add(oldAccountMap.get(accountId).Corporate_Group__c);
                       }
                       
                }
            }
            
            
            
        }
        
        updateOpportunityFlag(accountToProcess);
        
        //Updating merchant group childern with same ownerid
        if(!merchantGroupOwnerChange.isEmpty()) {
            UpdateAccountOwner.updateChildAccountOfMerchantGroup(merchantGroupOwnerChange);
        }
        
        //updating corporate group childern with same ownerid
        if(!corporateGroupOwnerChange.isEmpty()) {
            UpdateAccountOwner.updateChildAccountOfCorporateGroup(corporateGroupOwnerChange);
        }
        
        if(!corporateGroupsRecalculation.isEmpty()) {
            if(corporateGroupsRecalculation.contains(null)) {
                corporateGroupsRecalculation.remove(null);
            }
            
            changeSegmentAndPOSforCG(corporateGroupsRecalculation);
        }
        
        if(!cgForQantusAndTruRatingCalculation.isEmpty()) {
            LookupRollupEngine.Context ctx = new LookupRollupEngine.Context(Account.SobjectType, // parent object
                                            Account.SobjectType,  // child object
                                            Schema.SObjectType.Account.fields.Corporate_Group__c // relationship field name
                                            ); 
            ctx.add(
            new LookupRollupEngine.RollupSummaryField(
                                            Schema.SObjectType.Account.fields.Total_Mids_with_TruRating__c,
                                            Schema.SObjectType.Account.fields.MIDs_with_TruRating__c,
                                            LookupRollupEngine.RollupOperation.Sum
                                         ));
            ctx.add(
            new LookupRollupEngine.RollupSummaryField(
                                            Schema.SObjectType.Account.fields.Total_Mids_with_Qantas_Earn__c,
                                            Schema.SObjectType.Account.fields.MIDs_with_Qantas_Earn__c,
                                            LookupRollupEngine.RollupOperation.Sum
                                         ));
                                         
            Sobject[] masters = LookupRollupEngine.rollUp(ctx, cgForQantusAndTruRatingCalculation);   
            Database.update(masters, false);
        }
        
    }
    
    
    private static void updateChildAccountOwner(Map<Id, List<Account>> corporateGroupChildAccounts) {
        
        for(Account cgAccount : [SELECT Id, OwnerId FROM Account WHERE Id IN:corporateGroupChildAccounts.KeySet()]) {
            for(Account childAccounts : corporateGroupChildAccounts.get(cgAccount.Id)) {
                if(childAccounts.ownerId != cgAccount.OwnerId) {
                    childAccounts.OwnerId = cgAccount.OwnerId;
                }
            }
        }
    }
    
    private static void processAccountOwners (Map<Id, Account> allCorporateGroups, 
                                                Map<Id, Account> allMerchantGroups, 
                                                Map<Id, Account> allTyroCustomer,
                                                Set<String> accountAllocationRulesKey) {
        
        
        Map<String, Id> allocationRulesOwnerMap = new Map<String, Id>();
        
        for(Account_Allocation_Rules__c availableRules : [SELECT Id, Account_Manager__c, Rule_Allocation_Name__c 
                                                            FROM Account_Allocation_Rules__c 
                                                            WHERE Rule_Allocation_Name__c IN:accountAllocationRulesKey
                                                            AND Active__c = true 
                                                            AND Account_Manager__r.IsActive = true]) {
            allocationRulesOwnerMap.put(availableRules.Rule_Allocation_Name__c, availableRules.Account_Manager__c);
        }
        
        List<Account> updatedAccounts = AccountServiceClass.specificAccountsUpdatesOnly(allCorporateGroups, allMerchantGroups, 
                                                                                        allTyroCustomer, excludedAccounts, allocationRulesOwnerMap);
                                                                                        
    }
    
    private static void changeSegmentAndPOSforCG (Set<Id> cgAccountsToReprocess) {
        Map<Id, Double> AccountTxnMap;
        Map<Id, Id> accountPrimaryPOSMap = new Map<Id, String>();
        List<Account> accountToUdpate = new List<Account>();
        
        AccountTxnMap = AccountServiceClass.getNetTransactionVolumeForCorporateGroup(cgAccountsToReprocess);
        accountPrimaryPOSMap = AccountServiceClass.getCorporateGroupPrimaryPOS(cgAccountsToReprocess);
        
        Account newAccount;
        for(Id accountId : cgAccountsToReprocess) {
            newAccount = new Account(Id=accountId);
            if(AccountTxnMap.containsKey(accountId)) {
                newAccount.Segment__c = AccountServiceClass.findAccountSegment(AccountTxnMap.get(accountId));
            }
            if(accountPrimaryPOSMap.containsKey(accountId)) {
                newAccount.Integration_Product__c = accountPrimaryPOSMap.get(accountId);
            }
            
            accountToUdpate.add(newAccount);
            
        }
        
        
        if(!accountToUdpate.isEmpty()) {
            try{
                
                update accountToUdpate;
            }catch(exception ex) {
                system.debug('Exception while updating accounts'  + ex.getMessage());
            }
           
        }
        
    }
    
    //This fires the opportunity trigger which does not happen in case of standard account owership transfers
    private static void updateOpportunityFlag(Map<Id, Account> accountToProcess) {
        
        List<Opportunity> oppList = new List<Opportunity>();
        for(Opportunity oppstoUdpate : [SELECT Id, Account.OwnerId FROM Opportunity 
                                            WHERE accountid = :accountToProcess.keyset() 
                                            AND isClosed = false]) {
            oppstoUdpate.Owner_Changed_From_Account__c = true;
            oppList.add(oppstoUdpate);
        }
        
        if(!oppList.isEmpty()) {
            update oppList;
        }
        
        
    }
    
    
}