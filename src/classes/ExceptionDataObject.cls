global class ExceptionDataObject {
        
    public Exception currentException;
    public Integer exceptionLineNumber;
    public String exceptionMessage;
    public String exceptionStackTrace;
    public String exceptionType;
    public String classOrProcessName;
    public String methodOrSubProcessName;
    public String jsonPayload;
    
    public ExceptionDataObject(Exception ex) {
        this.currentException = ex;
        this.exceptionLineNumber = ex.getLineNumber();
        this.exceptionMessage = ex.getMessage();
        this.exceptionStackTrace = ex.getStackTraceString();
        this.exceptionType = ex.getTypeName();
    }
    
    public ExceptionDataObject() {
    }
    
    public ExceptionDataObject setJsonPayload(String jsonPayload) {
        this.jsonPayload = jsonPayload;
        return this;
    }
    
    public ExceptionDataObject setLineNumber(Integer exceptionLineNumber) {
        this.exceptionLineNumber = exceptionLineNumber;
        return this;
    }
    
    public ExceptionDataObject setMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
        return this;
    }
    
    public ExceptionDataObject setExcecptionStackTrace(String exceptionStackTrace) {
        this.exceptionStackTrace = exceptionStackTrace;
        return this;
    }
    
    public ExceptionDataObject setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
        return this;
    }
    
    public ExceptionDataObject setClassorProcessName(String classOrProcessName) {
        this.classOrProcessName = classOrProcessName;
        return this;
    }
    
    public ExceptionDataObject setMethodOrSubProcessName(String methodOrSubProcessName) {
        this.methodOrSubProcessName = methodOrSubProcessName;
        return this;
    }
    
}