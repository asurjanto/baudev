global class MassUpdateAccountSegmentAndPPOS implements Database.Batchable<sobject>, Database.Stateful  {
    
    public String recordTypeToProcess;
    public String recordTypeId;
    public Set<Id> alreadyUpdatedAccounts = new Set<Id>();
    public Set<Id> accountIdsTobeUpdated = new Set<Id>();
    public Boolean singleRecordRun;
    public Id singleAccountId;
    
    global MassUpdateAccountSegmentAndPPOS(Id AccountId, String x) {
        singleRecordRun = true;
        singleAccountId = AccountId;
    }
    
    global MassUpdateAccountSegmentAndPPOS(String recordTypeName) {
        singleRecordRun = false;
        this.recordTypeToProcess = recordTypeName;
        this.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        if(!singleRecordRun) {
            String Query = 'SELECT Id, Integration_Product__c, Segment__c, RecordType.Name FROM Account ';
            Query       += ' WHERE RecordType.Id =:recordTypeId ';
            return Database.getQueryLocator(Query);
        }else{
            
            String Query = 'SELECT Id, Integration_Product__c, Segment__c, RecordType.Name FROM Account ';
            Query       += ' WHERE Id =:singleAccountId ';
            
            
            return  Database.getQueryLocator(Query);
            
        }
        
        
    }
    
    global void execute(Database.BatchableContext bc,List<Account> accountsToBeUpdated) {
       
        if(singleRecordRun) {
            recordTypeToProcess = accountsToBeUpdated[0].RecordType.Name;
        }
        
        if(recordTypeToProcess == 'Tyro Customer') {
            List<Account> updatedAccounts = new List<Account>();
            Set<Id> allMids = new Set<Id>();
            Set<Id> accountId = new Set<Id>();
            for(Account acc : accountsToBeUpdated) {
                accountId.add(acc.Id);
            }
            
            Map<Id, Set<Id>> accountMids = AccountServiceClass.getAccountMerchantIds(accountId);
            
            for(Set<Id> Mids : accountMids.values()) {
                allMids.addAll(Mids);
            }
            
            
            Map<Id, Double> accountTxnMap = AccountServiceClass.getNetTransactionVolumeForTyroCustomers(accountMids, allMids);
            Map<Id, Id> accountPrimaryPOS = AccountServiceClass.getTyroCustomerPrimaryPOS(accountId);
            
            for(Id accId : accountMids.keySet()) {
                
                Account acc = new Account(Id=accId);
                if(accountTxnMap.containskey(accId)) {
                    acc.Segment__c = AccountServiceClass.findAccountSegment(accountTxnMap.get(accId));
                }
                if(accountPrimaryPOS.containskey(accId)) {
                    acc.Integration_Product__c = accountPrimaryPOS.get(accId);
                }
                updatedAccounts.add(acc);
            }
            
            update updatedAccounts;
            
        }else {
            List<Account> accountsToUpdate = new List<Account>();
            Set<Id> merchantCoporateAccountIds = new Set<Id>();
            for(Account acc : accountsToBeUpdated) {
                merchantCoporateAccountIds.add(acc.Id);
            }
            
            Account newAccount;
            Map<Id, Double> AccountTxnMap;
            Map<Id, Id> accountPrimaryPOSMap = new Map<Id, String>();
            if(recordTypeToProcess == 'Corporate Group') {
                AccountTxnMap = AccountServiceClass.getNetTransactionVolumeForCorporateGroup(merchantCoporateAccountIds);
                accountPrimaryPOSMap = AccountServiceClass.getCorporateGroupPrimaryPOS(merchantCoporateAccountIds);
            }else{
                AccountTxnMap = AccountServiceClass.getNetTransactionVolumeForMerchantGroup(merchantCoporateAccountIds);
                accountPrimaryPOSMap = AccountServiceClass.getMerchantGroupPrimaryPOS(merchantCoporateAccountIds); 
            }
            
            for(Id accountId : merchantCoporateAccountIds) {
                newAccount = new Account(Id=accountId);
                if(AccountTxnMap.containsKey(accountId)) {
                    newAccount.Segment__c = AccountServiceClass.findAccountSegment(AccountTxnMap.get(accountId));
                }
                if(accountPrimaryPOSMap.containsKey(accountId)) {
                    newAccount.Integration_Product__c = accountPrimaryPOSMap.get(accountId);
                }
                accountsToUpdate.add(newAccount);
            }
            
            update accountsToUpdate;
            
        }
        
    }
    
    global void finish(Database.BatchableContext bc) {
        if(!singleRecordRun) {
            if(recordTypeToProcess == 'Tyro Customer') {
                Database.executeBatch(new MassUpdateAccountSegmentAndPPOS('Merchant Group'));
            }else if(recordTypeToProcess == 'Merchant Group') {
                Database.executeBatch(new MassUpdateAccountSegmentAndPPOS('Corporate Group'));
            }
        }
        
            
    }
}