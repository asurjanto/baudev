/****************************************************************
* Class Name    : PopulateOwnerTeamBatch_Test
* Copyright     : Tyro Payments (c) 2018
* Description   : Test Class for PopulateOwnerTeamBatch
* Created By    : Arvind Thakur
* 
* Modification Log ==============================================
* Author                        Date                    JIRA-LINK
* Arvind Thakur (created)     28th June, 2018        User Role Update Process
* 
* 
****************************************************************/

@isTest
private class PopulateOwnerTeamBatch_Test {

	private static testMethod void testPopulateTeamOwnerBatch() {
	    
	    UserRole newRole = new UserRole (Name = 'Test Class Role');
	    insert newRole;
        Profile adminProfile = [Select Id from profile where Name = 'System Administrator']; 
        
        User testUser = new User( firstname = 'ABC',
                                    lastName = 'XYZ', 
                                    email = 'tstorer123@rtest.com', 
                                    Username = 'tstorer123@rtest.com.dev',
                                    EmailEncodingKey = 'ISO-8859-1', 
                                    Alias = 'testeet',
                                    isActive = true,
                                    TimeZoneSidKey = 'America/Los_Angeles', 
                                    LocaleSidKey = 'en_US', 
                                    LanguageLocaleKey = 'en_US', 
                                    ProfileId = adminProfile.Id, 
                                    UserRoleId = newRole.Id);
                                    
        system.runAs(testUser) {
            Id leadRTID = TestUtils.getrecordTypeId(Schema.SobjectType.Lead, 'Merchant Lead');
            Lead createdLead = Testutils.createLead('Test', 'LastLead', 'phone', 'Partners', '', leadRTID, true);
            Opportunity createOpportunity = Testutils.createOpportunity('Test', 'Open' , system.today(), true);
            
        }
        
        Database.executeBatch(new PopulateOwnerTeamBatch('Lead'));
        Database.executeBatch(new PopulateOwnerTeamBatch('Opportunity'));

	}

}