global class PracsoftCampaignEmailHandler implements Messaging.InboundEmailHandler
{
    ///****************************************************************
    ///Jira Item: SF-507
    ///The code below is redundant and therefore commented
    ///****************************************************************
	
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope)
	{
        return null;
		/*Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
		String myErr;
		
		try
		{
			String theBody;
			List<String> fieldList = new List<String>();
			if(email.plainTextBody != null)
			{
				myErr = 'plainTextBody=' + email.plainTextBody;
				theBody = email.plainTextBody;
			}
			else
			{
				myErr = 'htmlBody=' + email.htmlBody;
			}
			
			theBody = theBody.substring(theBody.indexOf('Name'));
			fieldList = theBody.split('\n',0);
			
			myErr += '\ntheBody = ' +theBody;
			myErr += '\nfieldList = ' + fieldList;
			Map<String,String> fieldMap = new Map<String,String>();
			for( String field : fieldList)
			{
				if(field.split(':',0).size() == 2)
				{
					String FieldName = field.split(':',0)[0];
					String FieldValue = field.split(':',0)[1];
					FieldName = FieldName.trim();
					FieldValue = FieldValue.trim();
					fieldMap.put(FieldName, FieldValue);
				}
			}
			
			//EmailUsers.emailAdmin('test split', fieldList[0]);
			//EmailUsers.emailAdmin('test split', fieldMap.get('Company'));
			List<Account> existingMatchingAccounts = new List<Account>();
			Account newAccountEntry = new Account();
			String phoneInHudFormat = FormatPhoneForHUD.FormatPhoneForHUD(fieldMap.get('Phone'));
			try
			{	
				existingMatchingAccounts = [select id, Name, Site, Type, Merchant_Admin_Email__c, HUD_Phone_No__c, Phone, Owner.Name from Account where Name = :fieldMap.get('Company') OR Site =:fieldMap.get('Company') OR HUD_Phone_No__c = :phoneInHudFormat OR Merchant_Admin_Email__c = :fieldMap.get('Email')];
				if(existingMatchingAccounts.size() == 0)
				{
					
					newAccountEntry.Name = fieldMap.get('Company');

					newAccountEntry.Site = fieldMap.get('Company');

					newAccountEntry.Merchant_Admin_Email__c = fieldMap.get('Email');

					newAccountEntry.Phone = fieldMap.get('Phone');		
					
					newAccountEntry.Channel__c = 'HCN - Health Communications Network';
					
					newAccountEntry.Industry = 'Health Industry - Primary';
					
					newAccountEntry.Sub_Industry__c = 'General Practice';
					
					newAccountEntry.Sign_up_Promo__c = 'PS Campaign 2010';
					
					newAccountEntry.Easyclaim_Installation_Status__c = 'Registered';
					
					newAccountEntry.Type = 'Merchant';
					
					newAccountEntry.Sales_Merchant_Status__c = 'Prospect';
					
					try
					{
						insert newAccountEntry;
						String newAccountLog = 'A new submission has been made from the Medicare 2010 promotion website.\n\nThe following account has been added to salesforce as no match was found:\n\n'+ theBody +'\n\nDirect Link: https://emea.salesforce.com/'+newAccountEntry.id;
						
						EmailUsers.emailSingleUser(email.fromAddress, 'A new submission has been made from the Medicare 2010 promotion website [' + newAccountEntry.Name+']', newAccountLog);
					}
					catch(DMLException e)
					{
						String errorMsg = ' There was a problem with INSERTING this email using the PracsoftCampaignEmailHandler class.\n\n';
						EmailUsers.emailAdmin(errorMsg, myErr);
						result.success = FALSE;
						return result;
					}								
				}
			}
			catch (QueryException qe)
			{
						String errorMsg = ' There was a problem with a QueryException using the PracsoftCampaignEmailHandler class.\n\n';
						EmailUsers.emailAdmin(errorMsg, myErr);
						result.success = FALSE;
						return result;
			}
			if(existingMatchingAccounts.size()> 0 )
			{
				//List<String> listOfChangesMade = new List<String>();
				String changeLog = '';
				for (Account a : existingMatchingAccounts)
				{
					String matchingAccountDetails = '**************************************\n\nFound existing account ('+a.Name + ')with similar details to new account submitted. (link: https://emea.salesforce.com/'+a.id +') Account Owner: '+ a.Owner.Name +'\n\n\n';
					String changesMade = '-----  The following changes were made: -----\n\n';
					String changesNotMade = '----- The following changes were NOT made: -----\n\n';
					String matchingFields = '----- The following fields match: -----\n\n';
					
					if(a.Name == fieldMap.get('Company'))
						matchingFields += 'Matching Company names\n';
					if(a.Site == fieldMap.get('Company'))
						matchingFields += 'Matching Trading names\n';
					if(a.Merchant_Admin_Email__c == fieldMap.get('Email'))
						matchingFields += 'Matching Admin Emails\n';
					if(a.HUD_Phone_No__c == phoneInHudFormat)
						matchingFields += 'Matching phone numbers\n';
	
						
					if(a.Name == null)
					{
						a.Name = fieldMap.get('Company');
						changesMade += 'Company Name updated\n';
					}
					else
					{
						changesNotMade += 'Company Name NOT updated - Value submitted in email: ' + fieldMap.get('Company')+'\n';
					}
					
					if(a.Site == null)
					{
						a.Site = fieldMap.get('Company');
						changesMade += 'Trading Name updated\n';
					}
					else
					{
						changesNotMade += 'Trading Name NOT updated - Value submitted in email: ' + fieldMap.get('Company')+'\n';
					}
										
					if(a.Merchant_Admin_Email__c == null)
					{
						a.Merchant_Admin_Email__c = fieldMap.get('Email');
						changesMade += 'Merchant Admin Email updated\n';
					}
					else
					{
						changesNotMade += 'Merchant Admin Email NOT updated - Value submitted in email: ' + fieldMap.get('Email')+'\n';
					}
											
					if(a.Phone == null)
					{
						a.Phone = fieldMap.get('Phone');
						changesMade += 'Phone updated\n';
					}
					else
					{
						changesNotMade += 'Phone NOT updated - Value submitted in email: ' + fieldMap.get('Phone')+'\n';
					}
					
					changeLog += matchingAccountDetails + matchingFields + '\n' + changesMade + '\n'+ changesNotMade +'\n';
					//listOfChangesMade.add(changesMade);
				}
				try
				{
					update existingMatchingAccounts;
					String completeEmailBody = 'A new submission has been made from the Medicare 2010 promotion website with similar details to accounts existing in salesforce. Please find below a copy of the details followed by a report of the existing accounts inside salesforce with matching details.\n\n' + theBody +'\n' + changeLog;
					EmailUsers.emailSingleUser(email.fromAddress, 'A new submission has been made from the Medicare 2010 promotion website [' + fieldMap.get('Company')+']', completeEmailBody);
				}
				catch(DMLException e)
				{
					String errorMsg = ' There was a problem with UPDATING this email using the PracsoftCampaignEmailHandler class.';
					EmailUsers.emailAdmin(errorMsg, myErr);
					result.success = FALSE;
					return result;
				}
			}					
		}
		catch(Exception e)
		{
			String errorMsg = ' There was a problem with PracsoftCampaignEmailHandler class.';			
			String errrBody= '\n\nException Type: '+e.getTypeName() + '\nCause: ' + e.getCause() + '\nMessage: ' + e.getMessage() +'\n\n'+myErr; 
			EmailUsers.emailAdmin(errorMsg, errrBody);
			result.success = FALSE;
			return result;
		}
		return result;*/
	}
}