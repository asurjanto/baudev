@isTest
private class Flow_GetSchemeAndEftposRates_Test {

    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP;
    private static final String TYRO_CUSTOMER;
    
    static {
        MERCHANT_GROUP  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();
    }
    
	private static testMethod void testInvocableClass() {
       
        Account tyroCustomer = TestUtils.createAccount('TyroCustomer', false);
	    tyroCustomer.RecordTypeId = TYRO_CUSTOMER;
	    //tyroCustomer.Corporate_Group__c = corporateGroupAccount.Id;
	    insert tyroCustomer;
	    
	    Terms_Of_Trade_Name__c newtotname = new Terms_Of_Trade_Name__c();
	    newtotName.Name = '12345';
	    newtotName.Rate_Id__c = '12345';
	    insert newtotName;
	    
	    Terms_of_Trade_2__c newTOTrate = new Terms_of_Trade_2__c();
	    newTOTrate.Terms_Of_Trade_Name__c = newtotName.Id;
	    newTOTrate.Product__c = '01tD0000002Nkez';
	    insert newTOTrate;
	    
	    
	    Opportunity newOpp = new Opportunity();
	    newOpp.AccountId = tyroCustomer.Id;
	    newOpp.CloseDate = system.today();
	    newOpp.recordtypeid = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Rate Review').getRecordTypeId();
	    newOpp.StageName = 'Proposal';
	    newOpp.Name = 'test Opp1';
	    insert newOpp;
	    
	    Quote newQuote = new Quote();
	    newQuote.recordTypeid = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Rate Review - Cost Plus').getRecordTypeId();
	    newQuote.status = 'Draft';
	    newQuote.Description = 'test';
	    newQuote.name = 'testQuote';
	    newQuote.OpportunityId = newOpp.Id;
	    newQuote.Pricebook2id = '01sD0000000kixMIAQ';
	    //newQuote.Proposed_Cost_Plus_Rate_2__c = newtotName.id;
	    //newQuote.Proposed_EFTPOS_Rate_2__c = newtotName.id;
	    //newQuote.PProposed_Scheme_Rate_2__c = newtotName.id;
	    newQuote.Current_Cost_Plus_Rate_2__c = newtotName.id;
	    //newQuote.Current_EFTPOS_Rate_2__c = newtotName.id;
	    //newQuote.Current_Scheme_Rate_2__c = newtotName.id;
	    insert newQuote;
	    
	    QuoteLineItem newQli = new QuoteLineItem();
	    newQli.QuoteId = newQuote.Id;
	    newQli.Product2id = '01tD0000002Nkez';
	    newQli.Terms_Of_Trade__c = newtotName.id;
	    newQli.PricebookEntryId = '01uD000000iOctbIAC';
	    newQli.quantity = 1;
	    newQli.UnitPrice = 0;
	    insert newQli;
	    
	    Quote newQuote1 = new Quote();
	    newQuote1.recordTypeid = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Normalised').getRecordTypeId();
	    newQuote1.status = 'Draft';
	    newQuote1.Description = 'test12';
	    newQuote1.name = 'testQuote12';
	    newQuote1.OpportunityId = newOpp.Id;
	    //newQuote1.Proposed_Cost_Plus_Rate_2__c = newtotName.id;
	    //newQuote1.Proposed_EFTPOS_Rate_2__c = newtotName.id;
	    //newQuote1.PProposed_Scheme_Rate_2__c = newtotName.id;
	    //newQuote1.Current_Cost_Plus_Rate_2__c = newtotName.id;
	    newQuote1.Current_EFTPOS_Rate_2__c = newtotName.id;
	    newQuote1.Current_Scheme_Rate_2__c = newtotName.id;
	    insert newQuote1;
	    
	    QuoteLineItem newQli1 = new QuoteLineItem();
	    newQli1.QuoteId = newQuote1.Id;
	    newQli1.Product2id = '01tD0000002Nkez';
	    newQli1.Terms_Of_Trade__c = newtotName.id;
	    newQli1.PricebookEntryId = '01uD000000iOctbIAC';
	    newQli1.quantity = 1;
	    newQli1.UnitPrice = 0;
	    insert newQli1;
	    
	    Flow_GetSchemeAndEftposRates.QuoteData classIns = new Flow_GetSchemeAndEftposRates.QuoteData();
	    classIns.quoteId = newQuote.id;
	    classIns.quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Rate Review - Cost Plus').getRecordTypeId();
	    
	    Flow_GetSchemeAndEftposRates.QuoteData classIns1 = new Flow_GetSchemeAndEftposRates.QuoteData();
	    classIns1.quoteId = newQuote1.id;
	    classIns1.quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Normalised').getRecordTypeId();
	    
	    Flow_GetSchemeAndEftposRates.setRatesInQuoteLineItems(new List<Flow_GetSchemeAndEftposRates.QuoteData> {classIns});
	    Flow_GetSchemeAndEftposRates.setRatesInQuoteLineItems(new List<Flow_GetSchemeAndEftposRates.QuoteData> {classIns1});
	    
        
        
	}
	
	@TestSetup
	private static void createTestData() {
	    
	    
	    
	}

}