/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Merchant_IdTest
{
    private static testmethod void testTrigger()
    {
        // Force the dlrs_Merchant_IdTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Merchant_Id__c());
    }
}