public with sharing class CaseCreateFlowExtension {
    
    public final Flow.Interview.Case_Create_Case_With_MID_And_Location_2 createCase {get; set;}
    public CaseCreateFlowExtension(ApexPages.StandardController stdController) {}
    public String getFinishLoc() { 
         return createCase==null? 'home/home.jsp': createCase.VAR_Case_Id; 
    }
     Public PageReference getFinishPageRef(){
//      PageReference pageRef = new PageReference('/' + getfinishLoc());
//      PageReference pageRef = new PageReference('/' + getfinishLoc() + 'e?');
        PageReference pageRef = new PageReference('/' + getfinishLoc() + '/e?retURL=%2F' + getfinishLoc());

        pageRef.setRedirect(true);
        return pageRef;
    }
}