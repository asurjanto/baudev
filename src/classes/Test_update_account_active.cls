@isTest
private class Test_update_account_active {

    static testMethod void myUnitTest() 
    {
        Account newAccount = new Account(Name = 'Test Account', Merchant_Status__c = 'Prospect');
		Account newAccount2 = new Account(Name = 'Test Account2', Merchant_Status__c = 'Boarding Completed');
		insert newAccount;
		insert newAccount2;
        
        Product2 objProduct1 = new Product2();
        objProduct1.Name = 'tempAsset';
        insert objProduct1;
        
        Product2 objProduct2 = new Product2();
        objProduct2.Name = 'tempAsset2';
        insert objProduct2;
        
        Asset tempAsset = new Asset(Name = 'tempAsset', accountid = newAccount.id, Status__c = 'In Stock', Terminal_ID__c = 12,
                                    //Following fields are populated to get the test working
                                    //These are required fields
                                    Hardware_Version__c = 'Hardware Version', Firmware_version__c = 'Firmware Version', Make__c = 'Make', General_Description__c = 'General Description', Physical_connections__c = 'Physical Connections', Last_inventory_performed__c = date.today(),
                                    //Following is added to cater for the validation rule
                                    Product2Id = objProduct1.Id);
        
        Asset tempAsset2 = new Asset(Name = 'tempAsset2', accountid = newAccount2.id, Status__c = 'In Use', Terminal_ID__c = 13, 
                                     //Following fields are populated to get the test working
                                     //These are required fields
                                     Hardware_Version__c = 'Hardware Version', Firmware_version__c = 'Firmware Version', Make__c = 'Make', General_Description__c = 'General Description', Physical_connections__c = 'Physical Connections', Last_inventory_performed__c = date.today(),
                                     //Following is added to cater for the validation rule
                                     Product2Id = objProduct2.Id);
		insert tempAsset;
		insert tempAsset2;
		
		//Now insert data causing an contact trigger to fire. 
		Test.startTest();
		update tempAsset;
		update tempAsset2;
		System.assertEquals('Prospect', newAccount.Merchant_Status__c);
		Test.stopTest();
    }
}