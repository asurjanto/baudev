/**************************************************************************************************
 * Name         : FAST_AmexEmailServiceClass
 * Description  : Build for Amex
 * Created By   : Arvind Thakur Jan'19
 * 
 * Amex Email service class.
 * 
***************************************************************************************************/

public class AmexEmailServiceClass {
    
    public static Boolean runningInASandbox;
    public static String sandboxTestContactId;
    public static String emailTemplateId;
    
    static {
        
        Amex_Configurations__c amexConfig = Amex_Configurations__c.getOrgDefaults();
        runningInASandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        sandboxTestContactId = amexConfig.Sandbox_Test_Contact_Id__c;
        emailTemplateId = amexConfig.Invitation_Email_Template__c;
    }
    
    
    /*
    Description : Send amex offer email to contact on case. 
                  Called From AmexEnablementComponentController.cls
    */
    public static String sendAmexInvitationEmail (Id selectedAccountId, String accountName, Contact selectedContact, Case createdCase) { 
        
        
        
        Messaging.SingleEmailMessage singleEmail;
        
        singleEmail = new Messaging.SingleEmailMessage();
        singleEmail.setWhatId(createdCase.Id);
        singleEmail.setTemplateId(emailTemplateId);
        singleEmail.setSenderDisplayName('support@tyro.com');
        if(!runningInASandbox || Test.isRunningTest()) {
            singleEmail.setTargetObjectId(selectedContact.Id);
        }else{
            singleEmail.setTargetObjectId(sandboxTestContactId);
        }
        
        
        List<Messaging.SendEmailResult> emailResult = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {singleEmail});
        return String.valueOf(emailResult[0].isSuccess());
        
    }
    
    
    /*
    Description : Send amex enabled email to contact on case. 
                  Called From CloseAmexCasesBatch.cls
                  This functionality has been removed and would be enabled when amex communication have been approved.
    
    public static void sendAmexEnabledEmail (List<Case> amexEnabledCases) {
        
        Amex_Configurations__c amexConfig = Amex_Configurations__c.getOrgDefaults();
        
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        
        Messaging.SingleEmailMessage singleEmail;
        
        for(Case amexEnabledCase : amexEnabledCases) {
            
            if(amexEnabledCase.contactId != null) {
                
                singleEmail = new Messaging.SingleEmailMessage();
                singleEmail.setSenderDisplayName('support@tyro.com'); //set to something meaningful
                singleEmail.setWhatId(amexEnabledCase.Id);
                singleEmail.setTemplateId(amexConfig.Invitation_Email_Template__c);
                if(!runningInASandbox || Test.isRunningTest()) {
                    singleEmail.setTargetObjectId(amexEnabledCase.contactId);
                }else{
                    singleEmail.setTargetObjectId('00325000011XHQU');
                } //set to amexEnabledCase.ContactId;
                
                emailList.add(singleEmail);
                
            }
            
        }
        
        
        List<Messaging.SendEmailResult> emailResult = Messaging.sendEmail( emailList );
    }
    */
    
    
}