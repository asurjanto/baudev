/*************************************************************************
Author : Isha Saxena
Description: To get the code coverage for Batch_Time_to_first_call
Created Date: 20 April 2017
Last modified Date:  3 Nov 2017
*************************************************************************/

@isTest
private class Batch_Time_to_first_call_test
{
    static testMethod void validatetaskcall()
    {
         
       //List<Task> tsk = new List<Task>();

        List<Lead> lstLead =   new List<Lead>{
                          new Lead(Company = 'JohnMiller', LastName = 'Mike', Status = 'Open',LeadSource = 'Digital Marketing',Lead_Sub_Source__c ='Direct'),
                          new Lead(Company = 'Nike', LastName = 'John', Status = 'Open',LeadSource = 'Digital Marketing',Lead_Sub_Source__c ='Direct'),
                          new Lead(Company = 'Miles', LastName = 'Davis', Status = 'Open',LeadSource = 'Digital Marketing',Lead_Sub_Source__c ='Direct'),
                          new Lead(Company = 'Reebok', LastName = 'Hillen', Status = 'Open',LeadSource = 'Digital Marketing',Lead_Sub_Source__c ='Direct'),
                          new Lead(Company = 'Addidas', LastName = 'Shrin', Status = 'Open',LeadSource = 'Digital Marketing',Lead_Sub_Source__c ='Direct')
                         };  
        insert lstLead;  
        List<Task> tsk = new List<Task>{
        new Task(Task_Category__c = 'First Call',Subject='call',Trading_Name__c='abc',Sub_Type__c='Outbound Call',WhoId=lstLead[0].Id),
        new Task(Task_Category__c = 'First Call',Subject='call',Trading_Name__c='def',Sub_Type__c='Outbound Call',WhoId=lstLead[1].Id),
        new Task(Task_Category__c = 'First Call',Subject='call',Trading_Name__c='ghy',Sub_Type__c='Outbound Call',WhoId=lstLead[0].Id),
        new Task(Task_Category__c = 'First Call',Subject='call',Trading_Name__c='test56',Sub_Type__c='Outbound Call',WhoId=lstLead[2].Id),
        new Task(Task_Category__c = 'First Call',Subject='call',Trading_Name__c='test4',Sub_Type__c='Outbound Call',WhoId=lstLead[3].Id),
        new Task(Task_Category__c = 'First Call',Subject='call',Trading_Name__c='aetc',Sub_Type__c='Outbound Call',WhoId=lstLead[1].Id),
        new Task(Task_Category__c = 'First Call',Subject='call',Trading_Name__c='abceee',Sub_Type__c='Outbound Call',WhoId=lstLead[4].Id),
        new Task(Task_Category__c = 'First Call',Subject='call',Trading_Name__c='abeceee',Sub_Type__c='Outbound Call',WhoId=lstLead[4].Id),
        new Task(Task_Category__c = 'First Call',Subject='call',Trading_Name__c='abwwsac',Sub_Type__c='Outbound Call',WhoId=lstLead[2].Id),
        new Task(Task_Category__c = 'First Call',Subject='call',Trading_Name__c='abcasss',Sub_Type__c='Outbound Call',WhoId=lstLead[3].Id)
        };
        
        insert tsk;
        
        lstLead[0].Status = 'Sales Accepted Lead';
        lstLead[1].Status = 'Nurtured Lead';

        update lstLead;
        
        Test.startTest();
            tsk[5].Subject = 'call test';
            update tsk;

            Batch_Time_to_first_call obj = new Batch_Time_to_first_call();
            DataBase.executeBatch(obj,10); 
            
        Test.stopTest();
    
    }
}