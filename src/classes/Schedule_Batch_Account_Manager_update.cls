/*************************************************************************
Author : Isha Saxena
Description: To update the current status time for lead to track the status : NEO335
Created Date: 11 April 2017
Last modified Date:  13 April 2017
*************************************************************************/

global class Schedule_Batch_Account_Manager_update implements Schedulable 
{

  global void execute(SchedulableContext sc)
   {
      Batch_Account_Manager_change_update  obj = new Batch_Account_Manager_change_update();
      database.executebatch(obj,50);

   }

}