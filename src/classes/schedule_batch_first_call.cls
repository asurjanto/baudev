/*************************************************************************
Author : Isha Saxena
Description: To update the current status time for lead to track the status : NEO335
Created Date: 11 April 2017
Last modified Date:  13 April 2017
*************************************************************************/

global class schedule_batch_first_call implements Schedulable 
{

  global void execute(SchedulableContext sc)
   {
      Batch_Time_to_first_call  obj = new Batch_Time_to_first_call ();
      database.executebatch(obj,1);

   }

}