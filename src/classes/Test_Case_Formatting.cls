@isTest
private class Test_Case_Formatting 
{
    static testMethod void Test_Case_FormattingTrigger() 
    {
        Account newAccount = new Account(Name = 'Test Case_Formatting Account', Channel__c = 'Light Storm');
        Account newAccount2 = new Account(Name = 'Test Case_Formatting Account2', Channel__c = 'FMIPS - FM Integrated Pharmacy Services');
        insert newAccount;
        insert newAccount2;
        
        Product2 objProduct1 = new Product2();
        objProduct1.Name = 'Xentissimo';
        insert objProduct1;
        
        Asset newAsset = new Asset(Name = 'XENTA', Account = newAccount, Accountid = newAccount.id,
                                   //Following fields are populated to get the test working
                                   //These are required fields
                                   Hardware_Version__c = 'Hardware Version', Firmware_version__c = 'Firmware Version', Make__c = 'Make', General_Description__c = 'General Description', Physical_connections__c = 'Physical Connections', Last_inventory_performed__c = date.today(),
                                   //Following is added to cater for the validation rule
                                   Product2Id = objProduct1.Id);
        insert newAsset;
        
        Case tempCase = new Case(Accountid = newAccount.id, subject = 'Test Case_Formatting', status = 'New', Fault_Description__c = 'Test', Reason = 'RMA Shipment', Terminal_1__c = newAsset.Id, Terminal_2__c = newAsset.Id, Terminal_3__c = newAsset.Id);
        Case tempCase2 = new Case(Accountid = newAccount2.id, subject = 'Test Case_Formatting NOT CHANGE', Fault_Description__c = 'Test' ,Reason = 'RMA Shipment', status = 'New', Terminal_1__c = newAsset.Id, Terminal_2__c = newAsset.Id, Terminal_3__c = newAsset.Id);
        insert tempCase;
        insert tempCase2;
        
        Case tempCaseAfter = [select subject from Case where id = :tempCase.id limit 1];
        Case tempCaseAfter2 = [select subject from Case where id = :tempCase2.id limit 1];
        
        System.assertEquals('Test Case_Formatting NOT CHANGE', tempCaseAfter2.subject);
    }
}