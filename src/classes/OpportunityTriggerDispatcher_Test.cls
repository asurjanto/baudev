/****************************************************************
* Class Name    : OpportunityTriggerDispatcher_Test
* Copyright     : Tyro Payments (c) 2018
* Description   : Test Class for OpportunityTriggerDispatcher
* Created By    : Arvind Thakur
* 
* Modification Log ==============================================
* Author                        Date                    JIRA-LINK
* Arvind Thakur (created)     28th June, 2018        User Role Update Process
* 
* 
****************************************************************/

@isTest
private class OpportunityTriggerDispatcher_Test {

	private static testMethod void testOpportunityDispatcher() {
        UserRole newRole = new UserRole (Name = 'Test Class Role');
        insert newRole;
        
        Id adminProfileId = TestUtils.getProfileId('System Administrator'); 
        User testUser = TestUtils.createNewUser('ABC43', 'XYZ', adminProfileId, true);
        User testUser2 = TestUtils.createNewUser('ABC1343', 'XYZ1', adminProfileId, true);
        
        system.runAs(testUser) {
            Id leadRTID = TestUtils.getrecordTypeId(Schema.SobjectType.Lead, 'Merchant Lead');
            Opportunity createOpportunity = Testutils.createOpportunity('Test', 'Open' , system.today(), true);
            createOpportunity.OwnerId = testUser2.Id;
            update createOpportunity;
        }
	}

}