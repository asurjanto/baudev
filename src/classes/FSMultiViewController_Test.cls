/****************************************************************
* Class Name    : FSMultiViewController_Test
* Copyright     : Tyro Payments (c) 2018
* Description   : Test Class for FSMultiViewController
* Created By    : Arvind Thakur
* 
* Modification Log ==============================================
* Author                        Date                    JIRA-LINK
* Arvind Thakur (created)     1st July, 2018            OFS-11334
* 
* 
****************************************************************/

@isTest
private class FSMultiViewController_Test {

	private static testMethod void testFSMultiViewController() {
	    
        FSMultiViewController con = new FSMultiViewController();
        system.assert(con.FSListId != null);
        system.assert(con.openRequestListId != null);    
	}

}