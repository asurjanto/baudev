/**************************************************************************************************
 * Name         : CloseAmexCasesBatch
 * Description  : Build for Amex
 * Created By   : Arvind Thakur Jan'19
 * 
 * Auto-picks up "Back Office Updated" status cases to move them to closed, if all the
 * MIDS mentioned in that case have Amex Merchant Id populated.
 * 
 * Schedule this batch to run every day.
 * 
***************************************************************************************************/

global class CloseAmexCasesBatch implements Database.Batchable<sobject>, Database.Stateful, Schedulable  {
    
    
    private static final String BO_UPDATED_STATUS;
    private static final String CASE_AMEX_RECORDTYPE;
    private static final Date CASE_CREATED_DATE_SELECTOR;
    private static final String AMEX_ENBALED_STATUS;
    
    
    global List<Case> amexEnabledCases;
    
    static {
        Amex_Configurations__c amexConfig = Amex_Configurations__c.getOrgDefaults();
        
        CASE_AMEX_RECORDTYPE    = amexConfig.Case_amex_record_type_Id__c;
        BO_UPDATED_STATUS       = amexConfig.Back_Office_Status_API_Name__c;
        AMEX_ENBALED_STATUS     = amexConfig.Amex_Enabled_Status_API_Name__c;
        CASE_CREATED_DATE_SELECTOR = Date.Today().addDays(Integer.valueOf(amexConfig.Created_Before_date__c));
        
        
    }
    
    global CloseAmexCasesBatch() {
        amexEnabledCases = new List<Case>();
    }
    
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new CloseAmexCasesBatch(), 10);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        
        String query = 'SELECT Id, Description, ContactId, AccountId, Amex_IEAP_Rate__c FROM Case '; 
        query       += ' WHERE Status = :BO_UPDATED_STATUS ';
        query       += ' AND createddate <= :CASE_CREATED_DATE_SELECTOR ';
        query       += ' AND Recordtype.Id = :CASE_AMEX_RECORDTYPE ';
        
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext bc,List<Case> casesToBeUpdated) {
        
        List<Case> caseToClose = new List<Case>();
        List<Account> updateAccountWithAmexRate = new List<Account>();
        List<Contact> updateContactAmexAcceptCheckboxField = new List<Contact>();
        
        Map<Id, Case> caseDetailMap = new Map<Id, Case>();
        Map<Id, Set<String>> caseMIDList = new Map<Id, Set<String>>();
        Set<String> allMids = new Set<String>();
        Set<String> allMidsClone = new Set<String>();
        
        for(Case amexCases : casesToBeUpdated) {
            
            caseDetailMap.put(amexCases.Id, amexCases);
            caseMIDList.put(amexCases.Id, new Set<String>());
            
            for(String descriptionLine : amexCases.Description.split('\n')) {
                caseMIDList.get(amexCases.Id).add(descriptionLine.split(',').remove(0));
            }
            
            allMids.addAll(caseMIDList.get(amexCases.Id));
            allMidsClone.addAll(caseMIDList.get(amexCases.Id));
        }
        
        
        
        if(!allMids.isEmpty()) {
            for(Merchant_Id__c mids : [SELECT Id, Name 
                                        FROM Merchant_Id__c 
                                        WHERE Name IN:allMidsClone 
                                        AND Amex_Merchant_Id__c != null ]) {
                allMids.remove(mids.Name);
            }
            
            for(Id caseId : caseMIDList.keySet()) {
                Boolean caseEligibleForUpdate = true;
                for(String caseMIDs : caseMIDList.get(caseId)) {
                    if(allMids.contains(caseMIDs)) {
                        caseEligibleForUpdate = false;
                        break;
                    }
                }
                
                if(caseEligibleForUpdate) {
                    caseToClose.add(new Case(Id=caseId, Status=AMEX_ENBALED_STATUS, ContactId = caseDetailMap.get(caseId).ContactId));
                    updateAccountWithAmexRate.add(new Account(Id = caseDetailMap.get(caseId).AccountId, Amex_IEAP_Rate__c = caseDetailMap.get(caseId).Amex_IEAP_Rate__c));
                    updateContactAmexAcceptCheckboxField.add(new Contact(Id=caseDetailMap.get(caseId).ContactId, Amex_Accept__c = false));
                }
                
            }
        }
        
        if(!caseToClose.isEmpty()) {
            try {
                update caseToClose;
                update updateAccountWithAmexRate;
                update updateContactAmexAcceptCheckboxField;
                amexEnabledCases.addAll(caseToClose);
            }catch(Exception ex) {
                system.debug( ex.getMessage());
            }
            
        }
        
    }
    
    
    global void finish(Database.BatchableContext bc) {
        
        //This functionality has been removed because amex communications have net yet been approved.
        //AmexEmailServiceClass.sendAmexEnabledEmail(amexEnabledCases);
        
    }
    
}