public class MerchantIDTriggerDispatcher extends TriggerHandler {
    
    //Private Variables
    private Map<Id, Merchant_Id__c> newMIDMap;
    private Map<Id, Merchant_Id__c> oldMIDMap;
    private List<Merchant_Id__c> newMIDList;
    private List<Merchant_Id__c> oldMIDList;
    
    public MerchantIDTriggerDispatcher() {
        
        newMIDMap = (Map<Id, Merchant_Id__c>)Trigger.newMap;
        oldMIDMap = (Map<Id, Merchant_Id__c>)Trigger.oldMap;
        newMIDList = (List<Merchant_Id__c>)Trigger.new;
        oldMIDList = (List<Merchant_Id__c>)Trigger.old;        
        
    }
    
    //Assuming that no Active MIDS would be inserted straight away into the system.
    public override void afterInsert() {

        
    }
    
    public override void afterUpdate() {
        
        
        Set<Id> recalculateAccountPOSAndSegment = new Set<Id>();
        
        for(Merchant_Id__c oldMid : oldMIDList) {
            
            //Status from Active to any other status or from any other status to active.
            //Adding both old and new account. Incase there is change, both will recalculate.
            if((newMIDMap.get(oldMid.Id).Status__c == 'Active' && oldMid.Status__c != 'Active') 
            || (newMIDMap.get(oldMid.Id).Status__c != 'Active' && oldMid.Status__c == 'Active')) {
                recalculateAccountPOSAndSegment.add(newMIDMap.get(oldMid.Id).Account__c);
                recalculateAccountPOSAndSegment.add(oldMid.Account__c);
                recalculateAccountPOSAndSegment.add(newMIDMap.get(oldMid.Id).Merchant_Group__c);
                recalculateAccountPOSAndSegment.add(oldMid.Merchant_Group__c);
            }
        }
        

        if(!recalculateAccountPOSAndSegment.isEmpty()) {
            Set<Id> corporateAccounts = new Set<Id>();
            for(Account childOfCorporate : [SELECT Id, Corporate_Group__c 
                                                FROM Account 
                                                WHERE Id IN:recalculateAccountPOSAndSegment 
                                                AND Corporate_Group__c != null]) {
                corporateAccounts.add(childOfCorporate.Corporate_Group__c);
            }
            
            recalculateAccountPOSAndSegment.addAll(corporateAccounts);
            if(recalculateAccountPOSAndSegment.contains(null)) {
                recalculateAccountPOSAndSegment.remove(null);
            }
            AccountsToUpdate(recalculateAccountPOSAndSegment);
        }
        
    }
    
    private static void AccountsToUpdate(Set<Id> accountIds) {
        

            Set<Id> merchantCoporateAccountIds = new Set<Id>();
            Set<Id> alreadyProcessedAccounts = new Set<Id>();
            Account newAccount;
            Map<Id, Double> AccountTxnMap = new Map<Id, Double>();
            Map<Id, Id> accountPrimaryPOSMap = new Map<Id, Id>();
            
            Map<Id, Account> accountsToUpdate = new Map<Id, Account>();
        
            
            AccountTxnMap = AccountServiceClass.getNetTransactionVolumeForCorporateGroup(accountIds);
            
            accountPrimaryPOSMap = AccountServiceClass.getCorporateGroupPrimaryPOS(accountIds);
            AccountTxnMap.putAll(AccountServiceClass.getNetTransactionVolumeForMerchantGroup(accountIds));
            accountPrimaryPOSMap.putAll(AccountServiceClass.getMerchantGroupPrimaryPOS(accountIds)); 
            
           
            
            for(Id accountId : AccountTxnMap.keySet()) {
                newAccount = new Account(Id=accountId);
                if(AccountTxnMap.containsKey(accountId)) {
                    newAccount.Segment__c = AccountServiceClass.findAccountSegment(AccountTxnMap.get(accountId));
                }
                if(accountPrimaryPOSMap.containsKey(accountId)) {
                    newAccount.Integration_Product__c = accountPrimaryPOSMap.get(accountId);
                }
                accountsToUpdate.put(newAccount.Id, newAccount);
                alreadyProcessedAccounts.add(accountId);
            }
            
            
            //THis return only for TYRO Customer.
            Map<Id, Set<Id>> accountMids = AccountServiceClass.getAccountMerchantIds(accountIds);
            
            //If this is non empty then there are typo customers.
            //AccountIds can contain Merchant Group or Corporate Gorups as well.
            
            if(!accountMids.isEmpty()) {
                Set<Id> allMids = new Set<Id>();
                for(Set<Id> Mids : accountMids.values()) {
                    allMids.addAll(Mids);
                }
                
                
                
                AccountTxnMap = AccountServiceClass.getNetTransactionVolumeForTyroCustomers(accountMids, allMids);
                
                Map<Id, Id> accountPrimaryPOS = AccountServiceClass.getTyroCustomerPrimaryPOS(accountIds);
                
                
               
                for(Id accId : accountMids.keySet()) {
                    //If only they were not updated before, standalone Tyro Customer. Without any CG or MG
                    
                    if(!alreadyProcessedAccounts.contains(accId)) {
                        Account acc = new Account(Id=accId);
                        
                        if(AccountTxnMap.containskey(accId)) {
                            acc.Segment__c = AccountServiceClass.findAccountSegment(AccountTxnMap.get(accId));
                        }
                        if(accountPrimaryPOS.containskey(accId)) {
                            acc.Integration_Product__c = accountPrimaryPOS.get(accId);
                        }
                        accountsToUpdate.put(acc.Id, acc);
                    }
                    
                }
            }

            try{
                update accountsToUpdate.values();
            }catch(exception ex) {
                system.debug('Update Faileds ::: -- MerchantIDTriggerDispatcher:AccountsToUpdate');
            }
        
    }
}