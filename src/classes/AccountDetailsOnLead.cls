/********************************************************************
* Class Name    : AccountDetailsOnLead
* Copyright     : Tyro Payments (c) 2018
* Created By    : Arvind Thakur
* Description   : Used by LeadTriggerDispatcher Class
* 
* This class is used by LeadTriggerDispatcher to handle lead's population
* with it's respective account details.
* If created from Portal - Lead's Partner Access is Onwer -> Account.Id
*                        - Lead's Channel is Owner -> Account -> Channel Agreement
*                        - Lead's Channel Manager email is Owner -> Account -> CM -> Email
* If created manually    - Lead's Partner Access is populated by the creating user.
*                        - It's Channel Agreement is populated thereafter from respective account
* 
* Modification Log ==============================================
* Author                        Date                    JIRA-LINK
* Arvind Thakur (created)     30th July, 2018       Lead-Assignment rules. Removal of Flows/Process builder.
* 
*     
********************************************************************/




public class AccountDetailsOnLead {
    
    
    /*
    Called from : BeforeInsert
    Description : return a Map of Account and Lead list. This works for portal leads.
    */
    public static Map<Id, List<Lead>> getAccountIdLeadMap(Map<Id, List<Lead>> ownerIdLeadMap) {
        
        Map<Id, List<Lead>> accountIdLeadMap = new Map<Id, List<Lead>>();
        
        for(User leadOwners : [SELECT Id, AccountId
                                FROM User 
                                WHERE Id IN :ownerIdLeadMap.KeySet() ]) {
            if(!accountIdLeadMap.containsKey(leadOwners.accountId)) {
                accountIdLeadMap.put(leadOwners.accountId, new List<Lead>());
            }                        
            accountIdLeadMap.get(leadOwners.accountId).addAll(ownerIdLeadMap.get(leadOwners.Id));
        }
        
        return accountIdLeadMap;
    }
    
    
    
    
    /*
    Called from : beforeInsert
    Populates 3 fields on newly created leads.
    Partner Access, Channel 2 and Channel Manager Email
    */
    public static void populateChannelAgreementOnLead(Map<Id, List<Lead>> accountIdLeadMap) {
        
        for(Channel_Agreement__c  channelAgreement : [SELECT Id, Account__c, Channel_Manager_Email__c,  Account__r.Account_No__c
                                                        FROM Channel_Agreement__c 
                                                        WHERE Account__c IN :accountIdLeadMap.keyset() ]) {
            for(Lead leadsToBeUpdate : accountIdLeadMap.get(channelAgreement.Account__c)) {
                leadsToBeUpdate.Channel_2__c = channelAgreement.Id;
                leadsToBeUpdate.Channel_Manager_Email__c = channelAgreement.Channel_Manager_Email__c;
                leadsToBeUpdate.Partner_Access__c = channelAgreement.Account__r.Account_No__c;
            }
        }
        
    }
    
    
    
    
    /*
    Called from : before insert, before update
    Description : If channel agreement is present on the new lead, or if it is updated to a new one.
    */
    public static void updatePartnerAccessOnLeads(Map<Id, List<Lead>> agreementIdLeadMap) {
        
        
        for(Channel_Agreement__c newChannelAgreement : [SELECT Id, Account__r.Account_No__c 
                                                            FROM Channel_Agreement__c 
                                                            WHERE Id IN :agreementIdLeadMap.keySet() ]) {
            
            for(Lead userLeads : agreementIdLeadMap.get(newChannelAgreement.Id) ) {
                userLeads.Partner_Access__c = newChannelAgreement.Account__r.Account_No__c ;
            }
            
        }
        
    }
    
    
    
    
    
}