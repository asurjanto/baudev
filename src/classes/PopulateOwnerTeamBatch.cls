/****************************************************************
* Class Name    : PopulateOwnerTeamBatch
* Copyright     : Tyro Payments (c) 2018
* Description   : This class is to fix all the data. The newly introducted
*   field is Team Role on Lead and Opportunity. All the existing Lead and 
*   opportunity record's field are populated with their owner's rolename's team
*   mentioned in the custom metadata setting.
*   CAUTION: Running this class will update all records. Be careful.
* Created By    : Arvind Thakur
* 
* Modification Log ==============================================
* Author                        Date                    JIRA-LINK
* Arvind Thakur (created)     28th June, 2018        User Role Update Process
* 
* 
****************************************************************/

global class PopulateOwnerTeamBatch implements Database.Batchable<sObject>  {
    
    private static Map<String, String> roleNameTeamName;
    
    static {
        
        roleNameTeamName = new Map<String, String>();
        
        for(User_Role_Reassignment__mdt Teamsetting : [SELECT Role_name__c, Team_name__c FROM User_Role_Reassignment__mdt WHERE is_Active__c = true]) {
            roleNameTeamName.put(Teamsetting.Role_Name__c, Teamsetting.Team_name__c);
        }
    }
    
    
    global String objectName;
    public List<Lead> leadsToUpdate;
    public List<Opportunity> opportunityToUpdate;
    
    //object name can be Lead or opportunity
    //While Executing the batch class pass object name to update its records
    global PopulateOwnerTeamBatch(String sObjectName)  {
        
        this.objectName = sObjectName;
        leadsToUpdate = new List<Lead>();
        opportunityToUpdate = new List<Opportunity>();
        
        
        
    }

    
    global Database.QueryLocator start(Database.BatchableContext ctx) {
        
        Set<String> newSet = new Set<String>(roleNameTeamName.keySet());
        
        String query = 'SELECT Id, Owner_Team__c, Owner.UserRole.Name FROM ';
        query       +=  objectName;
        query       += ' WHERE Owner.UserRole.Name IN : newSet AND Owner_Team__c = null';
        
        return Database.getQueryLocator(query);

    }
    
    global void execute(Database.BatchableContext ctx, List<sObject> scope) {
        
        if(objectName == 'Lead') {
            
            for(Lead leads : (List<Lead>)scope) {
                leads.Owner_Team__c = roleNameTeamName.get(leads.Owner.UserRole.Name);
                leadsToUpdate.add(leads);
            }
            
        }else{
            
            for(Opportunity opps : (list<Opportunity>)scope) {
                opps.Owner_Team__c = roleNameTeamName.get(opps.Owner.UserRole.Name);
                opportunityToUpdate.add(opps);
            }
        }
        
        
        if(!leadsToUpdate.isEmpty()) {
            try{
                system.debug('updating lead records');
                Database.update(leadsToUpdate, false);
            }catch(exception ex){
                system.debug('lead updation failed from batch : ' + ex.getmessage());
            }
            
        } 
        
        if(!opportunityToUpdate.isEmpty()) {
            try{
                Database.update(opportunityToUpdate, false);
            }catch(exception ex) {
                system.debug('Opportunity updation failed from batch : ' + ex.getmessage());
            }
            
        }
        
        
    }
    
    global void finish(Database.BatchableContext ctx) {
        
    }

}