@isTest
public class Test_TaskExtension 
{
	private static testmethod void TestConstructor()
    {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Application_received_date__c = date.today();
        objAccount.Merchant_Status__c = 'Prospect';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Last Name';
        objContact.AccountId = objAccount.Id;
        insert objContact;
        
        Task objTask = new Task(
            Subject = 'Test task', 
            OwnerId = UserInfo.getUserId(),
            WhatID = objAccount.id,
            WhoId = objContact.id,
            Status = 'Completed');
        insert objTask;
        
        Task objTask2 = new Task(
            Subject = 'Test task', 
            OwnerId = UserInfo.getUserId(),
            WhatID = objAccount.id,
            WhoId = objContact.id,
            Status = 'Completed');
        insert objTask2;

        Test.startTest();
		Test.setCurrentPage(Page.ActivityWithComments);
        TaskExtension objTaskExtension = new TaskExtension(new ApexPages.StandardController(objTask));
        list<Task> listTasksExisting = objTaskExtension.otherTask;
        list<Task> listTasksNotExisting = objTaskExtension.otherTask2;
        
        system.assertEquals(1, listTasksExisting.size());
        system.assertEquals(1, listTasksNotExisting.size());
        
        Test.stopTest();
    }
}