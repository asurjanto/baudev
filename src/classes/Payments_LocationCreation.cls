/******************************************************************
@Tyro Payments Limited 2003-2019 
Merchant Portal Order Accessories
Name            :  Payments_MIDCreation
Description     :  HttpGet Rest Endpoint for posting MIDs
Owners          : Payments Team, Team Salesforce. 
Change Log      : Created - 9th Oct, 2019  @Arvind 
JIRA            : 
******************************************************************/
@RestResource(urlMapping='/v1/locations')
global with sharing class Payments_LocationCreation {
	
    @HttpPost
    global static void createMerchantLocation() { 
        
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        
        Payments_LocationRequest locationRequest = (Payments_LocationRequest) System.JSON.deserialize(request.requestBody.toString(), Payments_LocationRequest.class);
        
        //Insert Location and then MID location
        Opportunity relatedOpportunity;
        for(Opportunity opp : [SELECT Id, eComm_Website_URL__c, eComm_Product_Type__c, eComm_Shopping_Cart__c, 
                                        StageName, Application_Specialist__c,  AccountId, Account.Trading_Name__c, eComm_MID_Location__c
                                    FROM Opportunity 
                                    WHERE Opportunity_No__c	=:locationRequest.requesterSourceId ]) {
            relatedOpportunity = opp;
        }
        if(relatedOpportunity != null) {
            
            Location__c newLocation = new Location__c(); 
            newLocation.Street__c = locationRequest.streetLine1;
            newLocation.Street_2__c = locationRequest.streetLine2;
            newLocation.City__c = locationRequest.suburb;
            newLocation.Postcode__c = locationRequest.postcode;
            newLocation.State__c = locationRequest.state;
            newLocation.Account__c = relatedOpportunity.AccountId;
            newLocation.Trading_Name__c = relatedOpportunity.Account.Trading_Name__c;
            insert newLocation;
            
            //Merchant_Id__c midReference = new Merchant_Id__c ();
            
            MID_Location__c midLocation = new MID_Location__c();
            midLocation.Location__c = newLocation.Id;
            midLocation.CAID__c = locationRequest.caid;
            midLocation.putSObject('Merchant_ID__r' , new Merchant_ID__c (MID__c = locationRequest.merchantId));
            
            //These variables belong to Website type locations. So Ignoring for EFTPOS type locations.
            if(locationRequest.locationType == 'WEBSITE') {
                midLocation.Website__c = relatedOpportunity.eComm_Website_URL__c;
                midLocation.eComm_Product_Type__c = relatedOpportunity.eComm_Product_Type__c;
                midLocation.eComm_Shopping_Cart__c = relatedOpportunity.eComm_Shopping_Cart__c;
                midLocation.Status__c = 'Active';
            }
            
            
            try{
                Database.insert(midLocation);
                
                if(midLocation.Website__c != null) {
                    Task newTask = new Task(
                        OwnerId = relatedOpportunity.Application_Specialist__c,
                        Subject = 'eCommerce Ready To Board',
                        ActivityDate = Date.Today().addDays(2),
                        Status = 'Not Started',
                        Priority = 'Normal',
                        Task_Category__c = 'Activation',
                        WhatId = midLocation.Id
                        
                    );
                    
                    insert newTask;
                }
                
                
                if(relatedOpportunity.StageName != 'Closed Won') {
                    relatedOpportunity.StageName = 'Closed Won';
                    if(relatedOpportunity.eComm_MID_Location__c == null && midLocation.Website__c != null) {
                        relatedOpportunity.eComm_MID_Location__c = midLocation.Id;
                    }
                    update relatedOpportunity;
                }
                
                response.responseBody = Blob.valueOf('Location is created');
                response.statusCode = 201;
                RestContext.response = response;
            }catch(Exception ex) {
                
                response.responseBody = Blob.valueOf(ex.getMessage());
                response.statusCode = 401;
                RestContext.response = response;
                
                //Exception Logging
                ExceptionDataObject newException = new ExceptionDataObject(ex);
                newException.setClassorProcessName('Payments_LocationCreation')
                            .setMethodOrSubProcessName('createMerchantLocation')
                            .setJsonPayload(request.requestBody.toString());
                SalesforceExceptionLogger.logException(newException);
            }
            
        }else{
            response.responseBody = Blob.valueOf('MID Opportunity not found');
            response.statusCode = 404;
            RestContext.response = response;
            
            //Exception logging
            ExceptionDataObject newException = new ExceptionDataObject();
            newException.setLineNumber(24)
                        .setExceptionType('Record Not Found')
                        .setMessage('MID Opportunity not found')
                        .setClassorProcessName('Payments_LocationCreation')
                        .setMethodOrSubProcessName('createMerchantLocation')
                        .setJsonPayload(request.requestBody.toString());
            SalesforceExceptionLogger.logException(newException);
        }
        
        
    } 
    
    @HttpPut
    global static void merchantUpdates() { 
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf('Update request received');
        response.statusCode = 200;
        RestContext.response = response;
        
    }
    
}