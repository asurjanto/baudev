/**************************************************************************************************
 * Name         : AccountServiceClass
 * Description  : Capture most frequent scenarios in Lightning
 * Class Hierarchy Description : 
 * ------------------------------------Methods Available-------------------------------------------
 *          Methods Available                                   USES
 * 1.) findAccountSegment                           MerchantGroupMemberTriggerDispatcher.cls, 
 *                                                  MerchantIDTriggerDispatcher.cls
 * 2.) getNetTransactionVolumeForMerchantGroup      MassUpdateAccountSegmentAndPPOS.batch, 
 *                                                  MerchantGroupMemberTriggerDispatcher.cls, MerchantIDTriggerDispatcher.cls                                                                                     
 * 3.) getNetTransactionVolumeForCorporateGroup     MassUpdateAccountSegmentAndPPOS.batch, 
 *                                                  MerchantGroupMemberTriggerDispatcher.cls, MerchantIDTriggerDispatcher.cls                                                                                                               
 * 4.) getTyroCustomerofCorporateGroup              AccountAllocationRulesTriggerDispatcher.cls, UpdateAccountOwner.cls - AccountTriggerDispatcher.cls                                                                               
 * 5.) getMerchantGroupofCorporateGroup             AccountAllocationRulesTriggerDispatcher.cls                                                                                 
 * 6.) getNetTransactionVolumeForTyroCustomers      MerchantIDTriggerDispatcher.cls                                                                                        
 * 7.) getNetTransactionVolumeForTyroCustomers      MassUpdateAccountSegmentAndPPOS.batch                                                                                     
 * 8.) getMearchantGroupMIDRecords                                                                                             
 * 9.) getTyroCustomerofMerchantGroups              AccountAllocationRulesTriggerDispatcher.cls                                                                               
 * 10.) getAccountMerchantIds                       MassUpdateAccountSegmentAndPPOS.batch                                                                    
 * 11.) getCorporateGroupPrimaryPOS                 MassUpdateAccountSegmentAndPPOS.batch, 
 *                                                  MerchantGroupMemberTriggerDispatcher.cls, MerchantIDTriggerDispatcher.cls                                                                                          
 * 12.) getMerchantGroupPrimaryPOS                  MassUpdateAccountSegmentAndPPOS.batch, 
 *                                                  MerchantGroupMemberTriggerDispatcher.cls, MerchantIDTriggerDispatcher.cls                                                                                           
 * 13.) countPrimaryPOSonCGandMG                                                                                             
 * 14.) getTyroCustomerPrimaryPOS                   MassUpdateAccountSegmentAndPPOS.batch, MerchantIDTriggerDispatcher.cls                                                                          
 * 15.) specificAccountsUpdatesOnly                 AccountTriggerDispatcher, AccountAllocationRulesTriggerDispatcher
***************************************************************************************************/
    
public without sharing class AccountServiceClass {
    
    private static List<SegmentWrapper> SegmentWrapperList;
    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP;
    private static final String TYRO_CUSTOMER;
    private static final Set<String> merchantStatusPickListValues;
    
    static {
        
        MERCHANT_GROUP  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();
        
        SegmentWrapperList = new List<SegmentWrapper>();
        SegmentWrapper newWrapper;
        for(Account_Segments__mdt segments : [SELECT Label, DeveloperName, Start_Value__c, End_Value__c
                                                FROM Account_Segments__mdt ORDER BY End_Value__c ASC]) {
            newWrapper = new SegmentWrapper((Double)segments.Start_Value__c, (Double)segments.End_Value__c, segments.DeveloperName);
            SegmentWrapperList.add(newWrapper);
        }
        
        merchantStatusPickListValues = new Set<String>();
        Schema.DescribeFieldResult fieldResult = Merchant_Id__c.Status__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple){
			merchantStatusPickListValues.add(pickListVal.getLabel());
		}
    }
    
    public class SegmentWrapper {
        public Double startValue;
        public Double endValue;
        public String segmentName;
        
        public SegmentWrapper(Double startValue, Double endValue, String segmentName) {
            
            this.startValue = startValue;
            this.endValue = endValue;
            this.segmentName = segmentName;
            
        }
        
        public Boolean isThisSegment(Double netTransactionValue) {
            
            if(this.startValue <= netTransactionValue && netTransactionValue <= this.endValue) {
                return true;
            }else{
                return false;
            }
        }
    }
    
    
    
    
    /*
    @Param -         Double averageTransactionValue - Avg transaction value over transaction months maxed out 12 months. 
                     This number is calulated by getNetTransactionVolumeForMerchantGroup and getNetTransactionVolumeForCorporateGroup
                     and getNetTransactionVolumeForTyroCustomers.
    Description -    The method finds which segment the value lies into based on the setting in custom metadata also called Matt Petterson
                     Segmentation Model 2.0
    */
    public static String findAccountSegment(Double averageTransactionValue) {
        
        String accountSegment;
        
        for(SegmentWrapper thisSegment : SegmentWrapperList) {
            if(thisSegment.isThisSegment(averageTransactionValue)) {
                accountSegment =  thisSegment.segmentName;
                break;
            }
        }
        
        return accountSegment;
    }
    

    
    
    
    /*
    @Param      -      Set of Account with RecordType name = Merchant Group
    @Return     -      Map of Merchant Group and their Averaged out Txn Values.
    Description -      First find all the merchant Ids of the merchant Group type accounts providided.
                       Pass this value to another method with Merchant Group and Merchant Id.
                       This method would group by MerchantIds and return Avg Txn of Merchant Group in a Map
                       of Merchant Group and their net txn value.
    */
    public static Map<Id, Double> getNetTransactionVolumeForMerchantGroup(Set<Id> merchantGroupAccountIdList) {
        
        system.debug('Reference:getNetTransactionVolumeForMerchantGroup');
        Map<Id, Set<Id>> merchantGroupChildren = AccountServiceClass.getMearchantGroupMIDRecords (merchantGroupAccountIdList);
        
        Set<Id> allMerchantIds = new Set<Id>();
        for(Set<Id> merchantIds : merchantGroupChildren.Values()) {
            allMerchantIds.addAll(merchantIds);
        }
        
        
        return AccountServiceClass.getNetTransactionVolumeForTyroCustomers(merchantGroupChildren, allMerchantIds);


    }
    
    
    
    
    /*
    @Param      -      Set of Account with RecordType name = Corporate Group
    @Return     -      Map of Corporate Group and their Averaged out Txn Values.
    Description -      Corporate Groups can have Merchant groups and Tyro Customer.
                       Seperate them out and call thier individual methods. Then Later combine them and add the averages.
    */
    public static Map<Id, Double> getNetTransactionVolumeForCorporateGroup(Set<Id> corporateGroupAccountIdList) {
        
        system.debug('Reference:getNetTransactionVolumeForCorporateGroup');
        Map<Id, Double> corporateGroupNetTxnValue = new Map<Id, Double>();
        Set<Id> allCustomerAccounts = new Set<Id>();
        Set<Id> allMerchantGroupAccounts = new Set<Id>();
        Map<Id, List<Id>> merchantGroupChildMap = new Map<Id, List<Id>>();
        Map<Id, List<Id>> tyroCustomerChildMap = new Map<Id, List<Id>>();
        
        for(Account corporateChildren : [SELECT Corporate_Group__c, RecordType.Name 
                                            FROM Account 
                                            WHERE Corporate_Group__c IN: corporateGroupAccountIdList ]) {
            
            if(corporateChildren.recordType.Id == TYRO_CUSTOMER) {
                if(!tyroCustomerChildMap.containsKey(corporateChildren.Corporate_Group__c)) {
                    tyroCustomerChildMap.put(corporateChildren.Corporate_Group__c, new List<Id>());
                }
                tyroCustomerChildMap.get(corporateChildren.Corporate_Group__c).add(corporateChildren.Id);
                allCustomerAccounts.add(corporateChildren.Id);
                
            }else if(corporateChildren.recordType.Id == MERCHANT_GROUP) {
                if(!merchantGroupChildMap.containsKey(corporateChildren.Corporate_Group__c)) {
                    merchantGroupChildMap.put(corporateChildren.Corporate_Group__c, new List<Id>());
                }
                merchantGroupChildMap.get(corporateChildren.Corporate_Group__c).add(corporateChildren.Id);
                allMerchantGroupAccounts.add(corporateChildren.Id);
            }
        }
       
        
        Map<Id, Set<Id>> tyroCustomerMids = getAccountMerchantIds(allCustomerAccounts);
        
        
        Set<Id> customerMids = new Set<Id>();
        for(Set<Id> tcId : tyroCustomerMids.values()) {
            customerMids.addAll(tcId);
        }
        
        
        
        
        Map<Id, Double> tyroCustomerTransactions = getNetTransactionVolumeForTyroCustomers(tyroCustomerMids, customerMids);
        Map<Id, Double> merchantGroupTransactions = getNetTransactionVolumeForMerchantGroup(allMerchantGroupAccounts);
        
        
        Double totalTXNValue;
        for(Id cgId : corporateGroupAccountIdList) {
            
            totalTXNValue = 0.0;
            
            
            if(tyroCustomerChildMap.containsKey(cgId) || merchantGroupChildMap.containsKey(cgId)) {
                if(merchantGroupChildMap.containsKey(cgId)) {
                    for(Id mgId : merchantGroupChildMap.get(cgId)) {
                        if(merchantGroupTransactions.containsKey(mgId)) {
                            totalTXNValue += merchantGroupTransactions.get(mgId);
                        }
                    }
                }
                
                if(tyroCustomerChildMap.containsKey(cgId)) {
                    for(Id tcId : tyroCustomerChildMap.get(cgId)) {
                        if(tyroCustomerTransactions.containsKey(tcId)) {
                            totalTXNValue+= tyroCustomerTransactions.get(tcId);
                        }
                    }
                }
                
                corporateGroupNetTxnValue.put(cgId, totalTXNValue);
            }
            
            
            
        }
        
       
        return corporateGroupNetTxnValue;
    }
    
    
    
     /*
    @Param      -      Set of Account with RecordType name = Corporate Group
    @Return     -      Map of Corporate Group with a list of all their Tyro Customers.
    @Used at    -      AccountAllocationRulesTriggerDispatcher
    Description -      Query all the tyro customer belonging to the given Corporate Groups
                       and Add them to the map.
    */
    
    public static Map<Id, Set<Id>> getTyroCustomerofCorporateGroup(Set<Id> corporateGroupAccountIdList) {
        
        system.debug('Reference:getTyroCustomerofCorporateGroup');
        Map<Id, Set<Id>> tyroCustomerChildMap = new Map<Id, Set<Id>>();
        for(Account corporateChildren : [SELECT Corporate_Group__c, RecordType.Name 
                                            FROM Account 
                                            WHERE Corporate_Group__c IN: corporateGroupAccountIdList
                                            AND RecordType.Id = :TYRO_CUSTOMER]) {
            
            if(!tyroCustomerChildMap.containsKey(corporateChildren.Corporate_Group__c)) {
                tyroCustomerChildMap.put(corporateChildren.Corporate_Group__c, new Set<Id>());
            }
            tyroCustomerChildMap.get(corporateChildren.Corporate_Group__c).add(corporateChildren.Id);
                                      
        }
        
        return tyroCustomerChildMap;
    }
    
    
    
    /*
    @Param      -      Set of Account with RecordType name = Corporate Group
    @Return     -      Map of Corporate Group with a list of all their Merchant Groups
    @Used at    -      AccountServiceClass.getMerchantGroupofCorporateGroup
    Description -      Query all the Merchant Group belonging to the given Corporate Groups
                       and Add them to the map.
    */
    public static Map<Id, Set<Id>> getMerchantGroupofCorporateGroup(Set<Id> corporateGroupAccountIdList) {
        
        system.debug('Reference:getMerchantGroupofCorporateGroup');
        Map<Id, Set<Id>> merchantGroupChildMap = new Map<id, Set<Id>>();
        for(Account corporateChildren : [SELECT Corporate_Group__c, RecordType.Name
                                            FROM Account 
                                            WHERE Corporate_Group__c IN: corporateGroupAccountIdList
                                            AND RecordType.Id = :MERCHANT_GROUP]) {
           
            if(!merchantGroupChildMap.containsKey(corporateChildren.Corporate_Group__c)) {
                    merchantGroupChildMap.put(corporateChildren.Corporate_Group__c, new Set<Id>());
            }
            merchantGroupChildMap.get(corporateChildren.Corporate_Group__c).add(corporateChildren.Id);
                                                
        }
        
        return merchantGroupChildMap;
    }
    
    
    
    /*
    @Param      -      Set of Merchant Ids
    @Return     -      Map of Tyro Customer with MIDs given in PARAM and their averaged out Txn volume over transaction months.
                       Maxed out to 12 months. If this number is less, then less months avearge is calculated.
    @Used at    -      AccountServiceClass.getNetTransactionVolumeForMerchantGroup
    Description -      Query all transaction history records of the MIDs, grouped by MID Account (Tyro Customer) and Merchant Id.
                       Query Return would have the following format :
                       10 Txn Histry records for MID 1234 having Account "Bar and Cafe TC" with net TXN Volume = 100,000
                       return would be "Bar and Cafe TC" - 10000
    
    public static Map<Id, Double> getNetTransactionVolumeForTyroCustomers(Set<Id> allMerchantIds) {
        
        system.debug('Reference:getNetTransactionVolumeForTyroCustomers');
        Map<Id, Double> accountTransactionMap = new Map<Id, Double>();
        Map<Id, Double> midMSVMap = new Map<Id, Double>();
        Set<Id> processedMids = new Set<Id>();
        
        //THis can be converted to AVG function- and get rid of inner calculation
        for(AggregateResult midTransaction : [SELECT count(id) txnHistoryRecords, Merchant_Id__r.Account__c midAcc, Merchant_Id__c mid, SUM(Net_Transaction_Value__c) txn
                                                        FROM Transaction_History__c Order 
                                                        WHERE Merchant_Id__c IN:allMerchantIds 
                                                        AND Merchant_Id__r.Status__c = 'Active'
                                                        AND Variance_Report_Date__c = LAST_N_MONTHS:12
                                                        GROUP BY Merchant_Id__r.Account__c, Merchant_Id__c]) {
            
            if((Double)midTransaction.get('txnHistoryRecords') >= 3) {
                processedMids.add((Id)midTransaction.get('mid'));
                Double netTxnValue = (Double)midTransaction.get('txn') == null ? 0 : (Double)midTransaction.get('txn');
                Double avergeTransactionVolume = netTxnValue/(Double)midTransaction.get('txnHistoryRecords') ;  
                if(!accountTransactionMap.containsKey((Id)midTransaction.get('midAcc'))) {
                    accountTransactionMap.put((Id)midTransaction.get('midAcc'), avergeTransactionVolume); 
                }else{
                    Double oldSum = accountTransactionMap.get((Id)midTransaction.get('midAcc'));
                    accountTransactionMap.put((Id)midTransaction.get('midAcc'), oldSum + avergeTransactionVolume); 
                }
            }
        }
        
        //If there is no txn history record, this map would be empty. Then we would use MSV value on MID
        for(Merchant_Id__c merchantId : [SELECT Id, Claimed_MSV__c , Account__c
                                            FROM Merchant_Id__c 
                                            WHERE Id IN:allMerchantIds 
                                            AND Id NOT IN: processedMids
                                            AND Status__c = 'Active']) {
            
            if(merchantId.Claimed_MSV__c == null) {
                midMSVMap.put(merchantId.Account__c, 0.0);
            }else{
                midMSVMap.put(merchantId.Account__c, merchantId.Claimed_MSV__c);
            }
            
        }
        
        accountTransactionMap.putAll(midMSVMap);
        
        return accountTransactionMap;
  
    }
    */
    
    
    /*
    @Param      -      
    @Return     -      
    @Used at    -   
    Description -     
    
    public static Map<Id, Double> getNetTransactionVolumeForTyroCustomers(List<Account> allTyroCustomerAccounts) {
        
        Set<Id> allAccountIds = new Set<Id>();
        Set<Id> allAccountMids = new Set<Id>();
        
        for(Account allAccounts : allTyroCustomerAccounts) {
            allAccountIds.add(allAccounts.Id);
        }
        
        Map<Id, Set<Id>> allaccountsAndMIDs = AccountServiceClass.getAccountMerchantIds(allAccountIds);
        
        for(Id accountId : allaccountsAndMIDs.keySet()) {
            allAccountMids.addAll(allaccountsAndMIDs.get(accountId));
        }
        
        
        return AccountServiceClass.getNetTransactionVolumeForTyroCustomers(allAccountMids);
        
    }
    */
    
    
    /*
    @Param      -      Set of Merchant Ids
    @Return     -      Map of Tyro Customer with MIDs given in PARAM and their averaged out Txn volume over transaction months.
                       Maxed out to 12 months. If this number is less, then less months avearge is calculated.
    @Used at    -      AccountServiceClass.getMerchantGroupofCorporateGroup
    Description -      Query all transaction history records of the MIDs, grouped by MID Account (Tyro Customer) and Merchant Id.
                       Query Return would have the following format :
                       10 Txn Histry records for MID 1234 having Account "Bar and Cafe MG" with net TXN Volume = 100,000
                       12 Txn Histry records for MID 3456 having Account "Bar and Cafe MG" with net TXN Volume = 120,000
                       return would be "Bar and Cafe MG" - 10000 + 12000
    */
    public static Map<Id, Double> getNetTransactionVolumeForTyroCustomers(Map<Id, Set<Id>> merchantGroupChildren, Set<Id> allMerchantIds) {
        
        system.debug('Reference:getNetTransactionVolumeForTyroCustomers');
        Map<Id, Double> merchantGroupTransactionMap = new Map<Id, Double>();
        
        Map<Id, Double> midTransactionMap = new Map<Id, Double>();
        Map<Id, Double> midTransactionMap2 = new Map<Id, Double>();
        Map<Id, Double> midHistoryCountMap = new Map<Id, Double>();
        
        Map<Id, Double> midMSVMap = new Map<Id, Double>();
        
        //THis can be converted to AVG function- and get rid of inner calculation
        for(AggregateResult midTransaction : [SELECT count(id) txnHistoryRecords, Merchant_Id__r.Account__c midAcc, Merchant_Id__c mid, SUM(Net_Transaction_Value__c) txn
                                                        FROM Transaction_History__c Order 
                                                        WHERE Merchant_Id__c IN:allMerchantIds 
                                                        AND Merchant_Id__r.Status__c = 'Active'
                                                        AND (Variance_Report_Date__c = LAST_N_MONTHS:11 OR Variance_Report_Date__c = THIS_MONTH)
                                                        GROUP BY Merchant_Id__r.Account__c, Merchant_Id__c]) {
            
            if((Double)midTransaction.get('txnHistoryRecords') >= 3) {
                
                Double netTxnValue = (Double)midTransaction.get('txn') == null ? 0 : (Double)midTransaction.get('txn');
                Double avergeTransactionVolume = netTxnValue/(Double)midTransaction.get('txnHistoryRecords') ;                            
                midTransactionMap.put((Id)midTransaction.get('mid'), avergeTransactionVolume);
                
            }
            
            
        }
        
        //If there is no txn history record, this map would be empty. Then we would use MSV value on MID
        for(Merchant_Id__c merchantId : [SELECT Id, Claimed_MSV__c 
                                            FROM Merchant_Id__c 
                                            WHERE Id IN:allMerchantIds 
                                            AND Id NOT IN: midTransactionMap.keySet()
                                            AND Status__c = 'Active']) {
            
            if(merchantId.Claimed_MSV__c == null) {
                midMSVMap.put(merchantId.Id, 0.0);
            }else{
                midMSVMap.put(merchantId.Id, merchantId.Claimed_MSV__c);
            }
            
        }
        
        midTransactionMap.putAll(midMSVMap);
        

        for(Id merchantAccountId : merchantGroupChildren.keySet()) {
            for(Id mids : merchantGroupChildren.get(merchantAccountId)) {
                if(!merchantGroupTransactionMap.containsKey(merchantAccountId)) {
                    merchantGroupTransactionMap.put(merchantAccountId, 0.0);
                }
                
                Double oldNumber = merchantGroupTransactionMap.get(merchantAccountId);
                
                
                merchantGroupTransactionMap.put(merchantAccountId, oldNumber + midTransactionMap.get(mids));
                
                
            }
        }
        
        return merchantGroupTransactionMap;
        
    }

    
    
    /*
    @Param      -      Set of Account with RecordType name = Merchant Group
    @Return     -      Map of Merchant Group and their List of MIDS
    Description -      Finds MIDS of Merchant Group with Active Status
    */
    public static Map<Id, Set<Id>> getMearchantGroupMIDRecords (Set<Id> merchantGroupAccounts) {
        
        system.debug('Reference:getMearchantGroupMIDRecords');
        Map<Id, Set<Id>> merchantGroupMIDMap = new Map<Id, Set<Id>>();
        for(Merchant_Group_Member__c mgm : [SELECT Merchant_ID__c, Merchant_Group__c
                                                FROM Merchant_Group_Member__c 
                                                WHERE Merchant_Group__c IN :merchantGroupAccounts
                                                AND Merchant_ID__r.Status__c = 'Active']) {
            if(!merchantGroupMIDMap.containsKey(mgm.Merchant_Group__c)) {
                merchantGroupMIDMap.put(mgm.Merchant_Group__c, new Set<Id> {});
            }
            merchantGroupMIDMap.get(mgm.Merchant_Group__c).add(mgm.Merchant_ID__c);
        }
        
        return merchantGroupMIDMap;
    }
    
    
    
    /*
    @Param      -      Set of Account with RecordType name = Merchant Group
    @Param      -      List of Merchant Status Values to be fetched.           
    @Return     -      Map of Merchant Group and their List of MIDS
    Used IN     -       UpdateAccountOwner.cls
                        Twice in AccountServiceClass.cls
    Description -      Finds Tyro Customers of Merchant Group having MIDS with Given Status Values
    */
    public static Map<Id, Set<Id>> getTyroCustomerofMerchantGroups(Set<Id> merchantGroupAccountIdSet, Set<String> merchantStatusFilter) {
        
        system.debug('Reference:getTyroCustomerofMerchantGroups');
        Map<Id, Set<Id>> merchantGroupAccountsMap = new Map<Id, Set<Id>>();
        for(Merchant_Group_Member__c merchantGroup : [SELECT Id, Account__c, Merchant_Group__c 
                                                        FROM Merchant_Group_Member__c 
                                                        WHERE Merchant_Group__c IN:merchantGroupAccountIdSet 
                                                        AND Account__r.RecordType.Id = :TYRO_CUSTOMER
                                                        AND Merchant_Id__r.Status__c IN: merchantStatusFilter]) {
            if(!merchantGroupAccountsMap.containsKey(merchantGroup.Merchant_Group__c)) {
                merchantGroupAccountsMap.put(merchantGroup.Merchant_Group__c, new Set<Id> {});
            }
            merchantGroupAccountsMap.get(merchantGroup.Merchant_Group__c).add(merchantGroup.Account__c);
        }
        
        return merchantGroupAccountsMap;
    }
    
    
    /*
    @Param      -      Set of Account with RecordType name = Tyro Customer
    @Param      -      List of MIDs belonging to Tyro Customer           
    @Return     -      Map of Tyro Customers and their List of MIDS
    Used IN     -      AccountServiceClass.cls
    Description -      
    */
    public static Map<Id, Set<Id>> getAccountMerchantIds(Set<Id> accountIds) {
        
        system.debug('Reference:getAccountMerchantIds');
        Map<Id, Set<Id>> accountMerchantIdMap = new Map<Id, Set<Id>>();
        for(Merchant_Id__c accMids : [SELECT Account__c FROM Merchant_Id__c 
                                        WHERE Account__c IN:accountIds
                                        AND Status__c = 'Active'
                                        AND Account__r.RecordType.Id =:TYRO_CUSTOMER]) {
            if(!accountMerchantIdMap.containsKey(accMids.Account__c)) {
                accountMerchantIdMap.put(accMids.Account__c, new Set<Id> {});
            }
            accountMerchantIdMap.get(accMids.Account__c).add(accMids.Id);
        }
        
        return accountMerchantIdMap;
        
    }
    
    

    /*
    @Param      -      Set of Account with RecordType name = Corporate Group
    @Return     -      Map of Corporate Group and It's Primary POS 
    Used IN     -      AccountServiceClass.cls
    Description -      Calculated Primary POS of Corporate Group
    */
    public static Map<Id, Id> getCorporateGroupPrimaryPOS(Set<Id> corportateGroupIds) {
        
        system.debug('Reference:getCorporateGroupPrimaryPOS');
        Map<Id, Set<Id>> corporateGroupMerchantGroups = getMerchantGroupofCorporateGroup(corportateGroupIds);
        Map<Id, Set<Id>> corporateGroupTyroCustomer = getTyroCustomerofCorporateGroup(corportateGroupIds);
        Map<Id, Set<Id>> ALLtyroCustomersOfCorporateGroup = new Map<Id, Set<Id>>();
        
        Set<Id> merchantGroupIds = new Set<Id>();
        for(Set<Id> mg : corporateGroupMerchantGroups.values()) {
            merchantGroupIds.addAll(mg);
        }
        
        Map<Id, Set<Id>> merchantGroupTyroCustomer = getTyroCustomerofMerchantGroups(merchantGroupIds, new Set<String>{'Active'});
        Set<Id> allTyroCustomer = new Set<Id>();
        
        
        for(Id cgId : corporateGroupMerchantGroups.keySet()) {
            for(Id mgId : corporateGroupMerchantGroups.get(cgId)) {
                if(merchantGroupTyroCustomer.get(mgId) != null) {
                    ALLtyroCustomersOfCorporateGroup.put(cgId, merchantGroupTyroCustomer.get(mgId));
                    allTyroCustomer.addAll(merchantGroupTyroCustomer.get(mgId));
                }else{
                    ALLtyroCustomersOfCorporateGroup.put(cgId, new Set<Id>{});
                    //allTyroCustomer.addAll(merchantGroupTyroCustomer.get(mgId));
                }
                
            }
        }
        
        for(Id cgId : corportateGroupIds) {
            if(corporateGroupTyroCustomer.containsKey(cgId)) {
                if(ALLtyroCustomersOfCorporateGroup.containsKey(cgId)) {
                    ALLtyroCustomersOfCorporateGroup.get(cgId).addAll(corporateGroupTyroCustomer.get(cgId));
                }else{
                    ALLtyroCustomersOfCorporateGroup.put(cgId, corporateGroupTyroCustomer.get(cgId));
                }
                allTyroCustomer.addAll(corporateGroupTyroCustomer.get(cgId));
            }
        }
      
        return countPrimaryPOSonCGandMG(ALLtyroCustomersOfCorporateGroup, getTyroCustomerPrimaryPOS(allTyroCustomer));
        
    }
    
    public static Map<Id, Id> getMerchantGroupPrimaryPOS(Set<Id> merchantGroupAccountIds) {
        
        system.debug('Reference:getMerchantGroupPrimaryPOS');
        Map<Id, Set<Id>> tyroCustomersOfMerchantGroups = getTyroCustomerofMerchantGroups(merchantGroupAccountIds, new Set<String>{'Active'});
        Set<Id> allTyroCustomer = new Set<Id>();
        for(Set<Id> customers : tyroCustomersOfMerchantGroups.values()) {
            allTyroCustomer.addAll(customers);
        }
        
        
        return countPrimaryPOSonCGandMG(tyroCustomersOfMerchantGroups, getTyroCustomerPrimaryPOS(allTyroCustomer));
        
        
    }
    
    private static Map<Id, Id> countPrimaryPOSonCGandMG(Map<Id, Set<Id>> tyroCustomersOfMerchantGroups, Map<Id, Id> customerPrimaryPOS) {
        
        system.debug('Reference:countPrimaryPOSonCGandMG');
        Map<String, Integer> POSCount;
        Map<Id, Id> CGMGPrimaryPOS = new Map<Id, Id>();
        
        for(Id accountMG : tyroCustomersOfMerchantGroups.keySet()) {
            
            POSCount = new Map<String, Integer>();
            for(Id tcAccounts : tyroCustomersOfMerchantGroups.get(accountMG)) {
                
               
                if(customerPrimaryPOS.containsKey(tcAccounts)) {
                    String key = accountMG + '_' + customerPrimaryPOS.get(tcAccounts);
                    
                    if(!POSCount.containsKey(key)) {
                        POSCount.put(key, 0);
                    }
                    
                    Integer count = POSCount.get(key);
                    POSCount.put(key, count+1);
                }
                
            }
            
            
            Id maxPOSId;
            Integer maxCount = 0;
            for(String mapkey : POSCount.keySet()) { 
                if(POSCount.get(mapkey) > maxCount) {
                    maxPOSId = mapkey.split('_').size() == 1 ? null : (Id)mapkey.split('_')[1];
                    maxCount = POSCount.get(mapkey);
                }
            }
            
            CGMGPrimaryPOS.put(accountMG, maxPOSId);
            
        }
        
        return CGMGPrimaryPOS;
    }
    
    public static Map<Id, Id> getTyroCustomerPrimaryPOS(Set<Id> tyroCustomerIds) {
        
        system.debug('Reference:getTyroCustomerPrimaryPOS');
        Map<Id, Id> accountPrimaryPOSMap = new Map<Id, Id>();
        Map<Id, Integer> accountPrimaryPOSCountMap = new Map<Id, Integer>();
        for(List<AggregateResult> accountAggr : [SELECT count(Id) cnt, Integration_Product__c posId, Account__c midAccount 
                                                    FROM Merchant_Id__c 
                                                    WHERE Account__c IN:tyroCustomerIds 
                                                    AND Status__c = 'Active'
                                                    AND Integration_Product__c != null
                                                    GROUP BY Integration_Product__c, Account__c]) {
             
            for(AggregateResult individualResult : accountAggr) {
                
                Id midAccountId     = (Id)individualResult.get('midAccount');
                Id posId            = (Id)individualResult.get('posId');
                Integer posCount    = (Integer)individualResult.get('cnt');
                
                if(accountPrimaryPOSMap.containsKey(midAccountId)) {
                    if(accountPrimaryPOSCountMap.get(midAccountId) < posCount) {
                        accountPrimaryPOSMap.put(midAccountId, posId);
                        accountPrimaryPOSCountMap.put(midAccountId, posCount);
                    }
                }else{
                    accountPrimaryPOSMap.put(midAccountId, posId);
                    accountPrimaryPOSCountMap.put(midAccountId, posCount);
                }
                
            }
            
        }
        
        return accountPrimaryPOSMap;
        
    }
    
    /*
    This method returns a list of only specifc accounts.
    First all CG, and then removes the child MG and TC off the list.
    Then all remaing MG and then removes the child TC off the list.
    Lastly updates the remaining TC, which are standalone accounts.
    Used by AccountTriggerDispatcher and AccountAllocationRulesTriggerDispatcher
    */
    public static List<Account> specificAccountsUpdatesOnly(Map<Id, Account> allCorporateGroups, 
                                                            Map<Id, Account> allMerchantGroups, 
                                                            Map<Id, Account> allTyroCustomer,
                                                            Set<Id> excludedAccounts,
                                                            Map<String, Id> accountAllocationRules) {
        
        List<Account> accountsToUpdate = new List<Account>();
        Account thisAccount;
        
        //Corporate Groups
        if(!allCorporateGroups.isEmpty()) {
            
            //get Tyro Customer of Corporate Groups
            Map<Id, Set<Id>> tcOfCorporateGroup = AccountServiceClass.getTyroCustomerofCorporateGroup(allCorporateGroups.keySet());
            //get Merchant Groups of corporate groups
            //These would be updated to same owner as CG in after update account trigger
            Map<Id, Set<Id>> mgOfCorporateGroup = AccountServiceClass.getMerchantGroupofCorporateGroup(allCorporateGroups.keySet());
            //These Tc will be updated to same owner as CF in after update accoutn trigger.
            Map<Id, Set<Id>> tcOfMerchantGroupOfCG  = AccountServiceClass.getTyroCustomerofMerchantGroups(mgOfCorporateGroup.keySet(), merchantStatusPickListValues);
            
            for(Id cgAccount : mgOfCorporateGroup.keySet()) {
                for(Id mgAccountsOfcg : mgOfCorporateGroup.get(cgAccount)) {
                    if(allMerchantGroups.containsKey(mgAccountsOfcg) || excludedAccounts.contains(mgAccountsOfcg)) {
                        allMerchantGroups.remove(mgAccountsOfcg);
                    }
                }
            }
            
            for(Id mgAccount : tcOfMerchantGroupOfCG.keySet()) {
                for(Id tcAccountsOfcg : tcOfMerchantGroupOfCG.get(mgAccount)) {
                    if(allMerchantGroups.containsKey(tcAccountsOfcg) || excludedAccounts.contains(tcAccountsOfcg)) {
                        allTyroCustomer.remove(tcAccountsOfcg);
                    }
                }
               
            }
            
            //Now we can update All corporate Groups and leave rest to Account Trigger to Roll down the owner to their respective childern.
            for(Id cgAccountId : allCorporateGroups.keySet()) {
                if(accountAllocationRules.containsKey(allCorporateGroups.get(cgAccountId).Account_Allocation_Parameters__c) && !excludedAccounts.contains(cgAccountId)) {
                    thisAccount = allCorporateGroups.get(cgAccountId);
                    thisAccount.OwnerId = accountAllocationRules.get(allCorporateGroups.get(cgAccountId).Account_Allocation_Parameters__c);
                    accountsToUpdate.add(thisAccount);
                }
                
            }
            
            
        }
        
        
        
        //These MG are standalong MG and not under any CG
        if(!allMerchantGroups.isEmpty()) {
            
            //All childern of Merchant Groups
            Map<Id, Set<Id>> tcOfMerchantGroup  = AccountServiceClass.getTyroCustomerofMerchantGroups(allMerchantGroups.keySet(), merchantStatusPickListValues);
            
            for(Id mgAccount : tcOfMerchantGroup.keySet()) {
                for(Id tcAccountOfMg : tcOfMerchantGroup.get(mgAccount)) {
                    if(allTyroCustomer.containsKey(tcAccountOfMg) || excludedAccounts.contains(tcAccountOfMg)) {
                        allTyroCustomer.remove(tcAccountOfMg);
                        
                    }
                }
                
            }
            
            //Now we can update all MG and leave the rest to Account Trigger to roll down to it's TC.
            for(Id mgAccountId : allMerchantGroups.keySet()) {
                if(accountAllocationRules.containsKey(allMerchantGroups.get(mgAccountId).Account_Allocation_Parameters__c) && !excludedAccounts.contains(mgAccountId)) {
                    thisAccount = allMerchantGroups.get(mgAccountId);
                    thisAccount.OwnerId = accountAllocationRules.get(allMerchantGroups.get(mgAccountId).Account_Allocation_Parameters__c);
                    accountsToUpdate.add(thisAccount);
                }
            }
            
        }
        
        
        
        //These are standalone Tyro Customers, not related to any CG or MG
        if(!allTyroCustomer.isEmpty()) {
            
            for(Id tcAccountId : allTyroCustomer.keySet()) {
                if(accountAllocationRules.containsKey(allTyroCustomer.get(tcAccountId).Account_Allocation_Parameters__c) && !excludedAccounts.contains(tcAccountId)) {
                    thisAccount = allTyroCustomer.get(tcAccountId);
                    thisAccount.OwnerId = accountAllocationRules.get(allTyroCustomer.get(tcAccountId).Account_Allocation_Parameters__c);
                    accountsToUpdate.add(thisAccount);
                }
                
            }
            
        }
        
        
        return accountsToUpdate;
        
    }
    
    
}