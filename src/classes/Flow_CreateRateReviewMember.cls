global class Flow_CreateRateReviewMember {
    
    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP;
    private static final String TYRO_CUSTOMER;
    private static final String SMART_EFTPOS_PRODUCTID;
    
    static {
        MERCHANT_GROUP  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();
        SMART_EFTPOS_PRODUCTID = '01tD0000006lqaQIAQ';
    }
        
   
   @InvocableMethod
   public static void createRateReviewMemberForCorporateGroups(List<Id> opportunityId) {
      
      Id currentAccountId;
      for(Opportunity currentOpp : [SELECT Id, AccountId FROM Opportunity WHERE Id =:opportunityId]) {
          currentAccountId = currentOpp.AccountId;
      }
      
      if(currentAccountId != null) {
      
          Set<Id> allTyroCustomerOfCG = new Set<Id>();
          for(Account tyroCustomer : [SELECT Id FROM Account WHERE Corporate_Group__c =:currentAccountId AND RecordType.Id =:TYRO_CUSTOMER]) {
              allTyroCustomerOfCG.add(tyroCustomer.Id);
          }
          
          
          if(!allTyroCustomerOfCG.isEmpty()) {
              Set<Id> tyroCustomerMids = new Set<Id>();
              for(Merchant_Id__c tcMids : [SELECT Id FROM Merchant_Id__c 
                                            WHERE Account__c IN:allTyroCustomerOfCG 
                                            AND (Status__c = 'Boarding Completed' OR Status__c = 'Terminal Shipped' OR Status__c = 'Active')]) {
                  tyroCustomerMids.add(tcMids.Id);
              }
              
              if(!tyroCustomerMids.isEmpty()) {
                  
                  Double netRealMAF = 0.0;
                  Double netTxnHistory = 0.0;
                  
                  List<Rate_Review_Member__c> newMembersToInsert = new List<Rate_Review_Member__c>();
                  List<OpportunityLineItem> oliToUpdate = new List<OpportunityLineItem>();
                  Set<Id> processedMids = new Set<Id>();
                  
                  Rate_Review_Member__c rrm;
                  
                  for(Transaction_History__c midTxnHistry : [SELECT Id, MAF_In_Last_30_Days__c, Real_MAF_In_Last_30_Days__c, 
                                                                        TXN_History_In_Last_30_Days__c, Merchant_ID__c, Merchant_ID__r.Account__c
                                                                FROM Transaction_History__c 
                                                                WHERE Merchant_ID__c IN :tyroCustomerMids 
                                                                AND TXN_History_In_Last_30_Days__c != 0]) {
                                                
                      netRealMAF += midTxnHistry.Real_MAF_In_Last_30_Days__c;
                      netTxnHistory += midTxnHistry.TXN_History_In_Last_30_Days__c;
                      
                      if(!processedMids.contains(midTxnHistry.Merchant_ID__c)) {
                          rrm = new Rate_Review_Member__c();
                          rrm.Account__c = midTxnHistry.Merchant_ID__r.Account__c;
                          rrm.Merchant_Id__c = midTxnHistry.Merchant_ID__c;
                          rrm.Opportunity__c = opportunityId[0];
                          newMembersToInsert.add(rrm);
                          processedMids.add(midTxnHistry.Merchant_ID__c);
                      }
                      
                  }
                  
                  if(newMembersToInsert.isEmpty()) {
                      insert newMembersToInsert;
                  }
                  
                  Opportunity opportuintyToUpdate = new Opportunity(Id=opportunityId[0]);
                  opportuintyToUpdate.Amount = netTxnHistory;
                  opportuintyToUpdate.MAF__c = netRealMAF;
                  opportuintyToUpdate.Merchant_Statement_Value__c = netTxnHistory;
                  
                  update opportuintyToUpdate;
                  
                  for(OpportunityLineItem oli : [SELECT Id FROM OpportunityLineItem 
                                                    WHERE OpportunityId =:opportunityId[0] 
                                                    AND Product2Id =:SMART_EFTPOS_PRODUCTID]) {
                      
                      oli.UnitPrice = netTxnHistory;
                      oliToUpdate.add(oli);
                  }
                  
                  if(!oliToUpdate.isEmpty()) {
                      update oliToUpdate;
                  }
                  
              }
          }
      }
      
      
   }
   
   
   
}