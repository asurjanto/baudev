/*************************************************************************
Author : Isha Saxena
Description: To update the current status time for lead to track the status : NEO335
Created Date: 23 Oct 2017
Last modified Date:  23 Oct 2017
*************************************************************************/

global class Schedule_Batch_Lead_Channel_Tyrodirect implements Schedulable 
{

  global void execute(SchedulableContext sc)
   {
      Batch_Update_Lead_Channel_to_Tyrodirect obj = new Batch_Update_Lead_Channel_to_Tyrodirect();
      database.executebatch(obj,1);

   }

}