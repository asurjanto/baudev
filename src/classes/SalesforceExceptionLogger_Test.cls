@isTest(seeAllData=false)
private class SalesforceExceptionLogger_Test {

    private static testMethod void testSalesforceExceptionLogger() {
        
        ExceptionDataObject newException = new ExceptionDataObject();
        newException.setClassorProcessName('SalesforceExceptionLogger_Test');
        newException.setExceptionType('TestException');
        newException.setJsonPayload('Test: jsonpayload');
        newException.setLineNumber(10);
        newException.setMessage('This should be handled');
        newException.setMethodOrSubProcessName('testSalesforceExceptionLogger');
        
        //Log an Exception
        SalesforceExceptionLogger.logException(newException);
        
        try{
            throw new DMLException();
        }catch(Exception ex){
            SalesforceExceptionLogger.logException(ex);
        }
        
        
    }
    
}