public with sharing class OpportunityDispatcherFBECommerce {
    

    private static final String CURRENT_PROFILE_NAME;

    static {
        
        for(User currentUser : [SELECT Id, Profile.Name FROM User WHERE Id = : UserInfo.getUserId() ]) {
            CURRENT_PROFILE_NAME = currentUser.Profile.Name;
        }
    }

    public OpportunityDispatcherFBECommerce() {

    }

    public static void UpdatedFrontBookECommOpportunity(Map<Id, Opportunity> oldFBECommMap, Map<Id, Opportunity> newFBECommMap) {

        Map<Id, String> opportunityStageCheck = new Map<Id, String>();

        for(Id opportunityId : oldFBECommMap.keySet()) {

            //Check Stage Value Change
            if(oldFBECommMap.get(opportunityId).StageName != newFBECommMap.get(opportunityId).StageName) {
                opportunityStageCheck.put(opportunityId, CURRENT_PROFILE_NAME + '_' + oldFBECommMap.get(opportunityId).StageName + '_' + newFBECommMap.get(opportunityId).StageName);
            }
        }

        if(!opportunityStageCheck.isEmpty()) {
            checkForValidStageChange(opportunityStageCheck, newFBECommMap);
        }
    }

    private static void checkForValidStageChange(Map<Id, String> opportunityStageCheck, Map<Id, Opportunity> oldMap) {
        
        Set<String> allowedStageChanges = new Set<String>();
        //Get Allowed Stage values based on profile from custom settings
        for(FrontBook_eCommerce_Stage_Lifecycle__mdt stageChange : [SELECT Id, Allowed_Profile__c, End_Stage__c, From_Stage__c 
                                                                    	FROM FrontBook_eCommerce_Stage_Lifecycle__mdt]) {
            allowedStageChanges.add(stageChange.Allowed_Profile__c + '_' + stageChange.From_Stage__c + '_' + stageChange.End_Stage__c);
        }

        for(Id opportunityId : opportunityStageCheck.keySet()) {
            if(!allowedStageChanges.contains(opportunityStageCheck.get(opportunityId))) {
                oldMap.get(opportunityId).StageName.addError('This Stage Change is not permitted');
            }
        }

    }


}