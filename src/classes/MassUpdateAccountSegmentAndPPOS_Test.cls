@isTest
private class MassUpdateAccountSegmentAndPPOS_Test {
    
    
    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP;
    private static final String TYRO_CUSTOMER;
    
    static {
        MERCHANT_GROUP  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();
    }
    
	private static testMethod void testBatchClass() {
        
        test.startTest();
       
            Database.ExecuteBatch(new MassUpdateAccountSegmentAndPPOS('Tyro Customer'));
        test.stopTest();
	}
	
	@TestSetup
	private static void createTestData() {
	    
	    TestUtils.createAccount('Test Account123', true);
	    Id adminProfileId = TestUtils.getProfileId('System Administrator'); 
	    User testUser = TestUtils.createNewUser('ABC123', 'XYZ', adminProfileId, true);
	    
	    
	    Account corporateGroupAccount = TestUtils.createAccount('CG Account', false);
	    corporateGroupAccount.RecordTypeId = CORPORATE_GROUP;
	    insert corporateGroupAccount;
	    
	    Account tyroCustomer = TestUtils.createAccount('TyroCustomer', false);
	    tyroCustomer.RecordTypeId = TYRO_CUSTOMER;
	    tyroCustomer.Corporate_Group__c = corporateGroupAccount.Id;
	    insert tyroCustomer;
	    
	    Account merchantGroupAccount = TestUtils.createAccount('MG Account', false);
	    merchantGroupAccount.RecordTypeId = MERCHANT_GROUP;
	    insert merchantGroupAccount;
	    
	    Contact tyroCustomerContact = TestUtils.createContact(tyroCustomer.Id, 'tyrocustomercontact', true);
	    Channel_Agreement__c newCA = TestUtils.createChannelAgreement('testchannel', tyroCustomer.Id, '32424', true);
	    Integration_Product__c newPOS = TestUtils.createNewPOS('myPOS', tyroCustomer.Id,  8, true);
	    Merchant_Id__c newMid = TestUtils.createMerchantId(tyroCustomer.Id, true, merchantGroupAccount.Id, 'Active', newPOS.Id, newCA.Id, tyroCustomerContact.Id, true);
        Location__c newLocation = TestUtils.createNewLocation(tyroCustomer.Id, 'testLocation', '155 Clearance street floor 1-5', 'Sydney', 'NSW', 'Australia', '2148', true);
        
        //A new MGM with TC and MG
        TestUtils.newMerchantGroupMember(merchantGroupAccount.Id, tyroCustomer.Id,  newLocation.Id, newMid.Id, True);
	    
	    
	    //Adding a new MG to CG.
	    
	    Account corporateGroupAccount2 = TestUtils.createAccount('CG Account 2', false);
	    corporateGroupAccount2.RecordTypeId = CORPORATE_GROUP;
	    insert corporateGroupAccount2;
	    
	    Account tyroCustomer2 = TestUtils.createAccount('TyroCustomerofMG', false);
	    tyroCustomer2.RecordTypeId = TYRO_CUSTOMER;
	    insert tyroCustomer2;
	    
	    Account merchantGroupAccount2 = TestUtils.createAccount('MG Account of CG', false);
	    merchantGroupAccount2.RecordTypeId = MERCHANT_GROUP;
	    merchantGroupAccount2.Corporate_Group__c = corporateGroupAccount2.Id;
	    insert merchantGroupAccount2;
	    
	    Contact tyroCustomerContact2 = TestUtils.createContact(tyroCustomer2.Id, 'tyrocustomercontact', true);
	    Channel_Agreement__c newCA2 = TestUtils.createChannelAgreement('testchannel', tyroCustomer2.Id, '32424', true);
	    Integration_Product__c newPOS2 = TestUtils.createNewPOS('myPOS', tyroCustomer2.Id,  8, true);
	    Merchant_Id__c newMid2 = TestUtils.createMerchantId(tyroCustomer2.Id, true, merchantGroupAccount2.Id, 'Active', newPOS2.Id, newCA2.Id, tyroCustomerContact2.Id, true);
        Location__c newLocation2 = TestUtils.createNewLocation(tyroCustomer2.Id, 'testLocation', '155 Clearance street floor 1-5', 'Sydney', 'NSW', 'Australia', '2148', true);
	    
	    TestUtils.newMerchantGroupMember(merchantGroupAccount2.Id, tyroCustomer2.Id,  newLocation2.Id, newMid2.Id, True);
	}

}