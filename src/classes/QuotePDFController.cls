public class QuotePDFController {
    
    public List<QuoteLineItem> lineItemList {get;set;}
    public Id quoteId {get;set;}
    
    public QuotePDFController(ApexPages.StandardController std) {
        quoteId = std.getId();
        
        lineItemList = new List<QuoteLineItem>();
        for(QuoteLineItem qli : [Select id, Product2.Id, Product2.Name From QuoteLineItem WHERE QuoteId =:quoteId]) {
            lineItemList.add(qli);
        }
    }
   
 
}