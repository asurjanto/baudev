 @isTest(seeAllData = false)
private class asset_shipped_test {

    static testMethod void myUnitTest() {
        list<Account> listAccounts = new list<Account>();
        
        Product2 objProduct = new Product2();
        objProduct.Name = 'Test Product';
        insert objProduct;
        
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Account';
        objAccount1.Channel__c = 'text';
        listAccounts.add(objAccount1);
        
        Account objAccount2 = new Account();
        objAccount2.Name = 'Tyro Stock';
        objAccount2.Channel__c = 'text';
        listAccounts.add(objAccount2);
        
        insert listAccounts;
        
        Asset objAsset = new Asset(Hardware_Version__c = 'Hardware Version', Firmware_version__c = 'Firmware Version', Make__c = 'Make', General_Description__c = 'General Description', Physical_connections__c = 'Physical Connections', Last_inventory_performed__c = date.today());
        objAsset.Name = 'AssetName';
        objAsset.SerialNumber = 'AGM1966';
        objAsset.AccountId = objAccount1.Id;
        objAsset.Product2Id = objProduct.Id;
        insert objAsset;
        
        Case objCase = new Case();
        objCase.Subject = 'Test Case';
        objCase.AccountId = objAccount1.Id;
        objCase.Terminal_1__c = objAsset.Id;
        Insert objCase;
        
        list<SFDC_Shipment__c> listSFDCShipment = new list<SFDC_Shipment__c>();
        SFDC_Shipment__c objSFDCShipment = new SFDC_Shipment__c();
        for(integer counter = 0; counter < 2; counter++)
        {
            objSFDCShipment = new SFDC_Shipment__c();
            objSFDCShipment.Case__c = objCase.Id;
            objSFDCShipment.Shipment_Status__c = (Math.Mod(counter,2) == 0 ? 'Outbound' : 'Inbound');
            listSFDCShipment.add(objSFDCShipment);
        }
        insert listSFDCShipment;
        
        list<TNT_Shipment__c> listTNTShipment = new list<TNT_Shipment__c>();
        TNT_Shipment__c objTNTShipment = new TNT_Shipment__c();
        for(integer counter = 0; counter < 2; counter++)
        {
            objTNTShipment = new TNT_Shipment__c();
            objTNTShipment.Case__c = objCase.Id;
            objTNTShipment.Shipment_Type__c = (Math.Mod(counter,2) == 0 ? 'Outbound' : 'Inbound');
            listTNTShipment.add(objTNTShipment);
        }
        insert listTNTShipment;
        
        SFDC_Shipment__c newShipment = [select id, Case__r.accountid from SFDC_Shipment__c where Shipment_Status__c = 'Outbound' limit 1];
        TNT_Shipment__c tntShipment = [select id, Case__r.accountid from TNT_Shipment__c where Shipment_Type__c = 'Outbound' limit 1];
        Asset currentAsset = [select id from Asset where SerialNumber = 'AGM1966' limit 1];
        Shipment_Content__c newContent = new Shipment_Content__c (Asset__c = currentAsset.id, Shipment__c = newShipment.id, TNT_Shipment__c = tntShipment.Id);
        insert newContent;
        Asset currentAsset2 = [select id, accountid, Status__c from Asset where SerialNumber = 'AGM1966' limit 1];
        //System.assertEquals(currentAsset2.accountid, newShipment.case__r.accountid);
        //System.assertEquals(currentAsset2.Status__c, 'Shipped');
        
        SFDC_Shipment__c newShipment2 = [select id, Case__r.accountid from SFDC_Shipment__c where Shipment_Status__c = 'Inbound' limit 1];
        TNT_Shipment__c tntShipment2 = [select id, Case__r.accountid from TNT_Shipment__c where Shipment_Type__c = 'Inbound' limit 1];
        Shipment_Content__c newContent2 = new Shipment_Content__c (Asset__c = currentAsset2.id, Shipment__c = newShipment2.id, TNT_Shipment__c = tntShipment2.Id);
        insert newContent2;
        currentAsset2 = [select id, accountid, Status__c from Asset where SerialNumber = 'AGM1966' limit 1];
        Account rmaAccount = [select id from Account where Name = 'Tyro Stock' limit 1];
       // System.assertEquals(currentAsset2.accountid, rmaAccount.id);
        //System.assertEquals(currentAsset2.Status__c, 'RMA');
        //Test 1
    }
}