/*****************************************************************************
*  Copyright (c) 2003 - 2019 Tyro Payments Limited. 
*  Name         : SalesforceExceptionLogger
*  Created By   : Arvind Thakur
*  Description  : Part of the error Loggin framework. This class publishes the
*                   error log event. It is upto the subscribers to consume the
*                   respective message generated.
* logException
* @Param - List of Exception
* @Param - List of ExceptionDataObject
* @Param - Single Exception
* @Param - Single ExceptionDataObject
* 
* firePlatformEvent
* @Param - List of Exception_Event__e
* @Param - Single Exception_Event__e
****************************************************************************/

global class SalesforceExceptionLogger {
    
    private String primaryProcess;
    private String methodName;
    private String jsonPayload;
    
    
    
    /****
    Constructor if you want to suppy ClassName and Method Name
    *****/
    public SalesforceExceptionLogger(String classOrProcessName, String methodOrSubProcessName, String jsonPayload) {
        this.primaryProcess = classOrProcessName;
        this.methodName = methodOrSubProcessName; 
        this.jsonPayload = jsonPayload;
    }
    
    
    public SalesforceExceptionLogger() {
        
    }
    
    
    public static void logException(List<Exception> exceptionList) {
        
        List<ExceptionDataObject> exceptionDataObjectList = new List<ExceptionDataObject>();
        for(Exception ex : exceptionList) {
            exceptionDataObjectList.add(new ExceptionDataObject(ex));
        }
        
        logException(exceptionDataObjectList); 

    }
    
    
    public static void logException(List<ExceptionDataObject> exceptionData) {
        
        List<Exception_Event__e> eventsToPublish = new List<Exception_Event__e>();
        for(ExceptionDataObject exceptionObject : exceptionData) {
            
            eventsToPublish.add(generateEventRecord(exceptionObject));
        }
        
    
        firePlatformEvent(eventsToPublish);
    }
    
    
    
    
    public static void logException(Exception singleException) {
        logException(new List<Exception> {singleException}); 
    }
    
    
    public static void logException(ExceptionDataObject exceptionData) {
        logException(new List<ExceptionDataObject> {exceptionData});
    }
    
    
    private static void firePlatformEvent(List<Exception_Event__e> eventsToPublish) {
        EventBus.publish(eventsToPublish);
    }
    
    private static void firePlatformEvent(Exception_Event__e eventsToPublish) {
        firePlatformEvent(new List<Exception_Event__e> {eventsToPublish});
    }
    
    
    private static Exception_Event__e generateEventRecord(ExceptionDataObject exceptionData) {
        return new Exception_Event__e(
                                    Line_Number__c = exceptionData.exceptionLineNumber,
                                    Message__c = exceptionData.exceptionMessage,
                                    Stack_Trace__c = exceptionData.exceptionStackTrace,
                                    Exception_Type__c = exceptionData.exceptionType,
                                    Process_Name__c = exceptionData.classOrProcessName,
                                    Method_Name__c = exceptionData.methodOrSubProcessName,
                                    Payload_Body__c = exceptionData.jsonPayload
        );
    }
    
    
}