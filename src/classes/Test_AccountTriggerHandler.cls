/****************************************** Code Modification Log ******************************************
Name    : Test_AccountTriggerHandler
Purpose : This is the test class to test AccountTriggerHandler class, which contains Account triggers functionality

------------------------------------------------------------------------------------------------------------
S.No#        Date           Developer                       Modification Comments
------------------------------------------------------------------------------------------------------------
1.          16/08/2017      Isha Saxena                   Created                                                         
*************************************************************************************************************/
@istest
public class Test_AccountTriggerHandler 
{
    @testSetup static void setup()
    {
       Profile prof = [Select id from Profile where name  = 'System Administrator'];
       
       Map<String,Schema.RecordTypeInfo> accRecordTypeMap = Schema.SObjectType.Account.getRecordTypeInfosByName();
       Id recType1 = AccRecordTypeMap.get('Tyro Customer').getRecordTypeId();
       Id recType2 = AccRecordTypeMap.get('Corporate Group').getRecordTypeId();
       Id recType3 = AccRecordTypeMap.get('Merchant Group').getRecordTypeId();
       Id rtypeId = AccRecordTypeMap.get('Partner').getRecordTypeId();
       
       
       
        List<User> userList = new List<User>();
        User userrec = new User(alias = 'Test5', profileid=prof.id, IsActive=true,     
        email='gcsdatamigrationTest3@testorg.com',firstName='Test', lastname='Test', EmailEncodingKey='UTF-8', TimeZoneSidKey = 'America/Los_Angeles', languagelocalekey='en_US',localesidkey='en_US',
        username='gcsdatamigrationTest3@testorg.com');
        userList.add(userrec); 
        
        User userrec1 = new User(alias = 'Test2', profileid=prof.id, IsActive=true, email='gcsdatamigrationTest@testorg.com',
        firstName='Data', lastname='Migration', EmailEncodingKey='UTF-8', TimeZoneSidKey = 'America/Los_Angeles', languagelocalekey='en_US',localesidkey='en_US',
        username='gcsdatamigrationTest@testorg.com');
        userList.add(userrec1);
        
        User userrec2 = new User(alias = 'Test3', profileid=prof.id, IsActive=true, email='gcsdatamigrationTest2@testorg.com',
        firstName='Data2', lastname='Migration2', EmailEncodingKey='UTF-8',languagelocalekey='en_US', TimeZoneSidKey = 'America/Los_Angeles',localesidkey='en_US',
        username='gcsdatamigrationTest2@testorg.com');       
        userList.add(userrec2);

        User userrec3 = new User(alias = 'Test4', profileid=prof.id, IsActive=true, email='gcsTestEMEA2@testorg.com',
        firstName='Test', lastname='Test', EmailEncodingKey='UTF-8', TimeZoneSidKey = 'America/Los_Angeles', languagelocalekey='en_US', localesidkey='en_US',
        username='gcsTestEMEA2@testorg.com');            
        userList.add(userrec3);
        
        insert userList;
        
        account pt1 = new account();
        pt1.Name = 'Partner test Account';
        pt1.Industry = 'Retail';
        pt1.RecordTypeId = rtypeId;
        insert pt1;
        
        Integration_Product__c inp = new Integration_Product__c();
        inp.Name = 'Postest';
        inp.Product_status__c = 'Production';
        inp.Sub_Industry_Sector__c = 'Auto Mechanics';
        inp.Industry_Sector__c = 'Automotive';
        inp.size_of_installed_base__c = 8;
        inp.Manufacturer_Account__c = pt1.Id;
        insert inp;
        
        account acc = new account();
        acc.Name = 'Corporate test Account';
        acc.Industry = 'Retail';
        acc.RecordTypeId = recType2 ;
        acc.OwnerId = userrec.Id;
        
        acc.Key_Account__c  = true;
        insert acc;
        
       
        
        account acc1 = new account();
        acc1.Name = 'Merchant test Account';
        acc1.ABN__c = '12 004 044 937';
        acc1.Industry = 'Retail';
        acc1.RecordTypeId = AccRecordTypeMap.get('Merchant Group').getRecordTypeId();
        acc1.OwnerId = userrec.Id;
        
        insert acc1;
        
         Location__c loc = new Location__c();
        loc.Account__c = acc1.Id ;
        loc.Trading_Name__c = 'Headquarters test';
        loc.Street__c ='155 Clearance street floor 1-5';
        loc.City__c = 'Sydney';
        loc.State__c='NSW';
        loc.Country__c ='Australia';
        loc.Postcode__c = '2148';
        loc.Active_Site__c = true;
        insert loc;
        
        account ac = new account();
        ac.Name = 'test Account';
        ac.RecordTypeId = AccRecordTypeMap.get('Merchant Group').getRecordTypeId();
        ac.OwnerId = userrec.Id;
        insert ac;
        
        
        System.runAs(userrec)
        {
                    List<Account> accList = new List<Account>();                               
                    Account acc01 = new Account(Name='accname', OwnerId = userrec.Id, RecordTypeId = recType1  ,Corporate_Group__c = acc.Id, Industry = 'Retail', Segment__c = 'Medium_1');
                    accList.add(acc01); 
                    Account acct =  new Account(Name='accname', OwnerId = userrec.Id, RecordTypeId = recType1 , Corporate_Group__c = acc.Id, Industry = 'Retail', Segment__c = 'Medium_1');
                    accList.add(acct); 
                    account acct1 =  new Account(Name='accname', OwnerId = userrec.Id,RecordTypeId = recType1 ,Corporate_Group__c = acc.Id, Industry = 'Retail', Segment__c = 'Medium_1');
                    accList.add(acct1); 
                    Account acct2 =  new Account(Name='accname', OwnerId = userrec.Id, RecordTypeId = recType1  ,Corporate_Group__c = acc.Id, Industry = 'Retail', Segment__c = 'Medium_1');
                    accList.add(acct2); 
                    Account acct3 =  new Account(Name='accname', OwnerId = userrec.Id, RecordTypeId = recType1  ,Corporate_Group__c = acc.Id,Industry = 'Retail', Segment__c = 'Medium_1');
                    accList.add(acct3); 
                    Account acct4 =  new Account(Name='accname', OwnerId = userrec.Id, RecordTypeId = recType1 ,Corporate_Group__c = acc.Id,Industry = 'Retail', Segment__c = 'Medium_1');
                    accList.add(acct4); 
                    Account acct5 =  new Account(Name='accname', OwnerId = userrec.Id, RecordTypeId = recType1  ,Corporate_Group__c = acc.Id,Industry = 'Retail', Segment__c = 'Medium_1');
                    accList.add(acct5); 
                    Account acct6 =  new Account(Name='accname', OwnerId = userrec.Id, RecordTypeId = recType1  ,Corporate_Group__c = acc.Id,Industry = 'Retail', Segment__c = 'Medium_1');
                    accList.add(acct6); 
                    Account acct7 =  new Account(Name='accname', OwnerId = userrec.Id, RecordTypeId = recType1  ,Corporate_Group__c = acc.Id,Industry = 'Retail', Segment__c = 'Medium_1');
                    accList.add(acct7); 
                    Account acct8 =  new Account(Name='accname', OwnerId = userrec.Id, RecordTypeId = recType1  ,Corporate_Group__c = acc.Id,Industry = 'Retail', Segment__c = 'Medium_1');
                    accList.add(acct8); 
                    Account acct9=  new Account(Name='accname', OwnerId = userrec.Id, RecordTypeId = recType1  ,Corporate_Group__c = acc.Id,Industry = 'Retail', Segment__c = 'Medium_1');
                    accList.add(acct9); 
                    insert accList;
                    
                    List<Contact> contList = new List<Contact>();
                    Contact con = new Contact();
                    con.LastName = 'Test_one';
                    con.AccountId = acc1.id;  
                    contList.add(con);
            
                    Contact con1 = new Contact();
                    con1.LastName = 'Test_one';
                    con1.AccountId = accList[3].id;
                    con1.Email = 'test@aexp.com';
                    con1.Title = 'Test';
                    con1.Phone = '6047379251';
                    contList.add(con1);
           
                    Contact c1 = new Contact();
                    c1.LastName = 'lntest';
                    c1.AccountId = accList[1].Id;
                    contList.add(c1);
                    insert contList;  
                    
                    loc.Contact__c = contList[1].Id;
                    update loc;
                    
                    List<Account> accList1 = new List<Account>();
                    Account acc11 = new Account(Name='accname', OwnerId = userrec.Id, RecordTypeId = recType1  , Industry = 'Retail', Segment__c = 'Medium_1');
                    accList1.add(acc11); 
                    Account acc12 =  new Account(Name='accname11', OwnerId = userrec.Id, RecordTypeId = recType1  , Industry = 'Retail', Segment__c = 'Medium_1');
                    accList1.add(acc12); 
                    account acc13 =  new Account(Name='accname11', OwnerId = userrec.Id,RecordTypeId = recType1 , Industry = 'Retail', Segment__c = 'Medium_1');
                    accList1.add(acc13); 
                    Account acc14 =  new Account(Name='accname11', OwnerId = userrec.Id, RecordTypeId = recType1 ,Industry = 'Retail', Segment__c = 'Medium_1');
                    accList1.add(acc14); 
                    Account acc15 =  new Account(Name='accname11', OwnerId = userrec.Id, RecordTypeId = recType1 ,Industry = 'Retail', Segment__c = 'Medium_1');
                    accList1.add(acc15); 
                    Account acc16 =  new Account(Name='accname11', OwnerId = userrec.Id, RecordTypeId = recType1  ,Industry = 'Retail', Segment__c = 'Medium_1');
                    accList1.add(acc16); 
                    Account acc17 =  new Account(Name='accname', OwnerId = userrec.Id, RecordTypeId = recType1  , Industry = 'Retail', Segment__c = 'Medium_1');
                    accList1.add(acc17); 
                    Account acc18 =  new Account(Name='accname', OwnerId = userrec.Id, RecordTypeId = recType1  ,Industry = 'Retail', Segment__c = 'Medium_1');
                    accList1.add(acc18); 
                    Account acc19 =  new Account(Name='accname', OwnerId = userrec.Id, RecordTypeId = recType1  ,Industry = 'Retail', Segment__c = 'Medium_1');
                    accList1.add(acc19); 
                    Account acc20 =  new Account(Name='accname', OwnerId = userrec.Id, RecordTypeId = recType1  ,Industry = 'Retail', Segment__c = 'Medium_1');
                    accList1.add(acc20); 
                    Account acc21=  new Account(Name='accname', OwnerId = userrec.Id, RecordTypeId = recType1  ,Industry = 'Retail', Segment__c = 'Medium_1');
                    accList1.add(acc21); 
                    insert accList1;
                                      
                    
                    List<Contact> contList1 = new List<Contact>();
                    Contact con3 = new Contact();
                    con3.LastName = 'Test_one';
                    con3.AccountId = accList1[2].id;  
                    contList1.add(con3);
            
                    Contact con4 = new Contact();
                    con4.LastName = 'Test_one';
                    con4.AccountId = accList1[3].id;
                    con4.Email = 'test@aexp.com';
                    con4.Title = 'Test';
                    con4.Phone = '6047379251';
                    contList1.add(con4);
           
                    Contact c5 = new Contact();
                    c5.LastName = 'lntest';
                    c5.AccountId = accList1[1].Id;
                    contList1.add(c5);
                    insert contList1;  
                    
                     Channel_Agreement__c ch = new Channel_Agreement__c(Name = 'merchant channel', Account__c= accList1[1].Id,Activity_Status__c = 'Signed',Email_Notification_Contact__c = contList1[2].Id, Backoffice_ID__c = '1234');
                     insert ch;
                    
                     List<Merchant_ID__c> merList = new List<Merchant_ID__c>();
                     Merchant_ID__c m1 = new Merchant_ID__c(MID__c = '76817276', Merchant_Group__c = acc1.Id,Merchant_Group_Member__c = true, Account__c = accList1[1].Id,Status__c = 'Active',Integration_Product__c = inp.Id,Channel__c = ch.Id, Contact__c = contList1[2].Id);
                     merList.add(m1);
                     Merchant_ID__c m2 = new Merchant_ID__c(MID__c = '76898276',Merchant_Group__c = acc1.Id,Merchant_Group_Member__c = true, Account__c = accList1[1].Id,Status__c = 'Active',Integration_Product__c = inp.Id,Channel__c = ch.Id, Contact__c = contList1[2].Id);
                     merList.add(m2);
                     Merchant_ID__c m3 = new Merchant_ID__c(MID__c = '99829276',Merchant_Group__c = acc1.Id,Merchant_Group_Member__c = true, Account__c = accList1[3].Id,Status__c = 'Active',Integration_Product__c = inp.Id,Channel__c = ch.Id, Contact__c = contList1[1].Id);
                     merList.add(m3);
                     Merchant_ID__c m4 = new Merchant_ID__c(MID__c = '8879291',Merchant_Group__c = acc1.Id,Merchant_Group_Member__c = true, Account__c = accList1[3].Id,Status__c = 'Active',Integration_Product__c = inp.Id,Channel__c = ch.Id, Contact__c = contList1[1].Id);
                     merList.add(m4);
                     Merchant_ID__c m5 = new Merchant_ID__c(MID__c = '7123232',Merchant_Group__c = acc1.Id,Merchant_Group_Member__c = true, Account__c = accList1[3].Id,Status__c = 'Active',Integration_Product__c = inp.Id,Channel__c = ch.Id, Contact__c = contList1[1].Id);
                     merList.add(m5);
                     
                     insert merList;
                    
                    
                    
                    
                    List<Merchant_Group_Member__c> memgrp = new List<Merchant_Group_Member__c>();
                    Merchant_Group_Member__c mgrp1 = new Merchant_Group_Member__c(Merchant_Group__c = acc1.Id ,Account__c = accList1[1].Id,Merchant_ID__c=merList[1].Id,Location__c=loc.Id);
                    memgrp.add(mgrp1);
                    Merchant_Group_Member__c mgrp2 = new Merchant_Group_Member__c(Merchant_Group__c = acc1.Id ,Account__c = accList1[1].Id,Merchant_ID__c=merList[1].Id,Location__c=loc.Id);
                    memgrp.add(mgrp2);
                    Merchant_Group_Member__c mgrp3 = new Merchant_Group_Member__c(Merchant_Group__c = acc1.Id ,Account__c = accList1[3].Id,Merchant_ID__c=merList[3].Id,Location__c=loc.Id);
                    memgrp.add(mgrp3);
                    Merchant_Group_Member__c mgrp4 = new Merchant_Group_Member__c(Merchant_Group__c = acc1.Id ,Account__c = accList1[3].Id,Merchant_ID__c=merList[3].Id,Location__c=loc.Id);
                    memgrp.add(mgrp4);
                    Merchant_Group_Member__c mgrp5 = new Merchant_Group_Member__c(Merchant_Group__c = acc1.Id ,Account__c = accList1[3].Id,Merchant_ID__c=merList[3].Id,Location__c=loc.Id);
                    memgrp.add(mgrp5);
                    Merchant_Group_Member__c mgrp6 = new Merchant_Group_Member__c(Merchant_Group__c = acc1.Id ,Account__c = accList1[3].Id,Merchant_ID__c=merList[3].Id,Location__c=loc.Id);
                    memgrp.add(mgrp6);
                    Merchant_Group_Member__c mgrp7 = new Merchant_Group_Member__c(Merchant_Group__c = acc1.Id ,Account__c = accList1[1].Id,Merchant_ID__c=merList[1].Id,Location__c=loc.Id);
                    memgrp.add(mgrp7);
                    
                    
                    Test.startTest();
                     Insert memgrp;         
                    Test.stopTest();
                    
        }
     }       
        
    static testMethod void check_Update_Account_manager()
    {   
        Test.startTest();
        List<account> Actup = new List<Account>();
        User testusr = [Select Id,Username,Email from User where Username = 'gcsdatamigrationTest2@testorg.com' Limit 1];
        User testusr1 = [Select Id,Username,Email from User where Username = 'gcsTestEMEA2@testorg.com' Limit 1];
        Account acc = [Select id,OwnerId,Name,RecordtypeId from Account where Name='Corporate test Account' Limit 1];
        acc.OwnerId = testusr.Id;
        Actup.add(acc);
        
        Account acc1 = [Select id,OwnerId,Name,RecordtypeId from Account where Name='Merchant test Account' Limit 1];
        acc1.OwnerId = testusr1.Id;
        Actup.add(acc1);
        update Actup;
        Test.stopTest();
    }
}