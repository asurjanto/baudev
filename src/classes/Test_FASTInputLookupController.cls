/*
 * Author: Enrico Murru (http://enree.co, @enreeco)
 */
@isTest (SeeAllData=false)
private class Test_FASTInputLookupController { 
    
    @testSetup
    private static void setup(){
        
        Account newAccount = TestUtils.createAccount('testAccount', true);
        TestUtils.createContact(newAccount.Id, 'Test1', true);
        TestUtils.createContact(newAccount.Id, 'Test2', true);
        TestUtils.createContact(newAccount.Id, 'Test3', true);
        
    }
    
    private static testmethod void test_get_name(){
        List<Contact> contacts = [Select Id, LastName, FirstName, Name From Contact];
        
        Test.startTest();
        
        String ret = FAST_InputLookupController.getCurrentValue(null, null);
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = FAST_InputLookupController.getCurrentValue('INVALID_OBJECT', 'INVALID_ID');
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = FAST_InputLookupController.getCurrentValue('INVALID_OBJECT', '000000000000000');
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = FAST_InputLookupController.getCurrentValue('Contact', '000000000000000');
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = FAST_InputLookupController.getCurrentValue('Contact', contacts[0].Id);
        System.assert(ret == contacts[0].Name, 'Should return '+contacts[0].Name+ ' ['+ret+']');
            
        Test.stopTest();
    }
    
    private static testmethod void test_search(){
        List<Contact> contacts = [Select Id, LastName, FirstName, Name From Contact];
        Test.startTest();
        
        String ret = FAST_InputLookupController.searchSObject(null, null, null, null);
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
       	ret = FAST_InputLookupController.searchSObject('INVALID_OBJECT', 'NO_RESULT_SEARCH_STRING', null, null);
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = FAST_InputLookupController.searchSObject('Contact', 'NO_RESULT_SEARCH_STRING', null, null);
        System.assert(String.isNotBlank(ret), 'Should return non null string ['+ret+']');
        List<FAST_InputLookupController.SearchResult> sResList = (List<FAST_InputLookupController.SearchResult>)JSON.deserialize(ret, 
			List<FAST_InputLookupController.SearchResult>.class);
        System.assert(sResList.isEmpty(), 'Why not empty list? ['+sResList.size()+' instead]');
        
        Test.setFixedSearchResults(new List<String>{contacts[0].Id,contacts[1].Id,contacts[2].Id});
        ret = FAST_InputLookupController.searchSObject('Contact', 'Test', null, null);
        System.assert(String.isNotBlank(ret), 'Should return a serialized list string ['+ret+']');
        sResList = (List<FAST_InputLookupController.SearchResult>)JSON.deserialize(ret, 
			List<FAST_InputLookupController.SearchResult>.class);
        System.assert(sResList.size() == 3, 'Why not 3 items found? ['+sResList.size()+' instead]');
        
        
        ret = FAST_InputLookupController.searchSObject('Contact', 'Test', 'Test', 'Name');
        System.assert(String.isNotBlank(ret), 'Should return a serialized list string ['+ret+']');
        sResList = (List<FAST_InputLookupController.SearchResult>)JSON.deserialize(ret, 
			List<FAST_InputLookupController.SearchResult>.class);
        
        
        Test.stopTest();
    }
    
    private static testmethod void test_articleSearch(){
        
        Test.startTest();
        //Create Article
	    Diagnostic_Internal_Article__kav newArticle = (Diagnostic_Internal_Article__kav)TestUtils.createKnowledgeArticle('Diagnostic_Internal_Article__kav', 'TestClass Article', 'Summary Article', 'testURL', true);
        //Publish Created Artcile
        Diagnostic_Internal_Article__kav obj1 = [SELECT Id,Title,KnowledgeArticleId FROM Diagnostic_Internal_Article__kav WHERE Id =: newArticle.Id];
        KbManagement.PublishingService.publishArticle(obj1.KnowledgeArticleId, true);
        
        String ret = FAST_InputLookupController.searchSObject('KnowledgeArticleVersion', 'test', null, null );
        List<FAST_InputLookupController.SearchResult> sResList = (List<FAST_InputLookupController.SearchResult>)JSON.deserialize(ret, 
			List<FAST_InputLookupController.SearchResult>.class);
        Test.stopTest();
        
    }
    
    
    
    
}