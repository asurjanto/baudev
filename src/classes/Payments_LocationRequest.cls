public class Payments_LocationRequest {
    
    public String merchantId;
    public String requesterSourceId;
    public String caid;
    public String locationType;
    public String streetLine1;
    public String streetLine2;
    public String suburb;
    public String postcode;
    public String state; 

        
}