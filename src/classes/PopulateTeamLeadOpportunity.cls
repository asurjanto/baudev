/********************************************************************
* Class Name    : LeadTriggerDispatcher
* Copyright     : Tyro Payments (c) 2018
* Created By    : Arvind Thakur
* Description   : Common class used by Lead and Opportunity Triggers
* 
* This class is used by Lead and Opportunity triggers to udpate respective
* record with Team Owner as mentioned in the custom setting. 
* 
* 
* Modification Log ==============================================
* Author                        Date                    JIRA-LINK
* Arvind Thakur (created)     28th June, 2018       Updating Team Role on lead
* 
* 
********************************************************************/

public class PopulateTeamLeadOpportunity {
    
    private static Map<String, String> roleNameTeamName;
    
    static {
        
        roleNameTeamName = new Map<String, String>();
        
        for(User_Role_Reassignment__mdt Teamsetting : [SELECT Role_name__c, Team_name__c FROM User_Role_Reassignment__mdt WHERE is_Active__c = true]) {
            roleNameTeamName.put(Teamsetting.Role_Name__c, Teamsetting.Team_name__c);
        }
    }
    
    public PopulateTeamLeadOpportunity() {
        
        
        
    }
    
    
    //This method is invoked by Lead Trigger on creation and update
    public static void PopulateTeamOnLead(Map<Id, List<Lead>> userOwnerLeads, Boolean isAfterUpdate) {
        
        Map<Id, String> ownerIdRoleMap = new Map<Id, String>();
        List<Lead> leadToUpdate = new List<Lead>();
        
        for(User leadOwner: [SELECT Id, UserRole.Name FROM User WHERE Id IN: userOwnerLeads.keySet()]) {
            ownerIdRoleMap.put(leadOwner.Id, leadOwner.UserRole.Name);
        }
        
        for(Id ownerId : ownerIdRoleMap.keySet()) {
            if(roleNameTeamName.containsKey(ownerIdRoleMap.get(ownerId))) {
                for(Lead updatedLeads : userOwnerLeads.get(ownerId)) {
                    if(updatedLeads.Owner_Team__c != roleNameTeamName.get(ownerIdRoleMap.get(ownerId))) {
                        if(isAfterUpdate) {
                            Lead newLead = new Lead(Id=updatedLeads.Id);
                            newLead.Owner_Team__c = roleNameTeamName.get(ownerIdRoleMap.get(ownerId));
                            leadToUpdate.add(newLead);
                        }else{
                            updatedLeads.Owner_Team__c = roleNameTeamName.get(ownerIdRoleMap.get(ownerId));
                        }
                        
                        
                    }
                }
            }
        }
        
        if(isAfterUpdate && !leadToUpdate.isEmpty()) {
            update leadToUpdate;
        }
        

    }
    
    
    //This method is invoked by Opportunity Trigger on creation and update
    public static void PopulateTeamOnOpportunity(Map<Id, List<Opportunity>> userOwnerOpps) {
        
        Map<Id, String> ownerIdRoleMap = new Map<Id, String>();
        Map<Id, Boolean> ownerIdOnboardingSpecSupportMap = new Map<Id, Boolean>();
        
        for(User opportunityOwner: [SELECT Id, UserRole.Name, Onboarding_Specialist_Support__c FROM User WHERE Id IN: userOwnerOpps.keySet()]) {
            ownerIdRoleMap.put(opportunityOwner.Id, opportunityOwner.UserRole.Name);
            ownerIdOnboardingSpecSupportMap.put(opportunityOwner.Id , opportunityOwner.Onboarding_Specialist_Support__c);
        }
  
        for(Id ownerId : ownerIdRoleMap.keySet()) {
            if(roleNameTeamName.containsKey(ownerIdRoleMap.get(ownerId))) {
                for(Opportunity updatedOpps : userOwnerOpps.get(ownerId)) {
                    updatedOpps.Owner_Team__c = roleNameTeamName.get(ownerIdRoleMap.get(ownerId));
                    updatedOpps.Owner_Changed_From_Account__c = false;
                    updatedOpps.Onboarding_Specialist_Support__c = ownerIdOnboardingSpecSupportMap.get(ownerId);
                }
            }
        }
        
    }
    
}