/*********************************************************************
 * Name         : AmexProcessedEmailService_Test
 * Description  : Test Class for AmexProcessedEmailService
 * Created By   : Arvind Thakur
 * 
 * Part of AMEX BUILD
 * ******************************************************************/
@isTest(seeAllData=false)
private class AmexProcessedEmailService_Test {
    
    private static final String AMEX_MERCHANT_HEADER_NAME;
    private static final String MID_NUMBER;
    private static final String EMAIL_SUCCESS_IN_BODY;
    private static final String MERCHANT_GROUP;
    private static final String CORPORATE_GROUP;
    private static final String TYRO_CUSTOMER;
    
    
    static {
        Amex_Configurations__c amexConfig   = Amex_Configurations__c.getOrgDefaults();
        EMAIL_SUCCESS_IN_BODY  = amexConfig.Inbound_Amex_Email_CSV_Body_Identifier__c;
        AMEX_MERCHANT_HEADER_NAME           = amexConfig.Inbound_Amex_Email_CSV_Amex_MID_Header__c;
        MID_NUMBER                          = amexConfig.Inbound_Amex_Email_CSV_MID_Header__c;
        MERCHANT_GROUP  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Merchant Group').getRecordTypeId();
        CORPORATE_GROUP = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Group').getRecordTypeId();
        TYRO_CUSTOMER   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Tyro Customer').getRecordTypeId();
    }

	private static testMethod void testIncomingAmexEmail() { 
	    
	    test.startTest();
	         // create a new email and envelope object
            Messaging.InboundEmail email = new Messaging.InboundEmail() ;
            Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
            
            // setup the data for the email
            email.subject = 'Amex Email Confirmation';
            email.fromAddress = 'amex@ieappartnership.com';
            email.plainTextBody = EMAIL_SUCCESS_IN_BODY;
            
            // add an Binary attachment
            Messaging.InboundEmail.TextAttachment attachment = new Messaging.InboundEmail.TextAttachment();
            String csvFile = AMEX_MERCHANT_HEADER_NAME + ',' + MID_NUMBER +  '\n';
            csvFile +=  '12121212' + ',' + '80000';
            attachment.body = csvFile;
            attachment.mimeTypeSubType = 'text/csv';
            attachment.fileName = 'amexEnabled.txt';
            email.textAttachments = new Messaging.inboundEmail.TextAttachment[] { attachment };
            
            
            // call the email service class and test it with the data in the testMethod
            AmexProcessedEmailService  testInbound = new AmexProcessedEmailService ();
            testInbound.handleInboundEmail(email, env);
	        
	    test.stopTest();
	    
    }
    
    @TestSetup
    private static void createTestData() {
        
        
        insert new Amex_Configurations__c (SetupOwnerId=UserInfo.getOrganizationId(), 
                                            Micro_Segment_Ceiling__c = 100000,
                                            Small_Segment_Ceiling__c = 1000000,
                                            Divisor_of_Net_Transaction_Volume__c = 20,
                                            Invitation_Email_Template__c = '00X8E000000JKSt',
                                            Inbound_Amex_Email_CSV_Body_Identifier__c = 'Success Upload',
                                            Inbound_Amex_Email_CSV_MID_Header__c = 'MID',
                                            Amex_Error_Notification_Email_List__c = 'athakur@tyro.com, test@test.com',
                                            Inbound_Amex_Email_CSV_Amex_MID_Header__c = 'MID AMEX'
                                           );
                                           
        Mcc_Description__c mccCode = TestUtils.createNewMCCDescription('testMCCCode', true, 'Health', '3434', true);
        Account tyroCustomer = TestUtils.createAccount('TyroCustomer', false);
	    tyroCustomer.RecordTypeId = TYRO_CUSTOMER;
	    insert tyroCustomer;
	    
	    //Inserting Corporate Group
	    Account corporateGroupAccount = TestUtils.createAccount('CG Account', false);
	    corporateGroupAccount.RecordTypeId = CORPORATE_GROUP;
	    insert corporateGroupAccount;
	    
	    Contact tyroCustomerContact = TestUtils.createContact(tyroCustomer.Id, 'tyrocustomercontact', true);
	    Channel_Agreement__c newCA = TestUtils.createChannelAgreement('testchannel', tyroCustomer.Id, '32424', true);
	    Integration_Product__c newPOS = TestUtils.createNewPOS('myPOS', tyroCustomer.Id,  8, true);
	    Merchant_Id__c newMid = TestUtils.createMerchantId(tyroCustomer.Id, false, null, 'Active', newPOS.Id, newCA.Id, tyroCustomerContact.Id, false);
	    newMid.Mcc_Description__c = mccCode.Id;
	    newMid.Name = '80000';
	    newMid.MID__c = '80000';
	    insert newMid;
	    
	    
	   
    }

}