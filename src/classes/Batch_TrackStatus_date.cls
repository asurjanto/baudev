/*************************************************************************
Author : Isha Saxena
Description: To update the current status time for lead to track the status : NEO335
Created Date: 6 April 2017
Last modified Date:  6 April 2017
*************************************************************************/

global class Batch_TrackStatus_date implements Database.Batchable<SObject>
{
            global final String query;
            global String queryString;
            BusinessHours stdBusinessHours = [select id,Name from businesshours where Name = 'Marketing_Hours'];
            DateTime startDate;
            DateTime endDate = System.NOW();
            
            
            //Description:    This is the start method of batch that queries the records for Lead status history
            // Inputs:        void
                  
            global Database.QueryLocator start(Database.BatchableContext bc) 
            {
                 queryString  = 'Select ld.Name, ld.OwnerId, ld.Days__c, ld.Business_hour__c, ld.Created_Date_Time__c, ld.From_Stage__c, ld.Lead__c, ld.Status_Elapsed_Time__c, ld.Status_Entry_Date_Time__c, ld.Stage_Change_Date__c, ld.To_Stage__c From Lead_Stage_History__c ld where ld.To_Stage__c = null AND ld.Stage_Change_Date__c = null' ;
                 return Database.getQueryLocator(queryString);

            }
            
           global void execute(Database.BatchableContext info, List<Lead_Stage_History__c> ls)
           {
                           List<Lead_Stage_History__c> leadlist = new List<Lead_Stage_History__c>();
                          for(Lead_Stage_History__c lsh : ls)
                          {    
                               
                                   startDate = lsh.Status_Entry_Date_Time__c;
                                   long td = BusinessHours.diff(stdBusinessHours.Id,startDate,endDate); 
                                   Long minutes = td / (60 * 1000);
                                   System.debug('&&&&&&min'+minutes);
                                   String hrs = (minutes / 60) + '.' + Math.mod(minutes, 60); // converting minutes to hours
                                   double  busi_cal = double.valueof(hrs);
                                   lsh.Status_Elapsed_Time__c = busi_cal;
                                   leadlist.add(lsh);

                           }
                           update leadlist;
           }
           
           global void finish(Database.BatchableContext info)
           {    

           }
}