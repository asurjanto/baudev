/*************************************************************************
Author : Isha Saxena
Description: To get the code coverage for Batch_Time_to_first_call
Created Date: 20 April 2017
Last modified Date:  3 Nov 2017
*************************************************************************/


@isTest
private class Track_Lead_History_Test
{
    static testMethod void validatelead()
    {
       List<Lead> lstLead =   new List<Lead>{
                          new Lead(Company = 'JohnMiller', LastName = 'Mike', Status = 'Open',LeadSource = 'Digital Marketing',Lead_Sub_Source__c ='Direct'),
                          new Lead(Company = 'Nike', LastName = 'John', Status = 'Open',LeadSource = 'Digital Marketing',Lead_Sub_Source__c ='Direct'),
                          new Lead(Company = 'Miles', LastName = 'Davis', Status = 'Open',LeadSource = 'Digital Marketing',Lead_Sub_Source__c ='Direct'),
                          new Lead(Company = 'Reebok', LastName = 'Hillen', Status = 'Open',LeadSource = 'Digital Marketing',Lead_Sub_Source__c ='Direct'),
                          new Lead(Company = 'Addidas', LastName = 'Shrin', Status = 'Open',LeadSource = 'Digital Marketing',Lead_Sub_Source__c ='Direct')
                         };  
      insert lstLead;  
      System.debug('Lead after inserting: ' + lstLead);
      lstLead[0].LastName = 'MikeMuller';
      lstLead[0].Status = 'Marketing Qualified Lead';
      lstLead[1].Status = 'Nurtured Lead';
      lstLead[3].Status = 'Sales Working Lead';
      lstLead[4].Status = 'Sales Accepted Lead';
      update lstLead;
      System.debug('Lead after update: ' + lstLead);
    
    }
    

}