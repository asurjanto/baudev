public without sharing class QuoteGenerationController {
    public QuoteGenerationController() {

    }

    @AuraEnabled(cacheable=true)
    public static list<Contact> findAccountContacts(String accountId) {
        List<Contact> accountContacts = new List<Contact>();
        for(Contact contact : [SELECT Id, Name
                                    FROM Contact
                                    WHERE AccountId =:accountId ]) {
            accountContacts.add(contact);
        }

        return accountContacts;
    }


    @AuraEnabled
    public static void uncheckPrimaryQuote(String opportunityId){
        List<Quote> updatedList = new List<Quote>();
        List<Quote> quotesUnderApprovalProcess = new List<Quote>();
        for(Quote oppQuotes : [SELECT Id, Primary_Quote__c, Status FROM Quote 
                                        WHERE OpportunityId =:opportunityId 
                                        AND Primary_Quote__c = true]) {
            oppQuotes.Primary_Quote__c = false;
            if(oppQuotes.status == 'Pending Approval') {
                quotesUnderApprovalProcess.add(oppQuotes);
            }
            updatedList.add(oppQuotes);
        }

        if(!quotesUnderApprovalProcess.isEmpty()) {
            //TO_DO
            //Bulkify this process, for now single records are gettting created and updated
            for(Quote underProcessQuotes : quotesUnderApprovalProcess) {
                eCommerceRiskApprovalComponentController.actionApprovalProcess(underProcessQuotes.Id, 'Removed', 'Recalled. New quote is generated');
            }
        }

        if(!updatedList.isEmpty()) {
            update updatedList;
        }
    }


    @AuraEnabled
    public static String submitForApproval(String quoteId) {
    
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setObjectId(quoteId);

        // Submit the approval request for the Opportunity
        Approval.ProcessResult result = Approval.process(req1);
        return JSON.serialize(result.isSuccess());
    }

    @AuraEnabled(cacheable=true)
    public static String getPricingGuideline(String rateType, Double quoteMSV){
        List<PricingStructureData> pricingStructureList = new List<PricingStructureData>();
        for(Pricing_Tier_Structure__mdt pricingStructure : [SELECT Id, Label, DeveloperName, Start_Value__c, End_Value__c, 
                                                                    Rate_Type__c, Segment__c, Headline_Monthly_Access_Fees__c, Headline_Price__c, Product__c	
                                                            FROM Pricing_Tier_Structure__mdt 
                                                            ORDER BY Start_Value__c ASC]) {
            pricingStructureList.add(new PricingStructureData(pricingStructure, quoteMSV));
        }

        return JSON.serialize(pricingStructureList);
    }

    public class PricingStructureData {
        public String recordId;
        public String MSVRange;
        public String HeadlineMonthlyFee;
        public String HeadlinePrice;
        public String RateType;
        public Boolean validMSVRange;

        public PricingStructureData(Pricing_Tier_Structure__mdt pricingStructure, Double quoteMSV) {
            this.recordId = pricingStructure.Id;
            this.MSVRange = pricingStructure.Start_Value__c + ' - ' + pricingStructure.End_Value__c;
            this.HeadlineMonthlyFee = String.valueOf(pricingStructure.Headline_Monthly_Access_Fees__c);
            this.HeadlinePrice = String.valueOf(pricingStructure.Headline_Price__c);
            this.RateType = pricingStructure.Rate_Type__c;

            if(pricingStructure.Start_Value__c <= quoteMSV && quoteMSV <= pricingStructure.End_Value__c) {
                this.validMSVRange = true;
            }else{
                this.validMSVRange = false;
            }
        }
    }

    
}