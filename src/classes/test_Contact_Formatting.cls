/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class test_Contact_Formatting {

    static testMethod void myUnitTest() 
    {
       /*Contact objContact = new Contact();
        objContact.LastName = 'Last Name';
        objContact.FirstName = 'First Name';
        objContact.MobilePhone = '987654321';
        objContact.Phone = '123456789';
        insert ObjContact;
        
        Contact tempContactBefore = [select id, MobilePhone  from Contact where MobilePhone = '987654321' limit 1];
        tempContactBefore.MobilePhone = 'abcd1234567890ab';
        update tempContactBefore;
        
        Contact tempContactAfter = [select id, HUD_Mob_No__c from Contact where MobilePhone = 'abcd1234567890ab' limit 1];
        System.assertEquals(tempContactAfter.HUD_Mob_No__c, '123-456-7890');
        
        tempContactBefore.MobilePhone = '611231231231';
        update tempContactBefore;
        
        Contact tempContactBefore2 = [select id, Phone from Contact where Phone = '123456789' limit 1];
        tempContactBefore2.Phone = 'abcd1234567890ab';
        update tempContactBefore2;
        
        Contact tempContactAfter2 = [select id, HUD_Phone_No__c from Contact where Phone = 'abcd1234567890ab' limit 1];
        System.assertEquals(tempContactAfter2.HUD_Phone_No__c, '123-456-7890');
        
        tempContactBefore.MobilePhone = '212341234';
        update tempContactBefore;*/
    }
    
}