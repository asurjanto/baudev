/****************************************************************
* Class Name    : LeadTriggerHandler_Test
* Copyright     : Tyro Payments (c) 2018
* Description   : Test Class for LeadTriggerDispatcher
* Created By    : Arvind Thakur
* 
* Modification Log ==============================================
* Author                        Date                    JIRA-LINK
* Arvind Thakur (created)     28th June, 2018        User Role Update Process
* 
* 
****************************************************************/
@isTest(seeAllData=false)
private class LeadTriggerDispatcher_Test {


	private static testMethod void testTriggerDispatcher() {
        
        UserRole newRole = new UserRole (Name = 'Test Class Role');
        insert newRole;
        
        Id adminProfileId = TestUtils.getProfileId('System Administrator'); 
        User testUser = TestUtils.createNewUser('ABC', 'XYZ', adminProfileId, true);
        User testUser2 = TestUtils.createNewUser('ABC1', 'XYZ1', adminProfileId, true);
        
                                
        Test.startTest();
        system.runAs(testUser) {
            Id leadRTID = TestUtils.getrecordTypeId(Schema.SobjectType.Lead, 'Merchant Lead');
            Lead createdLead = Testutils.createLead('Test', 'LastLead', 'phone', 'Partners', '', leadRTID, true);
            createdLead.OwnerId = testUser2.Id;
            update createdLead; 
        }
        Test.stoptest();
        
        
        
        
    }
    
    
    private static testmethod void testPortaluserLead() {
        
        Account newAccount = TestUtils.createAccount('Leadaccount', true);
        newAccount.isPartner = true;
        update newAccount;
        
        Account newAccount2 = TestUtils.createAccount('Leadaccount2', true);
        update newAccount2;
        
        Contact newContact = TestUtils.createContact(newAccount.Id, 'partnerportalcontact', true);
        Id portalProfileId = TestUtils.getProfileId('Tyro Partner Community Login User');
        User partnerPortalUser   = TestUtils.createPortalUser(newContact.Id, portalProfileId, true);
        Channel_Agreement__c agreement = TestUtils.createChannelAgreement('test', newAccount.Id, '12345', true);
        Channel_Agreement__c agreement2 = TestUtils.createChannelAgreement('test', newAccount2.Id, '12312', true);
        
        Test.startTest();
        
        Lead newLead;
        system.runAs(partnerPortalUser) {
    	        
    	        newLead = new Lead(FirstName = 'First Name Lead',
    	                                LastName = 'Last Name Lead',
    	                                Email = 'TestLead@portal.com',
    	                                Phone = '3434343434',
    	                                Company = 'Test Company',
    	                                Industry = 'Retail',
    	                                LeadSource = 'Partners',
    	                                Status = 'Lead Gen: Partial',
    	                                Lead_Sub_Source__c = 'Software Developer',
    	                                Web_Lead_Form__c = 'Partner Portal');
                insert newLead;
    	                                
    	        
        }
        
        
        Lead insertedLead = [SELECT Id, Channel_2__c, Partner_Access__c FROM Lead WHERE Id =: newLead.Id][0];
        system.assertEquals(agreement.Id, insertedLead.Channel_2__c);
        String oldPartnerAccess = insertedLead.Partner_Access__c; 
        
        
        newLead.Channel_2__c = agreement2.Id;
        update newLead;
        
        Lead updatedLead = [SELECT Id, Channel_2__c, Partner_Access__c FROM Lead WHERE Id =: newLead.Id][0];
        system.assertNotEquals(oldPartnerAccess, updatedLead.Partner_Access__c);
        
        //Manual Lead with Channel Agreement testing
        Id leadRTID = TestUtils.getrecordTypeId(Schema.SobjectType.Lead, 'Merchant Lead');
        Lead manualLead = Testutils.createLead('Test', 'LastLead', 'phone', 'Partners', '', leadRTID, false);
        manualLead.Channel_2__c = agreement.Id;
        insert manualLead;
        
        system.assert([SELECT Id, Channel_2__c, Partner_Access__c FROM Lead WHERE Id =: manualLead.Id][0].Partner_Access__c != null);
        
        
        
        Test.stoptest();
    }

}