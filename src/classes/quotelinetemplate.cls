/*****************************************************************************************************************************
Author : Isha Saxena
Description: To sort the Qoute line Items for creating a rate review quote email template
Created Date: 31 October 2017
Last modified Date:  31 October 2017
******************************************************************************************************************************/


public class quotelinetemplate
{
    public List<QuoteLineItem> results{get; set;}
    public ID qteId {get; set;}
   
        public quotelinetemplate() 
        {
           
             
        }
          
           public List<QuoteLineItem> getquotelinetemplate()
           {
             results = [SELECT Id,QuoteId,Product2Id,Product2.Name,Rates__c,New_Rates_2__c,MAF_Percent_Based__c,MAF_Fixed_Price_c__c,Product_CaseSafeId__c,Terms_Of_Trade_Product_ID__c,ListPrice,MSF_PC_OF_Trans_2__c,Product2.Label__c,Product_Label__c,MSF_Per_Trans_2__c
                FROM QuoteLineItem where QuoteId =: qteId
                ORDER BY Product2.Name]; 
            return results;
 
          }


}