<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Funnel</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>true</expandOthers>
            <header>Funnel</header>
            <legendPosition>Bottom</legendPosition>
            <report>L2O_Sales_Reports/LD_Lead_Funnel_x_Status</report>
            <showPercentage>true</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Funnel</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y2</axisBinding>
                <column>Lead.Lead_Age__c</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>OWNER</groupingColumn>
            <groupingColumn>STATUS</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>L2O_Sales_Reports/LD_Leads_By_Owner_Direct_3</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Leads By Owner</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y2</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <displayUnits>Millions</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Leads converted to Opportunity this month by value &amp; estimated close date</footer>
            <groupingColumn>LEAD_OWNER</groupingColumn>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>L2O_Sales_Reports/LD_Leads_Converted_This_Month_2</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Value / Cls Date - Leads Conv. This Mth</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Donut</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <legendPosition>Bottom</legendPosition>
            <report>L2O_Sales_Reports/LD_Leads_By_Channel_2</report>
            <showPercentage>true</showPercentage>
            <showTotal>true</showTotal>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Leads By Channel</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y2</axisBinding>
                <column>Lead.No_Days_Since_Last_Activity__c</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Open Leads where last activity date &gt; 30 days</footer>
            <groupingColumn>OWNER</groupingColumn>
            <groupingColumn>STATUS</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>L2O_Sales_Reports/LD_Neglected_Leads</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Neglected Leads</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Lead.Lead_Age__c</column>
            </chartSummary>
            <componentType>Line</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Average number of days taken to convert a Lead to an Opportunity</footer>
            <groupingColumn>CONVERTED_DATE</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>L2O_Sales_Reports/LD_Average_Days_To_Lead_Conversion_2</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Average Lead Conversion Days YTD</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>ASSIGNED</groupingColumn>
            <groupingColumn>Activity.Task_Category__c</groupingColumn>
            <header>Activity</header>
            <legendPosition>Bottom</legendPosition>
            <report>L2O_Sales_Reports/A_Completed_Activities_MTD</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Completed Activities MTD</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y2</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>L2O_Sales_Reports/P_Pipeline_This_Month</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Pipeline This Month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT</column>
            </chartSummary>
            <componentType>LineCumulative</componentType>
            <displayUnits>Thousands</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>L2O_Sales_Reports/P_Forecast_MSV_This_Month_Direct_3</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Forecast MSV This Month (Cumulative)</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FULL_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>true</calculatePercent>
                <column>AMOUNT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Top 10 Opportunities closing this month</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>L2O_Sales_Reports/P_Top_Prospects_This_Month</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Top 10 Prospects This Month</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y2</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <displayUnits>Millions</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Excl. this month</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>L2O_Sales_Reports/P_Pipeline_Next_90_Days</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Pipeline Next 90 Days</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FULL_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Top 10 deals closing next 90 days, excl. this month</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>L2O_Sales_Reports/P_Top_Prospects_Next_90_Days</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Top 10 Prospects Next 90 Days</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>STAGE_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Top 10 Opportunities where current stage duration &gt; 30 Days</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>L2O_Sales_Reports/P_Stuck_Opportunties</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Top 10 Stuck Opportunities</title>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Thousands</displayUnits>
            <gaugeMax>6000000.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Applications Received</header>
            <indicatorBreakpoint1>2000000.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>4000000.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Pokemon_Team_Nelson/AR_Applications_Received_This_Month</report>
            <showPercentage>false</showPercentage>
            <showRange>false</showRange>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Applications Received This Month</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>CLOSED_AMOUNT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y2</axisBinding>
                <column>CLOSED_PCTQUOTA</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <displayUnits>Thousands</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>FULL_NAME</groupingColumn>
            <groupingColumn>START_DATE2</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>L2O_Sales_Reports/AR_Appl_Rec_v_Quota_Direct_3</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Applications Received This Month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FULL_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>L2O_Sales_Reports/AR_App_Rec_This_Mth_x_Acct</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Top 10 Wins This Month</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT</column>
            </chartSummary>
            <componentType>Donut</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Opportunity.Channel_2__c</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>L2O_Sales_Reports/AR_Appl_Rec_This_Month_By_Channel</report>
            <showPercentage>true</showPercentage>
            <showTotal>false</showTotal>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Applications Received This Month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y2</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <groupingColumn>ROLLUP_DESCRIPTION</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>L2O_Sales_Reports/AR_Applications_Received_This_FY</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Applications Received YTD</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y2</axisBinding>
                <column>Opportunity.Lead_To_Application_Received_Days__c</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>L2O_Sales_Reports/AR_Applications_Received_This_FY</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Avg. MSV &amp; LD To AR Days YTD</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Merchant_IDs__r.Transaction_History__r$Total_Real_MAF__c</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y2</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Opportunity$Owner</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>L2O_Sales_Reports/OP_Opps_With_MID_TH_x_SCT_This_FY</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Cumulative MAF - YTD</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>amilan@tyro.com</runningUser>
    <textColor>#000000</textColor>
    <title>P: Forecast MSV This Month: Direct 3</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
