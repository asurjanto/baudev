<?xml version="1.0" encoding="UTF-8"?>
<Profile xmlns="http://soap.sforce.com/2006/04/metadata">
    <categoryGroupVisibilities>
        <dataCategoryGroup>Topics</dataCategoryGroup>
        <visibility>ALL</visibility>
    </categoryGroupVisibilities>
    <categoryGroupVisibilities>
        <dataCategoryGroup>Internal_Categories</dataCategoryGroup>
        <visibility>ALL</visibility>
    </categoryGroupVisibilities>
    <custom>true</custom>
    <loginIpRanges>
        <description>HQ Exetel</description>
        <endAddress>58.96.22.122</endAddress>
        <startAddress>58.96.22.122</startAddress>
    </loginIpRanges>
    <loginIpRanges>
        <description>SN Corp</description>
        <endAddress>58.96.29.9</endAddress>
        <startAddress>58.96.29.9</startAddress>
    </loginIpRanges>
    <loginIpRanges>
        <description>HQ BigAir</description>
        <endAddress>202.171.181.62</endAddress>
        <startAddress>202.171.181.62</startAddress>
    </loginIpRanges>
    <userLicense>Salesforce</userLicense>
    <userPermissions>
        <enabled>true</enabled>
        <name>ActivitiesAccess</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>AllowViewKnowledge</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ApiEnabled</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterInternalUser</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>LightningConsoleAllowedForUser</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewAllData</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewAllForecasts</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewEventLogFiles</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewHelpLink</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewRoles</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewSetup</name>
    </userPermissions>
</Profile>
